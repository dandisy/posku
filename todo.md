# Todo

- [x] buat cek ketika input unik barcode, apa sdh ada di datatable yg sedang diinput
- [ ] buat cek ketika input unik barcode, apa sdh ada di db
- [x] rapihin file manager di mobile
- [x] rapihin semua halaman resource index (tombol view list item), dan action datatable nya (edit & delete)
- [ ] setingan hide image item
- [ ] setingan aktifkan order meja
- [ ] buat user role selain admin, tidak bisa add new langsung di stok gudang
- [ ] buat user kasir, yang hanya bisa buka halaman penjualan
- [ ] tambah multi bahasa