@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Product Promo Condition
        </h1>
        
        {{--@include('product_promo_conditions.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('product_promo_conditions.show_fields')
                    <a href="{!! route('productPromoConditions.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
