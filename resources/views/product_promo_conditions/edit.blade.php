@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Product Promo Condition
        </h1>

        {{--@include('product_promo_conditions.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productPromoCondition, ['route' => ['productPromoConditions.update', $productPromoCondition->id], 'method' => 'patch']) !!}

                        @include('product_promo_conditions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection