<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sellReturn->id !!}</p>
</div>

<!-- Sell Id Field -->
<div class="form-group">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    <p>{!! $sellReturn->sell_id !!}</p>
</div>

<!-- Customer Field -->
<div class="form-group">
    {!! Form::label('customer', 'Customer:') !!}
    <p>{!! $sellReturn->customer !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $sellReturn->store !!}</p>
</div>

<!-- Bill Amount Field -->
<div class="form-group">
    {!! Form::label('bill_amount', 'Bill Amount:') !!}
    <p>{!! $sellReturn->bill_amount !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $sellReturn->payment_method !!}</p>
</div>

<!-- Payment Status Field -->
<div class="form-group">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    <p>{!! $sellReturn->payment_status !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $sellReturn->note !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $sellReturn->balance !!}</p>
</div>

<!-- Is Draft Field -->
<div class="form-group">
    {!! Form::label('is_draft', 'Is Draft:') !!}
    <p>{!! $sellReturn->is_draft !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $sellReturn->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $sellReturn->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sellReturn->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sellReturn->updated_at !!}</p>
</div>

