@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell Return
        </h1>

        {{--@include('sell_returns.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'sellReturns.store']) !!}

                        @include('sell_returns.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalAddItemLabel" rowkey="" rowindex="">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalAddItemLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Sell Return Id Field -->
                    <!-- {{--<div class="form-group col-sm-6">
                        {!! Form::label('sell_return_id', 'Sell Return Id:') !!}
                        {!! Form::number('sell_return_id', null, ['class' => 'form-control']) !!}
                    </div>--}} -->

                    {!! Form::hidden('sell_return_id', null, ['id' => 'sell_return_id']) !!}
                    {!! Form::hidden('unique_barcode', null, ['id' => 'unique_barcode']) !!}

                    <!-- Item Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item', 'Item:') !!}
                        <!-- {{--{!! Form::select('item', $items->pluck('name_barcode', 'barcode'), null, ['class' => 'form-control select2']) !!}--}} -->
                        <select class="form-control select2" name="item" id="item">
                            <option></option>
                            @foreach($items as $item)
                            <option value="{{$item->id}}" data-barcode="{{$item->unique_barcode}}" data-ismanyuniquebarcode="{{$item->is_many_unique_barcode}}">{{$item->name_barcode}}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--<!-- Item Discount Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_discount', 'Item Discount:') !!}
                        {!! Form::text('item_discount', null, ['class' => 'form-control thousand']) !!}
                    </div>

                    <!-- Item Price Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_price', 'Item Price:') !!}
                        {!! Form::text('item_price', null, ['class' => 'form-control thousand']) !!}
                    </div>--}}

                    <!-- Item Amount Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_amount', 'Item Amount:') !!}
                        {!! Form::number('item_amount', null, ['class' => 'form-control thousand']) !!}
                    </div>

                    {{--<!-- Discount Subtotal Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('discount_subtotal', 'Discount Subtotal:') !!}
                        {!! Form::text('discount_subtotal', null, ['class' => 'form-control thousand']) !!}
                    </div>

                    <!-- Price Subtotal Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('price_subtotal', 'Price Subtotal:') !!}
                        {!! Form::text('price_subtotal', null, ['class' => 'form-control thousand']) !!}
                    </div>

                    <!-- Tax Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('tax', 'Tax:') !!}
                        {!! Form::text('tax', null, ['class' => 'form-control thousand']) !!}
                    </div>--}}

                    <!-- Item Return Type Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_return_type', 'Item Return Type:') !!}
                        {!! Form::select('item_return_type', ['ready' => 'ready', 'rejected' => 'Rejected'], null, ['class' => 'form-control select2']) !!}
                    </div>

                    <!-- Note Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('item_note', 'Note:') !!}
                        {!! Form::textarea('item_note', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-form">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
