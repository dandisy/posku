@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell Return
        </h1>
        
        {{--@include('sell_returns.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('sell_returns.show_fields')
                    <a href="{!! route('sellReturns.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
