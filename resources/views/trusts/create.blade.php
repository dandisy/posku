@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Trust
        </h1>

        {{--@include('trusts.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'trusts.store']) !!}

                        @include('trusts.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
