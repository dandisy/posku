@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Trust
        </h1>
        
        {{--@include('trusts.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('trusts.show_fields')
                    <a href="{!! route('trusts.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
