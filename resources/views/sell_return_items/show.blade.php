@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell Return Item
        </h1>
        
        {{--@include('sell_return_items.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('sell_return_items.show_fields')
                    <a href="{!! route('sellReturnItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
