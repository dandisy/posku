@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell Return Item
        </h1>

        {{--@include('sell_return_items.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sellReturnItem, ['route' => ['sellReturnItems.update', $sellReturnItem->id], 'method' => 'patch']) !!}

                        @include('sell_return_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection