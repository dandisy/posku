<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sellReturnItem->id !!}</p>
</div>

<!-- Sell Return Id Field -->
<div class="form-group">
    {!! Form::label('sell_return_id', 'Sell Return Id:') !!}
    <p>{!! $sellReturnItem->sell_return_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $sellReturnItem->item !!}</p>
</div>

<!-- Unique Barcode Field -->
<div class="form-group">
    {!! Form::label('unique_barcode', 'Unique Barcode:') !!}
    <p>{!! $storeFrontItem->unique_barcode !!}</p>
</div>

<!-- Item Discount Field -->
<div class="form-group">
    {!! Form::label('item_discount', 'Item Discount:') !!}
    <p>{!! $sellReturnItem->item_discount !!}</p>
</div>

<!-- Item Price Field -->
<div class="form-group">
    {!! Form::label('item_price', 'Item Price:') !!}
    <p>{!! $sellReturnItem->item_price !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $sellReturnItem->item_amount !!}</p>
</div>

<!-- Discount Subtotal Field -->
<div class="form-group">
    {!! Form::label('discount_subtotal', 'Discount Subtotal:') !!}
    <p>{!! $sellReturnItem->discount_subtotal !!}</p>
</div>

<!-- Price Subtotal Field -->
<div class="form-group">
    {!! Form::label('price_subtotal', 'Price Subtotal:') !!}
    <p>{!! $sellReturnItem->price_subtotal !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $sellReturnItem->tax !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $sellReturnItem->note !!}</p>
</div>

<!-- Item Return Typee Field -->
<div class="form-group">
    {!! Form::label('item_return_type', 'Item Return Type:') !!}
    <p>{!! $storeFrontItem->item_return_type !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $sellReturnItem->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $sellReturnItem->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sellReturnItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sellReturnItem->updated_at !!}</p>
</div>

