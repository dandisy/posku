@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Composite
        </h1>
        
        {{--@include('composites.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('composites.show_fields')
                    <a href="{!! route('composites.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
