@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Composite
        </h1>

        {{--@include('composites.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'composites.store']) !!}

                        @include('composites.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalAddItemLabel" rowkey="" rowindex="">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalAddItemLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Composite Id Field -->
                    <!-- {{--<div class="form-group col-sm-6">
                        {!! Form::label('composite_id', 'Composite Id:') !!}
                        {!! Form::number('composite_id', null, ['class' => 'form-control']) !!}
                    </div>--}} -->

                    {!! Form::hidden('composite_id', null, ['id' => 'composite_id']) !!}
                    {!! Form::hidden('item_name', null, ['id' => 'item_name']) !!}

                    <!-- Item Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item', 'Item:') !!}
                        <!-- {{--{!! Form::select('item', $items->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}--}} -->
                        <select class="form-control select2" name="item" id="item">
                            <option></option>
                            @foreach($items as $item)
                            <option value="{{$item->id}}" data-itemname="{{$item->name}}" data-barcode="{{$item->unique_barcode}}" data-ismanyuniquebarcode="{{$item->is_many_unique_barcode}}">{{$item->name_barcode}}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Item Amount Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_amount', 'Item Amount:') !!}
                        {!! Form::number('item_amount', null, ['class' => 'form-control thousand']) !!}
                    </div>

                    <!-- Unit Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('unit', 'Unit:') !!}
                        {!! Form::select('unit', $units->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
                    </div>

                    <!-- Note Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('item_note', 'Note:') !!}
                        {!! Form::textarea('item_note', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-form">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
