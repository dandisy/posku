@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Stock Transfer Item
        </h1>
        
        {{--@include('stock_transfer_items.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('stock_transfer_items.show_fields')
                    <a href="{!! route('stockTransferItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
