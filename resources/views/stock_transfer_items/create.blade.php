@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Stock Transfer Item
        </h1>

        {{--@include('stock_transfer_items.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'stockTransferItems.store']) !!}

                        @include('stock_transfer_items.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
