@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Customer
        </h1>
        
        {{--@include('customers.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('customers.show_fields')
                    <a href="{!! route('customers.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
