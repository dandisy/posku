@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Log
        </h1>
        
        {{--@include('logs.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('logs.show_fields')
                    <a href="{!! route('logs.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
