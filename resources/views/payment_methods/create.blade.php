@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Payment Method
        </h1>

        {{--@include('payment_methods.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'paymentMethods.store']) !!}

                        @include('payment_methods.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
