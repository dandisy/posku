@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Payment Method
        </h1>
        
        {{--@include('payment_methods.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('payment_methods.show_fields')
                    <a href="{!! route('paymentMethods.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
