@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Cash
        </h1>
        
        {{--@include('cashes.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cashes.show_fields')
                    <a href="{!! route('cashes.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
