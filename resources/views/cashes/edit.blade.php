@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Cash
        </h1>

        {{--@include('cashes.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cash, ['route' => ['cashes.update', $cash->id], 'method' => 'patch']) !!}

                        @include('cashes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection