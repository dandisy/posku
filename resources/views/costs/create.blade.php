@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Cost
        </h1>

        {{--@include('costs.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costs.store']) !!}

                        @include('costs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
