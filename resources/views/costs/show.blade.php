@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Cost
        </h1>
        
        {{--@include('costs.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('costs.show_fields')
                    <a href="{!! route('costs.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
