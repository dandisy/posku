<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sellItem->id !!}</p>
</div>

<!-- Sell Id Field -->
<div class="form-group">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    <p>{!! $sellItem->sell_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $sellItem->item !!}</p>
</div>

<!-- Item Name Field -->
<div class="form-group">
    {!! Form::label('item_name', 'Item Name:') !!}
    <p>{!! $sellItem->item_name !!}</p>
</div>

<!-- Unique Barcode Field -->
<div class="form-group">
    {!! Form::label('unique_barcode', 'Unique Barcode:') !!}
    <p>{!! $sellItem->unique_barcode !!}</p>
</div>

<!-- Item Discount Field -->
<div class="form-group">
    {!! Form::label('item_discount', 'Item Discount:') !!}
    <p>{!! $sellItem->item_discount !!}</p>
</div>

<!-- Item Price Field -->
<div class="form-group">
    {!! Form::label('item_price', 'Item Price:') !!}
    <p>{!! $sellItem->item_price !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $sellItem->item_amount !!}</p>
</div>

<!-- Discount Subtotal Field -->
<div class="form-group">
    {!! Form::label('discount_subtotal', 'Discount Subtotal:') !!}
    <p>{!! $sellItem->discount_subtotal !!}</p>
</div>

<!-- Price Subtotal Field -->
<div class="form-group">
    {!! Form::label('price_subtotal', 'Price Subtotal:') !!}
    <p>{!! $sellItem->price_subtotal !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $sellItem->tax !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $sellItem->note !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $sellItem->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $sellItem->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sellItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sellItem->updated_at !!}</p>
</div>

