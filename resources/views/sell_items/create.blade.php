@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell Item
        </h1>

        {{--@include('sell_items.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'sellItems.store']) !!}

                        @include('sell_items.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
