@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Wrap
        </h1>

        {{--@include('wraps.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'wraps.store']) !!}

                        @include('wraps.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
