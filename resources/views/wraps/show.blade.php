@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Wrap
        </h1>
        
        {{--@include('wraps.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('wraps.show_fields')
                    <a href="{!! route('wraps.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
