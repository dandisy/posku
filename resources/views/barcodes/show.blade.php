@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Barcode
        </h1>
        
        {{--@include('barcodes.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('barcodes.show_fields')
                    <a href="{!! route('barcodes.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
