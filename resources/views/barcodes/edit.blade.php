@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Barcode
        </h1>

        {{--@include('barcodes.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($barcode, ['route' => ['barcodes.update', $barcode->id], 'method' => 'patch']) !!}

                        @include('barcodes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection