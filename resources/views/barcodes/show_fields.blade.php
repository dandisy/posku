<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $barcode->id !!}</p>
</div>

<!-- Purchase Id Field -->
<div class="form-group">
    {!! Form::label('purchase_id', 'Purchase Id:') !!}
    <p>{!! $barcode->purchase_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $barcode->item !!}</p>
</div>

<!-- Barcode Field -->
<div class="form-group">
    {!! Form::label('barcode', 'Barcode:') !!}
    <p>{!! $barcode->barcode !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $barcode->store !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $barcode->status !!}</p>
</div>

<!-- Purchase Return Id Field -->
<div class="form-group">
    {!! Form::label('purchase_return_id', 'Purchase Return Id:') !!}
    <p>{!! $barcode->purchase_return_id !!}</p>
</div>

<!-- Store Front Id Field -->
<div class="form-group">
    {!! Form::label('store_front_id', 'Store Front Id:') !!}
    <p>{!! $barcode->store_front_id !!}</p>
</div>

<!-- Sell Id Field -->
<div class="form-group">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    <p>{!! $barcode->sell_id !!}</p>
</div>

<!-- Sell Return Id Field -->
<div class="form-group">
    {!! Form::label('sell_return_id', 'Sell Return Id:') !!}
    <p>{!! $barcode->sell_return_id !!}</p>
</div>

<!-- Stock Transfer Id Field -->
<div class="form-group">
    {!! Form::label('stock_transfer_id', 'Stock Transfer Id:') !!}
    <p>{!! $barcode->stock_transfer_id !!}</p>
</div>

<!-- Rejected Item Id Field -->
<div class="form-group">
    {!! Form::label('rejected_item_id', 'Rejected Item Id:') !!}
    <p>{!! $barcode->rejected_item_id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $barcode->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $barcode->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $barcode->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $barcode->updated_at !!}</p>
</div>

