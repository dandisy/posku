@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Composite Item
        </h1>
        
        {{--@include('composite_items.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('composite_items.show_fields')
                    <a href="{!! route('compositeItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
