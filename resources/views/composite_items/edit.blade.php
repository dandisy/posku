@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Composite Item
        </h1>

        {{--@include('composite_items.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($compositeItem, ['route' => ['compositeItems.update', $compositeItem->id], 'method' => 'patch']) !!}

                        @include('composite_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection