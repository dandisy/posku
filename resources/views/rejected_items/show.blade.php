@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Rejected Item
        </h1>
        
        {{--@include('rejected_items.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('rejected_items.show_fields')
                    <a href="{!! route('rejectedItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
