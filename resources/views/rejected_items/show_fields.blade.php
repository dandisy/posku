<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $rejectedItem->id !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $rejectedItem->store !!}</p>
</div>

<!-- Source Field -->
<div class="form-group">
    {!! Form::label('source', 'Source:') !!}
    <p>{!! $rejectedItem->source !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $rejectedItem->item !!}</p>
</div>

<!-- Unique Barcode Field -->
<div class="form-group">
    {!! Form::label('unique_barcode', 'Unique Barcode:') !!}
    <p>{!! $storeFrontItem->unique_barcode !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $rejectedItem->item_amount !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $rejectedItem->note !!}</p>
</div>

<!-- Model Type Field -->
<div class="form-group">
    {!! Form::label('model_type', 'Model Type:') !!}
    <p>{!! $rejectedItem->model_type !!}</p>
</div>

<!-- Model Id Field -->
<div class="form-group">
    {!! Form::label('model_id', 'Model Id:') !!}
    <p>{!! $rejectedItem->model_id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $rejectedItem->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $rejectedItem->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $rejectedItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $rejectedItem->updated_at !!}</p>
</div>

