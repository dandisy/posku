@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Rejected Item
        </h1>

        {{--@include('rejected_items.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rejectedItem, ['route' => ['rejectedItems.update', $rejectedItem->id], 'method' => 'patch']) !!}

                        @include('rejected_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection