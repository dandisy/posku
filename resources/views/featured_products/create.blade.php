@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Featured Product
        </h1>

        {{--@include('featured_products.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'featuredProducts.store']) !!}

                        @include('featured_products.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
