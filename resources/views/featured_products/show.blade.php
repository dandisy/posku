@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Featured Product
        </h1>
        
        {{--@include('featured_products.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('featured_products.show_fields')
                    <a href="{!! route('featuredProducts.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
