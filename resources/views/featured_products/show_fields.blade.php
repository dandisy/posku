<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $featuredProduct->id !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $featuredProduct->store !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $featuredProduct->item !!}</p>
</div>

<!-- Publish On Field -->
<div class="form-group">
    {!! Form::label('publish_on', 'Publish On:') !!}
    <p>{!! $featuredProduct->publish_on !!}</p>
</div>

<!-- Publish Off Field -->
<div class="form-group">
    {!! Form::label('publish_off', 'Publish Off:') !!}
    <p>{!! $featuredProduct->publish_off !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $featuredProduct->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $featuredProduct->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $featuredProduct->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $featuredProduct->updated_at !!}</p>
</div>

