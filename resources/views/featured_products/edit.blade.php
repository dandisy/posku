@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Featured Product
        </h1>

        {{--@include('featured_products.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($featuredProduct, ['route' => ['featuredProducts.update', $featuredProduct->id], 'method' => 'patch']) !!}

                        @include('featured_products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection