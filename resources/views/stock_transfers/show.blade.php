@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Stock Transfer
        </h1>
        
        {{--@include('stock_transfers.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('stock_transfers.show_fields')
                    <a href="{!! route('stockTransfers.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
