@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Store Front Stock
        </h1>

        {{--@include('store_front_stocks.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'storeFrontStocks.store']) !!}

                        @include('store_front_stocks.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
