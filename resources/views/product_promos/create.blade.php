@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Product Promo
        </h1>

        {{--@include('product_promos.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'productPromos.store']) !!}

                        @include('product_promos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalAddItemLabel" rowkey="" rowindex="">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalAddItemLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Product Promo Id Field -->
                    <!-- {{--<div class="form-group col-sm-6">
                        {!! Form::label('product_promo_id', 'Product Promo Id:') !!}
                        {!! Form::number('product_promo_id', null, ['class' => 'form-control']) !!}
                    </div>--}} -->

                    {!! Form::hidden('product_promo_id', null, ['id' => 'product_promo_id']) !!}                    

                    <!-- Condition Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('condition', 'Condition:') !!}
                        {!! Form::select('condition', $promoConditions->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
                    </div>

                    <!-- Value Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('value_condition', 'Value:') !!}
                        {!! Form::text('value_condition', null, ['class' => 'form-control thousand']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-form">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
