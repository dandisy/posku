@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/dxDataGrid/css/dx.common.css') }}" />
    <link rel="dx-theme" data-theme="generic.light" href="{{ asset('vendor/dxDataGrid/css/dx.light.css') }}" />
    <style>
        .dx-datagrid .dx-data-row > td.bullet {
            padding-top: 0;
            padding-bottom: 0;
        }
        .dx-datagrid-content .dx-datagrid-table .dx-row .dx-command-edit {
            width: auto;
            min-width: 140px;
        }
    </style>
@endsection

<!-- Item Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item', 'Item:') !!}
    {!! Form::select('item', $item->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Unique Barcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unique_barcode', 'Unique Barcode:') !!}
    {!! Form::text('unique_barcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Publish On Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publish_on', 'Publish On:') !!}
    {!! Form::text('publish_on', null, ['class' => 'form-control datetime']) !!}
</div>

<!-- Publish Off Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publish_off', 'Publish Off:') !!}
    {!! Form::text('publish_off', null, ['class' => 'form-control datetime']) !!}
</div>

<!-- Store Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store', 'Store:') !!}
    {!! Form::select('store', $store->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<div class="clearfix"></div>
<hr>

<div class="col-sm-12">
    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalAddItem">
        Add Item
    </button>

    <div class="items-data"></div>
</div>

<div class="col-sm-12">
    <div class="dx-viewport">
        <div class="demo-container">
            <div id="gridContainer"></div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('productPromos.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script src="{{ asset('vendor/dxDataGrid/js/cldr.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/event.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/supplemental.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/unresolved.min.js') }}"></script>	
    <script src="{{ asset('vendor/dxDataGrid/js/globalize.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/message.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/number.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/date.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/currency.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/dx.web.js') }}"></script>

    <script>
        var dataSource = @json($productPromoConditions);

        function updateDataGrid(data) {
            if(data) {
                dataSource.push(data);
            }
            
            $("#gridContainer").dxDataGrid({ dataSource: dataSource });
        }
    </script>

    <script>
        var doDeselection;
        function logEvent(eventName) {
            var logList = $("#events ul"),
                newItem = $("<li>", { text: eventName });

            logList.prepend(newItem);
        }

        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            columnAutoWidth: true,
            allowColumnResizing: true,
            columnResizingMode: 'widget', // or 'nextColumn'
            rowAlternationEnabled: true,
            hoverStateEnabled: true, 
            // showBorders: true,
            grouping: {
                autoExpandAll: false,
                contextMenuEnabled: true
            },
            groupPanel: {
                visible: true
            },       
            searchPanel: {
                visible: true
            },   
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            columnFixing: {
                enabled: true
            },
            // height: 420,            
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 50, 100],
                showInfo: true
            },
            keyExpr: "id",
            columns: [
                {  
                    caption: '#',  
                    cellTemplate: function(cellElement, cellInfo) {  
                        cellElement.text(cellInfo.row.rowIndex + 1);
                    }  
                },
                {
                    dataField: 'id',
                    visible: false
                },
                "condition",
                {
                    dataField: "value",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                // "created_by",
                // "created_at",
                // "updated_by",
                // "updated_at"
            ],
            editing: {
                mode: "row",
                // allowUpdating: true,
                allowDeleting: function(e) {
                    return true;
                },
                // allowAdding: true
            },
            onCellPrepared: function (e) {
                if (e.rowType === "data" && e.column.command === "edit") {
                    e.cellElement.prepend('&nbsp;');
                    $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                    console.log(e.row.data);
                            if(/^\d+$/.test(e.key)) {
                                $('#myModalAddItem').modal('toggle');
                                $('#myModalAddItem').find('#myModalAddItemLabel').text('Edit Item');
                                $('#myModalAddItem').attr('rowkey', e.key);
                                $('#myModalAddItem').attr('rowindex', e.rowIndex);
                                $('#myModalAddItem').find('#product_promo_id').val(e.row.data.product_promo_id);
                                $('#myModalAddItem').find('#condition').val(e.row.data.condition);
                                $('#myModalAddItem').find('#value_condition').val(e.row.data.value);
                            } else {
                                $('#myModalAddItem').modal('toggle');
                                $('#myModalAddItem').find('#myModalAddItemLabel').text('Edit Item');
                                $('#myModalAddItem').attr('rowkey', e.key);
                                $('#myModalAddItem').attr('rowindex', e.rowIndex);
                                $('#myModalAddItem').find('#product_promo_id').val('');
                                $('#myModalAddItem').find('#condition').val(e.row.data.condition);
                                $('#myModalAddItem').find('#value_condition').val(e.row.data.value);
                            }
                        })
                        .prependTo(e.cellElement);
                }
            },
            onRowRemoved: function(e) {
                // if(/^\d+$/.test(e.key)) {
                //     $.ajaxSetup({
                //         headers: {
                //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //         }
                //     });
                //     $.ajax({
                //         url: '{{url("productPromoConditions")}}/'+JSON.stringify(e.data.id),
                //         type: 'delete'
                //     }).success(function(res) {
                //         $('.items-data').find('.rowkey'+e.key).remove();
                //     }).error(function(res) {
                //         console.log(res);
                //     });
                // } else {
                    $('.items-data').find('.rowkey'+e.key).remove();
                // }
                
                // logEvent("RowRemoved");
            },
        });
        
        // function OnClick() {
        //     var arrId = $("#gridContainer").dxDataGrid("instance").getSelectedRowKeys();
        //     console.log('id', arrId);
        // }
    </script>

    <script>
        $('#myModalAddItem').on('shown.bs.modal', function () {
            $('.select2').select2();
        });
        
        $('#myModalAddItem').on('hide.bs.modal', function () {
            $('#myModalAddItem').attr('rowkey', null);
            $('#myModalAddItem').attr('rowindex', null);
            $('#myModalAddItem').find('#myModalAddItemLabel').text('Add Item');
            $('#myModalAddItem').find('input').val('');
            $('#myModalAddItem').find('textarea').val('');
            $('#myModalAddItem').find('textarea').text('');
            $('#myModalAddItem').find('.select2').val('').trigger('change');
        });

        $(document).on('click', '.btn-save-form', function() {
            var input = $(this).parents('form').serializeArray();
            var data = {};

            if($('#myModalAddItem').attr('rowkey')) {
                if($('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).length) {
                    $(input).each(function(i, v) {
                        dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'item_note' ? 'note' : v.name] = v.value;
                        dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'value_condition' ? 'value' : v.name] = v.value;
                        $('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).find('[name="'+v.name+'[]"]').val(v.value);
                    });
                } else {
                    $('.items-data').append('<div/>');
                    $('.items-data > div:last-child').addClass('rowkey'+$('#myModalAddItem').attr('rowkey'));
                    $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="item_id[]" value="'+$('#myModalAddItem').attr('rowkey')+'">');
                    $(input).each(function(i, v) {
                        dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'item_note' ? 'note' : v.name] = v.value;
                        dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'value_condition' ? 'value' : v.name] = v.value;
                        $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value+'">');
                    });
                }
            
                updateDataGrid(null);
            } else {
                if(dataSource.length) {
                    data['id'] = (parseInt(dataSource[dataSource.length-1].id)+1)+'-tmp';
                } else {
                    data['id'] = '1-tmp';
                }
                $('.items-data').append('<div/>');
                $('.items-data > div:last-child').addClass('rowkey'+data['id']);
                $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="item_id[]" value="'+data['id']+'">');
                $(input).each(function(i, v) {
                    data[v.name === 'item_note' ? 'note' : v.name] = v.value;
                    data[v.name === 'value_condition' ? 'value' : v.name] = v.value;
                    $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value+'">');
                });
            
                updateDataGrid(data);
            }

            $('#myModalAddItem').find('input').val('');
            $('#myModalAddItem').find('textarea').val('');
            $('#myModalAddItem').find('textarea').text('');

            $('#myModalAddItem').modal('toggle');
        });
    </script>

<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
