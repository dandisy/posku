@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Product Promo
        </h1>
        
        {{--@include('product_promos.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('product_promos.show_fields')
                    <a href="{!! route('productPromos.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
