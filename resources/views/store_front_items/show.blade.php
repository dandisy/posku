@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Store Front Item
        </h1>
        
        {{--@include('store_front_items.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('store_front_items.show_fields')
                    <a href="{!! route('storeFrontItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
