@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Store Front Item
        </h1>

        {{--@include('store_front_items.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'storeFrontItems.store']) !!}

                        @include('store_front_items.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
