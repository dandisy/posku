<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $storeFrontItem->id !!}</p>
</div>

<!-- Store Front Id Field -->
<div class="form-group">
    {!! Form::label('store_front_id', 'Store Front Id:') !!}
    <p>{!! $storeFrontItem->store_front_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $storeFrontItem->item !!}</p>
</div>

<!-- Unique Barcode Field -->
<div class="form-group">
    {!! Form::label('unique_barcode', 'Unique Barcode:') !!}
    <p>{!! $storeFrontItem->unique_barcode !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $storeFrontItem->item_amount !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $storeFrontItem->note !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $storeFrontItem->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $storeFrontItem->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $storeFrontItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $storeFrontItem->updated_at !!}</p>
</div>

