@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Purchase Item
        </h1>

        {{--@include('purchase_items.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($purchaseItem, ['route' => ['purchaseItems.update', $purchaseItem->id], 'method' => 'patch']) !!}

                        @include('purchase_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection