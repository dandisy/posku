<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $purchaseItem->id !!}</p>
</div>

<!-- Purchase Id Field -->
<div class="form-group">
    {!! Form::label('purchase_id', 'Purchase Id:') !!}
    <p>{!! $purchaseItem->purchase_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $purchaseItem->item !!}</p>
</div>

<!-- Item Discount Field -->
<div class="form-group">
    {!! Form::label('item_discount', 'Item Discount:') !!}
    <p>{!! $purchaseItem->item_discount !!}</p>
</div>

<!-- Item Price Field -->
<div class="form-group">
    {!! Form::label('item_price', 'Item Price:') !!}
    <p>{!! $purchaseItem->item_price !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $purchaseItem->item_amount !!}</p>
</div>

<!-- Discount Subtotal Field -->
<div class="form-group">
    {!! Form::label('discount_subtotal', 'Discount Subtotal:') !!}
    <p>{!! $purchaseItem->discount_subtotal !!}</p>
</div>

<!-- Price Subtotal Field -->
<div class="form-group">
    {!! Form::label('price_subtotal', 'Price Subtotal:') !!}
    <p>{!! $purchaseItem->price_subtotal !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $purchaseItem->tax !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $purchaseItem->note !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $purchaseItem->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $purchaseItem->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $purchaseItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $purchaseItem->updated_at !!}</p>
</div>

