@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Store
        </h1>
        
        {{--@include('stores.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('stores.show_fields')
                    <a href="{!! route('stores.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
