@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Store Front
        </h1>
        
        {{--@include('store_fronts.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('store_fronts.show_fields')
                    <a href="{!! route('storeFronts.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
