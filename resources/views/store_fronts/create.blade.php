@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Store Front
        </h1>

        {{--@include('store_fronts.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'storeFronts.store']) !!}

                        @include('store_fronts.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalAddItemLabel" rowkey="" rowindex="">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalAddItemLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Store Front Id Field -->
                    <!-- {{--<div class="form-group col-sm-6">
                        {!! Form::label('store_front_id', 'Store Front Id:') !!}
                        {!! Form::number('store_front_id', null, ['class' => 'form-control']) !!}
                    </div>--}} -->

                    {!! Form::hidden('store_front_id', null, ['id' => 'store_front_id']) !!}
                    {!! Form::hidden('item_name', null, ['id' => 'item_name']) !!}
                    {!! Form::hidden('unique_barcode', null, ['id' => 'unique_barcode']) !!}

                    <!-- Item Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item', 'Item:') !!}
                        <!-- {{--{!! Form::select('item', $items->pluck('name_barcode', 'barcode'), null, ['class' => 'form-control select2']) !!}--}} -->
                        <select class="form-control select2" name="item" id="item">
                            <option></option>
                            @foreach($items as $item)
                            <option value="{{$item->id}}" data-itemname="{{$item->name}}" data-barcode="{{$item->unique_barcode}}" data-ismanyuniquebarcode="{{$item->is_many_unique_barcode}}">{{$item->name_barcode}}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Item Amount Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_amount', 'Item Amount:') !!}
                        {!! Form::number('item_amount', 1, ['class' => 'form-control thousand', 'min' => 1]) !!}
                    </div>

                    <!-- Note Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('item_note', 'Note:') !!}
                        {!! Form::textarea('item_note', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-form">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
