@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell
        </h1>
        
        {{--@include('sells.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('sells.show_fields')
                    <a href="{!! route('sells.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
