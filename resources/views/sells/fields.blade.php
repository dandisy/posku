@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/dxDataGrid/css/dx.common.css') }}" />
    <link rel="dx-theme" data-theme="generic.light" href="{{ asset('vendor/dxDataGrid/css/dx.light.css') }}" />
    <style>
        .dx-datagrid .dx-data-row > td.bullet {
            padding-top: 0;
            padding-bottom: 0;
        }
        .dx-datagrid-content .dx-datagrid-table .dx-row .dx-command-edit {
            width: auto;
            min-width: 140px;
        }
    </style>
@endsection

<!-- Customer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer', 'Customer:') !!}
    <div class="input-group">
        {!! Form::select('customer', $customer->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
        <div class="input-group-btn">
            <span type="button" class="btn btn-danger" data-toggle="modal" data-target="#myAddCustomerModal">Add Customer</span>
        </div>
    </div>
</div>

<!-- Store Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store', 'Store:') !!}
    {!! Form::select('store', $store->pluck('name', 'id'), null, ['class' => 'form-control select2', 'disabled']) !!}
</div>

{!! Form::hidden('bill_amount', null, ['id' => 'bill_amount']) !!}
{{--<!-- Bill Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bill_amount', 'Bill Amount:') !!}
    {!! Form::text('bill_amount', null, ['class' => 'form-control thousand']) !!}
</div>--}}

<div class="clearfix"></div>

@php
$tableOrderSettingValue = null;
$tableOrderSetting = \App\Models\Setting::where('key', 'tableOrder')->first();
if($tableOrderSetting) {
    $tableOrderSettingValue = $tableOrderSetting->value;
}
@endphp
@if($tableOrderSettingValue == 'yes')
<!-- Table Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('table_number', 'Table Number:') !!}
    {!! Form::text('table_number', null, ['class' => 'form-control']) !!}
</div>
@endif

{{--<!-- Table Order Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('table_order_status', 'Table Order Status:') !!}
    {!! Form::select('table_order_status', ['no-order' => 'No-Order', 'preparing' => 'Preparing', 'served' => 'Served'], null, ['class' => 'form-control select2']) !!}
</div>--}}

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::select('payment_method', $paymentmethod->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<div class="clearfix"></div>
<hr>

<div class="col-sm-12 col-lg-12">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default" style="margin:0">
                <div class="panel-body">
                    <div class="dx-viewport">
                        <div class="demo-container">
                            <div id="gridContainer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 shopping-cart-wrapper">
            <div class="panel panel-default" style="margin:0">
                <div class="panel-body">
                    <div class="row shopping-cart" style="height:330.8px;overflow-y:auto">
                        <div class="col-sm-12" style="line-height:34px;font-weight:bold;margin-bottom:5px">
                            <div style="margin-left:-15px;min-width:400px;border-bottom:1px solid #ddd">
                                <div class="col-xs-3" style="text-align:left">Item</div>
                                <div class="col-xs-2" style="text-align:center">QTY</div>
                                <div class="col-xs-2" style="text-align:center">Price</div>
                                <div class="col-xs-2" style="text-align:center">Discount</div>
                                <div class="col-xs-2" style="text-align:center">Subtotal</div>
                                <div class="col-xs-1" style="text-align:center"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @if(!empty($sellItems))
                        @foreach($sellItems as $k => $item)
                        @php($isReadonly = $item->unique_barcode ? 'readonly' : null)
                        <div class="col-sm-12 record" style="line-height:34px">
                            <div style="margin-left:-15px;min-width:400px">
                                <input class="item_id" type="hidden" name="item[]" value="{{$item->item}}">
                                <input type="hidden" name="item_name[]" value="{{$item->item_name}}">
                                <input class="unique_barcode" type="hidden" name="unique_barcode[]" value="{{$item->unique_barcode}}">
                                <input type="hidden" name="item_price[]" value="{{$item->item_price}}">
                                <input class="discount_subtotal" type="hidden" name="discount_subtotal[]" value="{{$item->discount_subtotal}}">
                                <input class="price_subtotal" type="hidden" name="price_subtotal[]" value="{{$item->price_subtotal}}">
                                <div class="col-xs-3 item_name">{{$item->item_name}}{{($item->unique_barcode != 'null' && !empty($item->unique_barcode)) ? ' ('.$item->unique_barcode.')' : ''}}</div>
                                <div class="col-xs-2"><input class="item_amount" type="number" name="item_amount[]" value="{{$item->item_amount}}" required {{$isReadonly}} style="width:100%;height:34px;text-align:center"></div>
                                <div class="col-xs-2 item_price" style="text-align:right">{{$item->item_price}}</div>
                                <div class="col-xs-2"><input class="item_discount" type="number" name="item_discount[]" value="{{$item->item_discount}}" required style="width:100%;height:34px;text-align:center"></div>
                                <div class="col-xs-2 price_subtotal_display" style="text-align:right">{{$item->price_subtotal}}</div>
                                <div class="col-xs-1" style="text-align:right"><span class="btn btn-warning btn-remove-item"><i class="fa fa-trash"></i></span></div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<div class="col-sm-12 col-lg-12">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default" style="margin:0;background-color:#f7f7f9">
                <div class="panel-body">
                    <div class="col-xs-12" style="line-height:34px;font-weight:bold">TOTAL :</div>                    
                    <div class="col-xs-12" style="line-height:39.45px">
                        <div class="total" style="text-align:center;font-weight:bold;font-size:3em">0</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default" style="margin:0">
                <div class="panel-body">
                    <div class="col-sm-12" style="line-height:34px;font-weight:bold">
                        <div class="col-xs-4" style="text-align:center">Subtotal</div>
                        <div class="col-xs-4" style="text-align:center">Discount</div> 
                        <div class="col-xs-4" style="text-align:center">Total</div>
                    </div>
                    <div class="col-sm-12" style="line-height:34px">
                        <div class="col-xs-4 subtotal" style="text-align:center">0</div>
                        <!-- <div class="col-sm-4 discount" style="text-align:center">0</div>  -->
                        <input type="number" class="col-xs-4 discount" min="0" value="0" style="text-align:center">
                        <div class="col-xs-4 total" style="text-align:center;font-weight:bold">0</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<!-- Payment Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    {!! Form::select('payment_status', ['unpaid' => 'Unpaid', 'paid' => 'Paid', 'failed' => 'Failed'], null, ['class' => 'form-control select2']) !!}
</div>

<!-- Selling Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('selling_type', 'Selling Type:') !!}
    {!! Form::select('selling_type', ['purchase' => 'Purchase', 'order' => 'Order', 'waiting' => 'Waiting'], null, ['class' => 'form-control select2', 'disabled']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>
<hr>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('sells.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script>
        $('#myAddCustomerModal').find('form').submit(function(e) {
            e.preventDefault();
            
            $.ajax({
                url: $('#myAddCustomerModal').find('form').attr('action'),
                type: 'post',
                data: $('#myAddCustomerModal').find('form').serialize()
            }).then(function(res) {
                // console.log(res)
                $('#customer').append('<option value="'+res.id+'" selected>'+res.name+'</option>');
                $('#customer').select2();
                $('#myAddCustomerModal').modal('hide');
            });
        });
    </script>

    <script src="{{ asset('vendor/dxDataGrid/js/cldr.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/event.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/supplemental.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/unresolved.min.js') }}"></script>	
    <script src="{{ asset('vendor/dxDataGrid/js/globalize.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/message.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/number.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/date.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/currency.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/dx.web.js') }}"></script>
    
    <script src="{{ asset('vendor/printThis/printThis.js') }}"></script>

    <script src="https://opensource.teamdf.com/number/jquery.number.js"></script>

    <script src="{{ asset('ProductPromo/promo-formula.js') }}"></script>

    <script>
        var total = 0;
    </script>

    <script>
        function getPromo(el, callback) {
            var shoppingRecords = $('.shopping-cart').find('.record');
            var item = el.parents('.record').find('[name="item[]"]').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url("getPromo") }}/'+item,
                type: 'get',
            }).success(function(res) {
                if(res) {
                    var startDate = moment(res.publish_on);
                    var endDate = moment(res.publish_off);

                    var testDate = moment();

                    if(testDate.isBetween(startDate, endDate, true)) {
                        if(res.conditions) {
                            promoInit(res, shoppingRecords, item, callback);
                        }
                    }
                }
            }).error(function(res) {
                console.log(res);
            });
        }
    </script>

    <script>
        function sellCount() {
            var tmpSellItems = [];
            $.each($(document).find('.item_name'), function(i, v) {
                var record = $(v).parents('.record');

                tmpSellItems.push({
                    Item: record.find('.item_name').text(),
                    UniqueBarcode: record.find('.unique_barcode').val(),
                    QTY: record.find('.item_amount').val(),
                    Discount: record.find('.item_discount').val(),
                    Subtotal: record.find('.price_subtotal').val()
                });
            });
            sellItems = tmpSellItems;

            var subtotal = 0;
            // var discount = 0;
            $.each($(document).find('.price_subtotal'), function(i, v) {
                subtotal += parseInt($(v).val());
            });
            // $.each($(document).find('.discount_subtotal'), function(i, v) {
            //     discount += parseInt($(v).val());
            // });

            $('.subtotal').text($.number(subtotal, 0));
            // $('.discount').text($.number(discount, 0));
            var discount = parseInt($('.discount').val());

            total = subtotal - discount;
            $('.total').text($.number(total, 0));
            $('#bill_amount').val(total);

            billFooterTable = [{
                Item: null,
                QTY: null,
                Discount: discount,
                Subtotal: total
            }];

            $('.discount').attr('max', subtotal);

            if($('.discount').val() > $('.discount').attr('max')) {
                $('.discount').val($('.discount').attr('max'));

                sellCount();
            }
        }
    </script>

    @if(!empty($sellItems))
    <script>
        sellCount();
    </script>
    @endif

    <script>
        $(document).on('change', 'input[type="number"]', function() {
            if($(this).val() > $(this).attr('max')) {
                $(this).val($(this).attr('max'));
                $(this).focus();

                alert('Nilai input melebihi batas maksimal!');
            }
        });

        $(document).on('click', '.btn-remove-item', function() {
            $(this).parents('.record').remove();

            sellCount();

            var uBarCodeRemoved = $(this).parents('.record').find('.unique_barcode').val();
            if(uBarCodeRemoved != 'null' && uBarCodeRemoved) {
                var dsAdded = dataSourceRemoved.filter(x => x.unique_barcode == uBarCodeRemoved);
                if(dsAdded.length > 0) {
                    dataSource.push(dsAdded[0]);
                    $("#gridContainer").dxDataGrid({ dataSource: dataSource });
                }
            } else {
                var itemIdRemoved = $(this).parents('.record').find('.item_id').val()
                var dsAdded = dataSourceRemoved.filter(x => x.id == itemIdRemoved);
                if(dsAdded.length > 0) {
                    dataSource.push(dsAdded[0]);
                    // dataSource = sortByKeyAsc(dataSource, 'id');
                    $("#gridContainer").dxDataGrid({ dataSource: dataSource });
                    // $("#gridContainer").dxDataGrid("instance").columnOption("id", {
                    //     sortOrder: "asc",
                    //     sortIndex: 0
                    // });
                }
            }
        });

        $(document).on('change', '.item_amount, .item_discount', function() {
            var item_amount = parseInt($(this).parents('.record').find('.item_amount').val());
            var item_price = parseInt($(this).parents('.record').find('.item_price').text());
            var item_discount = parseInt($(this).parents('.record').find('.item_discount').val());
            var discount_subtotal = item_discount;
            var price_subtotal = (item_amount * item_price) - item_discount;

            $(this).parents('.record').find('.discount_subtotal').val(discount_subtotal);
            $(this).parents('.record').find('.price_subtotal').val(price_subtotal);
            $(this).parents('.record').find('.price_subtotal_display').text($.number(price_subtotal, 0));

            sellCount();

            getPromo($(this), sellCount);
        });
        $(document).on('change', '.discount', function() {
            sellCount();
        });
    </script>
    
    <script>
        var sellItems = [];
        var billFooterTable = [];

        $('#sell-form').on('submit', function(e) {
            e.preventDefault();

            if(sellItems) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route("sells.store") }}',
                    type: 'post',
                    data: $(this).serialize()
                }).success(function(res) {
                    var $tr1 = $('<tr style="border-bottom:1px solid black">').append(
                        $('<th>').text('Item'),
                        $('<th>').text('Unique Barcode'),
                        $('<th width="50">').text('QTY'),
                        $('<th width="100">').text('Discount'),
                        $('<th width="100">').text('Subtotal')
                    );
                    var $table = $('<table width="400" style="margin-top:15px">').append($tr1);
                    
                    $.each(sellItems, function(i, item) {
                        var $tr = $('<tr>').append(
                            $('<td>').text(item.Item),
                            $('<td>').text(item.UniqueBarcode),
                            $('<td>').text(item.QTY),
                            $('<td>').text(item.Discount),
                            $('<td>').text(item.Subtotal)
                        );
                        $table.append($tr);
                    });
                    $.each(billFooterTable, function(i, item) {
                        var $tr = $('<tr style="border-top:1px solid black">').append(
                            $('<th>').text(item.Item),
                            $('<th>').text(item.UniqueBarcode),
                            $('<th>').text(item.QTY),
                            $('<th>').text(item.Discount),
                            $('<th>').text(item.Subtotal)
                        );
                        $table.append($tr);
                    });

                    $table.printThis({
                        // importCSS: false,
                        header: `
                            <h3>#`+res.id+`</h3>
                            <table>
                                <tr>
                                    <td>Store</td>
                                    <td width="20" style="text-align:center">:</td>
                                    <td>`+res.store+`</td>
                                </tr>
                                <tr>
                                    <td>Customer</td>
                                    <td width="20" style="text-align:center">:</td>
                                    <td>`+res.customer+`</td>
                                </tr>                                
                                <tr>
                                    <td>#Table</td>
                                    <td width="20" style="text-align:center">:</td>
                                    <td>`+res.table_number+`</td>
                                </tr>
                                <tr>
                                    <td>Created by</td>
                                    <td width="20" style="text-align:center">:</td>
                                    <td>`+res.created_by+`</td>
                                </tr>
                                <tr>
                                    <td>Created at</td>
                                    <td width="20" style="text-align:center">:</td>
                                    <td>`+res.created_at+`</td>
                                </tr>
                                <tr>
                                    <td>Note</td>
                                    <td width="20" style="text-align:center">:</td>
                                    <td>`+res.note+`</td>
                                </tr>
                            </table>
                        `,
                        afterPrint: function() {
                            location.reload();
                        }
                    });
                }).error(function(res) {
                    console.log(res);
                });
            }
        });
    </script>

    <script>
        var doDeselection;
        function logEvent(eventName) {
            var logList = $("#events ul"),
                newItem = $("<li>", { text: eventName });

            logList.prepend(newItem);
        }

        var dataSourceRemoved = [];
        @if(!empty($itemsExisting))
        dataSourceRemoved = @json($itemsExisting);
        @endif
        var dataSource = @json($items);
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            columnAutoWidth: true,
            allowColumnResizing: true,
            columnResizingMode: 'widget', // or 'nextColumn'
            rowAlternationEnabled: true,
            hoverStateEnabled: true, 
            // showBorders: true,
            grouping: {
                autoExpandAll: false,
                contextMenuEnabled: true
            },
            groupPanel: {
                visible: true
            },       
            searchPanel: {
                visible: true
            },   
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            columnFixing: {
                enabled: true
            },
            // height: 420,            
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 25, 50, 100],
                showInfo: true
            },
            keyExpr: "id",
            columns: [
                {
                    dataField: 'id',
                    visible: false
                },
                {
                    dataField: "featured",
                    caption: "*",
                    allowFiltering: true,
                    allowSorting: true,
                    sortIndex: 0,
                    sortOrder: 'desc',
                    cellTemplate: function (container, options) {
                        if(options.value) {
                            var startDate = moment(options.data.featured.publish_on);
                            var endDate = moment(options.data.featured.publish_off);

                            var testDate = moment();

                            if(testDate.isBetween(startDate, endDate, true)) {
                                $(container).append('<div>').append('<span class="fa fa-star" style="color:blue"></span>');
                            }
                        }
                    }
                },
                {
                    dataField: "image",
                    allowFiltering: false,
                    allowSorting: false,
                    cellTemplate: function (container, options) {
                        // $("<div>")
                        //     .append($("<img>", { "src": options.value }))
                        //     .appendTo(container);
                        if(options.value) {
                            $(container).append('<div>').append('<img height="30" src="'+options.value+'">');
                        }
                    }
                },
                "name",
                "code",
                // "unique_barcode",
                // "barcode",
                {
                    dataField: "barcode",
                    calculateCellValue: function(data) {
                        return (data.barcode ? data.barcode : '') + (data.unique_barcode ? ' (' + data.unique_barcode + ')' : '');
                    }
                },
                // "unit",
                // "wrap",
                // "store",
                {
                    dataField: "sell_price",
                    caption: "price",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                "category",
                // {
                //     dataField: "tax",
                //     format: {
                //         type: "fixedPoint",
                //         precision: 0
                //     }
                // },
                // "galery",
            ],
            editing: false,
            onRowClick: function(e) {
                if(e.rowType === "data") {
                    // e.component.editRow(e.rowIndex);
                    var isAmountReadonly = (e.data.is_many_unique_barcode == 'yes' || (e.data.barcode_status && e.data.barcode_status != 'ready')) ? 'readonly' : 'null';

                    if(e.data.stock) {
                        $('.shopping-cart').append(`
                            <div class="col-sm-12 record" style="line-height:34px">
                                <div style="margin-left:-15px;min-width:400px">
                                    <input class="item_id" type="hidden" name="item[]" value="`+e.data.id+`">
                                    <input type="hidden" name="item_name[]" value="`+e.data.name+`">
                                    <input class="unique_barcode" type="hidden" name="unique_barcode[]" value="`+e.data.unique_barcode+`">
                                    <input type="hidden" name="item_price[]" value="`+e.data.sell_price+`">
                                    <input class="discount_subtotal" type="hidden" name="discount_subtotal[]" value="0">
                                    <input class="price_subtotal" type="hidden" name="price_subtotal[]" value="`+e.data.sell_price+`">
                                    <div class="col-xs-3 item_name">`+e.data.name+(e.data.unique_barcode ? ' (' + e.data.unique_barcode + ')' : '')+`</div>
                                    <div class="col-xs-2"><input class="item_amount" type="number" name="item_amount[]" min="1" max="`+e.data.stock+`" value="1" required `+isAmountReadonly+` style="width:100%;height:34px;text-align:center"></div>
                                    <div class="col-xs-2 item_price" style="text-align:right">`+e.data.sell_price+`</div>
                                    <div class="col-xs-2"><input class="item_discount" type="number" name="item_discount[]" min="0" max="`+e.data.sell_price+`" value="0" required style="width:100%;height:34px;text-align:center"></div>
                                    <div class="col-xs-2 price_subtotal_display" style="text-align:right">`+$.number(e.data.sell_price, 0)+`</div>
                                    <div class="col-xs-1" style="text-align:right"><span class="btn btn-warning btn-remove-item"><i class="fa fa-trash"></i></span></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        `);

                        sellCount();

                        getPromo($('.shopping-cart').find('.record:last-child').find('.item_name'), sellCount);
                        
                        var dsRemoved = dataSource.filter(x => x.unique_barcode == e.data.unique_barcode);
                        if(dsRemoved.length > 0) {
                            dataSourceRemoved.push(dsRemoved[0]);
                        }
                        if(e.data.unique_barcode != 'null' && e.data.unique_barcode) {
                            dataSource = dataSource.filter(x => x.unique_barcode != e.data.unique_barcode);
                        } else {
                            dataSource = dataSource.filter(x => x.id != e.data.id);
                        }
                        $("#gridContainer").dxDataGrid({ dataSource: dataSource });
                    } else {
                        alert('Stok kosong!');
                    }
                }
            },
            onRowPrepared: function (e) {
                if(e.rowType === "data") {
                    // if(!e.data.stock || (e.data.barcode_status && e.data.barcode_status != 'ready')) {
                    if(!e.data.stock) {
                        e.rowElement.css('color', '#ccc');
                    }
                    // if(e.data.featured) {
                    //     e.rowElement.css('background', 'yellow');
                    // }
                }
            },
            // onCellPrepared: function (e) {
            //     // if (e.rowType === "data" && e.column.command === "edit") {
            //     //     e.cellElement.prepend('&nbsp;');
            //     //     $('<a/>').addClass('dx-link')
            //     //         .text('Edit')
            //     //         .on('dxclick', function () {
            //     //             window.location = '{{url("sellItems")}}/'+e.row.data.id+'/edit'
            //     //         })
            //     //         .prependTo(e.cellElement);
            //     // }
            // },
            // onRowRemoved: function(e) {
            //     $.ajaxSetup({
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     });
            //     $.ajax({
            //         url: '{{ url()->current() }}/'+JSON.stringify(e.data.id),
            //         type: 'delete',
            //         method: 'POST'
            //     }).success(function(res) {
            //         location = '{{ url()->current() }}';
            //     }).error(function(res) {
            //         location = '{{ url()->current() }}';
            //     });
                
            //     logEvent("RowRemoved");
            // },
        });
        
        // function OnClick() {
        //     var arrId = $("#gridContainer").dxDataGrid("instance").getSelectedRowKeys();
        //     console.log('id', arrId);
        // }
    </script>

<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
