<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sell->id !!}</p>
</div>

<!-- Customer Field -->
<div class="form-group">
    {!! Form::label('customer', 'Customer:') !!}
    <p>{!! $sell->customer !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $sell->store !!}</p>
</div>

<!-- Bill Amount Field -->
<div class="form-group">
    {!! Form::label('bill_amount', 'Bill Amount:') !!}
    <p>{!! $sell->bill_amount !!}</p>
</div>

<!-- Table Number Field -->
<div class="form-group">
    {!! Form::label('table_number', 'Table Number:') !!}
    <p>{!! $sell->table_number !!}</p>
</div>

<!-- Table Order Status Field -->
<div class="form-group">
    {!! Form::label('table_order_status', 'Table Order Status:') !!}
    <p>{!! $sell->table_order_status !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $sell->payment_method !!}</p>
</div>

<!-- Payment Status Field -->
<div class="form-group">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    <p>{!! $sell->payment_status !!}</p>
</div>

<!-- Selling Type Field -->
<div class="form-group">
    {!! Form::label('selling_type', 'Selling Type:') !!}
    <p>{!! $sell->selling_type !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $sell->note !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $sell->balance !!}</p>
</div>

<!-- Is Draft Field -->
<div class="form-group">
    {!! Form::label('is_draft', 'Is Draft:') !!}
    <p>{!! $sell->is_draft !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $sell->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $sell->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sell->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sell->updated_at !!}</p>
</div>

