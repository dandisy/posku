@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Sell
        </h1>

        {{--@include('sells.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sell, ['route' => ['sells.update', $sell->id], 'method' => 'patch', 'id' => 'sell-form']) !!}

                        @include('sells.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>

<!-- Modal -->
<div class="modal fade" id="myAddCustomerModal" tabindex="-1" role="dialog" aria-labelledby="myAddCustomerModalLabel">
    <div class="modal-dialog" role="document">
        <form class="modal-content" action="{!! route('customers.store') !!}" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myAddCustomerModalLabel">Add Customer</h4>
            </div>
            <div class="modal-body">
                <!-- Name Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Description Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('description', 'Description:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Phone Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('phone', 'Phone:') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Email Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Address Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('address', 'Address:') !!}
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Image Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('image', 'Image:') !!}
                    <div id="image-thumb">
                        <img src="https://via.placeholder.com/600x150?text=Image" style="width:100%">
                    </div>
                    <div class="input-group">
                        {!! Form::text('image', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        <div class="input-group-btn">
                            <a href="{!! url('assets/dialog?filter=all&appendId=image') !!}" class="btn btn-primary filemanager fancybox.iframe" data-fancybox-type="iframe">Choose File</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection