@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Image
        </h1>

        {{--@include('images.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($image, ['route' => ['images.update', $image->id], 'method' => 'patch']) !!}

                        @include('images.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection