<table class="table table-responsive" id="balance-table">
    <thead>
        <tr>
            <th>Aktiva</th>
            <th>Saldo</th>
            <th>Pasiva</th>
            <th>Saldo</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Kas</th>
            <td>{!! number_format($cashValue) !!}</td>
            <th>Utang</th>
            <td>{!! number_format($debtValue) !!}</td>
        </tr>
        <tr>
            <th>Persediaan</th>
            <td>{!! number_format($stockValue) !!}</td>
            <th>Modal</th>
            <td>{!! number_format($capital) !!}</td>
        </tr>
        <tr>
            <th>Saldo Keseluruhan</th>
            <th>{!! number_format($cashValue + $stockValue) !!}</th>
            <th>Saldo Keseluruhan</th>
            <th>{!! number_format($debtValue + $capital) !!}</th>
        </tr>
    </tbody>
</table>