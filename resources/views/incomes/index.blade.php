@extends('layouts.app')

@section('contents')    
    <section class="content-header">
        <h1 class="pull-left">Income</h1>
        <div class="pull-right">
            <span class="fa fa-print btn-print" style="cursor:pointer"></span>
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('incomes.table')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('vendor/printThis/printThis.js') }}"></script>

<script>
    $('.btn-print').on('click', function() {
        $('table').printThis({
            // importCSS: false,
            header: '<h3>Income</h3>',
            // afterPrint: function() {
            //     location.reload();
            // }
        });
    });
</script>
@endsection

