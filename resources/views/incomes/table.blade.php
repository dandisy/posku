<table class="table table-responsive" id="balance-table">
    <tr>
        <th>Nilai Penjualan</th>
        <td>{!! number_format($sellingSellPriceValue - $sellingPurchasePriceValue) !!}</td>
    </tr>
    <tr>
        <th>Pengeluaran</th>
        <td>{!! number_format($costValue) !!}</td>
    </tr>
    <tr>
        <th>Laba/Rugi</th>
        <th>{!! number_format(($sellingSellPriceValue - $sellingPurchasePriceValue) - $costValue) !!}</th>
    </tr>
</table>