@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/dxDataGrid/css/dx.common.css') }}" />
    <link rel="dx-theme" data-theme="generic.light" href="{{ asset('vendor/dxDataGrid/css/dx.light.css') }}" />
    <style>
        .dx-datagrid .dx-data-row > td.bullet {
            padding-top: 0;
            padding-bottom: 0;
        }
        .dx-datagrid-content .dx-datagrid-table .dx-row .dx-command-edit {
            width: auto;
            min-width: 140px;
        }
    </style>

    
<style>
    #QR-Code .thumbnail {
        border: 0;
        padding: 0;
    }

    #QR-Code .thumbnail .caption {
        padding: 0;
    }

    #webcodecam-canvas, #scanned-img {
        background-color: #2d2d2d;
    }

    #camera-select {
        display: inline-block;
        width: auto;
    }

    #QR-Code .well {
        position: relative;
        display: inline-block;
        padding: 0;
        background-color: transparent;
        box-shadow: none;
        border: none;
        border-radius: 0;
    }

    .scanner-laser {
        position: absolute;
        margin: 40px;
        height: 30px;
        width: 30px;
        opacity: 0.5;
    }

    .laser-leftTop {
        top: 0;
        left: 0;
        border-top: solid red 5px;
        border-left: solid red 5px;
    }

    .laser-leftBottom {
        bottom: 0;
        left: 0;
        border-bottom: solid red 5px;
        border-left: solid red 5px;
    }

    .laser-rightTop {
        top: 0;
        right: 0;
        border-top: solid red 5px;
        border-right: solid red 5px;
    }

    .laser-rightBottom {
        bottom: 0;
        right: 0;
        border-bottom: solid red 5px;
        border-right: solid red 5px;
    }

    #webcodecam-canvas {
        background-color: #272822;
    }
    
    #scanned-QR {
        word-break: break-word;
    }
</style>
@endsection

<!-- Store Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store', 'Store:') !!}
    {!! Form::select('store', $store->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<div class="clearfix"></div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category', $category->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Unit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unit', 'Unit:') !!}
    {!! Form::select('unit', $unit->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Wrap Field -->
<div class="form-group col-sm-6">
    {!! Form::label('wrap', 'Wrap:') !!}
    {!! Form::select('wrap', $wrap->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Purchase Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('purchase_price', 'Purchase Price:') !!}
    {!! Form::text('purchase_price', null, ['class' => 'form-control thousand']) !!}
</div>

<!-- Sell Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sell_price', 'Sell Price:') !!}
    {!! Form::text('sell_price', null, ['class' => 'form-control thousand']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax', 'Tax:') !!}
    {!! Form::text('tax', null, ['class' => 'form-control thousand']) !!}
</div>

<!-- Minimal Stock Field -->
<div class="form-group col-sm-6">
    {!! Form::label('minimal_stock', 'Minimal Stock:') !!}
    {!! Form::text('minimal_stock', null, ['class' => 'form-control thousand']) !!}
</div>

<!-- Is Many Unique Barcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_many_unique_barcode', 'Is Many Unique Barcode:') !!}
    {!! Form::checkbox('is_many_unique_barcode', 'yes') !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    <div id="image-thumb">
        <img src="https://via.placeholder.com/600x150?text=Image" style="width:100%">
    </div>
    <div class="input-group">
        {!! Form::text('image', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
        <div class="input-group-btn">
            <a href="{!! url('assets/dialog?filter=all&appendId=image') !!}" class="btn btn-primary filemanager fancybox.iframe" data-fancybox-type="iframe">Choose File</a>
        </div>
    </div>
</div>

<!-- Gallery Field -->
<div class="form-group album-manager col-sm-12">
    {!! Form::label('album', 'Gallery:') !!}
    <div class="input-group">
        {!! Form::text('album', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
        <div class="input-group-btn">
            <a href="{!! url('assets/dialog?filter=all&appendId=album') !!}" class="btn btn-primary filemanager fancybox.iframe" data-fancybox-type="iframe">Add File</a>
        </div>
    </div>
    <div id="album-thumb">
        @if(@$album)
            @foreach($album as $item)
                <div class="file-item">
                    <div class="col-md-1 col-sm-2 col-xs-3"><img src="{{ $item->file }}" style="width:100%"></div>
                    <div class="col-md-10 col-sm-9 col-xs-8" style="overflow-x:auto">{{ $item->file }}</div>
                    <div class="col-md-1 col-sm-1 col-xs-1 text-right"><span class="fa fa-trash" style="cursor:pointer;color:red" data-identity="{{ $item->id }}"></span></div>
                    <div class="clearfix"></div>
                </div>
            @endforeach  
        @endif
    </div>
</div>
<div class="clearfix"></div>

<!-- Barcode Field -->
<div class="form-group col-sm-12">
    {!! Form::label('barcode', 'Barcode:') !!}
    <!-- {{--{!! Form::text('barcode', null, ['class' => 'form-control']) !!}--}} -->
    <div class="input-group">
        {!! Form::text('barcode', null, ['class' => 'form-control', 'id' => 'barcode']) !!}
        <div class="input-group-btn">
            <span type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModalQRCode"><i class="fa fa-barcode"></i> Scan</span>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('items.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script src="{{ asset('vendor/dxDataGrid/js/jszip/3.1.5/jszip.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/20.1.6/dx.all.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/babel-polyfill/7.4.0/polyfill.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/exceljs/3.3.1/exceljs.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/FileSaver.js/1.3.8/FileSaver.min.js') }}"></script>

    
<script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/filereader.js') }}"></script>
<!-- Using jquery version: -->

    <!-- <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/jquery.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/qrcodelib.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/webcodecamjquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/mainjquery.js') }}"></script>

<!-- <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/qrcodelib.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/webcodecamjs.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/main.js') }}"></script> -->

    <script>
        var dataSource = @json($itemBarcodes);

        function updateDataGrid(data) {
            if(data) {
                dataSource.push(data);
            }
            
            $("#gridContainer").dxDataGrid({ dataSource: dataSource });
        }
    </script>

    <script>
        var doDeselection;
        function logEvent(eventName) {
            var logList = $("#events ul"),
                newItem = $("<li>", { text: eventName });

            logList.prepend(newItem);
        }

        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            columnAutoWidth: true,
            allowColumnResizing: true,
            columnResizingMode: 'widget', // or 'nextColumn'
            // rowAlternationEnabled: true,
            allowColumnReordering: true,
            columnChooser: {
                enabled: true,
                mode: "dragAndDrop" // or "select"
            },
            hoverStateEnabled: true, 
            // showBorders: true,
            // selection: {
            //     mode: 'multiple'
            // },
            export: {
                enabled: true,
                // allowExportSelectedData: true,
                // fileName: 'items',
            },
            grouping: {
                autoExpandAll: false,
                contextMenuEnabled: true
            },
            groupPanel: {
                visible: true
            },       
            searchPanel: {
                visible: true
            },   
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            columnFixing: {
                enabled: true
            },
            // height: 420,            
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 50, 100],
                showInfo: true
            },
            keyExpr: "id",
            columns: [
                {  
                    caption: '#',  
                    cellTemplate: function(cellElement, cellInfo) {  
                        cellElement.text(cellInfo.row.rowIndex + 1);
                    }  
                },
                {
                    dataField: 'id',
                    visible: false
                },
                "barcode"
            ],
            editing: {
                mode: "row",
                // allowUpdating: true,
                allowDeleting: function(e) {
                    return true;
                },
                // allowAdding: true
            },
            onExporting: e => {
                var workbook = new ExcelJS.Workbook();    
                var worksheet = workbook.addWorksheet('Main sheet');
                var PromiseArray = [];
                
                DevExpress.excelExporter.exportDataGrid({
                    component: e.component,
                    worksheet: worksheet,
                    autoFilterEnabled: true,
                    customizeCell: (options) => {
                        var { excelCell, gridCell } = options;
                        if(gridCell.rowType === "data") {
                            if(gridCell.column.dataField === "image") {
                                excelCell.value = undefined;
                                PromiseArray.push(
                                    new Promise((resolve, reject) => {
                                        addImage(gridCell.value, workbook, worksheet, excelCell, resolve); 
                                    })
                                );
                            }
                        }

                    }
                }).then(function() {
                    Promise.all(PromiseArray).then(() => {
                        workbook.xlsx.writeBuffer().then(function (buffer) {
                            saveAs(new Blob([buffer], { type: "application/octet-stream" }), "barcodes.xlsx");
                        });
                    });
                });
                e.cancel = true;
            },
            onRowRemoved: function(e) {
                $('.data-barcode').find('.rowkey'+e.data.id).remove();
                
                // logEvent("RowRemoved");
            },
        });
        
        // function OnClick() {
        //     var arrId = $("#gridContainer").dxDataGrid("instance").getSelectedRowKeys();
        //     console.log('id', arrId);
        // }
    </script>

<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
