<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $item->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $item->name !!}</p>
</div>

<!-- Category Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $item->category !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $item->code !!}</p>
</div>

<!-- Barcode Field -->
<div class="form-group">
    {!! Form::label('barcode', 'Barcode:') !!}
    <p>{!! $item->barcode !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $item->description !!}</p>
</div>

<!-- Unit Field -->
<div class="form-group">
    {!! Form::label('unit', 'Unit:') !!}
    <p>{!! $item->unit !!}</p>
</div>

<!-- Wrap Field -->
<div class="form-group">
    {!! Form::label('wrap', 'Wrap:') !!}
    <p>{!! $item->wrap !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $item->store !!}</p>
</div>

<!-- Purchase Price Field -->
<div class="form-group">
    {!! Form::label('purchase_price', 'Purchase Price:') !!}
    <p>{!! $item->purchase_price !!}</p>
</div>

<!-- Sell Price Field -->
<div class="form-group">
    {!! Form::label('sell_price', 'Sell Price:') !!}
    <p>{!! $item->sell_price !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $item->tax !!}</p>
</div>

<!-- Minimal Stock Field -->
<div class="form-group">
    {!! Form::label('minimal_stock', 'Minimal Stock:') !!}
    <p>{!! $item->minimal_stock !!}</p>
</div>

<!-- Is Many Unique Barcode Field -->
<div class="form-group">
    {!! Form::label('is_many_unique_barcode', 'Is Many Unique Barcode:') !!}
    <p>{!! $item->is_many_unique_barcode !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $item->image !!}</p>
</div>

<!-- Gallery Field -->
<div class="form-group">
    {!! Form::label('gallery', 'Gallery:') !!}
    <p>{!! $item->gallery !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $item->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $item->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $item->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $item->updated_at !!}</p>
</div>

