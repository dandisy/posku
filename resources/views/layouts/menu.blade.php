<li class="{{ Request::is('dashboard*') ? 'active' : '' }}">
    <a href="{{ url('/home') }}"><i class="fa fa-home"></i><span>Home</span></a>
</li>

@if(
    Request::is('sells*') or
    Request::is('purchases*') or
    Request::is('costs*') or
    Request::is('sellReturns*') or
    Request::is('purchaseReturns*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>TRANSAKSI</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@can('sells.index')
<li class="{{ Request::is('sells*') ? 'active' : '' }}">
    <a href="{!! route('sells.index') !!}"><i class="fa fa-money"></i><span>Penjualan</span></a>
</li>
@endrole

@can('purchases.index')
<li class="{{ Request::is('purchases*') ? 'active' : '' }}">
    <a href="{!! route('purchases.index') !!}"><i class="fa fa-cubes"></i><span>Pembelian</span></a>
</li>
@endrole

@can('costs.index')
<li class="{{ Request::is('costs*') ? 'active' : '' }}">
    <a href="{!! route('costs.index') !!}"><i class="fa fa-dollar"></i><span>Biaya</span></a>
</li>
@endrole

@can('sellReturns.index')
<li class="{{ Request::is('sellReturns*') ? 'active' : '' }}">
    <a href="{!! route('sellReturns.index') !!}"><i class="fa fa-level-up"></i><span>Return Penjualan</span></a>
</li>
@endrole

@can('purchaseReturns.index')
<li class="{{ Request::is('purchaseReturns*') ? 'active' : '' }}">
    <a href="{!! route('purchaseReturns.index') !!}"><i class="fa fa-level-down"></i><span>Return Pembelian</span></a>
</li>
@endrole
@endif

@if(
    Request::is('cashes*') or
    Request::is('trusts*') or
    Request::is('debts*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>AKUN</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@can('cashes.index')
<li class="{{ Request::is('cashes*') ? 'active' : '' }}">
    <a href="{!! route('cashes.index') !!}"><i class="fa fa-money"></i><span>Kas</span></a>
</li>
@endrole

@can('trusts.index')
<li class="{{ Request::is('trusts*') ? 'active' : '' }}">
    <a href="{!! route('trusts.index') !!}"><i class="fa fa-sticky-note"></i><span>Utang</span></a>
</li>
@endrole

@can('debts.index')
<li class="{{ Request::is('debts*') ? 'active' : '' }}">
    <a href="{!! route('debts.index') !!}"><i class="fa fa-dollar"></i><span>Piutang</span></a>
</li>
@endrole
@endif

@if(
    Request::is('balance*') or
    Request::is('income*') or
    Request::is('recaps*') or
    Request::is('logs*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>LAPORAN</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@role('admin')
<li class="{{ Request::is('balance*') ? 'active' : '' }}">
    <a href="{!! url('balance') !!}"><i class="fa fa-balance-scale"></i><span>Neraca</span></a>
</li>

<li class="{{ Request::is('income*') ? 'active' : '' }}">
    <a href="{!! url('income') !!}"><i class="fa fa-sort"></i><span>Laba/Rugi</span></a>
</li>
@endrole

@can('recaps.index')
<li class="{{ Request::is('recaps*') ? 'active' : '' }}">
    <a href="{!! route('recaps.index') !!}"><i class="fa fa-file-text"></i><span>Rekap</span></a>
</li>
@endcan

@can('logs.index')
<li class="{{ Request::is('logs*') ? 'active' : '' }}">
    <a href="{!! route('logs.index') !!}"><i class="fa fa-history"></i><span>Log</span></a>
</li>
@endcan
@endif

@if(
    Request::is('categories*') or
    Request::is('customers*') or
    Request::is('suppliers*') or
    Request::is('paymentMethods*') or
    Request::is('units*') or
    Request::is('wraps*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>DATA</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@can('categories.index')
<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-tag"></i><span>Kategori</span></a>
</li>
@endcan

@can('customers.index')
<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{!! route('customers.index') !!}"><i class="fa fa-users"></i><span>Pelanggan</span></a>
</li>
@endcan

@can('suppliers.index')
<li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
    <a href="{!! route('suppliers.index') !!}"><i class="fa fa-institution"></i><span>Pemasok</span></a>
</li>
@endcan

@can('paymentMethods.index')
<li class="{{ Request::is('paymentMethods*') ? 'active' : '' }}">
    <a href="{!! route('paymentMethods.index') !!}"><i class="fa fa-dollar"></i><span>Metode Pembayaran</span></a>
</li>
@endcan

@can('units.index')
<li class="{{ Request::is('units*') ? 'active' : '' }}">
    <a href="{!! route('units.index') !!}"><i class="fa fa-circle"></i><span>Satuan</span></a>
</li>
@endcan

@can('wrap.index')
<li class="{{ Request::is('wraps*') ? 'active' : '' }}">
    <a href="{!! route('wraps.index') !!}"><i class="fa fa-square"></i><span>Kemasan</span></a>
</li>
@endcan
@endif

@if(
    Request::is('stores*') or
    Request::is('stocks*') or
    Request::is('storeFrontStocks*') or
    Request::is('storeFronts*') or
    Request::is('compositeStocks*') or
    Request::is('items*') or
    Request::is('barcodes*') or
    Request::is('featuredProducts*') or
    Request::is('composites*') or
    Request::is('stockTransfers*') or
    Request::is('rejectedItems*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>Inventori</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@can('stores.index')
<li class="{{ Request::is('stores*') ? 'active' : '' }}">
    <a href="{!! route('stores.index') !!}"><i class="fa fa-university"></i><span>Toko</span></a>
</li>
@endcan

@can('stocks.index')
<li class="{{ Request::is('stocks*') ? 'active' : '' }}">
    <a href="{!! route('stocks.index') !!}"><i class="fa fa-sort"></i><span>Stok Gudang</span></a>
</li>
@endcan

@can('storeFrontStocks.index')
<li class="{{ Request::is('storeFrontStocks*') ? 'active' : '' }}">
    <a href="{!! route('storeFrontStocks.index') !!}"><i class="fa fa-sort"></i><span>Stok Etalase</span></a>
</li>
@endcan

@can('storeFronts.index')
<li class="{{ Request::is('storeFronts*') ? 'active' : '' }}">
    <a href="{!! route('storeFronts.index') !!}"><i class="fa fa-eye"></i><span>Atur Etalase</span></a>
</li>
@endcan

@can('compositeStocks.index')
<li class="{{ Request::is('compositeStocks*') ? 'active' : '' }}">
    <a href="{!! route('compositeStocks.index') !!}"><i class="fa fa-cubes"></i><span>Atur Resep/Paket</span></a>
</li>
@endcan

@can('stockTransfers.index')
<li class="{{ Request::is('stockTransfers*') ? 'active' : '' }}">
    <a href="{!! route('stockTransfers.index') !!}"><i class="fa fa-retweet"></i><span>Transfer Stok</span></a>
</li>
@endcan

@can('rejectedItems.index')
<li class="{{ Request::is('rejectedItems*') ? 'active' : '' }}">
    <a href="{!! route('rejectedItems.index') !!}"><i class="fa fa-ban"></i><span>Item Reject</span></a>
</li>
@endcan

@can('items.index')
<li class="{{ Request::is('items*') ? 'active' : '' }}">
    <a href="{!! route('items.index') !!}"><i class="fa fa-cube"></i><span>Produk</span></a>
</li>
@endcan

@can('barcodes.index')
<li class="{{ Request::is('barcodes*') ? 'active' : '' }}">
    <a href="{!! route('barcodes.index') !!}"><i class="fa fa-square"></i><span>Barcodes</span></a>
</li>
@endcan

@can('featuredProducts.index')
<li class="{{ Request::is('featuredProducts*') ? 'active' : '' }}">
    <a href="{!! route('featuredProducts.index') !!}"><i class="fa fa-star"></i><span>Produk Unggulan</span></a>
</li>
@endcan

@can('composites.index')
<li class="{{ Request::is('composites*') ? 'active' : '' }}">
    <a href="{!! route('composites.index') !!}"><i class="fa fa-flask"></i><span>Resep / Paket</span></a>
</li>
@endcan
@endif

@if(
    Request::is('productPromos*') or
    Request::is('promoConditions*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>Diskon</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@can('productPromos.index')
<li class="{{ Request::is('productPromos*') ? 'active' : '' }}">
    <a href="{!! route('productPromos.index') !!}"><i class="fa fa-scissors"></i><span>Promo</span></a>
</li>
@endcan

@can('promoConditions.index')
<li class="{{ Request::is('promoConditions*') ? 'active' : '' }}">
    <a href="{!! route('promoConditions.index') !!}"><i class="fa fa-filter"></i><span>Kondisi</span></a>
</li>
@endcan
@endif

@if(
    Request::is('attendances*') or
    Request::is('users*') or
    Request::is('roles*') or
    Request::is('accessControl*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>Karyawan</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

@can('attendances.index')
<li class="{{ Request::is('attendances*') ? 'active' : '' }}">
    <a href="{!! route('attendances.index') !!}"><i class="fa fa-street-view"></i><span>Abensi</span></a>
</li>
@endcan

@role('admin')
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>User</span></a>
</li>
@endrole

@role('admin')
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! url('roles') !!}"><i class="fa fa-shield"></i><span>Roles</span></a>
</li>
@endrole

<!-- {{--<li class="{{ Request::is('permissions*') ? 'active' : '' }}">
    <a href="{!! url('permissions') !!}"><i class="fa fa-shield"></i><span>Permissions</span></a>
</li>--}} -->

@role('admin')
<li class="{{ Request::is('accessControl*') ? 'active' : '' }}">
    <a href="{!! url('accessControl') !!}"><i class="fa fa-shield"></i><span>Atur Hak Akses</span></a>
</li>
@endrole
@endif

@if(
    Request::is('mails*') or
    Request::is('campaigns*') or
    Request::is('settings*') or
    Request::is('about*')
)
<!-- <li><hr class="light-grey-hr mb-10"/></li> -->
<li class="header navigation-header">
    <span>SISTEM</span> 
    <!-- <i class="zmdi zmdi-more"></i> -->
</li>

<li class="{{ Request::is('mails*') ? 'active' : '' }}">
    <a href="javascript:void(0)"><i class="fa fa-envelope"></i><span>Pesan (*Soon)</span></a>
</li>

<li class="{{ Request::is('campaigns*') ? 'active' : '' }}">
    <a href="javascript:void(0)"><i class="fa fa-bullhorn"></i><span>Pemasaran (*Soon)</span></a>
</li>

@can('settings.index')
<li class="{{ Request::is('settings*') ? 'active' : '' }}">
    <a href="{!! route('settings.index') !!}"><i class="fa fa-cog"></i><span>Pengaturan</span></a>
</li>
@endcan

<li class="{{ Request::is('info*') ? 'active' : '' }}">
    <a href="javascript:void(0)"><i class="fa fa-question"></i><span>Bantuan</span></a>
</li>
@endif

<!-- {{--<li class="{{ Request::is('sellItems*') ? 'active' : '' }}">
    <a href="{!! route('sellItems.index') !!}"><i class="fa fa-edit"></i><span>Sell Items</span></a>
</li>

<li class="{{ Request::is('sellReturnItems*') ? 'active' : '' }}">
    <a href="{!! route('sellReturnItems.index') !!}"><i class="fa fa-edit"></i><span>Sell Return Items</span></a>
</li>

<li class="{{ Request::is('purchaseItems*') ? 'active' : '' }}">
    <a href="{!! route('purchaseItems.index') !!}"><i class="fa fa-edit"></i><span>Purchase Items</span></a>
</li>

<li class="{{ Request::is('purchaseReturnItems*') ? 'active' : '' }}">
    <a href="{!! route('purchaseReturnItems.index') !!}"><i class="fa fa-edit"></i><span>Purchase Return Items</span></a>
</li>

<li class="{{ Request::is('productPromoConditions*') ? 'active' : '' }}">
    <a href="{!! route('productPromoConditions.index') !!}"><i class="fa fa-edit"></i><span>Product Promo Conditions</span></a>
</li>

<li class="{{ Request::is('compositeItems*') ? 'active' : '' }}">
    <a href="{!! route('compositeItems.index') !!}"><i class="fa fa-edit"></i><span>Composite Items</span></a>
</li>

<li class="{{ Request::is('storeFrontItems*') ? 'active' : '' }}">
    <a href="{!! route('storeFrontItems.index') !!}"><i class="fa fa-edit"></i><span>Store Front Items</span></a>
</li>

<li class="{{ Request::is('images*') ? 'active' : '' }}">
    <a href="{!! route('images.index') !!}"><i class="fa fa-edit"></i><span>Images</span></a>
</li>--}} -->

