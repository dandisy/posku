@extends('layouts.app')

@section('contents')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @include('flash::message')
        </div>
    </div>
    <h1>
        Home
        <small>Menu</small>
    </h1>
    {{--<ol class="breadcrumb">
        <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Main</li>
    </ol>--}}
</section>

<!-- Main content -->
<section class="content main-menu-wrapper">
    <!-- Info boxes -->
    <div class="row">
        <!-- {{--@role('superadministrator|administrator|user')--}} -->
        <!-- {{--@endrole--}} -->
        {{--<div class="col-md-1 col-sm-6 col-xs-12">
            <div class="main-menu icon-menu" style="margin:0">
                <a class="info-box" href="{{ url('dashboard') }}" style="display: block">
                    <span class="info-box-icon bg-orange"><i class="fa fa-dashboard"></i></span>

                    <div class="text-center">Dashboard</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>--}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <p>
                <strong>Cash Flow</strong>
            </p>
            <div class="info-box">
                <span class="info-box-icon bg-light-gray"><i class="fa fa-money"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Cash Total</span>
                    <span class="info-box-number">{{number_format($cashValue)}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->

            <div class="info-box">
                <span class="info-box-icon bg-light-gray"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sales</span>
                    <span class="info-box-number">{{number_format($sellAttendance)}}/<small>{{number_format($sellValue)}}</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
            <p class="text-center">
                <strong>Goal Completion</strong>
            </p>

            <div class="progress-group">
                <span class="progress-text">Storefront Stock</span>
                <span class="progress-number"><b>{{$storeFrontStockTotal}}({{$storeFrontStockCount}})</b>/{{$stockTotal}}({{$stockItemCount}})</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: {{$stockTotal ? (($storeFrontStockTotal/$stockTotal)*100) : 0}}%"></div>
                </div>
            </div>
            <!-- /.progress-group -->
            <div class="progress-group">
                <span class="progress-text">Stock is Running Out</span>
                <span class="progress-number"><b>{{$stockRunningOut}}</b>/{{$stockItemCount}}</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: {{$stockItemCount ? (($stockRunningOut/$stockItemCount)*100) : 0}}%"></div>
                </div>
            </div>
            <!-- /.progress-group -->
            <div class="progress-group">
                <span class="progress-text">New Customer</span>
                <span class="progress-number"><b>{{$sellNewCustomer}}</b>/{{$sellCount}}</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: {{$sellCount ? (($sellNewCustomer/$sellCount)*100) : 0}}%"></div>
                </div>
            </div>
            <!-- /.progress-group -->
            <div class="progress-group">
                <span class="progress-text">Void</span>
                <span class="progress-number"><b>{{$sellVoid}}</b>/{{$sellCount}}</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: {{$sellCount ? (($sellVoid/$sellCount)*100) : 0}}%"></div>
                </div>
            </div>
            <!-- /.progress-group -->
        </div>
        <!-- /.col -->
        <div class="col-md-5 col-sm-6 col-xs-12">
            <p class="text-center">
                <!-- <strong>Sales: {{\Carbon\Carbon::now()->subMonth()->subMonth()->subMonth()->format('F')}}, {{\Carbon\Carbon::now()->subMonth()->year}} - {{\Carbon\Carbon::now()->subMonth()->format('F')}}, {{\Carbon\Carbon::now()->subMonth()->year}}</strong> -->
                <strong>Sales: {{\Carbon\Carbon::now()->subDay()->subDay()->subDay()->format('F d')}}, {{\Carbon\Carbon::now()->subDay()->year}} - {{\Carbon\Carbon::now()->subDay()->format('F d')}}, {{\Carbon\Carbon::now()->subDay()->year}}</strong>
            </p>

            <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 180px;"></canvas>
            </div>
            <!-- /.chart-responsive -->
        </div>

        <div class="clearfix"></div>
        <div class="text-center">
            @role('admin')
            <a href="{{ url('dashboard') }}" class="btn btn-danger"><i class="fa fa-dashboard"></i> Dashboard</a>
            @endrole
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="icon-menu-wrapper">
            @role('admin')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('balance') }}" style="display: block">
                    <span class="info-box-icon bg-red"><i class="fa fa-bullseye"></i></span>
                    
                    <div class="text-center">Laporan</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endrole
        </div>

        <div class="icon-menu-wrapper">
            @can('sells.create.owned')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('sells/create') }}" style="display: block">
                    <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

                    <div class="text-center">Penjualan</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('purchases.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('purchases') }}" style="display: block">
                    <span class="info-box-icon bg-green"><i class="fa fa-cube"></i></span>

                    <div class="text-center">Pembelian</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            <div class="clearfix"></div>
            @can('costs.create.owned')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('costs/create') }}" style="display: block">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-dollar"></i></span>

                    <div class="text-center">Biaya</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('storeFrontStocks.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('storeFrontStocks') }}" style="display: block">
                    <span class="info-box-icon bg-light-blue"><i class="fa fa-university"></i></span>

                    <div class="text-center">Etalase</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            <div class="clearfix"></div>
            @can('purchaseReturns.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('purchaseReturns') }}" style="display: block">
                    <span class="info-box-icon bg-blue"><i class="fa fa-recycle"></i></span>

                    <div class="text-center">Purchase Return</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('sellReturns.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('sellReturns') }}" style="display: block">
                    <span class="info-box-icon bg-navy"><i class="fa fa-recycle"></i></span>

                    <div class="text-center">Sell Return</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
        </div>

        <div class="icon-menu-wrapper">
            @can('stockTransfers.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('stockTransfers') }}" style="display: block">
                    <span class="info-box-icon bg-maroon"><i class="fa fa-retweet"></i></span>

                    <div class="text-center">Transfer Stok</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('items.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('items') }}" style="display: block">
                    <span class="info-box-icon bg-navy"><i class="fa fa-cubes"></i></span>

                    <div class="text-center">Produk</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            <div class="clearfix"></div>
            <div class="main-menu icon-menu">
            @can('stocks.index')
                <a class="info-box" href="{{ url('stocks') }}" style="display: block">
                    <span class="info-box-icon bg-blue"><i class="fa fa-sort"></i></span>

                    <div class="text-center">Stok</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('productPromos.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('productPromos') }}" style="display: block">
                    <span class="info-box-icon bg-light-blue"><i class="fa fa-scissors"></i></span>

                    <div class="text-center">Promo</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            <div class="clearfix"></div>
            @can('rejectedItems.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('rejectedItems') }}" style="display: block">
                    <span class="info-box-icon bg-orange"><i class="fa fa-ban"></i></span>

                    <div class="text-center">Reject</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
        </div>

        <div class="icon-menu-wrapper">
            @can('cashes.create.owned')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('cashes/create') }}" style="display: block">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>

                    <div class="text-center">Kas</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('logs.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('logs') }}" style="display: block">
                    <span class="info-box-icon bg-black"><i class="fa fa-history"></i></span>

                    <div class="text-center">Riwayat</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
        </div>

        <div class="icon-menu-wrapper">
            @can('attendances.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('attendances') }}" style="display: block">
                    <span class="info-box-icon bg-green"><i class="fa fa-street-view"></i></span>

                    <div class="text-center">Absensi</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="icon-menu-wrapper">
            @role('admin')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('assets') }}" style="display: block">
                    <span class="info-box-icon bg-blue"><i class="fa fa-folder-open"></i></span>

                    <div class="text-center">Media</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endrole
        </div>
        
        <div class="icon-menu-wrapper">
            @can('settings.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('settings') }}" style="display: block">
                    <span class="info-box-icon bg-navy"><i class="fa fa-cog"></i></span>
                    
                    <div class="text-center">Konfigurasi</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
            @can('users.index')
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('users') }}" style="display: block">
                    <span class="info-box-icon bg-orange"><i class="fa fa-user"></i></span>
                    
                    <div class="text-center">User</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @endcan
        </div>
        
        <div class="icon-menu-wrapper">
            <div class="main-menu icon-menu">
                <a class="info-box" href="{{ url('api-docs') }}" target="_blank" style="display: block">
                    <span class="info-box-icon bg-light-blue"><i class="fa fa-question"></i></span>
                    
                    <div class="text-center">Info</div>
                    <!-- /.info-box-content -->
                </a>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('scripts')
    <!-- FastClick -->
    <script src="{{ asset('vendor/adminlte/plugins/fastclick/fastclick.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('vendor/adminlte/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('vendor/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{{ asset('vendor/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="{{ asset('vendor/adminlte/plugins/chartjs/Chart.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="{{ asset('vendor/adminlte/dist/js/pages/dashboard2.js') }}"></script> -->

    <script>
        //-----------------------
        //- MONTHLY SALES CHART -
        //-----------------------

        // Get context with jQuery - using jQuery's .get() method.
        var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: ["{{\Carbon\Carbon::now()->subDay()->subDay()->subDay()->format('d')}}", "{{\Carbon\Carbon::now()->subDay()->subDay()->format('d')}}", "{{\Carbon\Carbon::now()->subDay()->format('d')}}"],
            datasets: [
            {
                label: "Cash",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#3b8bba",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: [{{$sellValueDay_3}}, {{$sellValueDay_2}}, {{$sellValueDay_1}}]
            },
            // labels: ["{{\Carbon\Carbon::now()->subMonth()->subMonth()->subMonth()->format('F')}}", "{{\Carbon\Carbon::now()->subMonth()->subMonth()->format('F')}}", "{{\Carbon\Carbon::now()->subMonth()->format('F')}}"],
            // datasets: [
            // {
            //     label: "Cash",
            //     fillColor: "rgb(210, 214, 222)",
            //     strokeColor: "rgb(210, 214, 222)",
            //     pointColor: "rgb(210, 214, 222)",
            //     pointStrokeColor: "#c1c7d1",
            //     pointHighlightFill: "#fff",
            //     pointHighlightStroke: "rgb(220,220,220)",
            //     data: [{{$sellValueMonth_3}}, {{$sellValueMonth_2}}, {{$sellValueMonth_1}}]
            // },
            // {
            //     label: "Non Cash",
            //     fillColor: "rgba(60,141,188,0.9)",
            //     strokeColor: "rgba(60,141,188,0.8)",
            //     pointColor: "#3b8bba",
            //     pointStrokeColor: "rgba(60,141,188,1)",
            //     pointHighlightFill: "#fff",
            //     pointHighlightStroke: "rgba(60,141,188,1)",
            //     data: [28, 48, 40, 19, 86, 27]
            // }
            ]
        };

        var salesChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: false,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        salesChart.Line(salesChartData, salesChartOptions);
    </script>
@endsection
