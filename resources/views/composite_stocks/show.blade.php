@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Composite Stock
        </h1>
        
        {{--@include('composite_stocks.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('composite_stocks.show_fields')
                    <a href="{!! route('compositeStocks.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
