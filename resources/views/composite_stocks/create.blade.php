@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Composite Stock
        </h1>

        {{--@include('composite_stocks.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'compositeStocks.store']) !!}

                        @include('composite_stocks.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
