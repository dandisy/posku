<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $compositeStock->id !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $compositeStock->store !!}</p>
</div>

<!-- Composite Id Field -->
<div class="form-group">
    {!! Form::label('composite_id', 'Composite Id:') !!}
    <p>{!! $compositeStock->composite_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Composite Item:') !!}
    <p>{!! $compositeStock->item !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $compositeStock->item_amount !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $compositeStock->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $compositeStock->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $compositeStock->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $compositeStock->updated_at !!}</p>
</div>

