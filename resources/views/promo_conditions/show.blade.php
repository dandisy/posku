@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Promo Condition
        </h1>
        
        {{--@include('promo_conditions.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('promo_conditions.show_fields')
                    <a href="{!! route('promoConditions.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
