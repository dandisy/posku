@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Promo Condition
        </h1>

        {{--@include('promo_conditions.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($promoCondition, ['route' => ['promoConditions.update', $promoCondition->id], 'method' => 'patch']) !!}

                        @include('promo_conditions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection