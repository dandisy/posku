<div class="col-sm-6">
    <div class="row">
        {{--<!-- Gallery Field -->
        <div class="form-group album-manager col-sm-12">
            {!! Form::label('gallery', 'Gallery:') !!}
            <div class="input-group">
                {!! Form::text('gallery', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                <div class="input-group-btn">
                    <a href="{!! url('assets/dialog?filter=all&appendId=gallery') !!}" class="btn btn-primary filemanager fancybox.iframe" data-fancybox-type="iframe">Add File</a>
                </div>
            </div>
            <div id="gallery-thumb">
                @if(@$album)
                    @foreach($album as $item)
                        <div class="file-item">
                            <div class="col-md-3 col-sm-3 col-xs-3"><img src="{{ $item->file }}" style="width:100%"></div>
                            <div class="col-md-8" col-sm-8 col-xs-8" style="overflow-x:auto">{{ $item->file }}</div>
                            <div class="col-md-1" col-sm-1 col-xs-1"><span class="fa fa-trash" style="cursor:pointer;color:red" data-identity="{{ $item->id }}"></span></div>
                            <div class="clearfix"></div>
                        </div>
                    @endforeach  
                @endif
            </div>
        </div>
        <div class="clearfix"></div>--}}

        <!-- {{--<div class="form-group col-sm-12">
            {!! Form::label('image', 'Image:') !!}
            <div class="input-group">
                {!! Form::text('image', null, ['class' => 'form-control', 'accept' => 'image/*', 'capture' => 'user']) !!}
                <div class="input-group-btn">
                    <span class="btn btn-primary">Take Photo</span>
                </div>
            </div>
        </div>--}} -->

        <div class="form-group col-sm-12">
            {!! Form::label('image', 'Image:') !!}
            <div id="my_camera"></div>
            <div id="results"></div>
            <input style="margin-top:5px;margin-right:5px" class="btn btn-primary" type=button value="Take Snapshot" onClick="take_snapshot()">
            <input style="margin-top:5px" class="btn btn-default" type=button value="Retake" onClick="retake_snapshot()">
            <input type="hidden" name="image" class="image-tag">
        </div>
        <!-- <div class="col-sm-12">
            <div id="results">Your captured image will appear here...</div>
        </div> -->
    </div>
</div>

<div class="col-sm-6">
    <div class="row">
        <!-- Staff Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('staff', 'Staff:') !!}
            {!! Form::select('staff', $user->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
        </div>

        <!-- Store Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('store', 'Store:') !!}
            {!! Form::select('store', $store->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
        </div>
    </div>
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>
<hr>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('attendances.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
<script src="{{ asset('vendor/webcamjs/1.0.25/webcam.min.js') }}"></script>

<script>
    Webcam.set({
        width: 310,
        height: 233,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
  
    Webcam.attach('#my_camera');
  
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            $('#my_camera').hide();
            $('#results').html('<img src="'+data_uri+'"/>');
        });
    }

    function retake_snapshot() {
        $(".image-tag").val('');
        $('#my_camera').show();
        $('#results').empty();
    }
</script>

<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
