@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/dxDataGrid/css/dx.common.css') }}" />
    <link rel="dx-theme" data-theme="generic.light" href="{{ asset('vendor/dxDataGrid/css/dx.light.css') }}" />
    <style>
        .dx-datagrid .dx-data-row > td.bullet {
            padding-top: 0;
            padding-bottom: 0;
        }
        .dx-datagrid-content .dx-datagrid-table .dx-row .dx-command-edit {
            width: auto;
            min-width: 140px;
        }
    </style>
@endsection

<!-- Purchase Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('purchase_id', 'Purchase Id:') !!}
    {!! Form::select('purchase_id', $purchase->pluck('receipt_code', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Store Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store', 'Store:') !!}
    {!! Form::select('store', $store->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Supplier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier', 'Supplier:') !!}
    {!! Form::select('supplier', $supplier->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Receipt Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receipt_code', 'Receipt Code:') !!}
    {!! Form::text('receipt_code', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>
<hr>

<div class="col-sm-12">
    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalAddItem">
        Add Item
    </button>

    <div class="items-data"></div>
</div>

<div class="col-sm-12">
    <div class="dx-viewport">
        <div class="demo-container">
            <div id="gridContainer"></div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<!-- Receipt Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receipt_amount', 'Receipt Amount:') !!}
    {!! Form::text('receipt_amount', null, ['class' => 'form-control thousand']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>
<hr>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('purchaseReturns.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script src="{{ asset('vendor/dxDataGrid/js/cldr.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/event.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/supplemental.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/unresolved.min.js') }}"></script>	
    <script src="{{ asset('vendor/dxDataGrid/js/globalize.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/message.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/number.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/date.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/currency.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/dx.web.js') }}"></script>

    <script>
        var dataSource = @json($purchaseReturnItems);

        function updateDataGrid(data) {
            if(data) {
                dataSource.push(data);
            }
            
            $("#gridContainer").dxDataGrid({ dataSource: dataSource });
        }
    </script>

    <script>
        $('#myModalAddItem').on('change', '#item', function() {
            $('#myModalAddItem').find('#unique_barcode').val($(this).find('option:selected').data('barcode'));

            if($(this).find('option:selected').data('ismanyuniquebarcode')) {
                $('#myModalAddItem').find('#item_amount').attr('max', 1);
                $('#myModalAddItem').find('#item_amount').val('1');
                $('#myModalAddItem').find('#item_amount').prop('readonly', true);
            } else {
                $('#myModalAddItem').find('#item_amount').removeAttr('max');
                $('#myModalAddItem').find('#item_amount').val(null);
                $('#myModalAddItem').find('#item_amount').prop('readonly', false);
            }
        });
    </script>

    <script>
        var doDeselection;
        function logEvent(eventName) {
            var logList = $("#events ul"),
                newItem = $("<li>", { text: eventName });

            logList.prepend(newItem);
        }

        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            columnAutoWidth: true,
            allowColumnResizing: true,
            columnResizingMode: 'widget', // or 'nextColumn'
            rowAlternationEnabled: true,
            hoverStateEnabled: true, 
            // showBorders: true,
            grouping: {
                autoExpandAll: false,
                contextMenuEnabled: true
            },
            groupPanel: {
                visible: true
            },       
            searchPanel: {
                visible: true
            },   
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            columnFixing: {
                enabled: true
            },
            // height: 420,            
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 50, 100],
                showInfo: true
            },
            keyExpr: "id",
            columns: [
                {  
                    caption: '#',  
                    cellTemplate: function(cellElement, cellInfo) {  
                        cellElement.text(cellInfo.row.rowIndex + 1);
                    }  
                },
                {
                    dataField: 'id',
                    visible: false
                },
                "item",
                "unique_barcode",
                // {
                //     dataField: "item_discount",
                //     format: {
                //         type: "fixedPoint",
                //         precision: 0
                //     }
                // },
                // {
                //     dataField: "item_price",
                //     format: {
                //         type: "fixedPoint",
                //         precision: 0
                //     }
                // },
                {
                    dataField: "item_amount",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                // {
                //     dataField: "discount_subtotal",
                //     format: {
                //         type: "fixedPoint",
                //         precision: 0
                //     }
                // },
                // {
                //     dataField: "price_subtotal",
                //     format: {
                //         type: "fixedPoint",
                //         precision: 0
                //     }
                // },
                // {
                //     dataField: "tax",
                //     format: {
                //         type: "fixedPoint",
                //         precision: 0
                //     }
                // },
                "note",
                "created_by",
                "created_at",
                "updated_by",
                "updated_at"
            ],
            editing: {
                mode: "row",
                // allowUpdating: true,
                allowDeleting: function(e) {
                    return true;
                },
                // allowAdding: true
            },
            onCellPrepared: function (e) {
                if (e.rowType === "data" && e.column.command === "edit") {
                    e.cellElement.prepend('&nbsp;');
                    $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            if(/^\d+$/.test(e.key)) {
                                $('#myModalAddItem').modal('toggle');
                                $('#myModalAddItem').find('#myModalAddItemLabel').text('Edit Item');
                                $('#myModalAddItem').attr('rowkey', e.key);
                                $('#myModalAddItem').attr('rowindex', e.rowIndex);
                                $('#myModalAddItem').find('#purchase_return_id').val(e.row.data.purchase_id);
                                $('#myModalAddItem').find('#unique_barcode').val(e.row.data.unique_barcode);
                                $('#myModalAddItem').find('#item').val(e.row.data.item);
                                $('#myModalAddItem').find('#item_discount').val(e.row.data.item_discount);
                                $('#myModalAddItem').find('#item_price').val(e.row.data.item_price);
                                $('#myModalAddItem').find('#item_amount').val(e.row.data.item_amount);
                                $('#myModalAddItem').find('#discount_subtotal').val(e.row.data.item_discount);
                                $('#myModalAddItem').find('#price_subtotal').val(e.row.data.price_subtotal);
                                $('#myModalAddItem').find('#tax').val(e.row.data.tax);
                                $('#myModalAddItem').find('#item_note').val(e.row.data.note);
                                $('#myModalAddItem').find('#item_note').text(e.row.data.note);
                            } else {
                                $('#myModalAddItem').modal('toggle');
                                $('#myModalAddItem').find('#myModalAddItemLabel').text('Edit Item');
                                $('#myModalAddItem').attr('rowkey', e.key);
                                $('#myModalAddItem').attr('rowindex', e.rowIndex);
                                $('#myModalAddItem').find('#purchase_return_id').val('');
                                $('#myModalAddItem').find('#unique_barcode').val(e.row.data.unique_barcode);
                                $('#myModalAddItem').find('#item').val(e.row.data.item);
                                $('#myModalAddItem').find('#item_discount').val(e.row.data.item_discount);
                                $('#myModalAddItem').find('#item_price').val(e.row.data.item_price);
                                $('#myModalAddItem').find('#item_amount').val(e.row.data.item_amount);
                                $('#myModalAddItem').find('#discount_subtotal').val(e.row.data.item_discount);
                                $('#myModalAddItem').find('#price_subtotal').val(e.row.data.price_subtotal);
                                $('#myModalAddItem').find('#tax').val(e.row.data.tax);
                                $('#myModalAddItem').find('#item_note').val(e.row.data.note);
                                $('#myModalAddItem').find('#item_note').text(e.row.data.note);
                            }
                        })
                        .prependTo(e.cellElement);
                }
            },
            onRowRemoved: function(e) {
                // if(/^\d+$/.test(e.key)) {
                //     $.ajaxSetup({
                //         headers: {
                //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //         }
                //     });
                //     $.ajax({
                //         url: '{{url("purchaseReturnItems")}}/'+JSON.stringify(e.data.id),
                //         type: 'delete'
                //     }).success(function(res) {
                //         $('.items-data').find('.rowkey'+e.key).remove();
                //     }).error(function(res) {
                //         console.log(res);
                //     });
                // } else {
                    $('.items-data').find('.rowkey'+e.key).remove();
                // }
                
                // logEvent("RowRemoved");
            },
        });
        
        // function OnClick() {
        //     var arrId = $("#gridContainer").dxDataGrid("instance").getSelectedRowKeys();
        //     console.log('id', arrId);
        // }
    </script>

    <script>
        $('#myModalAddItem').on('shown.bs.modal', function () {
            $('.select2').select2();
        });
        
        $('#myModalAddItem').on('hide.bs.modal', function () {
            $('#myModalAddItem').attr('rowkey', null);
            $('#myModalAddItem').attr('rowindex', null);
            $('#myModalAddItem').find('#myModalAddItemLabel').text('Add Item');
            $('#myModalAddItem').find('input').val('');
            $('#myModalAddItem').find('textarea').val('');
            $('#myModalAddItem').find('textarea').text('');
            $('#myModalAddItem').find('.select2').val('').trigger('change');
            $('#myModalAddItem').find('#item_amount').removeAttr('max');
            $('#myModalAddItem').find('#item_amount').prop('readonly', false);
        });

        $(document).on('click', '.btn-save-form', function() {
            var input = $(this).parents('form').serializeArray();
            var data = {};

            if($('#myModalAddItem').attr('rowkey')) {
                if($('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).length) {
                    $(input).each(function(i, v) {
                        dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'item_note' ? 'note' : v.name] = v.value;
                        $('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).find('[name="'+v.name+'[]"]').val(v.value);
                    });
                } else {
                    $('.items-data').append('<div/>');
                    $('.items-data > div:last-child').addClass('rowkey'+$('#myModalAddItem').attr('rowkey'));
                    $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="item_id[]" value="'+$('#myModalAddItem').attr('rowkey')+'">');
                    $(input).each(function(i, v) {
                        dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'item_note' ? 'note' : v.name] = v.value;
                        $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value+'">');
                    });
                }
            
                updateDataGrid(null);
            } else {
                if(dataSource.length) {
                    data['id'] = (parseInt(dataSource[dataSource.length-1].id)+1)+'-tmp';
                } else {
                    data['id'] = '1-tmp';
                }
                $('.items-data').append('<div/>');
                $('.items-data > div:last-child').addClass('rowkey'+data['id']);
                $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="item_id[]" value="'+data['id']+'">');
                $(input).each(function(i, v) {
                    data[v.name === 'item_note' ? 'note' : v.name] = v.value;
                    $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value+'">');
                });
            
                updateDataGrid(data);
            }

            $('#myModalAddItem').find('input').val('');
            $('#myModalAddItem').find('textarea').val('');
            $('#myModalAddItem').find('textarea').text('');

            $('#myModalAddItem').modal('toggle');
        });
    </script>

<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
