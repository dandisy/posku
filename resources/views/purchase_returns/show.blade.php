@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Purchase Return
        </h1>
        
        {{--@include('purchase_returns.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('purchase_returns.show_fields')
                    <a href="{!! route('purchaseReturns.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
