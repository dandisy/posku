<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $purchaseReturn->id !!}</p>
</div>

<!-- Purchase Id Field -->
<div class="form-group">
    {!! Form::label('purchase_id', 'Purchase Id:') !!}
    <p>{!! $purchaseReturn->purchase_id !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $purchaseReturn->store !!}</p>
</div>

<!-- Supplier Field -->
<div class="form-group">
    {!! Form::label('supplier', 'Supplier:') !!}
    <p>{!! $purchaseReturn->supplier !!}</p>
</div>

<!-- Receipt Code Field -->
<div class="form-group">
    {!! Form::label('receipt_code', 'Receipt Code:') !!}
    <p>{!! $purchaseReturn->receipt_code !!}</p>
</div>

<!-- Receipt Amount Field -->
<div class="form-group">
    {!! Form::label('receipt_amount', 'Receipt Amount:') !!}
    <p>{!! $purchaseReturn->receipt_amount !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $purchaseReturn->note !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $purchaseReturn->balance !!}</p>
</div>

<!-- Is Draft Field -->
<div class="form-group">
    {!! Form::label('is_draft', 'Is Draft:') !!}
    <p>{!! $purchaseReturn->is_draft !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $purchaseReturn->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $purchaseReturn->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $purchaseReturn->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $purchaseReturn->updated_at !!}</p>
</div>

