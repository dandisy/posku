@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Recap
        </h1>
        
        {{--@include('recaps.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('recaps.show_fields')
                    <a href="{!! route('recaps.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
