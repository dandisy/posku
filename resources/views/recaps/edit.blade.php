@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Recap
        </h1>

        {{--@include('recaps.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($recap, ['route' => ['recaps.update', $recap->id], 'method' => 'patch']) !!}

                        @include('recaps.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection