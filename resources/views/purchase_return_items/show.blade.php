@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Purchase Return Item
        </h1>
        
        {{--@include('purchase_return_items.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('purchase_return_items.show_fields')
                    <a href="{!! route('purchaseReturnItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
