@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Purchase Return Item
        </h1>

        {{--@include('purchase_return_items.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($purchaseReturnItem, ['route' => ['purchaseReturnItems.update', $purchaseReturnItem->id], 'method' => 'patch']) !!}

                        @include('purchase_return_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection