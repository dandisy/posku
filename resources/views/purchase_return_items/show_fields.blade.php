<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $purchaseReturnItem->id !!}</p>
</div>

<!-- Purchase Return Id Field -->
<div class="form-group">
    {!! Form::label('purchase_return_id', 'Purchase Return Id:') !!}
    <p>{!! $purchaseReturnItem->purchase_return_id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $purchaseReturnItem->item !!}</p>
</div>

<!-- Unique Barcode Field -->
<div class="form-group">
    {!! Form::label('unique_barcode', 'Unique Barcode:') !!}
    <p>{!! $storeFrontItem->unique_barcode !!}</p>
</div>

<!-- Item Discount Field -->
<div class="form-group">
    {!! Form::label('item_discount', 'Item Discount:') !!}
    <p>{!! $purchaseReturnItem->item_discount !!}</p>
</div>

<!-- Item Price Field -->
<div class="form-group">
    {!! Form::label('item_price', 'Item Price:') !!}
    <p>{!! $purchaseReturnItem->item_price !!}</p>
</div>

<!-- Item Amount Field -->
<div class="form-group">
    {!! Form::label('item_amount', 'Item Amount:') !!}
    <p>{!! $purchaseReturnItem->item_amount !!}</p>
</div>

<!-- Discount Subtotal Field -->
<div class="form-group">
    {!! Form::label('discount_subtotal', 'Discount Subtotal:') !!}
    <p>{!! $purchaseReturnItem->discount_subtotal !!}</p>
</div>

<!-- Price Subtotal Field -->
<div class="form-group">
    {!! Form::label('price_subtotal', 'Price Subtotal:') !!}
    <p>{!! $purchaseReturnItem->price_subtotal !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $purchaseReturnItem->tax !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $purchaseReturnItem->note !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $purchaseReturnItem->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $purchaseReturnItem->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $purchaseReturnItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $purchaseReturnItem->updated_at !!}</p>
</div>

