@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Debt
        </h1>
        
        {{--@include('debts.version')--}}
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('debts.show_fields')
                    <a href="{!! route('debts.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
