@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Debt
        </h1>

        {{--@include('debts.version')--}}
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($debt, ['route' => ['debts.update', $debt->id], 'method' => 'patch']) !!}

                        @include('debts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection