@extends('layouts.app')

@section('contents')
    <section class="content-header">
        <h1>
            Purchase
        </h1>

        {{--@include('purchases.version')--}}
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'purchases.store']) !!}

                        @include('purchases.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalAddItemLabel" rowkey="" rowindex="">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalAddItemLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Purchase Id Field -->
                    <!-- {{--<div class="form-group col-sm-6">
                        {!! Form::label('purchase_id', 'Purchase Id:') !!}
                        {!! Form::number('purchase_id', null, ['class' => 'form-control']) !!}
                    </div>--}} -->

                    {!! Form::hidden('purchase_id', null, ['id' => 'purchase_id']) !!}
                    {!! Form::hidden('item_name', null, ['id' => 'item_name']) !!}

                    <!-- Item Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item', 'Item:') !!}
                        <!-- {{--{!! Form::select('item', $items->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}--}} -->
                        <select class="form-control select2" name="item" id="item">
                            <option></option>
                            @foreach($items as $item)
                            <option value="{{$item->id}}" data-itemname="{{$item->name}}" data-itemprice="{{$item->purchase_price}}" data-uniquebarcode="{{$item->is_many_unique_barcode}}">{{$item->name}} - {{$item->code}}{{$item->barcode ? (' - '.$item->barcode) : ''}}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Item Price Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_price', 'Item Price:') !!}
                        {!! Form::text('item_price', null, ['class' => 'form-control thousand', 'readonly']) !!}
                    </div>

                    <!-- Item Discount Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_discount', 'Item Discount:') !!}
                        {!! Form::number('item_discount', null, ['class' => 'form-control thousand', 'min' => '0']) !!}
                    </div>

                    <!-- Item Amount Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('item_amount', 'Item Amount:') !!}
                        {!! Form::number('item_amount', null, ['class' => 'form-control thousand', 'min' => '0']) !!}
                    </div>

                    <!-- Discount Subtotal Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('discount_subtotal', 'Discount Subtotal:') !!}
                        {!! Form::number('discount_subtotal', null, ['class' => 'form-control thousand', 'min' => '0']) !!}
                    </div>

                    <!-- Price Subtotal Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('price_subtotal', 'Price Subtotal:') !!}
                        {!! Form::text('price_subtotal', null, ['class' => 'form-control thousand', 'readonly']) !!}
                    </div>

                    <!-- Tax Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('tax', 'Tax:') !!}
                        {!! Form::number('tax', null, ['class' => 'form-control thousand', 'min' => '0']) !!}
                    </div>

                    <div class="clearfix"></div>
                    <hr>

                    <!-- Barcode Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('barcode', 'Barcode:') !!}
                        <!-- {{--{!! Form::text('barcode', null, ['class' => 'form-control']) !!}--}} -->
                        <div class="input-group">
                            {!! Form::text('barcodeInput', null, ['class' => 'form-control', 'id' => 'barcodeInput']) !!}
                            <div class="input-group-btn">
                                <span class="btn btn-primary btn-add-barcode">Add New</span>
                            </div>
                            <div class="input-group-btn">
                                <span type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModalQRCode"><i class="fa fa-barcode"></i> Scan</span>
                            </div>
                        </div>

                        <div class="data-barcode">
                            <!-- {{--@if(!empty($itemBarcodes))
                            @foreach($itemBarcodes as $item)
                            <input class="rowkey{{$item->id}}" type="hidden" name="barcode[]" value="{{$item->barcode}}">
                            @endforeach
                            @endif--}} -->
                        </div>

                        <div class="dx-viewport" style="margin-top:10px">
                            <div class="demo-container">
                                <div id="gridContainerBarcode"></div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr>

                    <!-- Note Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('item_note', 'Note:') !!}
                        {!! Form::textarea('item_note', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-form">Save</button>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalQRCode" tabindex="-1" role="dialog" aria-labelledby="myModalQRCodeLabel" rowkey="" rowindex="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalQRCodeLabel">Barcode Scanner</h4>
            </div>
            <div class="modal-body" id="QR-Code">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <div class="navbar-form navbar-left">
                            <h4>Scan</h4>
                        </div> -->
                        <div class="navbar-form navbar-right">
                            <select class="form-control" id="camera-select" style="margin:5px"></select>
                            <div class="form-group">
                                <input style="margin:5px" id="image-url" type="text" class="form-control" placeholder="Image url">
                                <button style="margin:5px" title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                                <button style="margin:5px" title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                                <button style="margin:5px" title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
                                <button style="margin:5px" title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                                <button style="margin:5px" title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="well" style="position: relative;display: inline-block;">
                                    <canvas width="308" height="240" id="webcodecam-canvas"></canvas>
                                    <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                    <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                    <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                    <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                                </div>
                                <!-- <div class="well" style="width: 100%;">
                                    <label id="zoom-value" width="100">Zoom: 2</label>
                                    <input id="zoom" onchange="Page.changeZoom();" type="range" min="10" max="30" value="20">
                                    <label id="brightness-value" width="100">Brightness: 0</label>
                                    <input id="brightness" onchange="Page.changeBrightness();" type="range" min="0" max="128" value="0">
                                    <label id="contrast-value" width="100">Contrast: 0</label>
                                    <input id="contrast" onchange="Page.changeContrast();" type="range" min="0" max="64" value="0">
                                    <label id="threshold-value" width="100">Threshold: 0</label>
                                    <input id="threshold" onchange="Page.changeThreshold();" type="range" min="0" max="512" value="0">
                                    <label id="sharpness-value" width="100">Sharpness: off</label>
                                    <input id="sharpness" onchange="Page.changeSharpness();" type="checkbox">
                                    <label id="grayscale-value" width="100">grayscale: off</label>
                                    <input id="grayscale" onchange="Page.changeGrayscale();" type="checkbox">
                                    <br>
                                    <label id="flipVertical-value" width="100">Flip Vertical: off</label>
                                    <input id="flipVertical" onchange="Page.changeVertical();" type="checkbox">
                                    <label id="flipHorizontal-value" width="100">Flip Horizontal: off</label>
                                    <input id="flipHorizontal" onchange="Page.changeHorizontal();" type="checkbox">
                                </div> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- <div class="row"> -->
                                <div class="thumbnail" id="result">
                                    <!-- <div class="well" style="overflow: hidden;">
                                        <img width="308" height="240" id="scanned-img" src="">
                                    </div> -->
                                    <div class="caption">
                                        <h5>Scanned result</h5>
                                        <p id="scanned-QR"></p>
                                    </div>
                                </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myAddSupplierModal" tabindex="-1" role="dialog" aria-labelledby="myAddSupplierModalLabel">
    <div class="modal-dialog" role="document">
        <form class="modal-content" action="{!! route('suppliers.store') !!}" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myAddSupplierModalLabel">Add Supplier</h4>
            </div>
            <div class="modal-body">
                <!-- Name Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Description Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('description', 'Description:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Phone Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('phone', 'Phone:') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Email Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Address Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('address', 'Address:') !!}
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Image Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('image', 'Image:') !!}
                    <div id="image-thumb">
                        <img src="https://via.placeholder.com/600x150?text=Image" style="width:100%">
                    </div>
                    <div class="input-group">
                        {!! Form::text('image', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        <div class="input-group-btn">
                            <a href="{!! url('assets/dialog?filter=all&appendId=image') !!}" class="btn btn-primary filemanager fancybox.iframe" data-fancybox-type="iframe">Choose File</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
