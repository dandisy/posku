@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/dxDataGrid/css/dx.common.css') }}" />
    <link rel="dx-theme" data-theme="generic.light" href="{{ asset('vendor/dxDataGrid/css/dx.light.css') }}" />
    <style>
        .dx-datagrid .dx-data-row > td.bullet {
            padding-top: 0;
            padding-bottom: 0;
        }
        .dx-datagrid-content .dx-datagrid-table .dx-row .dx-command-edit {
            width: auto;
            min-width: 140px;
        }
    </style>

    
    <style>
        #QR-Code .thumbnail {
            border: 0;
            padding: 0;
        }

        #QR-Code .thumbnail .caption {
            padding: 0;
        }

        #webcodecam-canvas, #scanned-img {
            background-color: #2d2d2d;
        }

        #camera-select {
            display: inline-block;
            width: auto;
        }

        #QR-Code .well {
            position: relative;
            display: inline-block;
            padding: 0;
            background-color: transparent;
            box-shadow: none;
            border: none;
            border-radius: 0;
        }

        .scanner-laser {
            position: absolute;
            margin: 40px;
            height: 30px;
            width: 30px;
            opacity: 0.5;
        }

        .laser-leftTop {
            top: 0;
            left: 0;
            border-top: solid red 5px;
            border-left: solid red 5px;
        }

        .laser-leftBottom {
            bottom: 0;
            left: 0;
            border-bottom: solid red 5px;
            border-left: solid red 5px;
        }

        .laser-rightTop {
            top: 0;
            right: 0;
            border-top: solid red 5px;
            border-right: solid red 5px;
        }

        .laser-rightBottom {
            bottom: 0;
            right: 0;
            border-bottom: solid red 5px;
            border-right: solid red 5px;
        }

        #webcodecam-canvas {
            background-color: #272822;
        }
        
        #scanned-QR {
            word-break: break-word;
        }
    </style>
@endsection

<!-- Store Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store', 'Store:') !!}
    {!! Form::select('store', $store->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Supplier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier', 'Supplier:') !!}
    <div class="input-group">
        {!! Form::select('supplier', $supplier->pluck('name', 'id'), null, ['class' => 'form-control select2']) !!}
        <div class="input-group-btn">
            <span type="button" class="btn btn-danger" data-toggle="modal" data-target="#myAddSupplierModal">Add Supplier</span>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<div class="col-sm-12">
    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalAddItem">
        Add Item
    </button>

    <div class="items-data">
        @if($purchaseItems)
        @foreach($purchaseItems as $item)
            <div class="rowkey{{$item->id}}">
                <input type="hidden" name="item_id[]" value="{{$item->id}}">
                <input type="hidden" name="purchase_id[]" value="{{$item->purchase_id}}">
                <input type="hidden" name="item[]" value="{{$item->item}}">
                <input type="hidden" name="item_discount[]" value="{{$item->item_discount}}">
                <input type="hidden" name="item_price[]" value="{{$item->item_price}}">
                <input type="hidden" name="item_amount[]" value="{{$item->item_amount}}">
                <input type="hidden" name="discount_subtotal[]" value="{{$item->discount_subtotal}}">
                <input type="hidden" name="price_subtotal[]" value="{{$item->price_subtotal}}">
                <input type="hidden" name="tax[]" value="{{$item->tax}}">
                @php($iBarcodes = $itemBarcodes->where('item', $item->item))
                @if($iBarcodes)
                @foreach($iBarcodes as $barcode)
                <input class="item-data-unique-barcode" type="hidden" name="barcode[{{$item->id}}][]" value="{{$barcode->barcode}}">
                @endforeach
                @endif
                <input type="hidden" name="item_note[]" value="{{$item->note}}">
            </div>
        @endforeach
        @endif
    </div>
</div>

<div class="col-sm-12">
    <div class="dx-viewport">
        <div class="demo-container">
            <div id="gridContainer"></div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr>

<!-- Receipt Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receipt_code', 'Receipt Code:') !!}
    {!! Form::text('receipt_code', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Receipt Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receipt_amount', 'Receipt Amount:') !!}
    {!! Form::text('receipt_amount', null, ['class' => 'form-control thousand']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>
<hr>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('purchases.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script>
        $('#myAddSupplierModal').find('form').submit(function(e) {
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({
                url: $('#myAddSupplierModal').find('form').attr('action'),
                type: 'post',
                data: $('#myAddSupplierModal').find('form').serialize()
            }).then(function(res) {
                // console.log(res)
                $('#supplier').append('<option value="'+res.id+'" selected>'+res.name+'</option>');
                $('#supplier').select2();
                $('#myAddSupplierModal').modal('hide');
            });
        });
    </script>

    <script src="{{ asset('vendor/dxDataGrid/js/cldr.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/event.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/supplemental.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/cldr/unresolved.min.js') }}"></script>	
    <script src="{{ asset('vendor/dxDataGrid/js/globalize.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/message.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/number.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/date.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/globalize/currency.min.js') }}"></script>
    <script src="{{ asset('vendor/dxDataGrid/js/dx.web.js') }}"></script>

    
    <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/filereader.js') }}"></script>
    <!-- Using jquery version: -->
    
        <!-- <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/jquery.js') }}"></script> -->
        <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/qrcodelib.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/webcodecamjquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/mainjquery.js') }}"></script>
   
    <!-- <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/qrcodelib.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/webcodecamjs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webcodecamjs/js/main.js') }}"></script> -->

    <script>
        var iDiscount = 0;
        var iAmount = 0;
        var discountSubtotal = 0;
    </script>

    <script>
        var dataSource = @json($purchaseItems);

        function updateDataGrid(data) {
            if(data) {
                dataSource.push(data);
            }
            
            $("#gridContainer").dxDataGrid({ dataSource: dataSource });
        }
    </script>

    <script>
        var dataSourceBarcode = [];
        // var dataSourceBarcode = @json($itemBarcodes);

        function updateDataGridBarcode(data) {
            if(data) {
                dataSourceBarcode.push(data);
            }
            
            $("#gridContainerBarcode").dxDataGrid({ dataSource: dataSourceBarcode });
        }
    </script>

    <script>
        function barcodeHandle() {
            var el = $('#barcodeInput');
            var val = el.val();

            if(val) {
                var iUniqueBarcode = [];
                var dataUniqueBarcodes = $(document).find('.item-unique-barcode');
                $.each(dataUniqueBarcodes, function(i,v) {
                    iUniqueBarcode.push($(v).val());
                });
                var itemsDataUniqueBarcodes = $(document).find('.item-data-unique-barcode');
                $.each(itemsDataUniqueBarcodes, function(i,v) {
                    iUniqueBarcode.push($(v).val());
                });

                // Todo : check existing barcode in database
                
                if(iUniqueBarcode.filter(x => x == val).length > 0) {
                    alert('Double input barcode!');
                } else {
                    if(dataSourceBarcode) {
                        var uniqueId = Date.now();
                        updateDataGridBarcode({
                            id: (uniqueId)+'-tmp',
                            barcode: val
                        });

                        $('.data-barcode').append('<input class="item-unique-barcode rowkey'+uniqueId+'-tmp" type="hidden" name="barcode[]" value="'+val+'">');
                    }
                    el.val('');

                    $('#myModalAddItem').find('#item_amount').val($('.data-barcode > input').length);
                    if($('#myModalAddItem').find('#item_discount').val()) {
                        $('#myModalAddItem').find('#item_discount').trigger('change');
                    } else {
                        $('#myModalAddItem').find('#discount_subtotal').trigger('change');
                    }
                }
            } else {
                alert('Can\'t be empty input!');
            }
        }

        $('.btn-add-barcode').on('click', function() {
            barcodeHandle();
        });
        $('#barcodeInput').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                barcodeHandle();

                event.preventDefault();
                return false;
            }
        });
    </script>

    <script>
        var doDeselection;
        function logEvent(eventName) {
            var logList = $("#events ul"),
                newItem = $("<li>", { text: eventName });

            logList.prepend(newItem);
        }

        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            columnAutoWidth: true,
            allowColumnResizing: true,
            columnResizingMode: 'widget', // or 'nextColumn'
            rowAlternationEnabled: true,
            hoverStateEnabled: true, 
            // showBorders: true,
            grouping: {
                autoExpandAll: false,
                contextMenuEnabled: true
            },
            groupPanel: {
                visible: true
            },       
            searchPanel: {
                visible: true
            },   
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            columnFixing: {
                enabled: true
            },
            // height: 420,            
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 50, 100],
                showInfo: true
            },
            keyExpr: "id",
            columns: [
                {  
                    caption: '#',  
                    cellTemplate: function(cellElement, cellInfo) {  
                        cellElement.text(cellInfo.row.rowIndex + 1);
                    }  
                },
                {
                    dataField: 'id',
                    visible: false
                },
                "item",
                "item_name",
                {
                    dataField: "item_discount",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                {
                    dataField: "item_price",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                {
                    dataField: "item_amount",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                {
                    dataField: "discount_subtotal",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                {
                    dataField: "price_subtotal",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                {
                    dataField: "tax",
                    format: {
                        type: "fixedPoint",
                        precision: 0
                    }
                },
                "note",
                "created_by",
                "created_at",
                "updated_by",
                "updated_at"
            ],
            editing: {
                mode: "row",
                // allowUpdating: true,
                allowDeleting: function(e) {
                    return true;
                },
                // allowAdding: true
            },
            onCellPrepared: function (e) {
                if (e.rowType === "data" && e.column.command === "edit") {
                    e.cellElement.prepend('&nbsp;');
                    $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            if(/^\d+$/.test(e.key)) {
                                $('#myModalAddItem').modal('toggle');
                                $('#myModalAddItem').find('#myModalAddItemLabel').text('Edit Item');
                                $('#myModalAddItem').attr('rowkey', e.key);
                                $('#myModalAddItem').attr('rowindex', e.rowIndex);
                                $('#myModalAddItem').find('#purchase_id').val(e.row.data.purchase_id);
                                $('#myModalAddItem').find('#item').val(e.row.data.item_name);
                                $('#myModalAddItem').find('#item').val(e.row.data.item);
                                $('#myModalAddItem').find('#item_discount').val(e.row.data.item_discount);
                                $('#myModalAddItem').find('#item_price').val(e.row.data.item_price);
                                $('#myModalAddItem').find('#item_amount').val(e.row.data.item_amount);
                                $('#myModalAddItem').find('#discount_subtotal').val(e.row.data.discount_subtotal);
                                $('#myModalAddItem').find('#price_subtotal').val(e.row.data.price_subtotal);
                                $('#myModalAddItem').find('#tax').val(e.row.data.tax);
                                $('#myModalAddItem').find('#item_note').val(e.row.data.note);
                                $('#myModalAddItem').find('#item_note').text(e.row.data.note);

                                dataSourceBarcode = [];
                                if($('.items-data > div.rowkey'+e.key).length) {
                                    $('.items-data > div.rowkey'+e.key).find('[name="barcode['+e.key+'][]"]').each(function(i, v) {
                                        dataSourceBarcode.push({id: i, barcode: $(v).val()});
                                    });
                                } else {
                                    dataSourceBarcode = (@json($itemBarcodes)).filter(x => x.item == e.row.data.item);
                                }
                                $("#gridContainerBarcode").dxDataGrid({ dataSource: dataSourceBarcode });
                                $.each(dataSourceBarcode, function(i, v) {
                                    $('#myModalAddItem').find('.data-barcode').append('<input class="item-data-unique-barcode rowkey'+v.id+'" type="hidden" name="barcode[]" value="'+v.barcode+'">');
                                });
                            } else {
                                $('#myModalAddItem').modal('toggle');
                                $('#myModalAddItem').find('#myModalAddItemLabel').text('Edit Item');
                                $('#myModalAddItem').attr('rowkey', e.key);
                                $('#myModalAddItem').attr('rowindex', e.rowIndex);
                                $('#myModalAddItem').find('#purchase_id').val('');
                                $('#myModalAddItem').find('#item').val(e.row.data.item_name);
                                $('#myModalAddItem').find('#item').val(e.row.data.item);
                                $('#myModalAddItem').find('#item_discount').val(e.row.data.item_discount);
                                $('#myModalAddItem').find('#item_price').val(e.row.data.item_price);
                                $('#myModalAddItem').find('#item_amount').val(e.row.data.item_amount);
                                $('#myModalAddItem').find('#discount_subtotal').val(e.row.data.discount_subtotal);
                                $('#myModalAddItem').find('#price_subtotal').val(e.row.data.price_subtotal);
                                $('#myModalAddItem').find('#tax').val(e.row.data.tax);
                                $('#myModalAddItem').find('#item_note').val(e.row.data.note);
                                $('#myModalAddItem').find('#item_note').text(e.row.data.note);

                                dataSourceBarcode = [];
                                $('.items-data').find('[name="barcode['+e.key+'][]"]').each(function(i, v) {
                                    dataSourceBarcode.push({id: i, barcode: $(v).val()});
                                    $('#myModalAddItem').find('.data-barcode').append('<input class="item-data-unique-barcode rowkey'+i+'" type="hidden" name="barcode[]" value="'+$(v).val()+'">');
                                });
                                $("#gridContainerBarcode").dxDataGrid({ dataSource: dataSourceBarcode });
                            }
                        })
                        .prependTo(e.cellElement);
                }
            },
            onRowRemoved: function(e) {
                // if(/^\d+$/.test(e.key)) {
                //     $.ajaxSetup({
                //         headers: {
                //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //         }
                //     });
                //     $.ajax({
                //         url: '{{url("purchaseItems")}}/'+JSON.stringify(e.data.id),
                //         type: 'delete'
                //     }).success(function(res) {
                //         $('.items-data').find('.rowkey'+e.key).remove();
                //     }).error(function(res) {
                //         console.log(res);
                //     });
                // } else {
                    $('.items-data').find('.rowkey'+e.key).remove();
                // }

                var receiptAmount = 0;
                $('.items-data').find('[name="price_subtotal[]"]').each(function(i, v) {
                    receiptAmount += parseInt($(v).val());
                    $('#receipt_amount').val(receiptAmount);
                });
                
                // logEvent("RowRemoved");
            },
        });

        $("#gridContainerBarcode").dxDataGrid({
            dataSource: dataSourceBarcode,
            columnAutoWidth: true,
            allowColumnResizing: true,
            columnResizingMode: 'widget', // or 'nextColumn'
            // rowAlternationEnabled: true,
            allowColumnReordering: false,
            columnChooser: {
                enabled: false,
                mode: "dragAndDrop" // or "select"
            },
            hoverStateEnabled: true, 
            // showBorders: true,
            // selection: {
            //     mode: 'multiple'
            // },
            export: {
                enabled: false,
                // allowExportSelectedData: true,
                // fileName: 'items',
            },
            grouping: {
                autoExpandAll: false,
                contextMenuEnabled: true
            },
            groupPanel: {
                visible: false
            },       
            searchPanel: {
                visible: false
            },   
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            columnFixing: {
                enabled: true
            },
            // height: 420,            
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 50, 100],
                showInfo: true
            },
            keyExpr: "id",
            columns: [
                {  
                    caption: '#',  
                    cellTemplate: function(cellElement, cellInfo) {  
                        cellElement.text(cellInfo.row.rowIndex + 1);
                    }  
                },
                {
                    dataField: 'id',
                    visible: false
                },
                "barcode"
            ],
            editing: {
                mode: "row",
                // allowUpdating: true,
                allowDeleting: function(e) {
                    return true;
                },
                // allowAdding: true
            },
            onExporting: e => {
                var workbook = new ExcelJS.Workbook();    
                var worksheet = workbook.addWorksheet('Main sheet');
                var PromiseArray = [];
                
                DevExpress.excelExporter.exportDataGrid({
                    component: e.component,
                    worksheet: worksheet,
                    autoFilterEnabled: true,
                    customizeCell: (options) => {
                        var { excelCell, gridCell } = options;
                        if(gridCell.rowType === "data") {
                            if(gridCell.column.dataField === "image") {
                                excelCell.value = undefined;
                                PromiseArray.push(
                                    new Promise((resolve, reject) => {
                                        addImage(gridCell.value, workbook, worksheet, excelCell, resolve); 
                                    })
                                );
                            }
                        }

                    }
                }).then(function() {
                    Promise.all(PromiseArray).then(() => {
                        workbook.xlsx.writeBuffer().then(function (buffer) {
                            saveAs(new Blob([buffer], { type: "application/octet-stream" }), "barcodes.xlsx");
                        });
                    });
                });
                e.cancel = true;
            },
            onRowRemoved: function(e) {
                $('.data-barcode').find('.rowkey'+e.data.id).remove();

                $('#myModalAddItem').find('#item_amount').val($('.data-barcode > input').length);
                calcPriceSubtotal();
                
                // logEvent("RowRemoved");
            },
        });
        
        // function OnClick() {
        //     var arrId = $("#gridContainer").dxDataGrid("instance").getSelectedRowKeys();
        //     console.log('id', arrId);
        // }
    </script>

    <script>
        function calcPriceSubtotal(el) {
            var el = el || null;

            $('#myModalAddItem').find('#item_name').val($('#myModalAddItem').find('#item').find('option:selected').data('itemname'));

            var iPrice = parseInt($('#myModalAddItem').find('#item').find('option:selected').data('itemprice'));

            if($('#myModalAddItem').find('#item').find('option:selected').data('uniquebarcode') == 'yes') {
                $('#myModalAddItem').find('#item_amount').prop('readonly', true);
                $('#myModalAddItem').find('#barcodeInput').prop('readonly', false);
                $('#myModalAddItem').find('.btn-add-barcode').prop('disabled', false);
            } else {
                $('#myModalAddItem').find('#item_amount').prop('readonly', false);
                $('#myModalAddItem').find('#barcodeInput').prop('readonly', true);
                $('#myModalAddItem').find('.btn-add-barcode').prop('disabled', true);
            }

            $('#myModalAddItem').find('#item_discount').attr('max', iPrice);
            $('#myModalAddItem').find('#discount_subtotal').attr('max', iPrice*iAmount);

            if($('#myModalAddItem').find('#item_discount').val()) {
                iDiscount = parseInt($('#myModalAddItem').find('#item_discount').val().replace(',',''));   
            }
            if($('#myModalAddItem').find('#item_amount').val()) {
                iAmount = parseInt($('#myModalAddItem').find('#item_amount').val().replace(',',''));
            }

            if($('#myModalAddItem').find('#item_discount').val() > iPrice) {
                $('#myModalAddItem').find('#item_discount').val(iPrice);
                $('#myModalAddItem').find('#item_discount').focus();

                alert('Input melebihi nilai maksimal');
            }
            if($('#myModalAddItem').find('#discount_subtotal').val() > (iPrice*iAmount)) {
                $('#myModalAddItem').find('#discount_subtotal').val(iPrice*iAmount);
                $('#myModalAddItem').find('#discount_subtotal').focus();

                alert('Input melebihi nilai maksimal');
            }

            $('#myModalAddItem').find('#item_price').val(iPrice);
            if(el) {
                if(el.attr('id') == 'item_amount') {
                    if($('.data-barcode > input').length) {
                        $('#myModalAddItem').find('#item_amount').val($('.data-barcode > input').length);
                        alert('Jika unique barcode diinput field ini akan tersisi otomatis!');
                    }
                }
                if(el.attr('id') == 'item_discount') {
                    $('#myModalAddItem').find('#discount_subtotal').val(iDiscount*iAmount);
                }
                if(el.attr('id') == 'discount_subtotal') {
                    $('#myModalAddItem').find('#item_discount').val(null);
                }
            }

            if($('#myModalAddItem').find('#discount_subtotal').val()) {
                discountSubtotal = parseInt($('#myModalAddItem').find('#discount_subtotal').val().replace(',',''));
            }

            $('#myModalAddItem').find('#price_subtotal').val((iPrice*iAmount) - discountSubtotal);
        }

        $('#myModalAddItem').on('change', '#item, #item_discount, #item_amount, #discount_subtotal', function() {
            calcPriceSubtotal($(this));
        });
    </script>

    <script>
        $('#myModalAddItem').on('shown.bs.modal', function () {
            $('.select2').select2();
            iDiscount = iAmount = discountSubtotal = 0;

            if($('#myModalAddItem').find('#item').find('option:selected').data('uniquebarcode') == 'yes') {
                $('#myModalAddItem').find('#item_amount').prop('readonly', true);
                $('#myModalAddItem').find('#barcodeInput').prop('readonly', false);
                $('#myModalAddItem').find('.btn-add-barcode').prop('disabled', false);
            } else {
                $('#myModalAddItem').find('#item_amount').prop('readonly', false);
                $('#myModalAddItem').find('#barcodeInput').prop('readonly', true);
                $('#myModalAddItem').find('.btn-add-barcode').prop('disabled', true);
            }
        });
        
        $('#myModalAddItem').on('hide.bs.modal', function () {
            $('#myModalAddItem').attr('rowkey', null);
            $('#myModalAddItem').attr('rowindex', null);
            $('#myModalAddItem').find('#myModalAddItemLabel').text('Add Item');
            $('#myModalAddItem').find('input').val('');
            $('#myModalAddItem').find('textarea').val('');
            $('#myModalAddItem').find('textarea').text('');
            $('#myModalAddItem').find('.select2').val('').trigger('change');

            dataSourceBarcode = [];
            $("#gridContainerBarcode").dxDataGrid({ dataSource: dataSourceBarcode });
            $('#myModalAddItem').find('.data-barcode').empty();
        });

        $(document).on('click', '.btn-save-form', function() {
            var input = $(this).parents('form').serializeArray();
            var data = {};

            if($('#myModalAddItem').attr('rowkey')) { // edit datagrid row
                if($('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).length) {
                    $('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).find('[name="barcode['+$('#myModalAddItem').attr('rowkey')+'][]"]').remove();
                    $(input).each(function(i, v) {
                        if($('#myModalAddItem').find('[name="'+v.name+'"]').hasClass('thousand')) {
                            dataSource[$('#myModalAddItem').attr('rowindex')][v.name] = v.value.replace(',','');
                            $('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).find('[name="'+v.name+'[]"]').val(v.value.replace(',',''));
                        } else {
                            if(v.name == 'barcode[]') {
                                $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input class="item-data-unique-barcode" type="hidden" name="barcode['+$('#myModalAddItem').attr('rowkey')+'][]" value="'+v.value+'">');
                            } else {
                                dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'item_note' ? 'note' : v.name] = v.value;
                                $('.items-data').find('.rowkey'+$('#myModalAddItem').attr('rowkey')).find('[name="'+v.name+'[]"]').val(v.value);
                            }
                        }
                    });
                } else {
                    $('.items-data').append('<div/>');
                    $('.items-data > div:last-child').addClass('rowkey'+$('#myModalAddItem').attr('rowkey'));
                    $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="item_id[]" value="'+$('#myModalAddItem').attr('rowkey')+'">');
                    $(input).each(function(i, v) {
                        if(v.name == 'barcode[]') {
                            // dataSource[$('#myModalAddItem').attr('rowindex')][v.name] = v.value;
                            $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input class="item-data-unique-barcode" type="hidden" name="barcode['+$('#myModalAddItem').attr('rowkey')+'][]" value="'+v.value+'">');
                        } else {
                            if($('#myModalAddItem').find('[name="'+v.name+'"]').hasClass('thousand')) {
                                dataSource[$('#myModalAddItem').attr('rowindex')][v.name] = v.value.replace(',','');
                                $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value.replace(',','')+'">');
                            } else {
                                dataSource[$('#myModalAddItem').attr('rowindex')][v.name === 'item_note' ? 'note' : v.name] = v.value;
                                $('.items-data > div.rowkey'+$('#myModalAddItem').attr('rowkey')).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value+'">');
                            }
                        }
                    });
                }
            
                updateDataGrid(null);
            } else { // add new button
                if(dataSource.length) {
                    data['id'] = (parseInt(dataSource[dataSource.length-1].id)+1)+'-tmp';
                } else {
                    data['id'] = '1-tmp';
                }
                $('.items-data').append('<div/>');
                $('.items-data > div:last-child').addClass('rowkey'+data['id']);
                $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="item_id[]" value="'+data['id']+'">');
                $(input).each(function(i, v) {
                    if(v.name == 'barcode[]') {
                        // data[v.name] = v.value;
                        $('.items-data > div.rowkey'+data['id']).append('<input class="item-data-unique-barcode" type="hidden" name="barcode['+data['id']+'][]" value="'+v.value+'">');
                    } else {
                        if($('#myModalAddItem').find('[name="'+v.name+'"]').hasClass('thousand')) {
                            data[v.name] = v.value.replace(',','');
                            $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value.replace(',','')+'">');
                        } else {
                            data[v.name === 'item_note' ? 'note' : v.name] = v.value;
                            $('.items-data > div.rowkey'+data['id']).append('<input type="hidden" name="'+v.name+'[]" value="'+v.value+'">');
                        }
                    }
                });
            
                updateDataGrid(data);
            }

            $('#myModalAddItem').find('input').val('');
            $('#myModalAddItem').find('textarea').val('');
            $('#myModalAddItem').find('textarea').text('');

            $('#myModalAddItem').modal('toggle');

            var receiptAmount = 0;
            $('.items-data').find('[name="price_subtotal[]"]').each(function(i, v) {
                receiptAmount += parseInt($(v).val());
                $('#receipt_amount').val(receiptAmount);
            });
        });
    </script>

<!-- Relational Form table -->
<script>
    $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;

        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             {!! Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    {!! Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->
@endsection
