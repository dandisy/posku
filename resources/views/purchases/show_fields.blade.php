<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $purchase->id !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $purchase->store !!}</p>
</div>

<!-- Supplier Field -->
<div class="form-group">
    {!! Form::label('supplier', 'Supplier:') !!}
    <p>{!! $purchase->supplier !!}</p>
</div>

<!-- Receipt Code Field -->
<div class="form-group">
    {!! Form::label('receipt_code', 'Receipt Code:') !!}
    <p>{!! $purchase->receipt_code !!}</p>
</div>

<!-- Receipt Amount Field -->
<div class="form-group">
    {!! Form::label('receipt_amount', 'Receipt Amount:') !!}
    <p>{!! $purchase->receipt_amount !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $purchase->note !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $purchase->balance !!}</p>
</div>

<!-- Is Draft Field -->
<div class="form-group">
    {!! Form::label('is_draft', 'Is Draft:') !!}
    <p>{!! $purchase->is_draft !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $purchase->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $purchase->updated_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $purchase->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $purchase->updated_at !!}</p>
</div>

