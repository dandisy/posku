<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompositeItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('composite_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('composite_id');
            $table->integer('store');
            $table->integer('item');
            $table->string('unique_barcode')->nullable();
            $table->double('item_amount');
            $table->integer('unit');
            $table->text('note')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('composite_items');
    }
}
