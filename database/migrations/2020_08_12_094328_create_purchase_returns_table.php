<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseReturnsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id');
            $table->integer('store');
            $table->integer('supplier');
            $table->string('receipt_code')->nullable();
            $table->double('receipt_amount')->nullable();
            $table->text('note')->nullable();
            $table->double('balance')->nullable();
            $table->integer('is_draft')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_returns');
    }
}
