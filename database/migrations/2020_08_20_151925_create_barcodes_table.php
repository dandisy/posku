<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarcodesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barcodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id');
            $table->string('item');
            $table->string('barcode');
            $table->string('store')->nullable();
            $table->string('status');
            $table->integer('purchase_return_id')->nullable();
            $table->integer('store_front_id')->nullable();
            $table->integer('sell_id')->nullable();
            $table->integer('sell_return_id')->nullable();
            $table->integer('stock_transfer_id')->nullable();
            $table->integer('rejected_item_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('barcodes');
    }
}
