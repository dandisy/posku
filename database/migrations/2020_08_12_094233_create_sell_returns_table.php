<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellReturnsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_id');
            $table->string('customer');
            $table->integer('store');
            $table->double('bill_amount')->nullable();
            $table->integer('payment_method');
            $table->integer('payment_status');
            $table->text('note')->nullable();
            $table->double('balance')->nullable();
            $table->integer('is_draft')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sell_returns');
    }
}
