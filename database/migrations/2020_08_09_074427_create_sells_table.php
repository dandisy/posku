<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sells', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer');
            $table->integer('store');
            $table->double('bill_amount');
            $table->string('table_number')->nullable();
            $table->integer('table_order_status')->nullable();
            $table->integer('payment_method');
            $table->string('payment_status');
            $table->string('selling_type');
            $table->text('note')->nullable();
            $table->double('balance')->nullable();
            $table->integer('is_draft')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sells');
    }
}
