<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRejectedItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rejected_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store');
            $table->string('source')->nullable();
            $table->integer('item');
            $table->string('unique_barcode')->nullable();
            $table->double('item_amount');
            $table->string('note')->nullable();
            $table->integer('model_type')->nullable();
            $table->integer('model_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rejected_items');
    }
}
