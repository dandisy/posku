<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseReturnItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_return_id');
            $table->integer('store');
            $table->integer('item');
            $table->string('unique_barcode')->nullable();
            $table->double('item_discount')->nullable();
            $table->double('item_price')->nullable();
            $table->integer('item_amount');
            $table->double('discount_subtotal')->nullable();
            $table->double('price_subtotal')->nullable();
            $table->double('tax')->nullable();
            $table->text('note')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_return_items');
    }
}
