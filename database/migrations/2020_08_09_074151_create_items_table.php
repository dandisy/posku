<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('category');
            $table->string('code');
            $table->string('barcode')->nullable();
            $table->text('description')->nullable();
            $table->string('unit');
            $table->string('wrap');
            $table->string('store')->nullable();
            $table->double('purchase_price');
            $table->double('sell_price')->nullable();
            $table->double('tax')->nullable();
            $table->double('minimal_stock')->nullable();
            $table->text('image')->nullable();
            $table->string('is_many_unique_barcode');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
