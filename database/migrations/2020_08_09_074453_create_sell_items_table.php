<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_id');
            $table->integer('store');
            $table->integer('item');
            $table->string('item_name')->nullable();
            $table->string('unique_barcode')->nullable();
            $table->double('item_discount')->nullable();
            $table->double('item_price');
            $table->integer('item_amount');
            $table->double('discount_subtotal')->nullable();
            $table->double('price_subtotal');
            $table->double('tax')->nullable();
            $table->text('note')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sell_items');
    }
}
