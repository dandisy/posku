-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table posku.attendances
CREATE TABLE IF NOT EXISTS `attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff` int(11) NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.attendances: ~0 rows (approximately)
/*!40000 ALTER TABLE `attendances` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendances` ENABLE KEYS */;

-- Dumping structure for table posku.barcodes
CREATE TABLE IF NOT EXISTS `barcodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) unsigned NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_return_id` int(11) DEFAULT NULL,
  `store_front_id` int(11) DEFAULT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `sell_return_id` int(11) DEFAULT NULL,
  `stock_transfer_id` int(11) DEFAULT NULL,
  `rejected_item_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.barcodes: ~0 rows (approximately)
/*!40000 ALTER TABLE `barcodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `barcodes` ENABLE KEYS */;

-- Dumping structure for table posku.cashes
CREATE TABLE IF NOT EXISTS `cashes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `transaction_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` double DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.cashes: ~0 rows (approximately)
/*!40000 ALTER TABLE `cashes` DISABLE KEYS */;
/*!40000 ALTER TABLE `cashes` ENABLE KEYS */;

-- Dumping structure for table posku.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `description`, `image`, `parent_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Category 1', NULL, NULL, NULL, 1, 1, '2020-08-09 12:59:20', '2020-08-09 12:59:20', NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table posku.composites
CREATE TABLE IF NOT EXISTS `composites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) DEFAULT NULL,
  `product` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.composites: ~0 rows (approximately)
/*!40000 ALTER TABLE `composites` DISABLE KEYS */;
/*!40000 ALTER TABLE `composites` ENABLE KEYS */;

-- Dumping structure for table posku.composite_items
CREATE TABLE IF NOT EXISTS `composite_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composite_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_amount` double NOT NULL,
  `unit` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.composite_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `composite_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `composite_items` ENABLE KEYS */;

-- Dumping structure for table posku.composite_stocks
CREATE TABLE IF NOT EXISTS `composite_stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `composite_id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_amount` double NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.composite_stocks: ~0 rows (approximately)
/*!40000 ALTER TABLE `composite_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `composite_stocks` ENABLE KEYS */;

-- Dumping structure for table posku.costs
CREATE TABLE IF NOT EXISTS `costs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `balance` double DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.costs: ~0 rows (approximately)
/*!40000 ALTER TABLE `costs` DISABLE KEYS */;
/*!40000 ALTER TABLE `costs` ENABLE KEYS */;

-- Dumping structure for table posku.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.customers: ~0 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `name`, `description`, `phone`, `email`, `address`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Anonimous', NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-08-09 13:49:17', '2020-08-09 13:49:17', NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table posku.debts
CREATE TABLE IF NOT EXISTS `debts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `transaction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` double DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.debts: ~0 rows (approximately)
/*!40000 ALTER TABLE `debts` DISABLE KEYS */;
/*!40000 ALTER TABLE `debts` ENABLE KEYS */;

-- Dumping structure for table posku.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table posku.featured_products
CREATE TABLE IF NOT EXISTS `featured_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `publish_on` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_off` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.featured_products: ~0 rows (approximately)
/*!40000 ALTER TABLE `featured_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `featured_products` ENABLE KEYS */;

-- Dumping structure for table posku.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_selected` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.images: ~0 rows (approximately)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Dumping structure for table posku.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wrap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_price` double NOT NULL,
  `sell_price` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `minimal_stock` double DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `is_many_unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.items: ~11 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `category`, `code`, `barcode`, `description`, `unit`, `wrap`, `store`, `purchase_price`, `sell_price`, `tax`, `minimal_stock`, `image`, `is_many_unique_barcode`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Item 1', '1', '001', NULL, NULL, '1', '1', '1', 1000, 1100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(2, 'Item 2', '1', '002', NULL, NULL, '1', '1', '1', 2000, 2100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(3, 'Item 3', '1', '003', NULL, NULL, '1', '1', '1', 3000, 3100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(4, 'Item 4', '1', '004', NULL, NULL, '1', '1', '1', 4000, 4100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(5, 'Item 5', '1', '005', NULL, NULL, '1', '1', '1', 5000, 5100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(6, 'Item 6', '1', '006', NULL, NULL, '1', '1', '1', 6000, 6100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(7, 'Item 7', '1', '007', NULL, NULL, '1', '1', '1', 7000, 7100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(8, 'Item 8', '1', '008', NULL, NULL, '1', '1', '1', 8000, 8100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(9, 'Item 9', '1', '009', NULL, NULL, '1', '1', '1', 9000, 9100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(10, 'Item 10', '1', '010', NULL, NULL, '1', '1', '1', 10000, 10100, NULL, 5, NULL, 'yes', 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL),
	(11, 'Item 11', '1', '011', NULL, NULL, '1', '1', '1', 11000, 11100, NULL, 5, NULL, NULL, 1, 1, '2020-08-21 16:26:15', '2020-08-21 16:26:15', NULL);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table posku.larametrics_logs
CREATE TABLE IF NOT EXISTS `larametrics_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trace` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.larametrics_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `larametrics_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `larametrics_logs` ENABLE KEYS */;

-- Dumping structure for table posku.larametrics_models
CREATE TABLE IF NOT EXISTS `larametrics_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original` text COLLATE utf8mb4_unicode_ci,
  `changes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.larametrics_models: ~0 rows (approximately)
/*!40000 ALTER TABLE `larametrics_models` DISABLE KEYS */;
/*!40000 ALTER TABLE `larametrics_models` ENABLE KEYS */;

-- Dumping structure for table posku.larametrics_notifications
CREATE TABLE IF NOT EXISTS `larametrics_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `notify_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'email',
  `last_fired_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.larametrics_notifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `larametrics_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `larametrics_notifications` ENABLE KEYS */;

-- Dumping structure for table posku.larametrics_requests
CREATE TABLE IF NOT EXISTS `larametrics_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headers` text COLLATE utf8mb4_unicode_ci,
  `start_time` double(16,4) DEFAULT NULL,
  `end_time` double(16,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.larametrics_requests: ~0 rows (approximately)
/*!40000 ALTER TABLE `larametrics_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `larametrics_requests` ENABLE KEYS */;

-- Dumping structure for table posku.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Dumping structure for table posku.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.migrations: ~52 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2018_06_01_000000_create_larametrics_models_table', 1),
	(9, '2018_07_01_000000_create_larametrics_requests_table', 1),
	(10, '2018_07_28_000000_create_larametrics_logs_table', 1),
	(11, '2018_08_02_000000_create_larametrics_notifications_table', 1),
	(12, '2018_08_08_100000_create_telescope_entries_table', 1),
	(13, '2019_08_19_000000_create_failed_jobs_table', 1),
	(14, '2019_11_29_112421_create_settings_table', 1),
	(15, '2019_11_29_112513_create_profiles_table', 1),
	(16, '2020_01_09_000000_add_user_id_column_to_larametrics_models_table', 1),
	(17, '2020_08_09_032422_create_attendances_table', 1),
	(18, '2020_08_09_032458_create_cashes_table', 1),
	(19, '2020_08_09_032532_create_categories_table', 1),
	(20, '2020_08_09_032552_create_costs_table', 1),
	(21, '2020_08_09_032632_create_customers_table', 1),
	(22, '2020_08_09_032915_create_logs_table', 1),
	(23, '2020_08_09_032952_create_payment_methods_table', 1),
	(24, '2020_08_09_033112_create_product_promo_conditions_table', 1),
	(25, '2020_08_09_033141_create_promo_conditions_table', 1),
	(26, '2020_08_09_033211_create_purchases_table', 1),
	(27, '2020_08_09_033501_create_stores_table', 1),
	(28, '2020_08_09_033522_create_suppliers_table', 1),
	(29, '2020_08_09_033543_create_units_table', 1),
	(30, '2020_08_09_033603_create_wraps_table', 1),
	(31, '2020_08_09_034334_create_permission_tables', 1),
	(32, '2020_08_09_070036_create_store_fronts_table', 1),
	(33, '2020_08_09_074039_create_debts_table', 1),
	(34, '2020_08_09_074151_create_items_table', 1),
	(35, '2020_08_09_074359_create_purchase_items_table', 1),
	(36, '2020_08_09_074427_create_sells_table', 1),
	(37, '2020_08_09_074453_create_sell_items_table', 1),
	(38, '2020_08_09_074529_create_stock_transfers_table', 1),
	(39, '2020_08_09_074551_create_stock_transfer_items_table', 1),
	(40, '2020_08_09_074648_create_store_front_items_table', 1),
	(41, '2020_08_09_074718_create_trusts_table', 1),
	(42, '2020_08_09_080658_create_composites_table', 1),
	(43, '2020_08_09_080721_create_composite_items_table', 1),
	(44, '2020_08_09_083246_create_product_promos_table', 1),
	(45, '2020_08_09_083306_create_featured_products_table', 1),
	(46, '2020_08_12_094042_create_stocks_table', 2),
	(47, '2020_08_12_094233_create_sell_returns_table', 2),
	(48, '2020_08_12_094255_create_sell_return_items_table', 2),
	(49, '2020_08_12_094328_create_purchase_returns_table', 2),
	(50, '2020_08_12_094347_create_purchase_return_items_table', 2),
	(51, '2020_08_15_030015_create_images_table', 3),
	(52, '2020_08_15_030300_create_recaps_table', 3),
	(53, '2020_08_12_094042_create_composite_stocks_table', 4),
	(54, '2020_08_15_154505_create_store_front_stocks_table', 5),
	(55, '2020_08_18_031032_create_rejected_items_table', 6),
	(56, '2020_08_20_151925_create_barcodes_table', 7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table posku.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.model_has_permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table posku.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.model_has_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\User', 1),
	(2, 'App\\User', 2);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table posku.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.oauth_access_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table posku.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table posku.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.oauth_clients: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table posku.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.oauth_personal_access_clients: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table posku.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.oauth_refresh_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table posku.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table posku.payment_methods
CREATE TABLE IF NOT EXISTS `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.payment_methods: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` (`id`, `name`, `description`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Cash', NULL, NULL, 1, 1, '2020-08-13 05:19:14', '2020-08-13 05:19:14', NULL);
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;

-- Dumping structure for table posku.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=538 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.permissions: ~493 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'accessControl.index', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(2, 'accessControl.index.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(3, 'accessControl.store.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(4, 'apiDocs', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(5, 'attendances.create.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(6, 'attendances.destroy', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(7, 'attendances.destroy.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(8, 'attendances.edit', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(9, 'attendances.edit.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(10, 'attendances.index', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(11, 'attendances.index.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(12, 'attendances.show', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(13, 'attendances.show.owned', 'web', '2020-08-27 06:59:42', '2020-08-27 06:59:42'),
	(14, 'attendances.store.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(15, 'attendances.update', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(16, 'attendances.update.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(17, 'balance', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(18, 'barcodes.create.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(19, 'barcodes.destroy', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(20, 'barcodes.destroy.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(21, 'barcodes.edit', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(22, 'barcodes.edit.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(23, 'barcodes.index', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(24, 'barcodes.index.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(25, 'barcodes.show', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(26, 'barcodes.show.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(27, 'barcodes.store.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(28, 'barcodes.update', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(29, 'barcodes.update.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(30, 'cashes.create.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(31, 'cashes.destroy', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(32, 'cashes.destroy.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(33, 'cashes.edit', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(34, 'cashes.edit.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(35, 'cashes.index', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(36, 'cashes.index.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(37, 'cashes.show', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(38, 'cashes.show.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(39, 'cashes.store.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(40, 'cashes.update', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(41, 'cashes.update.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(42, 'categories.create.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(43, 'categories.destroy', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(44, 'categories.destroy.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(45, 'categories.edit', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(46, 'categories.edit.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(47, 'categories.index', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(48, 'categories.index.owned', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(49, 'categories.show', 'web', '2020-08-27 06:59:43', '2020-08-27 06:59:43'),
	(50, 'categories.show.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(51, 'categories.store.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(52, 'categories.update', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(53, 'categories.update.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(54, 'compositeItems.create.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(55, 'compositeItems.destroy', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(56, 'compositeItems.destroy.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(57, 'compositeItems.edit', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(58, 'compositeItems.edit.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(59, 'compositeItems.index', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(60, 'compositeItems.index.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(61, 'compositeItems.show', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(62, 'compositeItems.show.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(63, 'compositeItems.store.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(64, 'compositeItems.update', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(65, 'compositeItems.update.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(66, 'compositeStocks.create.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(67, 'compositeStocks.destroy', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(68, 'compositeStocks.destroy.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(69, 'compositeStocks.edit', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(70, 'compositeStocks.edit.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(71, 'compositeStocks.index', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(72, 'compositeStocks.index.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(73, 'compositeStocks.show', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(74, 'compositeStocks.show.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(75, 'compositeStocks.store.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(76, 'compositeStocks.update', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(77, 'compositeStocks.update.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(78, 'composites.create.owned', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(79, 'composites.destroy', 'web', '2020-08-27 06:59:44', '2020-08-27 06:59:44'),
	(80, 'composites.destroy.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(81, 'composites.edit', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(82, 'composites.edit.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(83, 'composites.index', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(84, 'composites.index.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(85, 'composites.show', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(86, 'composites.show.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(87, 'composites.store.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(88, 'composites.update', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(89, 'composites.update.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(90, 'costs.create.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(91, 'costs.destroy', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(92, 'costs.destroy.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(93, 'costs.edit', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(94, 'costs.edit.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(95, 'costs.index', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(96, 'costs.index.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(97, 'costs.show', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(98, 'costs.show.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(99, 'costs.store.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(100, 'costs.update', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(101, 'costs.update.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(102, 'customers.create.owned', 'web', '2020-08-27 06:59:45', '2020-08-27 06:59:45'),
	(103, 'customers.destroy', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(104, 'customers.destroy.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(105, 'customers.edit', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(106, 'customers.edit.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(107, 'customers.index', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(108, 'customers.index.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(109, 'customers.show', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(110, 'customers.show.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(111, 'customers.store.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(112, 'customers.update', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(113, 'customers.update.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(114, 'dashboard', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(115, 'debts.create.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(116, 'debts.destroy', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(117, 'debts.destroy.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(118, 'debts.edit', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(119, 'debts.edit.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(120, 'debts.index', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(121, 'debts.index.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(122, 'debts.show', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(123, 'debts.show.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(124, 'debts.store.owned', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(125, 'debts.update', 'web', '2020-08-27 06:59:46', '2020-08-27 06:59:46'),
	(126, 'debts.update.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(127, 'featuredProducts.create.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(128, 'featuredProducts.destroy', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(129, 'featuredProducts.destroy.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(130, 'featuredProducts.edit', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(131, 'featuredProducts.edit.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(132, 'featuredProducts.index', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(133, 'featuredProducts.index.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(134, 'featuredProducts.show', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(135, 'featuredProducts.show.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(136, 'featuredProducts.store.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(137, 'featuredProducts.update', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(138, 'featuredProducts.update.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(139, 'home', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(140, 'images.create.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(141, 'images.destroy', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(142, 'images.destroy.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(143, 'images.edit', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(144, 'images.edit.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(145, 'images.index', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(146, 'images.index.owned', 'web', '2020-08-27 06:59:47', '2020-08-27 06:59:47'),
	(147, 'images.show', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(148, 'images.show.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(149, 'images.store.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(150, 'images.update', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(151, 'images.update.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(152, 'income', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(153, 'items.create.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(154, 'items.destroy', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(155, 'items.destroy.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(156, 'items.edit', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(157, 'items.edit.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(158, 'items.index', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(159, 'items.index.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(160, 'items.show', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(161, 'items.show.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(162, 'items.store.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(163, 'items.update', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(164, 'items.update.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(165, 'logs.create.owned', 'web', '2020-08-27 06:59:48', '2020-08-27 06:59:48'),
	(166, 'logs.destroy', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(167, 'logs.destroy.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(168, 'logs.edit', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(169, 'logs.edit.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(170, 'logs.index', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(171, 'logs.index.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(172, 'logs.show', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(173, 'logs.show.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(174, 'logs.store.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(175, 'logs.update', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(176, 'logs.update.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(177, 'paymentMethods.create.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(178, 'paymentMethods.destroy', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(179, 'paymentMethods.destroy.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(180, 'paymentMethods.edit', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(181, 'paymentMethods.edit.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(182, 'paymentMethods.index', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(183, 'paymentMethods.index.owned', 'web', '2020-08-27 06:59:49', '2020-08-27 06:59:49'),
	(184, 'paymentMethods.show', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(185, 'paymentMethods.show.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(186, 'paymentMethods.store.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(187, 'paymentMethods.update', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(188, 'paymentMethods.update.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(189, 'permissions.create.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(190, 'permissions.destroy', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(191, 'permissions.destroy.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(192, 'permissions.edit', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(193, 'permissions.edit.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(194, 'permissions.index', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(195, 'permissions.index.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(196, 'permissions.show', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(197, 'permissions.show.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(198, 'permissions.store.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(199, 'permissions.update', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(200, 'permissions.update.owned', 'web', '2020-08-27 06:59:50', '2020-08-27 06:59:50'),
	(201, 'productPromoConditions.create.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(202, 'productPromoConditions.destroy', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(203, 'productPromoConditions.destroy.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(204, 'productPromoConditions.edit', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(205, 'productPromoConditions.edit.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(206, 'productPromoConditions.index', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(207, 'productPromoConditions.index.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(208, 'productPromoConditions.show', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(209, 'productPromoConditions.show.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(210, 'productPromoConditions.store.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(211, 'productPromoConditions.update', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(212, 'productPromoConditions.update.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(213, 'productPromos.create.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(214, 'productPromos.destroy', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(215, 'productPromos.destroy.owned', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(216, 'productPromos.edit', 'web', '2020-08-27 06:59:51', '2020-08-27 06:59:51'),
	(217, 'productPromos.edit.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(218, 'productPromos.index', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(219, 'productPromos.index.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(220, 'productPromos.show', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(221, 'productPromos.show.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(222, 'productPromos.store.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(223, 'productPromos.update', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(224, 'productPromos.update.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(225, 'profiles.create.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(226, 'profiles.destroy', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(227, 'profiles.destroy.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(228, 'profiles.edit', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(229, 'profiles.edit.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(230, 'profiles.index', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(231, 'profiles.index.owned', 'web', '2020-08-27 06:59:52', '2020-08-27 06:59:52'),
	(232, 'profiles.show', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(233, 'profiles.show.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(234, 'profiles.store.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(235, 'profiles.update', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(236, 'profiles.update.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(237, 'promoConditions.create.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(238, 'promoConditions.destroy', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(239, 'promoConditions.destroy.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(240, 'promoConditions.edit', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(241, 'promoConditions.edit.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(242, 'promoConditions.index', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(243, 'promoConditions.index.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(244, 'promoConditions.show', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(245, 'promoConditions.show.owned', 'web', '2020-08-27 06:59:53', '2020-08-27 06:59:53'),
	(246, 'promoConditions.store.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(247, 'promoConditions.update', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(248, 'promoConditions.update.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(249, 'purchaseItems.create.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(250, 'purchaseItems.destroy', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(251, 'purchaseItems.destroy.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(252, 'purchaseItems.edit', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(253, 'purchaseItems.edit.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(254, 'purchaseItems.index', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(255, 'purchaseItems.index.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(256, 'purchaseItems.show', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(257, 'purchaseItems.show.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(258, 'purchaseItems.store.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(259, 'purchaseItems.update', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(260, 'purchaseItems.update.owned', 'web', '2020-08-27 06:59:54', '2020-08-27 06:59:54'),
	(261, 'purchaseReturnItems.create.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(262, 'purchaseReturnItems.destroy', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(263, 'purchaseReturnItems.destroy.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(264, 'purchaseReturnItems.edit', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(265, 'purchaseReturnItems.edit.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(266, 'purchaseReturnItems.index', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(267, 'purchaseReturnItems.index.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(268, 'purchaseReturnItems.show', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(269, 'purchaseReturnItems.show.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(270, 'purchaseReturnItems.store.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(271, 'purchaseReturnItems.update', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(272, 'purchaseReturnItems.update.owned', 'web', '2020-08-27 06:59:55', '2020-08-27 06:59:55'),
	(273, 'purchaseReturns.create.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(274, 'purchaseReturns.destroy', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(275, 'purchaseReturns.destroy.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(276, 'purchaseReturns.edit', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(277, 'purchaseReturns.edit.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(278, 'purchaseReturns.index', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(279, 'purchaseReturns.index.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(280, 'purchaseReturns.show', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(281, 'purchaseReturns.show.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(282, 'purchaseReturns.store.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(283, 'purchaseReturns.update', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(284, 'purchaseReturns.update.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(285, 'purchases.create.owned', 'web', '2020-08-27 06:59:56', '2020-08-27 06:59:56'),
	(286, 'purchases.destroy', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(287, 'purchases.destroy.owned', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(288, 'purchases.edit', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(289, 'purchases.edit.owned', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(290, 'purchases.index', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(291, 'purchases.index.owned', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(292, 'purchases.show', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(293, 'purchases.show.owned', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(294, 'purchases.store.owned', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(295, 'purchases.update', 'web', '2020-08-27 06:59:57', '2020-08-27 06:59:57'),
	(296, 'purchases.update.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(297, 'recaps.create.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(298, 'recaps.destroy', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(299, 'recaps.destroy.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(300, 'recaps.edit', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(301, 'recaps.edit.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(302, 'recaps.index', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(303, 'recaps.index.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(304, 'recaps.show', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(305, 'recaps.show.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(306, 'recaps.store.owned', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(307, 'recaps.update', 'web', '2020-08-27 06:59:58', '2020-08-27 06:59:58'),
	(308, 'recaps.update.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(309, 'rejectedItems.create.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(310, 'rejectedItems.destroy', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(311, 'rejectedItems.destroy.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(312, 'rejectedItems.edit', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(313, 'rejectedItems.edit.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(314, 'rejectedItems.index', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(315, 'rejectedItems.index.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(316, 'rejectedItems.show', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(317, 'rejectedItems.show.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(318, 'rejectedItems.store.owned', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(319, 'rejectedItems.update', 'web', '2020-08-27 06:59:59', '2020-08-27 06:59:59'),
	(320, 'rejectedItems.update.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(321, 'roles.create.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(322, 'roles.destroy', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(323, 'roles.destroy.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(324, 'roles.edit', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(325, 'roles.edit.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(326, 'roles.index', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(327, 'roles.index.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(328, 'roles.show', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(329, 'roles.show.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(330, 'roles.store.owned', 'web', '2020-08-27 07:00:00', '2020-08-27 07:00:00'),
	(331, 'roles.update', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(332, 'roles.update.owned', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(333, 'routeList', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(334, 'sellItems.create.owned', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(335, 'sellItems.destroy', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(336, 'sellItems.destroy.owned', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(337, 'sellItems.edit', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(338, 'sellItems.edit.owned', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(339, 'sellItems.index', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(340, 'sellItems.index.owned', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(341, 'sellItems.show', 'web', '2020-08-27 07:00:01', '2020-08-27 07:00:01'),
	(342, 'sellItems.show.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(343, 'sellItems.store.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(344, 'sellItems.update', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(345, 'sellItems.update.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(346, 'sellReturnItems.create.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(347, 'sellReturnItems.destroy', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(348, 'sellReturnItems.destroy.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(349, 'sellReturnItems.edit', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(350, 'sellReturnItems.edit.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(351, 'sellReturnItems.index', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(352, 'sellReturnItems.index.owned', 'web', '2020-08-27 07:00:02', '2020-08-27 07:00:02'),
	(353, 'sellReturnItems.show', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(354, 'sellReturnItems.show.owned', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(355, 'sellReturnItems.store.owned', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(356, 'sellReturnItems.update', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(357, 'sellReturnItems.update.owned', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(358, 'sellReturns.create.owned', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(359, 'sellReturns.destroy', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(360, 'sellReturns.destroy.owned', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(361, 'sellReturns.edit', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(362, 'sellReturns.edit.owned', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(363, 'sellReturns.index', 'web', '2020-08-27 07:00:03', '2020-08-27 07:00:03'),
	(364, 'sellReturns.index.owned', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(365, 'sellReturns.show', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(366, 'sellReturns.show.owned', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(367, 'sellReturns.store.owned', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(368, 'sellReturns.update', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(369, 'sellReturns.update.owned', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(370, 'sells.create.owned', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(371, 'sells.destroy', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(372, 'sells.destroy.owned', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(373, 'sells.edit', 'web', '2020-08-27 07:00:04', '2020-08-27 07:00:04'),
	(374, 'sells.edit.owned', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(375, 'sells.index', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(376, 'sells.index.owned', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(377, 'sells.show', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(378, 'sells.show.owned', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(379, 'sells.store.owned', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(380, 'sells.update', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(381, 'sells.update.owned', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(382, 'settings.create.owned', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(383, 'settings.destroy', 'web', '2020-08-27 07:00:05', '2020-08-27 07:00:05'),
	(384, 'settings.destroy.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(385, 'settings.edit', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(386, 'settings.edit.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(387, 'settings.index', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(388, 'settings.index.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(389, 'settings.show', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(390, 'settings.show.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(391, 'settings.store.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(392, 'settings.update', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(393, 'settings.update.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(394, 'stockTransferItems.create.owned', 'web', '2020-08-27 07:00:06', '2020-08-27 07:00:06'),
	(395, 'stockTransferItems.destroy', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(396, 'stockTransferItems.destroy.owned', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(397, 'stockTransferItems.edit', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(398, 'stockTransferItems.edit.owned', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(399, 'stockTransferItems.index', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(400, 'stockTransferItems.index.owned', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(401, 'stockTransferItems.show', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(402, 'stockTransferItems.show.owned', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(403, 'stockTransferItems.store.owned', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(404, 'stockTransferItems.update', 'web', '2020-08-27 07:00:07', '2020-08-27 07:00:07'),
	(405, 'stockTransferItems.update.owned', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(406, 'stockTransfers.create.owned', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(407, 'stockTransfers.destroy', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(408, 'stockTransfers.destroy.owned', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(409, 'stockTransfers.edit', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(410, 'stockTransfers.edit.owned', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(411, 'stockTransfers.index', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(412, 'stockTransfers.index.owned', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(413, 'stockTransfers.show', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(414, 'stockTransfers.show.owned', 'web', '2020-08-27 07:00:08', '2020-08-27 07:00:08'),
	(415, 'stockTransfers.store.owned', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(416, 'stockTransfers.update', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(417, 'stockTransfers.update.owned', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(418, 'stocks.create.owned', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(419, 'stocks.destroy', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(420, 'stocks.destroy.owned', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(421, 'stocks.edit', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(422, 'stocks.edit.owned', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(423, 'stocks.index', 'web', '2020-08-27 07:00:09', '2020-08-27 07:00:09'),
	(424, 'stocks.index.owned', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(425, 'stocks.show', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(426, 'stocks.show.owned', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(427, 'stocks.store.owned', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(428, 'stocks.update', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(429, 'stocks.update.owned', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(430, 'storeFrontItems.create.owned', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(431, 'storeFrontItems.destroy', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(432, 'storeFrontItems.destroy.owned', 'web', '2020-08-27 07:00:10', '2020-08-27 07:00:10'),
	(433, 'storeFrontItems.edit', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(434, 'storeFrontItems.edit.owned', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(435, 'storeFrontItems.index', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(436, 'storeFrontItems.index.owned', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(437, 'storeFrontItems.show', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(438, 'storeFrontItems.show.owned', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(439, 'storeFrontItems.store.owned', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(440, 'storeFrontItems.update', 'web', '2020-08-27 07:00:11', '2020-08-27 07:00:11'),
	(441, 'storeFrontItems.update.owned', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(442, 'storeFrontStocks.create.owned', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(443, 'storeFrontStocks.destroy', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(444, 'storeFrontStocks.destroy.owned', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(445, 'storeFrontStocks.edit', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(446, 'storeFrontStocks.edit.owned', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(447, 'storeFrontStocks.index', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(448, 'storeFrontStocks.index.owned', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(449, 'storeFrontStocks.show', 'web', '2020-08-27 07:00:12', '2020-08-27 07:00:12'),
	(450, 'storeFrontStocks.show.owned', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(451, 'storeFrontStocks.store.owned', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(452, 'storeFrontStocks.update', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(453, 'storeFrontStocks.update.owned', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(454, 'storeFronts.create.owned', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(455, 'storeFronts.destroy', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(456, 'storeFronts.destroy.owned', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(457, 'storeFronts.edit', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(458, 'storeFronts.edit.owned', 'web', '2020-08-27 07:00:13', '2020-08-27 07:00:13'),
	(459, 'storeFronts.index', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(460, 'storeFronts.index.owned', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(461, 'storeFronts.show', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(462, 'storeFronts.show.owned', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(463, 'storeFronts.store.owned', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(464, 'storeFronts.update', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(465, 'storeFronts.update.owned', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(466, 'stores.create.owned', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(467, 'stores.destroy', 'web', '2020-08-27 07:00:14', '2020-08-27 07:00:14'),
	(468, 'stores.destroy.owned', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(469, 'stores.edit', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(470, 'stores.edit.owned', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(471, 'stores.index', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(472, 'stores.index.owned', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(473, 'stores.show', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(474, 'stores.show.owned', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(475, 'stores.store.owned', 'web', '2020-08-27 07:00:15', '2020-08-27 07:00:15'),
	(476, 'stores.update', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(477, 'stores.update.owned', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(478, 'suppliers.create.owned', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(479, 'suppliers.destroy', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(480, 'suppliers.destroy.owned', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(481, 'suppliers.edit', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(482, 'suppliers.edit.owned', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(483, 'suppliers.index', 'web', '2020-08-27 07:00:16', '2020-08-27 07:00:16'),
	(484, 'suppliers.index.owned', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(485, 'suppliers.show', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(486, 'suppliers.show.owned', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(487, 'suppliers.store.owned', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(488, 'suppliers.update', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(489, 'suppliers.update.owned', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(490, 'trusts.create.owned', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(491, 'trusts.destroy', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(492, 'trusts.destroy.owned', 'web', '2020-08-27 07:00:17', '2020-08-27 07:00:17'),
	(493, 'trusts.edit', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(494, 'trusts.edit.owned', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(495, 'trusts.index', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(496, 'trusts.index.owned', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(497, 'trusts.show', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(498, 'trusts.show.owned', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(499, 'trusts.store.owned', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(500, 'trusts.update', 'web', '2020-08-27 07:00:18', '2020-08-27 07:00:18'),
	(501, 'trusts.update.owned', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(502, 'units.create.owned', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(503, 'units.destroy', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(504, 'units.destroy.owned', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(505, 'units.edit', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(506, 'units.edit.owned', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(507, 'units.index', 'web', '2020-08-27 07:00:19', '2020-08-27 07:00:19'),
	(508, 'units.index.owned', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(509, 'units.show', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(510, 'units.show.owned', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(511, 'units.store.owned', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(512, 'units.update', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(513, 'units.update.owned', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(514, 'users.create.owned', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(515, 'users.destroy', 'web', '2020-08-27 07:00:20', '2020-08-27 07:00:20'),
	(516, 'users.destroy.owned', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(517, 'users.edit', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(518, 'users.edit.owned', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(519, 'users.index', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(520, 'users.index.owned', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(521, 'users.show', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(522, 'users.show.owned', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(523, 'users.store.owned', 'web', '2020-08-27 07:00:21', '2020-08-27 07:00:21'),
	(524, 'users.update', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(525, 'users.update.owned', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(526, 'wraps.create.owned', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(527, 'wraps.destroy', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(528, 'wraps.destroy.owned', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(529, 'wraps.edit', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(530, 'wraps.edit.owned', 'web', '2020-08-27 07:00:22', '2020-08-27 07:00:22'),
	(531, 'wraps.index', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23'),
	(532, 'wraps.index.owned', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23'),
	(533, 'wraps.show', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23'),
	(534, 'wraps.show.owned', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23'),
	(535, 'wraps.store.owned', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23'),
	(536, 'wraps.update', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23'),
	(537, 'wraps.update.owned', 'web', '2020-08-27 07:00:23', '2020-08-27 07:00:23');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table posku.product_promos
CREATE TABLE IF NOT EXISTS `product_promos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` int(11) NOT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_on` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_off` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.product_promos: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_promos` DISABLE KEYS */;
INSERT INTO `product_promos` (`id`, `item`, `unique_barcode`, `value`, `publish_on`, `publish_off`, `store`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, '10%', '2020-08-19 23:47:56', '2020-08-20 23:47:58', 1, 1, 1, '2020-08-19 16:48:37', '2020-08-19 16:48:37', NULL);
/*!40000 ALTER TABLE `product_promos` ENABLE KEYS */;

-- Dumping structure for table posku.product_promo_conditions
CREATE TABLE IF NOT EXISTS `product_promo_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_promo_id` int(11) NOT NULL,
  `condition` int(11) DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.product_promo_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_promo_conditions` DISABLE KEYS */;
INSERT INTO `product_promo_conditions` (`id`, `product_promo_id`, `condition`, `value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 1, '2,000', 1, 1, '2020-08-19 16:48:37', '2020-08-19 16:48:37', NULL);
/*!40000 ALTER TABLE `product_promo_conditions` ENABLE KEYS */;

-- Dumping structure for table posku.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.profiles: ~0 rows (approximately)
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;

-- Dumping structure for table posku.promo_conditions
CREATE TABLE IF NOT EXISTS `promo_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.promo_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `promo_conditions` DISABLE KEYS */;
INSERT INTO `promo_conditions` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Minimal Belanja', NULL, 1, 1, '2020-08-19 16:31:48', '2020-08-19 16:31:48', NULL);
/*!40000 ALTER TABLE `promo_conditions` ENABLE KEYS */;

-- Dumping structure for table posku.purchases
CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `receipt_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receipt_amount` double DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `balance` double DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.purchases: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;

-- Dumping structure for table posku.purchase_items
CREATE TABLE IF NOT EXISTS `purchase_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_discount` double DEFAULT NULL,
  `item_price` double NOT NULL,
  `item_amount` int(11) NOT NULL,
  `discount_subtotal` double DEFAULT NULL,
  `price_subtotal` double NOT NULL,
  `tax` double DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.purchase_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_items` ENABLE KEYS */;

-- Dumping structure for table posku.purchase_returns
CREATE TABLE IF NOT EXISTS `purchase_returns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `receipt_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_amount` double DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `balance` double DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.purchase_returns: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_returns` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_returns` ENABLE KEYS */;

-- Dumping structure for table posku.purchase_return_items
CREATE TABLE IF NOT EXISTS `purchase_return_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_return_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` double DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_amount` int(11) NOT NULL,
  `discount_subtotal` double DEFAULT NULL,
  `price_subtotal` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.purchase_return_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_return_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_return_items` ENABLE KEYS */;

-- Dumping structure for table posku.recaps
CREATE TABLE IF NOT EXISTS `recaps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.recaps: ~0 rows (approximately)
/*!40000 ALTER TABLE `recaps` DISABLE KEYS */;
/*!40000 ALTER TABLE `recaps` ENABLE KEYS */;

-- Dumping structure for table posku.rejected_items
CREATE TABLE IF NOT EXISTS `rejected_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` int(11) NOT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_amount` double NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.rejected_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `rejected_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `rejected_items` ENABLE KEYS */;

-- Dumping structure for table posku.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'web', '2020-08-09 08:36:35', '2020-08-09 08:36:35'),
	(2, 'staff', 'web', '2020-08-26 02:25:06', '2020-08-26 02:25:06');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table posku.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.role_has_permissions: ~488 rows (approximately)
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(8, 1),
	(10, 1),
	(12, 1),
	(14, 1),
	(15, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(21, 1),
	(23, 1),
	(25, 1),
	(27, 1),
	(28, 1),
	(30, 1),
	(31, 1),
	(33, 1),
	(35, 1),
	(37, 1),
	(39, 1),
	(40, 1),
	(42, 1),
	(43, 1),
	(45, 1),
	(47, 1),
	(49, 1),
	(51, 1),
	(52, 1),
	(54, 1),
	(55, 1),
	(57, 1),
	(59, 1),
	(61, 1),
	(63, 1),
	(64, 1),
	(66, 1),
	(67, 1),
	(69, 1),
	(71, 1),
	(73, 1),
	(75, 1),
	(76, 1),
	(78, 1),
	(79, 1),
	(81, 1),
	(83, 1),
	(85, 1),
	(87, 1),
	(88, 1),
	(90, 1),
	(91, 1),
	(93, 1),
	(95, 1),
	(97, 1),
	(99, 1),
	(100, 1),
	(102, 1),
	(103, 1),
	(105, 1),
	(107, 1),
	(109, 1),
	(111, 1),
	(112, 1),
	(114, 1),
	(115, 1),
	(116, 1),
	(118, 1),
	(120, 1),
	(122, 1),
	(124, 1),
	(125, 1),
	(127, 1),
	(128, 1),
	(130, 1),
	(132, 1),
	(134, 1),
	(136, 1),
	(137, 1),
	(139, 1),
	(140, 1),
	(141, 1),
	(143, 1),
	(145, 1),
	(147, 1),
	(149, 1),
	(150, 1),
	(152, 1),
	(153, 1),
	(154, 1),
	(156, 1),
	(158, 1),
	(160, 1),
	(162, 1),
	(163, 1),
	(165, 1),
	(166, 1),
	(168, 1),
	(170, 1),
	(172, 1),
	(174, 1),
	(175, 1),
	(177, 1),
	(178, 1),
	(180, 1),
	(182, 1),
	(184, 1),
	(186, 1),
	(187, 1),
	(189, 1),
	(190, 1),
	(192, 1),
	(194, 1),
	(196, 1),
	(198, 1),
	(199, 1),
	(201, 1),
	(202, 1),
	(204, 1),
	(206, 1),
	(208, 1),
	(210, 1),
	(211, 1),
	(213, 1),
	(214, 1),
	(216, 1),
	(218, 1),
	(220, 1),
	(222, 1),
	(223, 1),
	(225, 1),
	(226, 1),
	(228, 1),
	(230, 1),
	(232, 1),
	(234, 1),
	(235, 1),
	(237, 1),
	(238, 1),
	(240, 1),
	(242, 1),
	(244, 1),
	(246, 1),
	(247, 1),
	(249, 1),
	(250, 1),
	(252, 1),
	(254, 1),
	(256, 1),
	(258, 1),
	(259, 1),
	(261, 1),
	(262, 1),
	(264, 1),
	(266, 1),
	(268, 1),
	(270, 1),
	(271, 1),
	(273, 1),
	(274, 1),
	(276, 1),
	(278, 1),
	(280, 1),
	(282, 1),
	(283, 1),
	(285, 1),
	(286, 1),
	(288, 1),
	(290, 1),
	(292, 1),
	(294, 1),
	(295, 1),
	(297, 1),
	(298, 1),
	(300, 1),
	(302, 1),
	(304, 1),
	(306, 1),
	(307, 1),
	(309, 1),
	(310, 1),
	(312, 1),
	(314, 1),
	(316, 1),
	(318, 1),
	(319, 1),
	(321, 1),
	(322, 1),
	(324, 1),
	(326, 1),
	(328, 1),
	(330, 1),
	(331, 1),
	(333, 1),
	(334, 1),
	(335, 1),
	(337, 1),
	(339, 1),
	(341, 1),
	(343, 1),
	(344, 1),
	(346, 1),
	(347, 1),
	(349, 1),
	(351, 1),
	(353, 1),
	(355, 1),
	(356, 1),
	(358, 1),
	(359, 1),
	(361, 1),
	(363, 1),
	(365, 1),
	(367, 1),
	(368, 1),
	(370, 1),
	(371, 1),
	(373, 1),
	(375, 1),
	(377, 1),
	(379, 1),
	(380, 1),
	(382, 1),
	(383, 1),
	(385, 1),
	(387, 1),
	(389, 1),
	(391, 1),
	(392, 1),
	(394, 1),
	(395, 1),
	(397, 1),
	(399, 1),
	(401, 1),
	(403, 1),
	(404, 1),
	(406, 1),
	(407, 1),
	(409, 1),
	(411, 1),
	(413, 1),
	(415, 1),
	(416, 1),
	(418, 1),
	(419, 1),
	(421, 1),
	(423, 1),
	(425, 1),
	(427, 1),
	(428, 1),
	(430, 1),
	(431, 1),
	(433, 1),
	(435, 1),
	(437, 1),
	(439, 1),
	(440, 1),
	(442, 1),
	(443, 1),
	(445, 1),
	(447, 1),
	(449, 1),
	(451, 1),
	(452, 1),
	(454, 1),
	(455, 1),
	(457, 1),
	(459, 1),
	(461, 1),
	(463, 1),
	(464, 1),
	(466, 1),
	(467, 1),
	(469, 1),
	(471, 1),
	(473, 1),
	(475, 1),
	(476, 1),
	(478, 1),
	(479, 1),
	(481, 1),
	(483, 1),
	(485, 1),
	(487, 1),
	(488, 1),
	(490, 1),
	(491, 1),
	(493, 1),
	(495, 1),
	(497, 1),
	(499, 1),
	(500, 1),
	(502, 1),
	(503, 1),
	(505, 1),
	(507, 1),
	(509, 1),
	(511, 1),
	(512, 1),
	(514, 1),
	(515, 1),
	(517, 1),
	(519, 1),
	(521, 1),
	(523, 1),
	(524, 1),
	(526, 1),
	(527, 1),
	(529, 1),
	(531, 1),
	(533, 1),
	(535, 1),
	(536, 1),
	(4, 2),
	(5, 2),
	(14, 2),
	(23, 2),
	(25, 2),
	(30, 2),
	(39, 2),
	(42, 2),
	(45, 2),
	(47, 2),
	(49, 2),
	(51, 2),
	(52, 2),
	(59, 2),
	(61, 2),
	(66, 2),
	(69, 2),
	(71, 2),
	(73, 2),
	(75, 2),
	(76, 2),
	(78, 2),
	(81, 2),
	(83, 2),
	(85, 2),
	(87, 2),
	(88, 2),
	(90, 2),
	(93, 2),
	(95, 2),
	(97, 2),
	(99, 2),
	(100, 2),
	(102, 2),
	(105, 2),
	(107, 2),
	(109, 2),
	(111, 2),
	(112, 2),
	(115, 2),
	(118, 2),
	(120, 2),
	(122, 2),
	(124, 2),
	(125, 2),
	(127, 2),
	(130, 2),
	(132, 2),
	(134, 2),
	(136, 2),
	(137, 2),
	(139, 2),
	(145, 2),
	(147, 2),
	(153, 2),
	(156, 2),
	(158, 2),
	(160, 2),
	(162, 2),
	(163, 2),
	(177, 2),
	(180, 2),
	(182, 2),
	(184, 2),
	(186, 2),
	(187, 2),
	(206, 2),
	(208, 2),
	(213, 2),
	(216, 2),
	(218, 2),
	(220, 2),
	(222, 2),
	(223, 2),
	(225, 2),
	(229, 2),
	(233, 2),
	(234, 2),
	(236, 2),
	(237, 2),
	(240, 2),
	(242, 2),
	(244, 2),
	(246, 2),
	(247, 2),
	(252, 2),
	(254, 2),
	(256, 2),
	(266, 2),
	(268, 2),
	(273, 2),
	(276, 2),
	(278, 2),
	(280, 2),
	(282, 2),
	(283, 2),
	(285, 2),
	(288, 2),
	(290, 2),
	(292, 2),
	(294, 2),
	(295, 2),
	(309, 2),
	(312, 2),
	(314, 2),
	(316, 2),
	(318, 2),
	(319, 2),
	(339, 2),
	(341, 2),
	(351, 2),
	(353, 2),
	(358, 2),
	(361, 2),
	(363, 2),
	(365, 2),
	(367, 2),
	(368, 2),
	(370, 2),
	(373, 2),
	(375, 2),
	(377, 2),
	(379, 2),
	(380, 2),
	(385, 2),
	(387, 2),
	(389, 2),
	(391, 2),
	(392, 2),
	(399, 2),
	(401, 2),
	(406, 2),
	(409, 2),
	(411, 2),
	(413, 2),
	(415, 2),
	(416, 2),
	(423, 2),
	(425, 2),
	(435, 2),
	(437, 2),
	(447, 2),
	(449, 2),
	(454, 2),
	(457, 2),
	(459, 2),
	(461, 2),
	(463, 2),
	(464, 2),
	(466, 2),
	(469, 2),
	(471, 2),
	(473, 2),
	(475, 2),
	(476, 2),
	(478, 2),
	(481, 2),
	(483, 2),
	(485, 2),
	(487, 2),
	(488, 2),
	(502, 2),
	(505, 2),
	(507, 2),
	(509, 2),
	(511, 2),
	(512, 2),
	(526, 2),
	(529, 2),
	(531, 2),
	(533, 2),
	(535, 2),
	(536, 2);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Dumping structure for table posku.sells
CREATE TABLE IF NOT EXISTS `sells` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` int(11) NOT NULL,
  `bill_amount` double NOT NULL,
  `table_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_order_status` int(11) DEFAULT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `selling_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `balance` double DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.sells: ~0 rows (approximately)
/*!40000 ALTER TABLE `sells` DISABLE KEYS */;
/*!40000 ALTER TABLE `sells` ENABLE KEYS */;

-- Dumping structure for table posku.sell_items
CREATE TABLE IF NOT EXISTS `sell_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` double DEFAULT NULL,
  `item_price` double NOT NULL,
  `item_amount` int(11) NOT NULL,
  `discount_subtotal` double DEFAULT NULL,
  `price_subtotal` double NOT NULL,
  `tax` double DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.sell_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `sell_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `sell_items` ENABLE KEYS */;

-- Dumping structure for table posku.sell_returns
CREATE TABLE IF NOT EXISTS `sell_returns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store` int(11) NOT NULL,
  `bill_amount` double DEFAULT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `balance` double DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.sell_returns: ~0 rows (approximately)
/*!40000 ALTER TABLE `sell_returns` DISABLE KEYS */;
/*!40000 ALTER TABLE `sell_returns` ENABLE KEYS */;

-- Dumping structure for table posku.sell_return_items
CREATE TABLE IF NOT EXISTS `sell_return_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sell_return_id` int(11) NOT NULL,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` double DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_amount` int(11) NOT NULL,
  `discount_subtotal` double DEFAULT NULL,
  `price_subtotal` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `item_return_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.sell_return_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `sell_return_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `sell_return_items` ENABLE KEYS */;

-- Dumping structure for table posku.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table posku.stocks
CREATE TABLE IF NOT EXISTS `stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_amount` double NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.stocks: ~0 rows (approximately)
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;

-- Dumping structure for table posku.stock_transfers
CREATE TABLE IF NOT EXISTS `stock_transfers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.stock_transfers: ~0 rows (approximately)
/*!40000 ALTER TABLE `stock_transfers` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_transfers` ENABLE KEYS */;

-- Dumping structure for table posku.stock_transfer_items
CREATE TABLE IF NOT EXISTS `stock_transfer_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stock_transfer_id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wrap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.stock_transfer_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `stock_transfer_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_transfer_items` ENABLE KEYS */;

-- Dumping structure for table posku.stores
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `store_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.stores: ~0 rows (approximately)
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` (`id`, `name`, `description`, `image`, `store_type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Toko 1', NULL, 'http://localhost/posku/public/storage/ca373ba91792e3c5452b2741a4bdf0bf.jpg', 'In-House', 1, 1, '2020-08-09 13:02:00', '2020-08-12 14:31:21', NULL),
	(2, 'Toko 2', NULL, NULL, 'In-House', 1, 1, '2020-08-16 15:22:09', '2020-08-16 15:22:09', NULL);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;

-- Dumping structure for table posku.store_fronts
CREATE TABLE IF NOT EXISTS `store_fronts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `is_draft` int(11) DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.store_fronts: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_fronts` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_fronts` ENABLE KEYS */;

-- Dumping structure for table posku.store_front_items
CREATE TABLE IF NOT EXISTS `store_front_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_front_id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_amount` int(11) NOT NULL,
  `unique_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.store_front_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_front_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_front_items` ENABLE KEYS */;

-- Dumping structure for table posku.store_front_stocks
CREATE TABLE IF NOT EXISTS `store_front_stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `item_amount` double NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.store_front_stocks: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_front_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_front_stocks` ENABLE KEYS */;

-- Dumping structure for table posku.suppliers
CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.suppliers: ~0 rows (approximately)
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` (`id`, `name`, `description`, `phone`, `email`, `address`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Supplier 1', NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-08-09 13:49:59', '2020-08-09 13:49:59', NULL);
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;

-- Dumping structure for table posku.telescope_entries
CREATE TABLE IF NOT EXISTS `telescope_entries` (
  `sequence` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  KEY `telescope_entries_batch_id_index` (`batch_id`),
  KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`),
  KEY `telescope_entries_family_hash_index` (`family_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.telescope_entries: ~111 rows (approximately)
/*!40000 ALTER TABLE `telescope_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `telescope_entries` ENABLE KEYS */;

-- Dumping structure for table posku.telescope_entries_tags
CREATE TABLE IF NOT EXISTS `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  KEY `telescope_entries_tags_tag_index` (`tag`),
  CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.telescope_entries_tags: ~69 rows (approximately)
/*!40000 ALTER TABLE `telescope_entries_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `telescope_entries_tags` ENABLE KEYS */;

-- Dumping structure for table posku.telescope_monitoring
CREATE TABLE IF NOT EXISTS `telescope_monitoring` (
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.telescope_monitoring: ~0 rows (approximately)
/*!40000 ALTER TABLE `telescope_monitoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `telescope_monitoring` ENABLE KEYS */;

-- Dumping structure for table posku.trusts
CREATE TABLE IF NOT EXISTS `trusts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `transaction_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` double DEFAULT NULL,
  `attendance_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.trusts: ~0 rows (approximately)
/*!40000 ALTER TABLE `trusts` DISABLE KEYS */;
/*!40000 ALTER TABLE `trusts` ENABLE KEYS */;

-- Dumping structure for table posku.units
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.units: ~0 rows (approximately)
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` (`id`, `name`, `code`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Piece', 'PCS', NULL, 1, 1, '2020-08-09 13:01:03', '2020-08-09 13:01:03', NULL);
/*!40000 ALTER TABLE `units` ENABLE KEYS */;

-- Dumping structure for table posku.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notif_token` text COLLATE utf8mb4_unicode_ci,
  `forgot_token` text COLLATE utf8mb4_unicode_ci,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `full_name`, `reference_code`, `email_verified_at`, `password`, `notif_token`, `forgot_token`, `is_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@app.com', NULL, NULL, NULL, '$2y$10$neJ0/dKntYYvP20fka52seZiGev56rrGfCEqMvztb5zdN/hCg5mF6', NULL, NULL, 0, NULL, '2020-08-09 08:36:31', '2020-08-09 08:36:31'),
	(2, 'staff', 'staff@app.com', NULL, NULL, NULL, '$2y$10$iDHc78jQQd2xN7dfwCRq8OOXI/J0YtUQ2KM0O5vr3P5d1pqJFOkiy', NULL, NULL, 0, NULL, '2020-08-26 04:57:48', '2020-08-26 04:57:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table posku.wraps
CREATE TABLE IF NOT EXISTS `wraps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table posku.wraps: ~0 rows (approximately)
/*!40000 ALTER TABLE `wraps` DISABLE KEYS */;
INSERT INTO `wraps` (`id`, `name`, `code`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Box', 'BOX', NULL, 1, 1, '2020-08-09 13:01:26', '2020-08-09 13:01:26', NULL);
/*!40000 ALTER TABLE `wraps` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
