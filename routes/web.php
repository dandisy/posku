<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::post('uploadCropped', function(Request $request) {
    $folderPath = storage_path('app/public');

    $image_parts = explode(";base64,", $request->img);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);
    $file = $folderPath . '/cropped-' . uniqid() . '.'.$image_type;

    file_put_contents($file, $image_base64);
});

Route::get('about', function() {
    return view('about');
});


// Route::get('rulable-type', function(Request $request) {
//     $ns = '\\'.$request->type;
//     $model = new $ns();

//     return $model->get();
// });

Route::get('/routeList', function() {
    $routes = [];
    // dump(new ReflectionClass('Illuminate\Routing\RouteCollection'))->getMethods();
    foreach(\Illuminate\Support\Facades\Route::getRoutes()->get() as $route) {
        if($route->action['namespace'] == 'App\Http\Controllers' && $route->action['prefix'] == null) {
            if($route->action['uses'] instanceof Closure) {
                $clo = new ReflectionFunction($route->action['uses']);
                if(isset($route->action['as'])) {
                    // dump($route->action['as']);
                    $routes[$route->action['as']] = $route->action['as'];
                } else {
                    // dump('closure'.$clo->getStartLine() . 'to' . $clo->getEndLine());
                    $routes['closure'.$clo->getStartLine() . 'to' . $clo->getEndLine()] = 'closure'.$clo->getStartLine() . 'to' . $clo->getEndLine();
                }
            } else {
                if(substr($route->action['uses'], 0, 25) !== "App\Http\Controllers\Auth") {
                    if(isset($route->action['as'])) {
                        // dump($route->action['as']);
                        $routes[$route->action['as']] = $route->action['as'];
                    } else {
                        // dump($route->action['uses']);
                        $routes[$route->action['uses']] = $route->action['uses'];
                    }
                }
            }
        }
    }

    ksort($routes);
    dd($routes);
})->name('routeList');


Route::get('/', function () {
    return view('welcome');
});


Auth::routes(['register' => false]);

// /img/folderImage/fileImage.jpg
Route::get('/img/{path?}', function(Filesystem $filesystem, $path) {
    $server = ServerFactory::create([
        'response' => new LaravelResponseFactory(app('request')),
        'source' => $filesystem->getDriver(),
        'cache' => $filesystem->getDriver(),
        'cache_path_prefix' => '.cache',
        'base_url' => 'img',
    ]);
    
    if($filesystem->exists('public/'.$path)) {
        return $server->getImageResponse('public/'.$path, request()->all());
    } 
    
    // return $server->getImageResponse('public/default.jpg', []);
    return abort(404);
})->where('path', '.*');


Route::get('/check-sso', function () {
    // $analyze = new \SEOCheckup\Analyze('https://www.blackxperience.com/blackinnovation/blackbox/ilmuwan-kembangkan-hologram-di-kaca-depan-mobil-navigasi-lebih-aman');
    $analyze = (new \SeoAnalyzer\Analyzer())->analyzeUrl('https://www.blackxperience.com/blackinnovation/blackbox/ilmuwan-kembangkan-hologram-di-kaca-depan-mobil-navigasi-lebih-aman', 'otomotif');

    // dd($analyze->MetaDescription());
    dd($analyze);
});

Route::get('/fcm-test', function () {
    return view('fcm_test');
});

Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => 3,
        'redirect_uri' => 'http://localhost/gamify/public/auth/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect(url('oauth/authorize?'.$query));
});

Route::get('auth/callback', function(Request $request) {
    echo $request->code;
});

// Route::get('/callback', function (Request $request) {
//     $http = new GuzzleHttp\Client;

//     $response = $http->post('http://your-app.com/oauth/token', [
//         'form_params' => [
//             'grant_type' => 'authorization_code',
//             'client_id' => 'client-id',
//             'client_secret' => 'client-secret',
//             'redirect_uri' => 'http://example.com/callback',
//             'code' => $request->code,
//         ],
//     ]);

//     return json_decode((string) $response->getBody(), true);
// });

Route::get('api-docs', function() {
    // return view('api_docs');
    return view('api_docs.index');
})->name('apiDocs');


// Route::get('/setAdmin', function () {
//     $role = \App\Role::create(['name' => 'admin', 'guard_name' => 'web']);
//     $user = \App\User::find(1);
//     $user->assignRole('admin');

//     return redirect('/home');
// });

Route::get('/createRole', function(Request $request) {
    \Spatie\Permission\Models\Role::create(['name' => $request->name]);
});

Route::get('/getPromo/{id}', function ($id, Request $request) {
    $userStore = \App\Models\Attendance::where('created_by', $request->user()->id)->latest()->first()->store;
    return \App\Models\ProductPromo::with('conditions')->where('store', $userStore)->where('item', $id)->first();
});


Route::middleware(['role:admin'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::resource('users', 'UserController');
    // Route::post('importUser', 'UserController@import');

    Route::resource('roles', 'RoleController');
    // Route::post('importRole', 'RoleController@import');

    Route::resource('permissions', 'PermissionController');
    // Route::post('importPermission', 'PermissionController@import');

    Route::get('/accessControl', function(Request $request) {
        // models
        $models = array_map(function ($file) {
            $fileName = explode('.', $file);
            if(count($fileName) > 0) {
                return $fileName[0];
            }
        }, Storage::disk('model')->allFiles());
        // $models = array_combine($models, $models);

        $users = \App\User::all();
        $roles = \Spatie\Permission\Models\Role::all();

        $permissions = collect();
        if($request->type == 'user') {
            $permissions = (\App\User::find($request->id))->getAllPermissions();
        }
        if($request->type == 'role') {
            $permissions = (\Spatie\Permission\Models\Role::find($request->id))->getAllPermissions();
        }
        $permissions = $permissions->pluck('name')->toArray();
        $permissions = array_combine($permissions, $permissions);

        // routes
        $routes = [];
        foreach(\Illuminate\Support\Facades\Route::getRoutes()->get() as $route) {
            if($route->action['namespace'] == 'App\Http\Controllers' && $route->action['prefix'] == null) {
                if($route->action['uses'] instanceof Closure) {
                    $clo = new ReflectionFunction($route->action['uses']);
                    if(isset($route->action['as'])) {
                        $routes[$route->action['as']] = $route->action['as'];
                    } else {
                        // $routes['closure'.$clo->getStartLine() . 'to' . $clo->getEndLine()] = 'closure'.$clo->getStartLine() . 'to' . $clo->getEndLine();
                    }
                } else {
                    if(substr($route->action['uses'], 0, 25) !== "App\Http\Controllers\Auth") {
                        if(isset($route->action['as'])) {
                            $routes[$route->action['as']] = $route->action['as'];
                        } else {
                            $routes[$route->action['uses']] = $route->action['uses'];
                        }
                    }
                }
            }
        }
        ksort($routes);

        return view('accessControl', compact('models', 'users', 'roles', 'permissions', 'routes'));
    })->name('accessControl.index');

    Route::post('/accessControl', function(Request $request) {
        $m = null;
        $toPermission = explode('.', $request->user_role);
        if($toPermission[0] == 'user') {
            $m = \Spatie\Permission\Models\User::find($toPermission[1]);
        }
        if($toPermission[0] == 'role') {
            $m = \Spatie\Permission\Models\Role::find($toPermission[1]);
        }
        
        foreach($request->permissions as $k => $v) {
            \Spatie\Permission\Models\Permission::firstOrCreate(['name' => "$k"]);

            if($v == 'on') {
                $m->givePermissionTo($k);
            } else {
                $m->revokePermissionTo($k);
            }
        }

        if($request->ajax()){
            return $m;
        }

        return redirect('/accessControl?type='.$toPermission[0].'&id='.$toPermission[1]);
    })->name('accessControl.store');

    Route::get('/balance', function () {
        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $cashes = \App\Models\Cash::where('store', $userStore)->get();
        // $debts = \App\Models\Debt::where('store', $userStore)->get();
        $stocks = \App\Models\Stock::where('store', $userStore)->get();

        // $cashDebit = 0;
        // $cashCredit = 0;
        // foreach($cashes as $k => $cash) {
        //     $cashDebit += $cash->debit;
        //     $cashCredit += $cash->credit;
        // }
        // $cashValue = $cashDebit - $cashCredit;
        $cashValue= \App\Models\Cash::where('store', $userStore)->sum('value');


        // $debtDebit = 0;
        // $debtCredit = 0;
        // foreach($debts as $k => $debt) {
        //     $debtDebit += $debt->debit;
        //     $debtCredit += $debt->credit;
        // }
        // $debtValue = $debtCredit - $debtDebit;
        $debtValue = \App\Models\Debt::where('store', $userStore)->sum('value');

        $stockValue = 0;
        foreach($stocks as $k => $stock) {
            $stockValue += $stock->item_amount * \App\Models\Item::find($stock->item)->purchase_price;
        }

        $capital = ($stockValue + $cashValue) - $debtValue;

        return view('balances.index', compact('cashValue', 'debtValue', 'stockValue', 'capital'));
    })->name('balance');

    Route::get('/income', function () {
        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        $costValue = \App\Models\Cost::where('store', $userStore)->sum('value');
        $sellItems = \App\Models\SellItem::where('store', $userStore)->get();
        // $stocks = \App\Models\Stock::where('store', $userStore)->get();

        // $stockValue = 0;
        // foreach($stocks as $k => $stock) {
        //     $stockValue += $stock->item_amount * \App\Models\Item::find($stock->item)->purchase_price;
        // }
        
        $sellingSellPriceValue = 0;
        $sellingPurchasePriceValue = 0;
        foreach($sellItems as $k => $sellItem) {
            $item = \App\Models\Item::find($sellItem->item);
            $sellingSellPriceValue += $sellItem->item_amount * $item->sell_price;
            $sellingPurchasePriceValue += $sellItem->item_amount * $item->purchase_price;
        }

        return view('incomes.index', compact('costValue', 'sellingSellPriceValue', 'sellingPurchasePriceValue'));
    })->name('income');
});

Route::get('settings', 'SettingController@index')->middleware(['can:settings.index'])->name('settings.index');
Route::get('settings/create', 'SettingController@create')->middleware(['can:settings.create.owned'])->name('settings.create');
Route::post('settings', 'SettingController@store')->middleware(['can:settings.store.owned'])->name('settings.store');
Route::get('settings/{setting}', 'SettingController@show')->middleware(['can:settings.show'])->name('settings.show');
Route::get('settings/{setting}/edit', 'SettingController@edit')->middleware(['can:settings.edit'])->name('settings.edit');
Route::put('settings/{setting}', 'SettingController@update')->middleware(['can:settings.update'])->name('settings.update');
Route::patch('settings/{setting}', 'SettingController@update')->middleware(['can:settings.update'])->name('settings.update');
Route::delete('settings/{setting}', 'SettingController@destroy')->middleware(['can:settings.destroy'])->name('settings.destroy');
// Route::resource('settings', 'SettingController');
// Route::post('importSetting', 'SettingController@import');

Route::get('profiles', 'ProfileController@index')->middleware(['can:profiles.index'])->name('profiles.index');
Route::get('profiles/create', 'ProfileController@create')->middleware(['can:profiles.create.owned'])->name('profiles.create');
Route::post('profiles', 'ProfileController@store')->middleware(['can:profiles.store.owned'])->name('profiles.store');
Route::get('profiles/{profile}', 'ProfileController@show')->middleware(['can:profiles.show'])->name('profiles.show');
Route::get('profiles/{profile}/edit', 'ProfileController@edit')->middleware(['can:profiles.edit'])->name('profiles.edit');
Route::put('profiles/{profile}', 'ProfileController@update')->middleware(['can:profiles.update'])->name('profiles.update');
Route::patch('profiles/{profile}', 'ProfileController@update')->middleware(['can:profiles.update'])->name('profiles.update');
Route::delete('profiles/{profile}', 'ProfileController@destroy')->middleware(['can:profiles.destroy'])->name('profiles.destroy');
// Route::resource('profiles', 'ProfileController');
// Route::post('importProfile', 'ProfileController@import');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('attendances', 'AttendanceController@index')->middleware(['can:attendances.index'])->name('attendances.index');
Route::get('attendances/create', 'AttendanceController@create')->middleware(['can:attendances.create.owned'])->name('attendances.create');
Route::post('attendances', 'AttendanceController@store')->middleware(['can:attendances.store.owned'])->name('attendances.store');
Route::get('attendances/{attendance}', 'AttendanceController@show')->middleware(['can:attendances.show'])->name('attendances.show');
Route::get('attendances/{attendance}/edit', 'AttendanceController@edit')->middleware(['can:attendances.edit'])->name('attendances.edit');
Route::put('attendances/{attendance}', 'AttendanceController@update')->middleware(['can:attendances.update'])->name('attendances.update');
Route::patch('attendances/{attendance}', 'AttendanceController@update')->middleware(['can:attendances.update'])->name('attendances.update');
Route::delete('attendances/{attendance}', 'AttendanceController@destroy')->middleware(['can:attendances.destroy'])->name('attendances.destroy');
// Route::resource('attendances', 'AttendanceController');
// Route::post('importAttendance', 'AttendanceController@import');

Route::get('cashes', 'CashController@index')->middleware(['can:cashes.index'])->name('cashes.index');
Route::get('cashes/create', 'CashController@create')->middleware(['can:cashes.create.owned'])->name('cashes.create');
Route::post('cashes', 'CashController@store')->middleware(['can:cashes.store.owned'])->name('cashes.store');
Route::get('cashes/{cash}', 'CashController@show')->middleware(['can:cashes.show'])->name('cashes.show');
Route::get('cashes/{cash}/edit', 'CashController@edit')->middleware(['can:cashes.edit'])->name('cashes.edit');
Route::put('cashes/{cash}', 'CashController@update')->middleware(['can:cashes.update'])->name('cashes.update');
Route::patch('cashes/{cash}', 'CashController@update')->middleware(['can:cashes.update'])->name('cashes.update');
Route::delete('cashes/{cash}', 'CashController@destroy')->middleware(['can:cashes.destroy'])->name('cashes.destroy');
// Route::resource('cashes', 'CashController');
// Route::post('importCash', 'CashController@import');

Route::get('categories', 'CategoryController@index')->middleware(['can:categories.index'])->name('categories.index');
Route::get('categories/create', 'CategoryController@create')->middleware(['can:categories.create.owned'])->name('categories.create');
Route::post('categories', 'CategoryController@store')->middleware(['can:categories.store.owned'])->name('categories.store');
Route::get('categories/{category}', 'CategoryController@show')->middleware(['can:categories.index'])->name('categories.show');
Route::get('categories/{category}/edit', 'CategoryController@edit')->middleware(['can:categories.edit'])->name('categories.edit');
Route::put('categories/{category}', 'CategoryController@update')->middleware(['can:categories.update'])->name('categories.update');
Route::patch('categories/{category}', 'CategoryController@update')->middleware(['can:categories.update'])->name('categories.update');
Route::delete('categories/{category}', 'CategoryController@destroy')->middleware(['can:categories.destroy'])->name('categories.destroy');
// Route::resource('categories', 'CategoryController');
// Route::post('importCategory', 'CategoryController@import');

Route::get('costs', 'CostController@index')->middleware(['can:costs.index'])->name('costs.index');
Route::get('costs/create', 'CostController@create')->middleware(['can:costs.create.owned'])->name('costs.create');
Route::post('costs', 'CostController@store')->middleware(['can:costs.store.owned'])->name('costs.store');
Route::get('costs/{cost}', 'CostController@show')->middleware(['can:costs.index'])->name('costs.show');
Route::get('costs/{cost}/edit', 'CostController@edit')->middleware(['can:costs.edit'])->name('costs.edit');
Route::put('costs/{cost}', 'CostController@update')->middleware(['can:costs.update'])->name('costs.update');
Route::patch('costs/{cost}', 'CostController@update')->middleware(['can:costs.update'])->name('costs.update');
Route::delete('costs/{cost}', 'CostController@destroy')->middleware(['can:costs.destroy'])->name('costs.destroy');
// Route::resource('costs', 'CostController');
// Route::post('importCost', 'CostController@import');

Route::get('customers', 'CustomerController@index')->middleware(['can:customers.index'])->name('customers.index');
Route::get('customers/create', 'CustomerController@create')->middleware(['can:customers.create.owned'])->name('customers.create');
Route::post('customers', 'CustomerController@store')->middleware(['can:customers.store.owned'])->name('customers.store');
Route::get('customers/{customer}', 'CustomerController@show')->middleware(['can:customers.show'])->name('customers.show');
Route::get('customers/{customer}/edit', 'CustomerController@edit')->middleware(['can:customers.edit'])->name('customers.edit');
Route::put('customers/{customer}', 'CustomerController@update')->middleware(['can:customers.update'])->name('customers.update');
Route::patch('customers/{customer}', 'CustomerController@update')->middleware(['can:customers.update'])->name('customers.update');
Route::delete('customers/{customer}', 'CustomerController@destroy')->middleware(['can:customers.destroy'])->name('customers.destroy');
// Route::resource('customers', 'CustomerController');
// Route::post('importCustomer', 'CustomerController@import');

Route::get('logs', 'LogController@index')->middleware(['can:logs.index'])->name('logs.index');
Route::get('logs/create', 'LogController@create')->middleware(['can:logs.create.owned'])->name('logs.create');
Route::post('logs', 'LogController@store')->middleware(['can:logs.store.owned'])->name('logs.store');
Route::get('logs/{log}', 'LogController@show')->middleware(['can:logs.show'])->name('logs.show');
Route::get('logs/{log}/edit', 'LogController@edit')->middleware(['can:logs.edit'])->name('logs.edit');
Route::put('logs/{log}', 'LogController@update')->middleware(['can:logs.update'])->name('logs.update');
Route::patch('logs/{log}', 'LogController@update')->middleware(['can:logs.update'])->name('logs.update');
Route::delete('logs/{log}', 'LogController@destroy')->middleware(['can:logs.destroy'])->name('logs.destroy');
// Route::resource('logs', 'LogController');
// Route::post('importLog', 'LogController@import');

Route::get('paymentMethods', 'PaymentMethodController@index')->middleware(['can:paymentMethods.index'])->name('paymentMethods.index');
Route::get('paymentMethods/create', 'PaymentMethodController@create')->middleware(['can:paymentMethods.create.owned'])->name('paymentMethods.create');
Route::post('paymentMethods', 'PaymentMethodController@store')->middleware(['can:paymentMethods.store.owned'])->name('paymentMethods.store');
Route::get('paymentMethods/{paymentMethod}', 'PaymentMethodController@show')->middleware(['can:paymentMethods.show'])->name('paymentMethods.show');
Route::get('paymentMethods/{paymentMethod}/edit', 'PaymentMethodController@edit')->middleware(['can:paymentMethods.edit'])->name('paymentMethods.edit');
Route::put('paymentMethods/{paymentMethod}', 'PaymentMethodController@update')->middleware(['can:paymentMethods.update'])->name('paymentMethods.update');
Route::patch('paymentMethods/{paymentMethod}', 'PaymentMethodController@update')->middleware(['can:paymentMethods.update'])->name('paymentMethods.update');
Route::delete('paymentMethods/{paymentMethod}', 'PaymentMethodController@destroy')->middleware(['can:paymentMethods.destroy'])->name('paymentMethods.destroy');
// Route::resource('paymentMethods', 'PaymentMethodController');
// Route::post('importPaymentMethod', 'PaymentMethodController@import');

Route::get('productPromos', 'ProductPromoController@index')->middleware(['can:productPromos.index'])->name('productPromos.index');
Route::get('productPromos/create', 'ProductPromoController@create')->middleware(['can:productPromos.create.owned'])->name('productPromos.create');
Route::post('productPromos', 'ProductPromoController@store')->middleware(['can:productPromos.store.owned'])->name('productPromos.store');
Route::get('productPromos/{productPromo}', 'ProductPromoController@show')->middleware(['can:productPromos.show'])->name('productPromos.show');
Route::get('productPromos/{productPromo}/edit', 'ProductPromoController@edit')->middleware(['can:productPromos.edit'])->name('productPromos.edit');
Route::put('productPromos/{productPromo}', 'ProductPromoController@update')->middleware(['can:productPromos.update'])->name('productPromos.update');
Route::patch('productPromos/{productPromo}', 'ProductPromoController@update')->middleware(['can:productPromos.update'])->name('productPromos.update');
Route::delete('productPromos/{productPromo}', 'ProductPromoController@destroy')->middleware(['can:productPromos.destroy'])->name('productPromos.destroy');
// Route::resource('productPromos', 'ProductPromoController');
// Route::post('importProductPromo', 'ProductPromoController@import');

Route::get('productPromoConditions', 'ProductPromoConditionController@index')->middleware(['can:productPromoConditions.index'])->name('productPromoConditions.index');
Route::get('productPromoConditions/create', 'ProductPromoConditionController@create')->middleware(['can:productPromoConditions.create.owned'])->name('productPromoConditions.create');
Route::post('productPromoConditions', 'ProductPromoConditionController@store')->middleware(['can:productPromoConditions.store.owned'])->name('productPromoConditions.store');
Route::get('productPromoConditions/{productPromoCondition}', 'ProductPromoConditionController@show')->middleware(['can:productPromoConditions.show'])->name('productPromoConditions.show');
Route::get('productPromoConditions/{productPromoCondition}/edit', 'ProductPromoConditionController@edit')->middleware(['can:productPromoConditions.edit'])->name('productPromoConditions.edit');
Route::put('productPromoConditions/{productPromoCondition}', 'ProductPromoConditionController@update')->middleware(['can:productPromoConditions.update'])->name('productPromoConditions.update');
Route::patch('productPromoConditions/{productPromoCondition}', 'ProductPromoConditionController@update')->middleware(['can:productPromoConditions.update'])->name('productPromoConditions.update');
Route::delete('productPromoConditions/{productPromoCondition}', 'ProductPromoConditionController@destroy')->middleware(['can:productPromoConditions.destroy'])->name('productPromoConditions.destroy');
// Route::resource('productPromoConditions', 'ProductPromoConditionController');
// Route::post('importProductPromoCondition', 'ProductPromoConditionController@import');

Route::get('promoConditions', 'PromoConditionController@index')->middleware(['can:promoConditions.index'])->name('promoConditions.index');
Route::get('promoConditions/create', 'PromoConditionController@create')->middleware(['can:promoConditions.create.owned'])->name('promoConditions.create');
Route::post('promoConditions', 'PromoConditionController@store')->middleware(['can:promoConditions.store.owned'])->name('promoConditions.store');
Route::get('promoConditions/{promoCondition}', 'PromoConditionController@show')->middleware(['can:promoConditions.show'])->name('promoConditions.show');
Route::get('promoConditions/{promoCondition}/edit', 'PromoConditionController@edit')->middleware(['can:promoConditions.edit'])->name('promoConditions.edit');
Route::put('promoConditions/{promoCondition}', 'PromoConditionController@update')->middleware(['can:promoConditions.update'])->name('promoConditions.update');
Route::patch('promoConditions/{promoCondition}', 'PromoConditionController@update')->middleware(['can:promoConditions.update'])->name('promoConditions.update');
Route::delete('promoConditions/{promoCondition}', 'PromoConditionController@destroy')->middleware(['can:promoConditions.destroy'])->name('promoConditions.destroy');
// Route::resource('promoConditions', 'PromoConditionController');
// Route::post('importPromoCondition', 'PromoConditionController@import');

Route::get('purchases', 'PurchaseController@index')->middleware(['can:purchases.index'])->name('purchases.index');
Route::get('purchases/create', 'PurchaseController@create')->middleware(['can:purchases.create.owned'])->name('purchases.create');
Route::post('purchases', 'PurchaseController@store')->middleware(['can:purchases.store.owned'])->name('purchases.store');
Route::get('purchases/{purchase}', 'PurchaseController@show')->middleware(['can:purchases.show'])->name('purchases.show');
Route::get('purchases/{purchase}/edit', 'PurchaseController@edit')->middleware(['can:purchases.edit'])->name('purchases.edit');
Route::put('purchases/{purchase}', 'PurchaseController@update')->middleware(['can:purchases.update'])->name('purchases.update');
Route::patch('purchases/{purchase}', 'PurchaseController@update')->middleware(['can:purchases.update'])->name('purchases.update');
Route::delete('purchases/{purchase}', 'PurchaseController@destroy')->middleware(['can:purchases.destroy'])->name('purchases.destroy');
// Route::resource('purchases', 'PurchaseController');
// Route::post('importPurchase', 'PurchaseController@import');

Route::get('purchaseItems', 'PurchaseItemController@index')->middleware(['can:purchaseItems.index'])->name('purchaseItems.index');
Route::get('purchaseItems/create', 'PurchaseItemController@create')->middleware(['can:purchaseItems.create.owned'])->name('purchaseItems.create');
Route::post('purchaseItems', 'PurchaseItemController@store')->middleware(['can:purchaseItems.store.owned'])->name('purchaseItems.store');
Route::get('purchaseItems/{purchaseItem}', 'PurchaseItemController@show')->middleware(['can:purchaseItems.show'])->name('purchaseItems.show');
Route::get('purchaseItems/{purchaseItem}/edit', 'PurchaseItemController@edit')->middleware(['can:purchaseItems.edit'])->name('purchaseItems.edit');
Route::put('purchaseItems/{purchaseItem}', 'PurchaseItemController@update')->middleware(['can:purchaseItems.update'])->name('purchaseItems.update');
Route::patch('purchaseItems/{purchaseItem}', 'PurchaseItemController@update')->middleware(['can:purchaseItems.update'])->name('purchaseItems.update');
Route::delete('purchaseItems/{purchaseItem}', 'PurchaseItemController@destroy')->middleware(['can:purchaseItems.destroy'])->name('purchaseItems.destroy');
// Route::resource('purchaseItems', 'PurchaseItemController');
// Route::post('importPurchaseItem', 'PurchaseItemController@import');

Route::get('stores', 'StoreController@index')->middleware(['can:stores.index'])->name('stores.index');
Route::get('stores/create', 'StoreController@create')->middleware(['can:stores.create.owned'])->name('stores.create');
Route::post('stores', 'StoreController@store')->middleware(['can:stores.store.owned'])->name('stores.store');
Route::get('stores/{store}', 'StoreController@show')->middleware(['can:stores.show'])->name('stores.show');
Route::get('stores/{store}/edit', 'StoreController@edit')->middleware(['can:stores.edit'])->name('stores.edit');
Route::put('stores/{store}', 'StoreController@update')->middleware(['can:stores.update'])->name('stores.update');
Route::patch('stores/{store}', 'StoreController@update')->middleware(['can:stores.update'])->name('stores.update');
Route::delete('stores/{store}', 'StoreController@destroy')->middleware(['can:stores.destroy'])->name('stores.destroy');
// Route::resource('stores', 'StoreController');
// Route::post('importStore', 'StoreController@import');

Route::get('suppliers', 'SupplierController@index')->middleware(['can:suppliers.index'])->name('suppliers.index');
Route::get('suppliers/create', 'SupplierController@create')->middleware(['can:suppliers.create.owned'])->name('suppliers.create');
Route::post('suppliers', 'SupplierController@store')->middleware(['can:suppliers.store.owned'])->name('suppliers.store');
Route::get('suppliers/{supplier}', 'SupplierController@show')->middleware(['can:suppliers.show'])->name('suppliers.show');
Route::get('suppliers/{supplier}/edit', 'SupplierController@edit')->middleware(['can:suppliers.edit'])->name('suppliers.edit');
Route::put('suppliers/{supplier}', 'SupplierController@update')->middleware(['can:suppliers.update'])->name('suppliers.update');
Route::patch('suppliers/{supplier}', 'SupplierController@update')->middleware(['can:suppliers.update'])->name('suppliers.update');
Route::delete('suppliers/{supplier}', 'SupplierController@destroy')->middleware(['can:suppliers.destroy'])->name('suppliers.destroy');
// Route::resource('suppliers', 'SupplierController');
// Route::post('importSupplier', 'SupplierController@import');

Route::get('units', 'UnitController@index')->middleware(['can:units.index'])->name('units.index');
Route::get('units/create', 'UnitController@create')->middleware(['can:units.create.owned'])->name('units.create');
Route::post('units', 'UnitController@store')->middleware(['can:units.store.owned'])->name('units.store');
Route::get('units/{unit}', 'UnitController@show')->middleware(['can:units.show'])->name('units.show');
Route::get('units/{unit}/edit', 'UnitController@edit')->middleware(['can:units.edit'])->name('units.edit');
Route::put('units/{unit}', 'UnitController@update')->middleware(['can:units.update'])->name('units.update');
Route::patch('units/{unit}', 'UnitController@update')->middleware(['can:units.update'])->name('units.update');
Route::delete('units/{unit}', 'UnitController@destroy')->middleware(['can:units.destroy'])->name('units.destroy');
// Route::resource('units', 'UnitController');
// Route::post('importUnit', 'UnitController@import');

Route::get('wraps', 'WrapController@index')->middleware(['can:wraps.index'])->name('wraps.index');
Route::get('wraps/create', 'WrapController@create')->middleware(['can:wraps.create.owned'])->name('wraps.create');
Route::post('wraps', 'WrapController@store')->middleware(['can:wraps.store.owned'])->name('wraps.store');
Route::get('wraps/{wrap}', 'WrapController@show')->middleware(['can:wraps.show'])->name('wraps.show');
Route::get('wraps/{wrap}/edit', 'WrapController@edit')->middleware(['can:wraps.edit'])->name('wraps.edit');
Route::put('wraps/{wrap}', 'WrapController@update')->middleware(['can:wraps.update'])->name('wraps.update');
Route::patch('wraps/{wrap}', 'WrapController@update')->middleware(['can:wraps.update'])->name('wraps.update');
Route::delete('wraps/{wrap}', 'WrapController@destroy')->middleware(['can:wraps.destroy'])->name('wraps.destroy');
// Route::resource('wraps', 'WrapController');
// Route::post('importWrap', 'WrapController@import');

Route::get('storeFronts', 'StoreFrontController@index')->middleware(['can:storeFronts.index'])->name('storeFronts.index');
Route::get('storeFronts/create', 'StoreFrontController@create')->middleware(['can:storeFronts.create.owned'])->name('storeFronts.create');
Route::post('storeFronts', 'StoreFrontController@store')->middleware(['can:storeFronts.store.owned'])->name('storeFronts.store');
Route::get('storeFronts/{storeFront}', 'StoreFrontController@show')->middleware(['can:storeFronts.show'])->name('storeFronts.show');
Route::get('storeFronts/{storeFront}/edit', 'StoreFrontController@edit')->middleware(['can:storeFronts.edit'])->name('storeFronts.edit');
Route::put('storeFronts/{storeFront}', 'StoreFrontController@update')->middleware(['can:storeFronts.update'])->name('storeFronts.update');
Route::patch('storeFronts/{storeFront}', 'StoreFrontController@update')->middleware(['can:storeFronts.update'])->name('storeFronts.update');
Route::delete('storeFronts/{storeFront}', 'StoreFrontController@destroy')->middleware(['can:storeFronts.destroy'])->name('storeFronts.destroy');
// Route::resource('storeFronts', 'StoreFrontController');
// Route::post('importStoreFront', 'StoreFrontController@import');

Route::get('storeFrontItems', 'StoreFrontItemController@index')->middleware(['can:storeFrontItems.index'])->name('storeFrontItems.index');
Route::get('storeFrontItems/create', 'StoreFrontItemController@create')->middleware(['can:storeFrontItems.create.owned'])->name('storeFrontItems.create');
Route::post('storeFrontItems', 'StoreFrontItemController@store')->middleware(['can:storeFrontItems.store.owned'])->name('storeFrontItems.store');
Route::get('storeFrontItems/{storeFrontItem}', 'StoreFrontItemController@show')->middleware(['can:storeFrontItems.show'])->name('storeFrontItems.show');
Route::get('storeFrontItems/{storeFrontItem}/edit', 'StoreFrontItemController@edit')->middleware(['can:storeFrontItems.edit'])->name('storeFrontItems.edit');
Route::put('storeFrontItems/{storeFrontItem}', 'StoreFrontItemController@update')->middleware(['can:storeFrontItems.update'])->name('storeFrontItems.update');
Route::patch('storeFrontItems/{storeFrontItem}', 'StoreFrontItemController@update')->middleware(['can:storeFrontItems.update'])->name('storeFrontItems.update');
Route::delete('storeFrontItems/{storeFrontItem}', 'StoreFrontItemController@destroy')->middleware(['can:storeFrontItems.destroy'])->name('storeFrontItems.destroy');
// Route::resource('storeFrontItems', 'StoreFrontItemController');
// Route::post('importStoreFrontItem', 'StoreFrontItemController@import');

Route::get('debts', 'DebtController@index')->middleware(['can:debts.index'])->name('debts.index');
Route::get('debts/create', 'DebtController@create')->middleware(['can:debts.create.owned'])->name('debts.create');
Route::post('debts', 'DebtController@store')->middleware(['can:debts.store.owned'])->name('debts.store');
Route::get('debts/{debt}', 'DebtController@show')->middleware(['can:debts.show'])->name('debts.show');
Route::get('debts/{debt}/edit', 'DebtController@edit')->middleware(['can:debts.edit'])->name('debts.edit');
Route::put('debts/{debt}', 'DebtController@update')->middleware(['can:debts.update'])->name('debts.update');
Route::patch('debts/{debt}', 'DebtController@update')->middleware(['can:debts.update'])->name('debts.update');
Route::delete('debts/{debt}', 'DebtController@destroy')->middleware(['can:debts.destroy'])->name('debts.destroy');
// Route::resource('debts', 'DebtController');
// Route::post('importDebt', 'DebtController@import');

Route::get('trusts', 'TrustController@index')->middleware(['can:trusts.index'])->name('trusts.index');
Route::get('trusts/create', 'TrustController@create')->middleware(['can:trusts.create.owned'])->name('trusts.create');
Route::post('trusts', 'TrustController@store')->middleware(['can:trusts.store.owned'])->name('trusts.store');
Route::get('trusts/{trust}', 'TrustController@show')->middleware(['can:trusts.show'])->name('trusts.show');
Route::get('trusts/{trust}/edit', 'TrustController@edit')->middleware(['can:trusts.edit'])->name('trusts.edit');
Route::put('trusts/{trust}', 'TrustController@update')->middleware(['can:trusts.update'])->name('trusts.update');
Route::patch('trusts/{trust}', 'TrustController@update')->middleware(['can:trusts.update'])->name('trusts.update');
Route::delete('trusts/{trust}', 'TrustController@destroy')->middleware(['can:trusts.destroy'])->name('trusts.destroy');
// Route::resource('trusts', 'TrustController');
// Route::post('importTrust', 'TrustController@import');

Route::get('featuredProducts', 'FeaturedProductController@index')->middleware(['can:featuredProducts.index'])->name('featuredProducts.index');
Route::get('featuredProducts/create', 'FeaturedProductController@create')->middleware(['can:featuredProducts.create.owned'])->name('featuredProducts.create');
Route::post('featuredProducts', 'FeaturedProductController@store')->middleware(['can:featuredProducts.store.owned'])->name('featuredProducts.store');
Route::get('featuredProducts/{featuredProduct}', 'FeaturedProductController@show')->middleware(['can:featuredProducts.show'])->name('featuredProducts.show');
Route::get('featuredProducts/{featuredProduct}/edit', 'FeaturedProductController@edit')->middleware(['can:featuredProducts.edit'])->name('featuredProducts.edit');
Route::put('featuredProducts/{featuredProduct}', 'FeaturedProductController@update')->middleware(['can:featuredProducts.update'])->name('featuredProducts.update');
Route::patch('featuredProducts/{featuredProduct}', 'FeaturedProductController@update')->middleware(['can:featuredProducts.update'])->name('featuredProducts.update');
Route::delete('featuredProducts/{featuredProduct}', 'FeaturedProductController@destroy')->middleware(['can:featuredProducts.destroy'])->name('featuredProducts.destroy');
// Route::resource('featuredProducts', 'FeaturedProductController');
// Route::post('importFeaturedProduct', 'FeaturedProductController@import');

Route::get('items', 'ItemController@index')->middleware(['can:items.index'])->name('items.index');
Route::get('items/create', 'ItemController@create')->middleware(['can:items.create.owned'])->name('items.create');
Route::post('items', 'ItemController@store')->middleware(['can:items.create.owned'])->name('items.store');
Route::get('items/{item}', 'ItemController@show')->middleware(['can:items.index'])->name('items.show');
Route::get('items/{item}/edit', 'ItemController@edit')->middleware(['can:items.update'])->name('items.edit');
Route::put('items/{item}', 'ItemController@update')->middleware(['can:items.update'])->name('items.update');
Route::patch('items/{item}', 'ItemController@update')->middleware(['can:items.update'])->name('items.update');
Route::delete('items/{item}', 'ItemController@destroy')->middleware(['can:items.destroy'])->name('items.destroy');
// Route::resource('items', 'ItemController');
// Route::post('importItem', 'ItemController@import');

Route::get('sells', 'SellController@index')->middleware(['can:sells.index'])->name('sells.index');
Route::get('sells/create', 'SellController@create')->middleware(['can:sells.create.owned'])->name('sells.create');
Route::post('sells', 'SellController@store')->middleware(['can:sells.store.owned'])->name('sells.store');
Route::get('sells/{sell}', 'SellController@show')->middleware(['can:sells.show'])->name('sells.show');
Route::get('sells/{sell}/edit', 'SellController@edit')->middleware(['can:sells.edit'])->name('sells.edit');
Route::put('sells/{sell}', 'SellController@update')->middleware(['can:sells.update'])->name('sells.update');
Route::patch('sells/{sell}', 'SellController@update')->middleware(['can:sells.update'])->name('sells.update');
Route::delete('sells/{sell}', 'SellController@destroy')->middleware(['can:sells.destroy'])->name('sells.destroy');
// Route::resource('sells', 'SellController');
// Route::post('importSell', 'SellController@import');

Route::get('sellItems', 'SellItemController@index')->middleware(['can:sellItems.index'])->name('sellItems.index');
Route::get('sellItems/create', 'SellItemController@create')->middleware(['can:sellItems.create.owned'])->name('sellItems.create');
Route::post('sellItems', 'SellItemController@store')->middleware(['can:sellItems.store.owned'])->name('sellItems.store');
Route::get('sellItems/{sellItem}', 'SellItemController@show')->middleware(['can:sellItems.show'])->name('sellItems.show');
Route::get('sellItems/{sellItem}/edit', 'SellItemController@edit')->middleware(['can:sellItems.edit'])->name('sellItems.edit');
Route::put('sellItems/{sellItem}', 'SellItemController@update')->middleware(['can:sellItems.update'])->name('sellItems.update');
Route::patch('sellItems/{sellItem}', 'SellItemController@update')->middleware(['can:sellItems.update'])->name('sellItems.update');
Route::delete('sellItems/{sellItem}', 'SellItemController@destroy')->middleware(['can:sellItems.destroy'])->name('sellItems.destroy');
// Route::resource('sellItems', 'SellItemController');
// Route::post('importSellItem', 'SellItemController@import');

Route::get('stockTransfers', 'StockTransferController@index')->middleware(['can:stockTransfers.index'])->name('stockTransfers.index');
Route::get('stockTransfers/create', 'StockTransferController@create')->middleware(['can:stockTransfers.create.owned'])->name('stockTransfers.create');
Route::post('stockTransfers', 'StockTransferController@store')->middleware(['can:stockTransfers.store.owned'])->name('stockTransfers.store');
Route::get('stockTransfers/{stockTransfer}', 'StockTransferController@show')->middleware(['can:stockTransfers.show'])->name('stockTransfers.show');
Route::get('stockTransfers/{stockTransfer}/edit', 'StockTransferController@edit')->middleware(['can:stockTransfers.edit'])->name('stockTransfers.edit');
Route::put('stockTransfers/{stockTransfer}', 'StockTransferController@update')->middleware(['can:stockTransfers.update'])->name('stockTransfers.update');
Route::patch('stockTransfers/{stockTransfer}', 'StockTransferController@update')->middleware(['can:stockTransfers.update'])->name('stockTransfers.update');
Route::delete('stockTransfers/{stockTransfer}', 'StockTransferController@destroy')->middleware(['can:stockTransfers.destroy'])->name('stockTransfers.destroy');
// Route::resource('stockTransfers', 'StockTransferController');
// Route::post('importStockTransfer', 'StockTransferController@import');

Route::get('stockTransferItems', 'StockTransferItemController@index')->middleware(['can:stockTransferItems.index'])->name('stockTransferItems.index');
Route::get('stockTransferItems/create', 'StockTransferItemController@create')->middleware(['can:stockTransferItems.create.owned'])->name('stockTransferItems.create');
Route::post('stockTransferItems', 'StockTransferItemController@store')->middleware(['can:stockTransferItems.store.owned'])->name('stockTransferItems.store');
Route::get('stockTransferItems/{stockTransferItem}', 'StockTransferItemController@show')->middleware(['can:stockTransferItems.show'])->name('stockTransferItems.show');
Route::get('stockTransferItems/{stockTransferItem}/edit', 'StockTransferItemController@edit')->middleware(['can:stockTransferItems.edit'])->name('stockTransferItems.edit');
Route::put('stockTransferItems/{stockTransferItem}', 'StockTransferItemController@update')->middleware(['can:stockTransferItems.update'])->name('stockTransferItems.update');
Route::patch('stockTransferItems/{stockTransferItem}', 'StockTransferItemController@update')->middleware(['can:stockTransferItems.update'])->name('stockTransferItems.update');
Route::delete('stockTransferItems/{stockTransferItem}', 'StockTransferItemController@destroy')->middleware(['can:stockTransferItems.destroy'])->name('stockTransferItems.destroy');
// Route::resource('stockTransferItems', 'StockTransferItemController');
// Route::post('importStockTransferItem', 'StockTransferItemController@import');

Route::get('composites', 'CompositeController@index')->middleware(['can:composites.index'])->name('composites.index');
Route::get('composites/create', 'CompositeController@create')->middleware(['can:composites.create.owned'])->name('composites.create');
Route::post('composites', 'CompositeController@store')->middleware(['can:composites.store.owned'])->name('composites.store');
Route::get('composites/{composite}', 'CompositeController@show')->middleware(['can:composites.show'])->name('composites.show');
Route::get('composites/{composite}/edit', 'CompositeController@edit')->middleware(['can:composites.edit'])->name('composites.edit');
Route::put('composites/{composite}', 'CompositeController@update')->middleware(['can:composites.update'])->name('composites.update');
Route::patch('composites/{composite}', 'CompositeController@update')->middleware(['can:composites.update'])->name('composites.update');
Route::delete('composites/{composite}', 'CompositeController@destroy')->middleware(['can:composites.destroy'])->name('composites.destroy');
// Route::resource('composites', 'CompositeController');
// Route::post('importComposite', 'CompositeController@import');

Route::get('compositeItems', 'CompositeItemController@index')->middleware(['can:compositeItems.index'])->name('compositeItems.index');
Route::get('compositeItems/create', 'CompositeItemController@create')->middleware(['can:compositeItems.create.owned'])->name('compositeItems.create');
Route::post('compositeItems', 'CompositeItemController@store')->middleware(['can:compositeItems.store.owned'])->name('compositeItems.store');
Route::get('compositeItems/{compositeItem}', 'CompositeItemController@show')->middleware(['can:compositeItems.show'])->name('compositeItems.show');
Route::get('compositeItems/{compositeItem}/edit', 'CompositeItemController@edit')->middleware(['can:compositeItems.edit'])->name('compositeItems.edit');
Route::put('compositeItems/{compositeItem}', 'CompositeItemController@update')->middleware(['can:compositeItems.update'])->name('compositeItems.update');
Route::patch('compositeItems/{compositeItem}', 'CompositeItemController@update')->middleware(['can:compositeItems.update'])->name('compositeItems.update');
Route::delete('compositeItems/{compositeItem}', 'CompositeItemController@destroy')->middleware(['can:compositeItems.destroy'])->name('compositeItems.destroy');
// Route::resource('compositeItems', 'CompositeItemController');
// Route::post('importCompositeItem', 'CompositeItemController@import');

Route::get('stocks', 'StockController@index')->middleware(['can:stocks.index'])->name('stocks.index');
Route::get('stocks/create', 'StockController@create')->middleware(['can:stocks.create.owned'])->name('stocks.create');
Route::post('stocks', 'StockController@store')->middleware(['can:stocks.store.owned'])->name('stocks.store');
Route::get('stocks/{stock}', 'StockController@show')->middleware(['can:stocks.show'])->name('stocks.show');
Route::get('stocks/{stock}/edit', 'StockController@edit')->middleware(['can:stocks.edit'])->name('stocks.edit');
Route::put('stocks/{stock}', 'StockController@update')->middleware(['can:stocks.update'])->name('stocks.update');
Route::patch('stocks/{stock}', 'StockController@update')->middleware(['can:stocks.update'])->name('stocks.update');
Route::delete('stocks/{stock}', 'StockController@destroy')->middleware(['can:stocks.destroy'])->name('stocks.destroy');
// Route::resource('stocks', 'StockController');
// Route::post('importStock', 'StockController@import');

Route::get('sellReturns', 'SellReturnController@index')->middleware(['can:sellReturns.index'])->name('sellReturns.index');
Route::get('sellReturns/create', 'SellReturnController@create')->middleware(['can:sellReturns.create.owned'])->name('sellReturns.create');
Route::post('sellReturns', 'SellReturnController@store')->middleware(['can:sellReturns.store.owned'])->name('sellReturns.store');
Route::get('sellReturns/{sellReturn}', 'SellReturnController@show')->middleware(['can:sellReturns.show'])->name('sellReturns.show');
Route::get('sellReturns/{sellReturn}/edit', 'SellReturnController@edit')->middleware(['can:sellReturns.edit'])->name('sellReturns.edit');
Route::put('sellReturns/{sellReturn}', 'SellReturnController@update')->middleware(['can:sellReturns.update'])->name('sellReturns.update');
Route::patch('sellReturns/{sellReturn}', 'SellReturnController@update')->middleware(['can:sellReturns.update'])->name('sellReturns.update');
Route::delete('sellReturns/{sellReturn}', 'SellReturnController@destroy')->middleware(['can:sellReturns.destroy'])->name('sellReturns.destroy');
// Route::resource('sellReturns', 'SellReturnController');
// Route::post('importSellReturn', 'SellReturnController@import');

Route::get('sellReturnItems', 'SellReturnItemController@index')->middleware(['can:sellReturnItems.index'])->name('sellReturnItems.index');
Route::get('sellReturnItems/create', 'SellReturnItemController@create')->middleware(['can:sellReturnItems.create.owned'])->name('sellReturnItems.create');
Route::post('sellReturnItems', 'SellReturnItemController@store')->middleware(['can:sellReturnItems.store.owned'])->name('sellReturnItems.store');
Route::get('sellReturnItems/{sellReturnItem}', 'SellReturnItemController@show')->middleware(['can:sellReturnItems.show'])->name('sellReturnItems.show');
Route::get('sellReturnItems/{sellReturnItem}/edit', 'SellReturnItemController@edit')->middleware(['can:sellReturnItems.edit'])->name('sellReturnItems.edit');
Route::put('sellReturnItems/{sellReturnItem}', 'SellReturnItemController@update')->middleware(['can:sellReturnItems.update'])->name('sellReturnItems.update');
Route::patch('sellReturnItems/{sellReturnItem}', 'SellReturnItemController@update')->middleware(['can:sellReturnItems.update'])->name('sellReturnItems.update');
Route::delete('sellReturnItems/{sellReturnItem}', 'SellReturnItemController@destroy')->middleware(['can:sellReturnItems.destroy'])->name('sellReturnItems.destroy');
// Route::resource('sellReturnItems', 'SellReturnItemController');
// Route::post('importSellReturnItem', 'SellReturnItemController@import');

Route::get('purchaseReturns', 'PurchaseReturnController@index')->middleware(['can:purchaseReturns.index'])->name('purchaseReturns.index');
Route::get('purchaseReturns/create', 'PurchaseReturnController@create')->middleware(['can:purchaseReturns.create.owned'])->name('purchaseReturns.create');
Route::post('purchaseReturns', 'PurchaseReturnController@store')->middleware(['can:purchaseReturns.store.owned'])->name('purchaseReturns.store');
Route::get('purchaseReturns/{purchaseReturn}', 'PurchaseReturnController@show')->middleware(['can:purchaseReturns.show'])->name('purchaseReturns.show');
Route::get('purchaseReturns/{purchaseReturn}/edit', 'PurchaseReturnController@edit')->middleware(['can:purchaseReturns.edit'])->name('purchaseReturns.edit');
Route::put('purchaseReturns/{purchaseReturn}', 'PurchaseReturnController@update')->middleware(['can:purchaseReturns.update'])->name('purchaseReturns.update');
Route::patch('purchaseReturns/{purchaseReturn}', 'PurchaseReturnController@update')->middleware(['can:purchaseReturns.update'])->name('purchaseReturns.update');
Route::delete('purchaseReturns/{purchaseReturn}', 'PurchaseReturnController@destroy')->middleware(['can:purchaseReturns.destroy'])->name('purchaseReturns.destroy');
// Route::resource('purchaseReturns', 'PurchaseReturnController');
// Route::post('importPurchaseReturn', 'PurchaseReturnController@import');

Route::get('purchaseReturnItems', 'PurchaseReturnItemController@index')->middleware(['can:purchaseReturnItems.index'])->name('purchaseReturnItems.index');
Route::get('purchaseReturnItems/create', 'PurchaseReturnItemController@create')->middleware(['can:purchaseReturnItems.create.owned'])->name('purchaseReturnItems.create');
Route::post('purchaseReturnItems', 'PurchaseReturnItemController@store')->middleware(['can:purchaseReturnItems.store.owned'])->name('purchaseReturnItems.store');
Route::get('purchaseReturnItems/{purchaseReturnItem}', 'PurchaseReturnItemController@show')->middleware(['can:purchaseReturnItems.show'])->name('purchaseReturnItems.show');
Route::get('purchaseReturnItems/{purchaseReturnItem}/edit', 'PurchaseReturnItemController@edit')->middleware(['can:purchaseReturnItems.edit'])->name('purchaseReturnItems.edit');
Route::put('purchaseReturnItems/{purchaseReturnItem}', 'PurchaseReturnItemController@update')->middleware(['can:purchaseReturnItems.update'])->name('purchaseReturnItems.update');
Route::patch('purchaseReturnItems/{purchaseReturnItem}', 'PurchaseReturnItemController@update')->middleware(['can:purchaseReturnItems.update'])->name('purchaseReturnItems.update');
Route::delete('purchaseReturnItems/{purchaseReturnItem}', 'PurchaseReturnItemController@destroy')->middleware(['can:purchaseReturnItems.destroy'])->name('purchaseReturnItems.destroy');
// Route::resource('purchaseReturnItems', 'PurchaseReturnItemController');
// Route::post('importPurchaseReturnItem', 'PurchaseReturnItemController@import');

Route::get('images', 'ImageController@index')->middleware(['can:images.index'])->name('images.index');
Route::get('images/create', 'ImageController@create')->middleware(['can:images.create.owned'])->name('images.create');
Route::post('images', 'ImageController@store')->middleware(['can:images.store.owned'])->name('images.store');
Route::get('images/{image}', 'ImageController@show')->middleware(['can:images.show'])->name('images.show');
Route::get('images/{image}/edit', 'ImageController@edit')->middleware(['can:images.edit'])->name('images.edit');
Route::put('images/{image}', 'ImageController@update')->middleware(['can:images.update'])->name('images.update');
Route::patch('images/{image}', 'ImageController@update')->middleware(['can:images.update'])->name('images.update');
Route::delete('images/{image}', 'ImageController@destroy')->middleware(['can:images.destroy'])->name('images.destroy');
// Route::resource('images', 'ImageController');
// Route::post('importImage', 'ImageController@import');

Route::get('recaps', 'RecapController@index')->middleware(['can:recaps.index'])->name('recaps.index');
Route::get('recaps/create', 'RecapController@create')->middleware(['can:recaps.create.owned'])->name('recaps.create');
Route::post('recaps', 'RecapController@store')->middleware(['can:recaps.store.owned'])->name('recaps.store');
Route::get('recaps/{recap}', 'RecapController@show')->middleware(['can:recaps.show'])->name('recaps.show');
Route::get('recaps/{recap}/edit', 'RecapController@edit')->middleware(['can:recaps.edit'])->name('recaps.edit');
Route::put('recaps/{recap}', 'RecapController@update')->middleware(['can:recaps.update'])->name('recaps.update');
Route::patch('recaps/{recap}', 'RecapController@update')->middleware(['can:recaps.update'])->name('recaps.update');
Route::delete('recaps/{recap}', 'RecapController@destroy')->middleware(['can:recaps.destroy'])->name('recaps.destroy');
// Route::resource('recaps', 'RecapController');
// Route::post('importRecap', 'RecapController@import');

Route::get('storeFrontStocks', 'StoreFrontStockController@index')->middleware(['can:storeFrontStocks.index'])->name('storeFrontStocks.index');
Route::get('storeFrontStocks/create', 'StoreFrontStockController@create')->middleware(['can:storeFrontStocks.create.owned'])->name('storeFrontStocks.create');
Route::post('storeFrontStocks', 'StoreFrontStockController@store')->middleware(['can:storeFrontStocks.store.owned'])->name('storeFrontStocks.store');
Route::get('storeFrontStocks/{storeFrontStock}', 'StoreFrontStockController@show')->middleware(['can:storeFrontStocks.show'])->name('storeFrontStocks.show');
Route::get('storeFrontStocks/{storeFrontStock}/edit', 'StoreFrontStockController@edit')->middleware(['can:storeFrontStocks.edit'])->name('storeFrontStocks.edit');
Route::put('storeFrontStocks/{storeFrontStock}', 'StoreFrontStockController@update')->middleware(['can:storeFrontStocks.update'])->name('storeFrontStocks.update');
Route::patch('storeFrontStocks/{storeFrontStock}', 'StoreFrontStockController@update')->middleware(['can:storeFrontStocks.update'])->name('storeFrontStocks.update');
Route::delete('storeFrontStocks/{storeFrontStock}', 'StoreFrontStockController@destroy')->middleware(['can:storeFrontStocks.destroy'])->name('storeFrontStocks.destroy');
// Route::resource('storeFrontStocks', 'StoreFrontStockController');
// Route::post('importStoreFrontStock', 'StoreFrontStockController@import');

Route::get('compositeStocks', 'CompositeStockController@index')->middleware(['can:compositeStocks.index'])->name('compositeStocks.index');
Route::get('compositeStocks/create', 'CompositeStockController@create')->middleware(['can:compositeStocks.create.owned'])->name('compositeStocks.create');
Route::post('compositeStocks', 'CompositeStockController@store')->middleware(['can:compositeStocks.store.owned'])->name('compositeStocks.store');
Route::get('compositeStocks/{compositeStock}', 'CompositeStockController@show')->middleware(['can:compositeStocks.show'])->name('compositeStocks.show');
Route::get('compositeStocks/{compositeStock}/edit', 'CompositeStockController@edit')->middleware(['can:compositeStocks.edit'])->name('compositeStocks.edit');
Route::put('compositeStocks/{compositeStock}', 'CompositeStockController@update')->middleware(['can:compositeStocks.update'])->name('compositeStocks.update');
Route::patch('compositeStocks/{compositeStock}', 'CompositeStockController@update')->middleware(['can:compositeStocks.update'])->name('compositeStocks.update');
Route::delete('compositeStocks/{compositeStock}', 'CompositeStockController@destroy')->middleware(['can:compositeStocks.destroy'])->name('compositeStocks.destroy');
// Route::resource('compositeStocks', 'CompositeStockController');
// Route::post('importCompositeStock', 'CompositeStockController@import');

Route::get('rejectedItems', 'RejectedItemController@index')->middleware(['can:rejectedItems.index'])->name('rejectedItems.index');
Route::get('rejectedItems/create', 'RejectedItemController@create')->middleware(['can:rejectedItems.create.owned'])->name('rejectedItems.create');
Route::post('rejectedItems', 'RejectedItemController@store')->middleware(['can:rejectedItems.store.owned'])->name('rejectedItems.store');
Route::get('rejectedItems/{rejectedItem}', 'RejectedItemController@show')->middleware(['can:rejectedItems.show'])->name('rejectedItems.show');
Route::get('rejectedItems/{rejectedItem}/edit', 'RejectedItemController@edit')->middleware(['can:rejectedItems.edit'])->name('rejectedItems.edit');
Route::put('rejectedItems/{rejectedItem}', 'CashController@update')->middleware(['can:rejectedItems.update'])->name('rejectedItems.update');
Route::patch('rejectedItems/{rejectedItem}', 'RejectedItemController@update')->middleware(['can:rejectedItems.update'])->name('rejectedItems.update');
Route::delete('rejectedItems/{rejectedItem}', 'RejectedItemController@destroy')->middleware(['can:rejectedItems.destroy'])->name('rejectedItems.destroy');
// Route::resource('rejectedItems', 'RejectedItemController');
// Route::post('importRejectedItem', 'RejectedItemController@import');

Route::get('barcodes', 'BarcodeController@index')->middleware(['can:barcodes.index'])->name('barcodes.index');
Route::get('barcodes/create', 'BarcodeController@create')->middleware(['can:barcodes.create.owned'])->name('barcodes.create');
Route::post('barcodes', 'BarcodeController@store')->middleware(['can:barcodes.store.owned'])->name('barcodes.store');
Route::get('barcodes/{barcode}', 'BarcodeController@show')->middleware(['can:barcodes.show'])->name('barcodes.show');
Route::get('barcodes/{barcode}/edit', 'BarcodeController@edit')->middleware(['can:barcodes.edit'])->name('barcodes.edit');
Route::put('barcodes/{barcode}', 'BarcodeController@update')->middleware(['can:barcodes.update'])->name('barcodes.update');
Route::patch('barcodes/{barcode}', 'BarcodeController@update')->middleware(['can:barcodes.update'])->name('barcodes.update');
Route::delete('barcodes/{cbarcodeash}', 'BarcodeController@destroy')->middleware(['can:barcodes.destroy'])->name('barcodes.destroy');
// Route::resource('barcodes', 'BarcodeController');
// Route::post('importBarcode', 'BarcodeController@import');