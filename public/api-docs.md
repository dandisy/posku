# Specific API

### Get Board

URL : 

	/api/boards/1

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// board data
}
```

### Post Quest

URL : 

	/api/histories?model_type=App\Models\Quest&model_id=1&user_id=1

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// board data
}
```

### Post Redeem

URL : 

	/api/histories?model_type=App\Models\Redeem&model_id=1&user_id=1

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// lucky data
	// board data
}
```

### Upload File

URL : 

	/api/upload

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"file" : "{input file or base64 encoded file}",
	"extention" : "[jpg|jpeg|png|pdf]",
	"type" : "[discussion|{type}]",
	"user_id" : "{Logged in user id}"
}
```

Response :

```json
{
	"status": true,
	"message": "file saved successfully",
	"data": "{http://{your-domain}/storage/uploads/1/1_discussion_1575440737.jpg}"
}
```

# Elorest API

**If model namespace is App\Models :**

> https://your-domain-name/api/elorest/Models/Post?leftJoin=comments,posts.id,comments.post_id&whereIn=category_id,[2,4,5]&select=*&get=
> 
> https://your-domain-name/api/elorest/Models/Post?join[]=authors,posts.id,authors.author_id&join[]=comments,posts.id,comments.post_id&whereIn=category_id,[2,4,5]&select=posts.*,authors.name as author_name,comments.title as comment_title&get=
> 
> https://your-domain-name/api/elorest/Models/Post?&with=author,comment&get=*
> 
> https://your-domain-name/api/elorest/Models/Post?&with=author(where=name,like,%dandisy%),comment&get=*
> 
	
*Multi first nested closure deep :*

> https://your-domain-name/api/elorest/Models/Post?&with=author(where=name,like,%dandisy%)(where=nick,like,%dandisy%),comment&get=*
> 
	
*Second nested closure deep :*

> https://your-domain-name/api/elorest/Models/Post?&with=author(with=city(where=name,like,%jakarta%)),comment&get=*
> 
> https://your-domain-name/api/elorest/Models/Post?&with[]=author(where=name,like,%dandisy%)&with[]=comment(where=title,like,%test%)&get=*
> 
> https://your-domain-name/api/elorest/Models/Post?paginate=10&page=1
> 

**If model namespace only App :**

> https://your-domain-name/api/elorest/User?paginate=10&page=1

### Get Quest

URL : 

	/api/elorest/Models/Quest

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// quest data
}
```

### Get Redeem

URL : 

	/api/elorest/Models/Redeem

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// redeem data
}
```

### Get History

URL : 

	/api/elorest/Models/History

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// history data
}
```

### Get Discussion

URL : 

	/api/elorest/Models/Discussion

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// discussion data
}
```

### Get Moment

URL : 

	/api/elorest/Models/Moment

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// moment data
}
```

### Get Page

URL : 

	/api/elorest/Models/Page

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// page data
}
```

### Get Banner

URL : 

	/api/elorest/Models/Banner

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// banner data
}
```

### Get Product

URL : 

	/api/elorest/Models/Product

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// product data
}
```

### Get Community

URL : 

	/api/elorest/Models/Community

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// community data
}
```

### Get Store Locator

URL : 

	/api/elorest/Models/StoreLocator

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// store locator data
}
```

### Get Quiz

URL : 

	/api/elorest/Models/Quiz

Type :

	GET

Header :

	"Authorization" : "bearer {api_key}"

Request :

Response :

```
{
	// quiz data
}
```

### Post Moment

URL : 

	/api/elorest/Models/Moment

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"title" : "{title}",
	"description" : "{description}",
	"image" : "{image}",
	"place" : "{place}",
	"date" : "{date}",
	"start_time" : "{start time}",
	"end_time" : "{end time}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// moment data
}
```

### Post MomentInvitation

URL : 

	/api/elorest/Models/MomentInvitation

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"moment_id" : "{moment id}",
	"email" : "{email}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// moment invitation data
}
```

### Post MomentImage

URL : 

	/api/elorest/Models/MomentImage

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"name" : "[{name}|null]",
	"image" : "{image}",
 	"moment_id" : "{moment id}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// moment image data
}
```

### Post MomentParticipant

URL : 

	/api/elorest/Models/MomentParticipant

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
 	"moment_id" : "{moment id}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// moment participant data
}
```

### Post PublicPost

URL : 

	/api/elorest/Models/PublicPost

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"model_type" : "[{model type}|App\\Models\\Moment|null]",
	"model_id" : "[{model id}|null]",
	"link" : "{link}",
	"category_id" : "[{category id}|null]",
	"type_id" :  "[{type id}|null]",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// public post data
}
```

### Post Discussion

URL : 

	/api/elorest/Models/Discussion

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"topic" : "{title}",
	"message" : "{message}",
	"image" : "{http://{your-domain}/storage/uploads/1/1_discussion_1575440737.jpg}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// discussion data
}
```

### Post Comment

URL : 

	/api/elorest/Models/Comment

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"message" : "{message}",
	"image" : "{http://{your-domain}/storage/uploads/1/1_discussion_1575440737.jpg}",
	"model_type" : "[App\\Models\\Discussion|App\\Models\\Moment|{model type}]",
	"model_id" : "{model id}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// comment data
}
```

### Post Ambassador

URL : 

	/api/elorest/Models/Ambassador

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```
{
	"created_by" : "{logged in user id}"
}
```

Response :

```
{
	// ambassador data
}
```

### Post Shipment

URL : 

	/api/elorest/Models/Shipment

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"receiver_name" : "{receiver name}",
	"address" : "{address}",
	"country" : "{country}",
	"state_province" : "{state/provice}",
	"postal_code" : "{postal code}",
	"phone_code" : "{phone code}",
	"phone_number" : "{phone number}",
	"model_type" : "[App\\Models\\Redeem|{model type}]",
	"model_id" : "{redeemable merchandise id}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// shipment data
}
```

### Post Preference

URL : 

	/api/elorest/Models/Preference

Type :

	POST

Header :

	"Authorization" : "bearer {api_key}"

Request :

```json
{
	"model_type" : "[App\\Models\\Product|{model type}]",
	"model_id" : "{model id}",
	"created_by" : "{Logged in user id}"
}
```

Response :

```
{
	// preference data
}
```