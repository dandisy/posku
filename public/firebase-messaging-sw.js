// Import and configure the Firebase SDK
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

// Your web app's Firebase configuration
var firebaseConfig = {    
		apiKey: "AIzaSyBMNmyMeO8Qt8boF66l-YQNQsl8DUyQ220",
		authDomain: "clove-8fc8d.firebaseapp.com",
		databaseURL: "https://clove-8fc8d.firebaseio.com",
		projectId: "clove-8fc8d",
		storageBucket: "clove-8fc8d.appspot.com",
		messagingSenderId: "854304257793",
		appId: "1:854304257793:web:4bab04cc16e03d1fad8d00",
		measurementId: "G-D1D8PDQZ2T"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var messaging = firebase.messaging();


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: 'https://raw.githubusercontent.com/firebase/quickstart-js/4be200b1c55616535159365b74bfd1fc128c1ebf/messaging/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
// [END background_handler]