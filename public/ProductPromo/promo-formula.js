
function promoInit(res, shoppingRecords, item, callback) {
    var res = res;
    var shoppingRecords = shoppingRecords;
    var item = item;
    var callback = callback;

    (function minimalBelanja() {
        var minimalBelanja = res.conditions.find(e => e.condition == 1);
        if(minimalBelanja) {
            if(total >= minimalBelanja.value.replace(",", "")) {
                // discount formula
                function getDiscount(item_amount, item_price) {
                    return (item_amount*item_price)*(res.value.replace(/[%]/g,'')/100);
                }
    
                // find and calc each item
                shoppingRecords.each(function(i, record) {
                    if($(record).find('[name="item[]"]').val() == item) {
                        var item_amount = parseInt($(record).find('.item_amount').val());
                        var item_price = parseInt($(record).find('.item_price').text());
    
                        var item_discount = $(record).find('.item_discount');
                        item_discount.val(getDiscount(item_amount, item_price));
    
                        $(record).find('.discount_subtotal').val(parseInt(item_discount.val()));
    
                        var price_subtotal = (item_amount * item_price) - parseInt(item_discount.val());
                        $(record).find('.price_subtotal').val(price_subtotal);
                        $(record).find('.price_subtotal_display').text($.number(price_subtotal, 0));
                    }
                });
    
                // recalculate total
                callback();
            }
        }
    })();
}