### SEO Checkup Toolbox

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/frnxstd/seo-checkup/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/frnxstd/seo-checkup/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/frnxstd/seo-checkup/badges/build.png?b=master)](https://scrutinizer-ci.com/g/frnxstd/seo-checkup/build-status/master)
[![Code Intelligence Status](https://scrutinizer-ci.com/g/frnxstd/seo-checkup/badges/code-intelligence.svg?b=master)](https://scrutinizer-ci.com/code-intelligence)

It's a basic package written in PHP for good. As most people know that, SEO services have been expensive for low budget 
developers. I made a simple package to let you check yourself with this package. It became quite ugly while I was coding,
but I am going to make a free service in the future and I will use this package. During the development process of the service, 
I will update this class as much as I need.

Any contribution in this case, more than welcome.

### Installation

You will need, `Composer`. If you already have it, run the code, shown below
```
composer require frnxstd/seo-checkup
```

Enjoy.