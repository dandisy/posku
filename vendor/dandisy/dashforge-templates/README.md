Dashforge templates for Webcore
==============================

For https://github.com/dandisy/webcore

This package based on https://github.com/InfyOmLabs/adminlte-templates

Installation steps are located [here](http://labs.infyom.com/laravelgenerator/docs/master/adminlte-templates)

Note :
for more generic if any artisan command use generate instead infyom

### Usage (if this package is installed manually)

0. unzip dashforge-templates.zip to vendor/dandisy

1. add autoload classmap in composer.json

    {
        . . .
        "autoload": {
            "classmap": [
                . . .
                "vendor/dandisy"
            ],
            . . .

2. register this package in config/app.php

    /*
    * Package Service Providers...
    */
    . . .    
    Webcore\DashforgeTemplates\DashforgeTemplatesServiceProvider::class,

3. composer dump-autoload
4. publish the templates package to webcore project

    php artisan vendor:publish --provider="Webcore\DashforgeTemplates\DashforgeTemplatesServiceProvider" --force

5. change templates configuration in config/webcore/laravel_generator.php to dashforge-templates

    /*
    |--------------------------------------------------------------------------
    | Templates
    |--------------------------------------------------------------------------
    |
    */

    // 'templates'         => 'adminlte-templates',
    // 'templates'         => 'dashforge-templates',