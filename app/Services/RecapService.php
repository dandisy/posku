<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class RecapService
{

    public function __construct()
    {
        //
    }

    public static function handle(array $params) {
        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        
        $params['store'] = $attendance->store;
        $params['updated_by'] = Auth::user()->id;

        $recap = \App\Models\Recap::where('store', $attendance->store)
            ->where('model_type', $params['model_type']);

        if(!empty($params['model_field'])) {
            $recap->where('model_field', $params['model_field']);
        }

        if(!empty($params['operation'])) {
            $recap->where('operation', $params['operation']);
        }

        $recap = $recap->first();
        
        if($recap) {
            if($params['operation'] == 'sum' || $params['operation'] == 'count') {
                $recap->update(['value' => $recap->value + $params['value']]);
            }
            if($params['operation'] == 'avg') {
                $recap->update(['value' => ($recap->value + $params['value'])/2]);
            }
        } else {
            $params['created_by'] = Auth::user()->id;

            \App\Models\Recap::create($params);
        }
    }

}
