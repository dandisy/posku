<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class LogService
{

    public function __construct()
    {
        //
    }

    public static function handle(array $params) {
        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        
        $params['attendance_id'] = $attendance->id;
        $params['store'] = $attendance->store;
        $params['created_by'] = Auth::user()->id;
        $params['updated_by'] = Auth::user()->id;

        \App\Models\Log::create($params);
    }

}
