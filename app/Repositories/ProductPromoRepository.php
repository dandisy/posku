<?php

namespace App\Repositories;

use App\Models\ProductPromo;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class ProductPromoRepository
 * @package App\Repositories
 * @version August 9, 2020, 8:32 am UTC
 *
 * @method ProductPromo findWithoutFail($id, $columns = ['*'])
 * @method ProductPromo find($id, $columns = ['*'])
 * @method ProductPromo first($columns = ['*'])
*/
class ProductPromoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item',
        'unique_barcode',
        'value',
        'publish_on',
        'publish_off',
        'store',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductPromo::class;
    }
}
