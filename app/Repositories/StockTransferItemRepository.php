<?php

namespace App\Repositories;

use App\Models\StockTransferItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class StockTransferItemRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:45 am UTC
 *
 * @method StockTransferItem findWithoutFail($id, $columns = ['*'])
 * @method StockTransferItem find($id, $columns = ['*'])
 * @method StockTransferItem first($columns = ['*'])
*/
class StockTransferItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'transfer_stock_id',
        'item',
        'unique_barcode',
        'unit',
        'wrap',
        'item_amount',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockTransferItem::class;
    }
}
