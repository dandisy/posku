<?php

namespace App\Repositories;

use App\Models\Recap;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class RecapRepository
 * @package App\Repositories
 * @version August 15, 2020, 3:03 am UTC
 *
 * @method Recap findWithoutFail($id, $columns = ['*'])
 * @method Recap find($id, $columns = ['*'])
 * @method Recap first($columns = ['*'])
*/
class RecapRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'model_type',
        'value',
        'model_field',
        'operation',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recap::class;
    }
}
