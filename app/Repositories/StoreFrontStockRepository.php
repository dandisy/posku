<?php

namespace App\Repositories;

use App\Models\StoreFrontStock;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class StoreFrontStockRepository
 * @package App\Repositories
 * @version August 15, 2020, 3:45 pm UTC
 *
 * @method StoreFrontStock findWithoutFail($id, $columns = ['*'])
 * @method StoreFrontStock find($id, $columns = ['*'])
 * @method StoreFrontStock first($columns = ['*'])
*/
class StoreFrontStockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'item',
        'item_amount',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreFrontStock::class;
    }
}
