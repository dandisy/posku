<?php

namespace App\Repositories;

use App\Models\SellItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class SellItemRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:44 am UTC
 *
 * @method SellItem findWithoutFail($id, $columns = ['*'])
 * @method SellItem find($id, $columns = ['*'])
 * @method SellItem first($columns = ['*'])
*/
class SellItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sell_id',
        'store',
        'item',
        'item_name',
        'unique_barcode',
        'item_discount',
        'item_price',
        'item_amount',
        'discount_subtotal',
        'price_subtotal',
        'tax',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellItem::class;
    }
}
