<?php

namespace App\Repositories;

use App\Models\Trust;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class TrustRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:47 am UTC
 *
 * @method Trust findWithoutFail($id, $columns = ['*'])
 * @method Trust find($id, $columns = ['*'])
 * @method Trust first($columns = ['*'])
*/
class TrustRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'description',
        'value',
        'transaction_type',
        'balance',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trust::class;
    }
}
