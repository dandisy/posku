<?php

namespace App\Repositories;

use App\Models\Log;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class LogRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:29 am UTC
 *
 * @method Log findWithoutFail($id, $columns = ['*'])
 * @method Log find($id, $columns = ['*'])
 * @method Log first($columns = ['*'])
*/
class LogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'description',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Log::class;
    }
}
