<?php

namespace App\Repositories;

use App\Models\CompositeStock;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class CompositeStockRepository
 * @package App\Repositories
 * @version August 17, 2020, 12:06 am UTC
 *
 * @method CompositeStock findWithoutFail($id, $columns = ['*'])
 * @method CompositeStock find($id, $columns = ['*'])
 * @method CompositeStock first($columns = ['*'])
*/
class CompositeStockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'composite_id',
        'item',
        'item_amount',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CompositeStock::class;
    }
}
