<?php

namespace App\Repositories;

use App\Models\StockTransfer;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class StockTransferRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:45 am UTC
 *
 * @method StockTransfer findWithoutFail($id, $columns = ['*'])
 * @method StockTransfer find($id, $columns = ['*'])
 * @method StockTransfer first($columns = ['*'])
*/
class StockTransferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'type',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockTransfer::class;
    }
}
