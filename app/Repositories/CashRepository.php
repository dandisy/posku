<?php

namespace App\Repositories;

use App\Models\Cash;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class CashRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:24 am UTC
 *
 * @method Cash findWithoutFail($id, $columns = ['*'])
 * @method Cash find($id, $columns = ['*'])
 * @method Cash first($columns = ['*'])
*/
class CashRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'description',
        'value',
        'transaction_type',
        'balance',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cash::class;
    }
}
