<?php

namespace App\Repositories;

use App\Models\Purchase;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class PurchaseRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:32 am UTC
 *
 * @method Purchase findWithoutFail($id, $columns = ['*'])
 * @method Purchase find($id, $columns = ['*'])
 * @method Purchase first($columns = ['*'])
*/
class PurchaseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'supplier',
        'receipt_code',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase::class;
    }
}
