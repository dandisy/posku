<?php

namespace App\Repositories;

use App\Models\SellReturn;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class SellReturnRepository
 * @package App\Repositories
 * @version August 12, 2020, 9:42 am UTC
 *
 * @method SellReturn findWithoutFail($id, $columns = ['*'])
 * @method SellReturn find($id, $columns = ['*'])
 * @method SellReturn first($columns = ['*'])
*/
class SellReturnRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sell_id',
        'customer',
        'store',
        'bill_amount',
        'payment_method',
        'payment_status',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellReturn::class;
    }
}
