<?php

namespace App\Repositories;

use App\Models\StoreFront;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class StoreFrontRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:00 am UTC
 *
 * @method StoreFront findWithoutFail($id, $columns = ['*'])
 * @method StoreFront find($id, $columns = ['*'])
 * @method StoreFront first($columns = ['*'])
*/
class StoreFrontRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreFront::class;
    }
}
