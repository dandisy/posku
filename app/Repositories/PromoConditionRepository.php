<?php

namespace App\Repositories;

use App\Models\PromoCondition;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class PromoConditionRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:31 am UTC
 *
 * @method PromoCondition findWithoutFail($id, $columns = ['*'])
 * @method PromoCondition find($id, $columns = ['*'])
 * @method PromoCondition first($columns = ['*'])
*/
class PromoConditionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromoCondition::class;
    }
}
