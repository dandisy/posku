<?php

namespace App\Repositories;

use App\Models\PurchaseItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class PurchaseItemRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:43 am UTC
 *
 * @method PurchaseItem findWithoutFail($id, $columns = ['*'])
 * @method PurchaseItem find($id, $columns = ['*'])
 * @method PurchaseItem first($columns = ['*'])
*/
class PurchaseItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'purchase_id',
        'store',
        'item',
        'item_discount',
        'item_price',
        'item_amount',
        'discount_subtotal',
        'price_subtotal',
        'tax',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PurchaseItem::class;
    }
}
