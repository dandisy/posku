<?php

namespace App\Repositories;

use App\Models\Composite;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class CompositeRepository
 * @package App\Repositories
 * @version August 9, 2020, 8:06 am UTC
 *
 * @method Composite findWithoutFail($id, $columns = ['*'])
 * @method Composite find($id, $columns = ['*'])
 * @method Composite first($columns = ['*'])
*/
class CompositeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'product',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Composite::class;
    }
}
