<?php

namespace App\Repositories;

use App\Models\SellReturnItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class SellReturnItemRepository
 * @package App\Repositories
 * @version August 12, 2020, 9:42 am UTC
 *
 * @method SellReturnItem findWithoutFail($id, $columns = ['*'])
 * @method SellReturnItem find($id, $columns = ['*'])
 * @method SellReturnItem first($columns = ['*'])
*/
class SellReturnItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sell_return_id',
        'store',
        'item',
        'unique_barcode',
        'item_discount',
        'item_price',
        'item_amount',
        'discount_subtotal',
        'price_subtotal',
        'tax',
        'note',
        'item_return_type',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellReturnItem::class;
    }
}
