<?php

namespace App\Repositories;

use App\Models\Sell;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class SellRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:44 am UTC
 *
 * @method Sell findWithoutFail($id, $columns = ['*'])
 * @method Sell find($id, $columns = ['*'])
 * @method Sell first($columns = ['*'])
*/
class SellRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer',
        'store',
        'bill_amount',
        'table_number',
        'table_order_status',
        'payment_method',
        'payment_status',
        'selling_type',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sell::class;
    }
}
