<?php

namespace App\Repositories;

use App\Models\Barcode;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class BarcodeRepository
 * @package App\Repositories
 * @version August 20, 2020, 3:19 pm UTC
 *
 * @method Barcode findWithoutFail($id, $columns = ['*'])
 * @method Barcode find($id, $columns = ['*'])
 * @method Barcode first($columns = ['*'])
*/
class BarcodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'purchase_id',
        'item',
        'barcode',
        'store',
        'status',
        'purchase_return_id',
        'store_front_id',
        'sell_id',
        'sell_return_id',
        'stock_transfer_id',
        'rejected_item_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Barcode::class;
    }
}
