<?php

namespace App\Repositories;

use App\Models\Stock;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class StockRepository
 * @package App\Repositories
 * @version August 12, 2020, 9:40 am UTC
 *
 * @method Stock findWithoutFail($id, $columns = ['*'])
 * @method Stock find($id, $columns = ['*'])
 * @method Stock first($columns = ['*'])
*/
class StockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'item',
        'item_amount',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Stock::class;
    }
}
