<?php

namespace App\Repositories;

use App\Models\Attendance;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class AttendanceRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:24 am UTC
 *
 * @method Attendance findWithoutFail($id, $columns = ['*'])
 * @method Attendance find($id, $columns = ['*'])
 * @method Attendance first($columns = ['*'])
*/
class AttendanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'staff',
        'store',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Attendance::class;
    }
}
