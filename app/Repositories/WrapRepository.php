<?php

namespace App\Repositories;

use App\Models\Wrap;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class WrapRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:36 am UTC
 *
 * @method Wrap findWithoutFail($id, $columns = ['*'])
 * @method Wrap find($id, $columns = ['*'])
 * @method Wrap first($columns = ['*'])
*/
class WrapRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Wrap::class;
    }
}
