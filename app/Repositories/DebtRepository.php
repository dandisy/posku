<?php

namespace App\Repositories;

use App\Models\Debt;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class DebtRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:40 am UTC
 *
 * @method Debt findWithoutFail($id, $columns = ['*'])
 * @method Debt find($id, $columns = ['*'])
 * @method Debt first($columns = ['*'])
*/
class DebtRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'description',
        'value',
        'transaction',
        'balance',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Debt::class;
    }
}
