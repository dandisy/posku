<?php

namespace App\Repositories;

use App\Models\StoreFrontItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class StoreFrontItemRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:46 am UTC
 *
 * @method StoreFrontItem findWithoutFail($id, $columns = ['*'])
 * @method StoreFrontItem find($id, $columns = ['*'])
 * @method StoreFrontItem first($columns = ['*'])
*/
class StoreFrontItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_front_id',
        'item',
        'unique_barcode',
        'item_amount',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreFrontItem::class;
    }
}
