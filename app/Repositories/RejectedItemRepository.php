<?php

namespace App\Repositories;

use App\Models\RejectedItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class RejectedItemRepository
 * @package App\Repositories
 * @version August 18, 2020, 3:10 am UTC
 *
 * @method RejectedItem findWithoutFail($id, $columns = ['*'])
 * @method RejectedItem find($id, $columns = ['*'])
 * @method RejectedItem first($columns = ['*'])
*/
class RejectedItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'source',
        'item',
        'unique_barcode',
        'item_amount',
        'note',
        'model_type',
        'model_id',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RejectedItem::class;
    }
}
