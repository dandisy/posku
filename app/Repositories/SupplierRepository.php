<?php

namespace App\Repositories;

use App\Models\Supplier;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class SupplierRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:35 am UTC
 *
 * @method Supplier findWithoutFail($id, $columns = ['*'])
 * @method Supplier find($id, $columns = ['*'])
 * @method Supplier first($columns = ['*'])
*/
class SupplierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'email',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Supplier::class;
    }
}
