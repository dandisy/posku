<?php

namespace App\Repositories;

use App\Models\PurchaseReturnItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class PurchaseReturnItemRepository
 * @package App\Repositories
 * @version August 12, 2020, 9:43 am UTC
 *
 * @method PurchaseReturnItem findWithoutFail($id, $columns = ['*'])
 * @method PurchaseReturnItem find($id, $columns = ['*'])
 * @method PurchaseReturnItem first($columns = ['*'])
*/
class PurchaseReturnItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'purchase_return_id',
        'store',
        'item',
        'unique_barcode',
        'item_discount',
        'item_price',
        'item_amount',
        'discount_subtotal',
        'price_subtotal',
        'tax',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PurchaseReturnItem::class;
    }
}
