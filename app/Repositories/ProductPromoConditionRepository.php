<?php

namespace App\Repositories;

use App\Models\ProductPromoCondition;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class ProductPromoConditionRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:31 am UTC
 *
 * @method ProductPromoCondition findWithoutFail($id, $columns = ['*'])
 * @method ProductPromoCondition find($id, $columns = ['*'])
 * @method ProductPromoCondition first($columns = ['*'])
*/
class ProductPromoConditionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_promo_id',
        'condition',
        'value',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductPromoCondition::class;
    }
}
