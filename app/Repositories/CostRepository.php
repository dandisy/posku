<?php

namespace App\Repositories;

use App\Models\Cost;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class CostRepository
 * @package App\Repositories
 * @version August 9, 2020, 3:25 am UTC
 *
 * @method Cost findWithoutFail($id, $columns = ['*'])
 * @method Cost find($id, $columns = ['*'])
 * @method Cost first($columns = ['*'])
*/
class CostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'description',
        'value',
        'balance',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cost::class;
    }
}
