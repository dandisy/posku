<?php

namespace App\Repositories;

use App\Models\FeaturedProduct;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class FeaturedProductRepository
 * @package App\Repositories
 * @version August 9, 2020, 8:33 am UTC
 *
 * @method FeaturedProduct findWithoutFail($id, $columns = ['*'])
 * @method FeaturedProduct find($id, $columns = ['*'])
 * @method FeaturedProduct first($columns = ['*'])
*/
class FeaturedProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store',
        'item',
        'publish_on',
        'publish_off',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FeaturedProduct::class;
    }
}
