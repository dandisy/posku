<?php

namespace App\Repositories;

use App\Models\Item;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class ItemRepository
 * @package App\Repositories
 * @version August 9, 2020, 7:41 am UTC
 *
 * @method Item findWithoutFail($id, $columns = ['*'])
 * @method Item find($id, $columns = ['*'])
 * @method Item first($columns = ['*'])
*/
class ItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'category',
        'code',
        'barcode',
        'unit',
        'wrap',
        'store',
        'purchase_price',
        'sell_price',
        'tax',
        'minimal_stock',
        'is_many_unique_barcode',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Item::class;
    }
}
