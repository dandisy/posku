<?php

namespace App\Repositories;

use App\Models\CompositeItem;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class CompositeItemRepository
 * @package App\Repositories
 * @version August 9, 2020, 8:07 am UTC
 *
 * @method CompositeItem findWithoutFail($id, $columns = ['*'])
 * @method CompositeItem find($id, $columns = ['*'])
 * @method CompositeItem first($columns = ['*'])
*/
class CompositeItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'composite_id',
        'store',
        'item',
        'unique_barcode',
        'item_amount',
        'Unit',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CompositeItem::class;
    }
}
