<?php

namespace App\Repositories;

use App\Models\PurchaseReturn;
use Webcore\Generator\Common\BaseRepository;

/**
 * Class PurchaseReturnRepository
 * @package App\Repositories
 * @version August 12, 2020, 9:43 am UTC
 *
 * @method PurchaseReturn findWithoutFail($id, $columns = ['*'])
 * @method PurchaseReturn find($id, $columns = ['*'])
 * @method PurchaseReturn first($columns = ['*'])
*/
class PurchaseReturnRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'purchase_id',
        'store',
        'supplier',
        'receipt_code',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PurchaseReturn::class;
    }
}
