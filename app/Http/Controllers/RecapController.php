<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecapRequest;
use App\Http\Requests\UpdateRecapRequest;
use App\Repositories\RecapRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class RecapController extends AppBaseController
{
    /** @var  RecapRepository */
    private $recapRepository;

    public function __construct(RecapRepository $recapRepo)
    {
        $this->middleware('auth');
        $this->recapRepository = $recapRepo;
    }

    /**
     * Display a listing of the Recap.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->recapRepository->pushCriteria(new RequestCriteria($request));
        $recaps = $this->recapRepository->all();

        return view('recaps.index')
            ->with('recaps', $recaps);
    }

    /**
     * Show the form for creating a new Recap.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('recaps.create');
        return view('recaps.create');
    }

    /**
     * Store a newly created Recap in storage.
     *
     * @param CreateRecapRequest $request
     *
     * @return Response
     */
    public function store(CreateRecapRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $recap = $this->recapRepository->create($input);
        $recap = \App\Models\Recap::create($input);

        Flash::success('Recap saved successfully.');

        return redirect(route('recaps.index'));
    }

    /**
     * Display the specified Recap.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recap = $this->recapRepository->findWithoutFail($id);

        if (empty($recap)) {
            Flash::error('Recap not found');

            return redirect(route('recaps.index'));
        }

        return view('recaps.show')->with('recap', $recap);
    }

    /**
     * Show the form for editing the specified Recap.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $recap = $this->recapRepository->findWithoutFail($id);

        if (empty($recap)) {
            Flash::error('Recap not found');

            return redirect(route('recaps.index'));
        }

        // edited by dandisy
        // return view('recaps.edit')->with('recap', $recap);
        return view('recaps.edit')
            ->with('recap', $recap);        
    }

    /**
     * Update the specified Recap in storage.
     *
     * @param  int              $id
     * @param UpdateRecapRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecapRequest $request)
    {
        $recap = $this->recapRepository->findWithoutFail($id);

        if (empty($recap)) {
            Flash::error('Recap not found');

            return redirect(route('recaps.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $recap = $this->recapRepository->update($input, $id);
        $recap = $recap->update($input);

        Flash::success('Recap updated successfully.');

        return redirect(route('recaps.index'));
    }

    /**
     * Remove the specified Recap from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recap = $this->recapRepository->findWithoutFail($id);

        if (empty($recap)) {
            Flash::error('Recap not found');

            return redirect(route('recaps.index'));
        }

        // $this->recapRepository->delete($id);
        $recap->delete();

        Flash::success('Recap deleted successfully.');

        return redirect(route('recaps.index'));
    }

    /**
     * Store data Recap from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $recap = $this->recapRepository->create($item->toArray());
            });
        });

        Flash::success('Recap saved successfully.');

        return redirect(route('recaps.index'));
    }
}
