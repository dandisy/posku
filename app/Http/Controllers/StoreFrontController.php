<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStoreFrontRequest;
use App\Http\Requests\UpdateStoreFrontRequest;
use App\Repositories\StoreFrontRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy
use Illuminate\Support\Facades\DB;

class StoreFrontController extends AppBaseController
{
    /** @var  StoreFrontRepository */
    private $storeFrontRepository;

    public function __construct(StoreFrontRepository $storeFrontRepo)
    {
        $this->middleware('auth');
        $this->storeFrontRepository = $storeFrontRepo;
    }

    /**
     * Display a listing of the StoreFront.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeFrontRepository->pushCriteria(new RequestCriteria($request));
        $storeFronts = $this->storeFrontRepository->with(['store'])->all();

        return view('store_fronts.index')
            ->with('storeFronts', $storeFronts);
    }

    /**
     * Show the form for creating a new StoreFront.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $storeFrontItems = collect();

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        $existingItemUniqueBarcode = \App\Models\StoreFrontItem::pluck('unique_barcode')->toArray();

        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->join('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->leftJoin('barcodes', function($query) use ($existingItemUniqueBarcode) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->where('barcodes.status', 'ready')
                    ->whereNull('barcodes.deleted_at')
                    ->whereNotIn('barcodes.barcode', $existingItemUniqueBarcode);
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            // ->where('barcodes.status', 'ready')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();
        

        // edited by dandisy
        // return view('store_fronts.create');
        return view('store_fronts.create')
            ->with('store', $store)
            ->with('storeFrontItems', $storeFrontItems)
            ->with('items', $items);
    }

    /**
     * Store a newly created StoreFront in storage.
     *
     * @param CreateStoreFrontRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreFrontRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;

        // $storeFront = $this->storeFrontRepository->create($input);
        $storeFront = \App\Models\StoreFront::create([
            'store' => $userStore,
            'note' => $request->note,
            'is_draft' => $request->is_draft,
            'attendance_id' => $attendanceId,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\StoreFrontItem::create([
                'store_front_id' => $storeFront->id,
                'item' => $item,
                'unique_barcode' => $request->unique_barcode[$k],
                'item_amount' => $request->item_amount[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
            
            $stockItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $item)->first();
            if($stockItem) {
                $stockItem->update(['item_amount' => $stockItem->item_amount + $request->item_amount[$k]]);
            } else {
                \App\Models\StoreFrontStock::create([
                    'store' => $userStore,
                    'item' => $item, 
                    'item_amount' => $request->item_amount[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);
            }

            if($request->unique_barcode[$k]) {
                \App\Models\Barcode::where('item', $item)
                    ->where('barcode', $request->unique_barcode[$k])
                    ->where('store', $userStore)
                    ->first()
                    ->update([
                        'status' => 'ready',
                        'store_front_id' => $storeFront->id,
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }

        \App\Services\LogService::handle([
            'description' => 'storeFronts.store : ' . $storeFront->id
        ]);

        Flash::success('Store Front saved successfully.');

        return redirect(route('storeFronts.index'));
    }

    /**
     * Display the specified StoreFront.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $storeFront = $this->storeFrontRepository->findWithoutFail($id);

        if (empty($storeFront)) {
            Flash::error('Store Front not found');

            return redirect(route('storeFronts.index'));
        }

        return view('store_fronts.show')->with('storeFront', $storeFront);
    }

    /**
     * Show the form for editing the specified StoreFront.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $storeFront = $this->storeFrontRepository->findWithoutFail($id);

        if (empty($storeFront)) {
            Flash::error('Store Front not found');

            return redirect(route('storeFronts.index'));
        }

        // added by dandisy
        $store = \App\Models\Store::all();

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        $existingItemUniqueBarcode = \App\Models\StoreFrontItem::pluck('unique_barcode')->toArray();

        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->join('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->leftJoin('barcodes', function($query) use ($existingItemUniqueBarcode) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->where('barcodes.status', 'ready')
                    ->whereNull('barcodes.deleted_at')
                    ->whereNotIn('barcodes.barcode', $existingItemUniqueBarcode);
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            ->where('barcodes.status', 'ready')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();
        
        // $storeFrontItems = \App\Models\StoreFrontItem::where('store_front_id', $storeFront->id)->get();
        $storeFrontItems = \Illuminate\Support\Facades\DB::table('store_front_items')
            ->join('items', 'items.id', '=', 'store_front_items.item')
            ->where('store_front_items.store_front_id', $storeFront->id)
            ->select('store_front_items.*', 'items.name as item_name')
            ->get();

        // edited by dandisy
        // return view('store_fronts.edit')->with('storeFront', $storeFront);
        return view('store_fronts.edit')
            ->with('storeFront', $storeFront)
            ->with('store', $store)
            ->with('storeFrontItems', $storeFrontItems)
            ->with('items', $items);        
    }

    /**
     * Update the specified StoreFront in storage.
     *
     * @param  int              $id
     * @param UpdateStoreFrontRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreFrontRequest $request)
    {
        $storeFront = $this->storeFrontRepository->findWithoutFail($id);

        if (empty($storeFront)) {
            Flash::error('Store Front not found');

            return redirect(route('storeFronts.index'));
        }

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;
        $input['attendance_id'] = $attendanceId;
        $input['store'] = $userStore;

        // $storeFront = $this->storeFrontRepository->update($input, $id);
        $storeFront = $storeFront->update($input);

        // deleting items
        $existingItems = \App\Models\StoreFrontItem::where('store_front_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\StoreFrontItem::where('store_front_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }
        // update barcode
        $existingBarcodes = \App\Models\Barcode::where('store_front_id', $id)
            ->where('store', $userStore)
            ->pluck('barcode')
            ->toArray();
        if($existingBarcodes) {
            $updateBarcodes = array_diff(array_values($existingBarcodes), $request->unique_barcode);
            if($updateBarcodes) {
                \App\Models\Barcode::where('store_front_id', $id)
                    ->where('store', $userStore)
                    ->whereIn('barcode', $updateBarcodes)
                    ->update([
                        'status' => 'ready',
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }
        foreach($request->store_front_id as $k => $relId) {
            if($relId) {
                $existingStoreFrontItem = \App\Models\StoreFrontItem::find($request->item_id[$k]);                
                $existingItemAmount = $existingStoreFrontItem->item_amount;

                $existingStoreFrontItem->update([
                    'item' => $request->item[$k],
                    'unique_barcode' => $request->unique_barcode[$k],
                    'item_amount' => $request->item_amount[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);

                $stockItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockItem->update(['item_amount' => $stockItem->item_amount + ($request->item_amount[$k] - $existingItemAmount)]);
            } else {
                \App\Models\StoreFrontItem::create([
                    'store_front_id' => $id,
                    'item' => $request->item[$k],
                    'unique_barcode' => $request->unique_barcode[$k],
                    'item_amount' => $request->item_amount[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $stockItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $request->item[$k])->first();
                if($stockItem) {
                    $stockItem->update(['item_amount' => $stockItem->item_amount + $request->item_amount[$k]]);
                } else {
                    \App\Models\StoreFrontStock::create([
                        'store' => $userStore,
                        'item' => $request->item[$k], 
                        'item_amount' => $request->item_amount[$k],
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }

            if($request->unique_barcode[$k]) {
                $currentBarcode = \App\Models\Barcode::where('item', $request->item[$k])
                    ->where('barcode', $request->unique_barcode[$k])
                    ->where('store', $userStore)
                    ->first();                    
                if($currentBarcode) {
                    $currentBarcode->update([
                        'status' => 'ready',
                        'store_front_id' => $id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }
        }

        \App\Services\LogService::handle([
            'description' => 'storeFronts.update : ' . $id
        ]);

        Flash::success('Store Front updated successfully.');

        return redirect(route('storeFronts.index'));
    }

    /**
     * Remove the specified StoreFront from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $storeFront = $this->storeFrontRepository->findWithoutFail($id);

        if (empty($storeFront)) {
            Flash::error('Store Front not found');

            return redirect(route('storeFronts.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->storeFrontRepository->delete($id);
        $storeFront->delete();

        $storeFrontItems = \App\Models\StoreFrontItem::where('store_front_id', $id)->get();
        foreach($storeFrontItems as $k => $item) {
            $stockItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount - $item->item_amount]);
        }
        $storeFrontItems->delete();
        \App\Models\Barcode::where('store_front_id', $id)->update([
            'status' => 'ready',
            'updated_by' => Auth::user()->id
        ]);

        \App\Services\LogService::handle([
            'description' => 'storeFronts.delete : ' . $id
        ]);

        Flash::success('Store Front deleted successfully.');

        return redirect(route('storeFronts.index'));
    }

    /**
     * Store data StoreFront from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $storeFront = $this->storeFrontRepository->create($item->toArray());
            });
        });

        Flash::success('Store Front saved successfully.');

        return redirect(route('storeFronts.index'));
    }
}
