<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStockTransferRequest;
use App\Http\Requests\UpdateStockTransferRequest;
use App\Repositories\StockTransferRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy
use Illuminate\Support\Facades\DB;

class StockTransferController extends AppBaseController
{
    /** @var  StockTransferRepository */
    private $stockTransferRepository;

    public function __construct(StockTransferRepository $stockTransferRepo)
    {
        $this->middleware('auth');
        $this->stockTransferRepository = $stockTransferRepo;
    }

    /**
     * Display a listing of the StockTransfer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->stockTransferRepository->pushCriteria(new RequestCriteria($request));
        $stockTransfers = $this->stockTransferRepository->with(['store'])->all();

        return view('stock_transfers.index')
            ->with('stockTransfers', $stockTransfers);
    }

    /**
     * Show the form for creating a new StockTransfer.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $stockTransferItems = collect();
        $units = \App\Models\Unit::all();
        $wraps = \App\Models\Wrap::all();

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->whereNull('barcodes.deleted_at');
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            ->whereNotIn('barcodes.status', ['sold', 'rejected'])
            // ->where('stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();        

        // edited by dandisy
        // return view('stock_transfers.create');
        return view('stock_transfers.create')
            ->with('store', $store)
            ->with('stockTransferItems', $stockTransferItems)
            ->with('units', $units)
            ->with('wraps', $wraps)
            ->with('items', $items);
    }

    /**
     * Store a newly created StockTransfer in storage.
     *
     * @param CreateStockTransferRequest $request
     *
     * @return Response
     */
    public function store(CreateStockTransferRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;

        // $stockTransfer = $this->stockTransferRepository->create($input);
        $stockTransfer = \App\Models\StockTransfer::create([
            'store' => $userStore,
            'type' => $request->type,
            'note' => $request->note,
            'attendance_id' => $attendanceId,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\StockTransferItem::create([
                'stock_transfer_id' => $stockTransfer->id,
                'item' => $item,
                'unit' => $request->unit[$k],
                'wrap' => $request->wrap[$k],
                'item_amount' => $request->item_amount[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item)->first();
            if($request->type == 'out') {
                $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount[$k]]);
                
                if($request->unique_barcode[$k]) {
                    $currentBarcode = \App\Models\Barcode::where('item', $item)
                        ->where('barcode', $request->unique_barcode[$k])
                        ->first();
                    if($currentBarcode) {
                        $currentBarcode->update([
                            'store' => $userStore,
                            'status' => 'transfered',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
            if($request->type == 'in') {
                if($stockItem) {
                    $existingStockStoreAmount = $stockItem->item_amount;

                    $stockItem->update([
                        'item_amount' => $existingStockStoreAmount + $request->item_amount[$k],
                        'updated_by' => Auth::user()->id
                    ]);
                } else {                
                    \App\Models\Stock::create([
                        'store' => $userStore,
                        'item' => $item,
                        'item_amount' => $request->item_amount[$k],
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
                
                if($request->unique_barcode[$k]) {
                    $currentBarcode = \App\Models\Barcode::where('item', $item)
                        ->where('barcode', $request->unique_barcode[$k])
                        ->first();
                    if($currentBarcode) {
                        $currentBarcode->update([
                            'store' => $userStore,
                            'status' => 'ready',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }

        \App\Services\LogService::handle([
            'description' => 'stockTransfers.store : ' . $stockTransfer->id
        ]);

        Flash::success('Stock Transfer saved successfully.');

        return redirect(route('stockTransfers.index'));
    }

    /**
     * Display the specified StockTransfer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stockTransfer = $this->stockTransferRepository->findWithoutFail($id);

        if (empty($stockTransfer)) {
            Flash::error('Stock Transfer not found');

            return redirect(route('stockTransfers.index'));
        }

        return view('stock_transfers.show')->with('stockTransfer', $stockTransfer);
    }

    /**
     * Show the form for editing the specified StockTransfer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stockTransfer = $this->stockTransferRepository->findWithoutFail($id);

        if (empty($stockTransfer)) {
            Flash::error('Stock Transfer not found');

            return redirect(route('stockTransfers.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();

        $units = \App\Models\Unit::all();
        $wraps = \App\Models\Wrap::all();

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;
        
        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->whereNull('barcodes.deleted_at');
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            ->whereNotIn('barcodes.status', ['sold', 'rejected'])
            // ->where('stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();

        // $stockTransferItems = \App\Models\StockTransferItem::where('stock_transfer_id', $stockTransfer->id)->get();
        $stockTransferItems = \Illuminate\Support\Facades\DB::table('stock_transfer_items')
            ->join('items', 'items.id', '=', 'stock_transfer_items.item')
            ->where('stock_transfer_items.stock_transfer_id', $stockTransfer->id)
            ->select('stock_transfer_items.*', 'items.name as item_name')
            ->get();

        // edited by dandisy
        // return view('stock_transfers.edit')->with('stockTransfer', $stockTransfer);
        return view('stock_transfers.edit')
            ->with('stockTransfer', $stockTransfer)
            ->with('store', $store)
            ->with('units', $units)
            ->with('wraps', $wraps)
            ->with('items', $items)
            ->with('stockTransferItems', $stockTransferItems);        
    }

    /**
     * Update the specified StockTransfer in storage.
     *
     * @param  int              $id
     * @param UpdateStockTransferRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockTransferRequest $request)
    {
        $stockTransfer = $this->stockTransferRepository->findWithoutFail($id);

        if (empty($stockTransfer)) {
            Flash::error('Stock Transfer not found');

            return redirect(route('stockTransfers.index'));
        }

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;
        $input['attendance_id'] = $attendanceId;
        $input['store'] = $userStore;

        // $stockTransfer = $this->stockTransferRepository->update($input, $id);
        $stockTransfer = $stockTransfer->update($input);

        // deleting items
        $existingItems = \App\Models\StockTransferItem::where('stock_transfer_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\StockTransferItem::where('stock_transfer_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }
        foreach($request->stock_trasfer_id as $k => $relId) {
            if($relId) {
                $existingStockTransferItem = \App\Models\StockTransferItem::find($relId);
                $existingItemAmount = $existingStockTransferItem->item_amount;

                $existingStockTransferItem->update([
                    'item' => $request->item[$k],
                    'unit' => $request->unit[$k],
                    'wrap' => $request->wrap[$k],
                    'item_amount' => $request->item_amount[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);
                
                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                if($request->type == 'out') {
                    $stockItem->update(['item_amount' => $stockItem->item_amount - ($request->item_amount[$k] - $existingItemAmount)]);
                }
                if($request->type == 'in') {
                    if($stockItem) {
                        $existingStockStoreAmount = $stockItem->item_amount;
        
                        $stockItem->update([
                            'store' => $userStore,
                            'item_amount' => $existingStockStoreAmount + ($request->item_amount[$k] - $existingItemAmount),
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        \App\Models\Stock::create([
                            'store' => $userStore,
                            'item' => $request->item[$k],
                            'item_amount' => $request->item_amount[$k],
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            } else {
                \App\Models\StockTransferItem::create([
                    'stock_trasfer_id' => $id,
                    'item' => $request->item[$k],
                    'unique_barcode' => $request->unique_barcode[$k],
                    'unit' => $request->unit[$k],
                    'wrap' => $request->wrap[$k],
                    'item_amount' => $request->item_amount[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                if($request->type == 'out') {
                    $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount[$k]]);
                }
                if($request->type == 'in') {
                    if($stockItem) {
                        $existingStockStoreAmount = $stockItem->item_amount;
        
                        $stockItem->update([
                            'store' => $userStore,
                            'item_amount' => $existingStockStoreAmount + ($request->item_amount[$k] - $existingItemAmount),
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        \App\Models\Stock::create([
                            'store' => $userStore,
                            'item' => $request->item[$k],
                            'item_amount' => $request->item_amount[$k],
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            if($request->type == 'out') {
                // update barcode
                $existingBarcodes = \App\Models\Barcode::where('stock_transfer_id', $id)
                    ->where('item', $request->item[$k])
                    ->where('store', $userStore)
                    ->pluck('barcode')
                    ->toArray();
                if($existingBarcodes) {
                    $updateBarcodes = array_diff(array_values($existingBarcodes), $request->unique_barcode[$k]);
                    if($updateBarcodes) {
                        \App\Models\Barcode::where('stock_transfer_id', $id)
                        ->where('item', $request->item[$k])
                        ->where('store', $userStore)
                        ->whereIn('barcode', $updateBarcodes)
                        ->update([
                            'status' => 'ready',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
                if($request->unique_barcode[$k]) {
                    $currentBarcode = \App\Models\Barcode::where('stock_transfer_id', $id)
                        ->where('item', $request->item[$k])
                        ->where('store', $userStore)
                        ->where('barcode', $request->unique_barcode[$k])
                        ->first();                    
                    if($currentBarcode) {
                        $currentBarcode->update([
                            'status' => 'transfered',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
            if($request->type == 'in') {
                // update barcode
                $existingBarcodes = \App\Models\Barcode::where('stock_transfer_id', $id)
                    ->where('item', $request->item[$k])
                    ->pluck('barcode')
                    ->toArray();
                if($existingBarcodes) {
                    $updateBarcodes = array_diff(array_values($existingBarcodes), $request->unique_barcode[$k]);
                    if($updateBarcodes) {
                        \App\Models\Barcode::where('store_front_id', $id)
                        ->where('item', $request->item[$k])
                        ->whereIn('barcode', $updateBarcodes)
                        ->update([
                            'store' => 0, // todo: system cannot track value before
                            'status' => 'transfered',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
                if($request->unique_barcode[$k]) {
                    $currentBarcode = \App\Models\Barcode::where('stock_transfer_id', $id)
                        ->where('item', $request->item[$k])
                        ->where('barcode', $request->unique_barcode[$k])
                        ->first();                    
                    if($currentBarcode) {
                        $currentBarcode->update([
                            'store' => $userStore,
                            'status' => 'ready',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }

        \App\Services\LogService::handle([
            'description' => 'stockTransfers.update : ' . $id
        ]);

        Flash::success('Stock Transfer updated successfully.');

        return redirect(route('stockTransfers.index'));
    }

    /**
     * Remove the specified StockTransfer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stockTransfer = $this->stockTransferRepository->findWithoutFail($id);

        if (empty($stockTransfer)) {
            Flash::error('Stock Transfer not found');

            return redirect(route('stockTransfers.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->stockTransferRepository->delete($id);
        $stockTransfer->delete();

        $stockTransferItems = \App\Models\StockTransferItem::where('stock_transfer_id', $id);
        foreach($stockTransferItems->get() as $k => $item) {
            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            if($stockTransfer->type == 'out') {
                $stockItem->update(['item_amount' => $stockItem->item_amount + $item->item_amount]);
            }
            if($stockTransfer->type == 'in') {
                $stockItem->update(['item_amount' => $stockItem->item_amount - $item->item_amount]);
            }
        }
        $stockTransferItems->delete();
        \App\Models\Barcode::where('stock_transfer_id', $id)->update([
            'store' => 0, // todo: system cannot track value before
            'status' => 'ready',
            'updated_by' => Auth::user()->id
        ]);

        \App\Services\LogService::handle([
            'description' => 'stockTransfers.delete : ' . $id
        ]);

        Flash::success('Stock Transfer deleted successfully.');

        return redirect(route('stockTransfers.index'));
    }

    /**
     * Store data StockTransfer from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $stockTransfer = $this->stockTransferRepository->create($item->toArray());
            });
        });

        Flash::success('Stock Transfer saved successfully.');

        return redirect(route('stockTransfers.index'));
    }
}
