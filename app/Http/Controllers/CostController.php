<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCostRequest;
use App\Http\Requests\UpdateCostRequest;
use App\Repositories\CostRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class CostController extends AppBaseController
{
    /** @var  CostRepository */
    private $costRepository;

    public function __construct(CostRepository $costRepo)
    {
        $this->middleware('auth');
        $this->costRepository = $costRepo;
    }

    /**
     * Display a listing of the Cost.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costRepository->pushCriteria(new RequestCriteria($request));
        $costs = $this->costRepository->all();

        return view('costs.index')
            ->with('costs', $costs);
    }

    /**
     * Show the form for creating a new Cost.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('costs.create');
        return view('costs.create')
            ->with('store', $store);
    }

    /**
     * Store a newly created Cost in storage.
     *
     * @param CreateCostRequest $request
     *
     * @return Response
     */
    public function store(CreateCostRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $cost = $this->costRepository->create($input);
        $cost = \App\Models\Cost::create($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Cost',
            'value' => $request->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'costs.store : ' . $cost->id
        ]);

        Flash::success('Cost saved successfully.');

        return redirect(route('costs.index'));
    }

    /**
     * Display the specified Cost.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cost = $this->costRepository->findWithoutFail($id);

        if (empty($cost)) {
            Flash::error('Cost not found');

            return redirect(route('costs.index'));
        }

        return view('costs.show')->with('cost', $cost);
    }

    /**
     * Show the form for editing the specified Cost.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cost = $this->costRepository->findWithoutFail($id);

        if (empty($cost)) {
            Flash::error('Cost not found');

            return redirect(route('costs.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('costs.edit')->with('cost', $cost);
        return view('costs.edit')
            ->with('cost', $cost)
            ->with('store', $store);        
    }

    /**
     * Update the specified Cost in storage.
     *
     * @param  int              $id
     * @param UpdateCostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCostRequest $request)
    {
        $cost = $this->costRepository->findWithoutFail($id);

        if (empty($cost)) {
            Flash::error('Cost not found');

            return redirect(route('costs.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $cost = $this->costRepository->update($input, $id);
        $cost = $cost->update($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Cost',
            'value' => $request->value - $cashValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'costs.update : ' . $id
        ]);

        Flash::success('Cost updated successfully.');

        return redirect(route('costs.index'));
    }

    /**
     * Remove the specified Cost from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cost = $this->costRepository->findWithoutFail($id);

        if (empty($cost)) {
            Flash::error('Cost not found');

            return redirect(route('costs.index'));
        }

        // $this->costRepository->delete($id);
        $cost->delete();

        \App\Services\RecapService::handle([
            'model_type' => 'Cost',
            'value' => -1 * $cost->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'costs.dalete : ' . $id
        ]);

        Flash::success('Cost deleted successfully.');

        return redirect(route('costs.index'));
    }

    /**
     * Store data Cost from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $cost = $this->costRepository->create($item->toArray());
            });
        });

        Flash::success('Cost saved successfully.');

        return redirect(route('costs.index'));
    }
}
