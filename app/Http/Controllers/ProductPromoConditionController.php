<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductPromoConditionRequest;
use App\Http\Requests\UpdateProductPromoConditionRequest;
use App\Repositories\ProductPromoConditionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class ProductPromoConditionController extends AppBaseController
{
    /** @var  ProductPromoConditionRepository */
    private $productPromoConditionRepository;

    public function __construct(ProductPromoConditionRepository $productPromoConditionRepo)
    {
        $this->middleware('auth');
        $this->productPromoConditionRepository = $productPromoConditionRepo;
    }

    /**
     * Display a listing of the ProductPromoCondition.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productPromoConditionRepository->pushCriteria(new RequestCriteria($request));
        $productPromoConditions = $this->productPromoConditionRepository->all();

        return view('product_promo_conditions.index')
            ->with('productPromoConditions', $productPromoConditions);
    }

    /**
     * Show the form for creating a new ProductPromoCondition.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $promocondition = \App\Models\PromoCondition::all();
        

        // edited by dandisy
        // return view('product_promo_conditions.create');
        return view('product_promo_conditions.create')
            ->with('promocondition', $promocondition);
    }

    /**
     * Store a newly created ProductPromoCondition in storage.
     *
     * @param CreateProductPromoConditionRequest $request
     *
     * @return Response
     */
    public function store(CreateProductPromoConditionRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $productPromoCondition = $this->productPromoConditionRepository->create($input);
        $productPromoCondition = \App\Models\ProductPromoCondition::create($input);

        \App\Services\LogService::handle([
            'description' => 'productPromoConditions.store : ' . $productPromoCondition->id
        ]);

        Flash::success('Product Promo Condition saved successfully.');

        return redirect(route('productPromoConditions.index'));
    }

    /**
     * Display the specified ProductPromoCondition.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productPromoCondition = $this->productPromoConditionRepository->findWithoutFail($id);

        if (empty($productPromoCondition)) {
            Flash::error('Product Promo Condition not found');

            return redirect(route('productPromoConditions.index'));
        }

        return view('product_promo_conditions.show')->with('productPromoCondition', $productPromoCondition);
    }

    /**
     * Show the form for editing the specified ProductPromoCondition.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productPromoCondition = $this->productPromoConditionRepository->findWithoutFail($id);

        if (empty($productPromoCondition)) {
            Flash::error('Product Promo Condition not found');

            return redirect(route('productPromoConditions.index'));
        }
        
        // added by dandisy
        $promocondition = \App\Models\PromoCondition::all();

        // edited by dandisy
        // return view('product_promo_conditions.edit')->with('productPromoCondition', $productPromoCondition);
        return view('product_promo_conditions.edit')
            ->with('productPromoCondition', $productPromoCondition)
            ->with('promocondition', $promocondition);        
    }

    /**
     * Update the specified ProductPromoCondition in storage.
     *
     * @param  int              $id
     * @param UpdateProductPromoConditionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductPromoConditionRequest $request)
    {
        $productPromoCondition = $this->productPromoConditionRepository->findWithoutFail($id);

        if (empty($productPromoCondition)) {
            Flash::error('Product Promo Condition not found');

            return redirect(route('productPromoConditions.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $productPromoCondition = $this->productPromoConditionRepository->update($input, $id);
        $productPromoCondition = $productPromoCondition->update($input);

        \App\Services\LogService::handle([
            'description' => 'productPromoConditions.update : ' . $id
        ]);

        Flash::success('Product Promo Condition updated successfully.');

        return redirect(route('productPromoConditions.index'));
    }

    /**
     * Remove the specified ProductPromoCondition from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productPromoCondition = $this->productPromoConditionRepository->findWithoutFail($id);

        if (empty($productPromoCondition)) {
            if($request->ajax()){
                return 'Product Promo Condition not found';
            }

            Flash::error('Product Promo Condition not found');

            return redirect(route('productPromoConditions.index'));
        }

        // $this->productPromoConditionRepository->delete($id);
        $productPromoCondition->delete();

        \App\Services\LogService::handle([
            'description' => 'productPromoConditions.delete : ' . $id
        ]);
        
        if($request->ajax()){
            return 'Product Promo Condition deleted successfully.';
        }

        Flash::success('Product Promo Condition deleted successfully.');

        return redirect(route('productPromoConditions.index'));
    }

    /**
     * Store data ProductPromoCondition from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $productPromoCondition = $this->productPromoConditionRepository->create($item->toArray());
            });
        });

        Flash::success('Product Promo Condition saved successfully.');

        return redirect(route('productPromoConditions.index'));
    }
}
