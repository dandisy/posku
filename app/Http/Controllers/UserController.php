<?php

namespace App\Http\Controllers;

use App\User;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; // added by dandisy

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the User.
     *
     * @return Response
     */
    public function index()
    {
        return view('users.index')->with('users', \App\User::all());
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create')->with('role', \Spatie\Permission\Models\Role::all());
    }

    /**
     * Store a newly created User in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->password === $request->confirm_password) {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $user->assignRole($request->role);

            \App\Services\LogService::handle([
                'description' => 'users.store : ' . $user->id
            ]);

            Flash::success('User saved successfully.');
        } else {
            Flash::error('Password not match.');
        }

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // handling edit profile non admin
        if(!Auth::user()->hasRole('admin') && Auth::user()->id != $id) {
            return abort(404);
        }

        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }
        
        $user['role'] = $user->roles->first()->id;

        return view('users.edit')->with('user', $user)->with('role', \Spatie\Permission\Models\Role::all());
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        // handling edit profile non admin
        if(!Auth::user()->hasRole('admin') && Auth::user()->id != $id) {
            return abort(404);
        }
        
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');
            
            // handling edit profile non admin
            if(Auth::user()->hasRole('admin')) {
                return redirect(url('dashboard'));
            }

            return redirect(route('users.index'));
        }

        if($request->role) {
            $user->syncRoles($request->role);
        }

        $input = $request->all();
        if($request->password) {
            if($request->password === $request->confirm_password) {
                $input['password'] = bcrypt($request->password);
            } else {
                Flash::success('Password not match.');

                // handling edit profile non admin
                if(Auth::user()->hasRole('admin')) {
                    return redirect(url('dashboard'));
                }

                return redirect(route('users.index'));
            }
        } else {
            $input['password'] = $user->password;
        }

        $user = $user->update($input);

        \App\Services\LogService::handle([
            'description' => 'users.update : ' . $id
        ]);

        Flash::success('User updated successfully.');

        // handling edit profile non admin
        if(Auth::user()->hasRole('admin')) {
            return redirect(url('dashboard'));
        }

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $user->delete();

        \App\Services\LogService::handle([
            'description' => 'users.delete : ' . $id
        ]);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
