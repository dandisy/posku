<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSellReturnRequest;
use App\Http\Requests\UpdateSellReturnRequest;
use App\Repositories\SellReturnRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy
use Illuminate\Support\Facades\DB;

class SellReturnController extends AppBaseController
{
    /** @var  SellReturnRepository */
    private $sellReturnRepository;

    public function __construct(SellReturnRepository $sellReturnRepo)
    {
        $this->middleware('auth');
        $this->sellReturnRepository = $sellReturnRepo;
    }

    /**
     * Display a listing of the SellReturn.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sellReturnRepository->pushCriteria(new RequestCriteria($request));
        $sellReturns = $this->sellReturnRepository->with(['customer'])->all();

        return view('sell_returns.index')
            ->with('sellReturns', $sellReturns);
    }

    /**
     * Show the form for creating a new SellReturn.
     *
     * @return Response
     */
    public function create()
    {
        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // added by dandisy
        $sell = \App\Models\Sell::where('store', $userStore)->get();
        $customer = \App\Models\Customer::all();
        $store = \App\Models\Store::all();
        $paymentmethod = \App\Models\PaymentMethod::all();
        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->whereNull('barcodes.deleted_at');
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            ->where('barcodes.status', 'sold')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();

        $sellReturnItems = collect();
        

        // edited by dandisy
        // return view('sell_returns.create');
        return view('sell_returns.create')
            ->with('sell', $sell)
            ->with('customer', $customer)
            ->with('store', $store)
            ->with('paymentmethod', $paymentmethod)
            ->with('items', $items)
            ->with('sellReturnItems', $sellReturnItems);
    }

    /**
     * Store a newly created SellReturn in storage.
     *
     * @param CreateSellReturnRequest $request
     *
     * @return Response
     */
    public function store(CreateSellReturnRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;

        // $sellReturn = $this->sellReturnRepository->create($input);
        $sellReturn = \App\Models\SellReturn::create([
            'sell_id' => $request->sell_id,
            'customer' => $request->customer,
            'store' => $userStore,
            'bill_amount' => $request->bill_amount,
            'payment_method' => $request->payment_method,
            'payment_status' => $request->payment_status,
            'note' => $request->note,
            'is_draft' => $request->is_draft,
            'attendance_id' => $attendanceId,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\SellReturnItem::create([
                'sell_return_id' => $sellReturn->id,
                'store' => $userStore,
                'item' => $item,                
                'unique_barcode' => $request->unique_barcode[$k],
                'item_discount' => $request->item_discount[$k],
                'item_price' => $request->item_price[$k],
                'item_amount' => $request->item_amount[$k],
                'discount_subtotal' => $request->discount_subtotal[$k],
                'price_subtotal' => $request->price_subtotal[$k],
                'tax' => $request->tax[$k],
                'item_return_type' => $request->item_return_type[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
            
            if($request->item_return_type[$k] == 'ready') {
                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item)->first();            
                $stockItem->update(['item_amount' => $stockItem->item_amount + $request->item_amount[$k]]);
            }
            if($request->item_return_type[$k] == 'rejected') {
                $itemReturn = \App\Models\RejectedItem::where('store', $userStore)->where('item', $item)->first();
                if($itemReturn) {
                    $itemReturn->update(['item_amount' => $itemReturn->item_amount + $request->item_amount[$k]]);
                } else {
                    \App\Models\RejectedItem::create([
                        'store' => $userStore,
                        'source' => 'return',
                        'item' => $item,
                        'unique_barcode' => $request->unique_barcode[$k],
                        'item_amount' => $request->item_amount[$k],
                        'note' => $request->item_note[$k],
                        'model_type' => 'SellReturn',
                        'model_id' => $sellReturn->id,
                        'attendance_id' => $attendanceId,
                    ]);
                }
            }

            if($request->unique_barcode[$k]) {
                \App\Models\Barcode::where('item', $item)
                    ->where('barcode', $request->unique_barcode[$k])
                    ->where('store', $userStore)
                    ->first()
                    ->update([
                        'status' => $request->item_return_type[$k],
                        'sell_return_id' => $sellReturn->id,
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'SellReturn',
            'value' => $request->bill_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'sellReturns.store : ' . $sellReturn->id
        ]);

        Flash::success('Sell Return saved successfully.');

        return redirect(route('sellReturns.index'));
    }

    /**
     * Display the specified SellReturn.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sellReturn = $this->sellReturnRepository->findWithoutFail($id);

        if (empty($sellReturn)) {
            Flash::error('Sell Return not found');

            return redirect(route('sellReturns.index'));
        }

        return view('sell_returns.show')->with('sellReturn', $sellReturn);
    }

    /**
     * Show the form for editing the specified SellReturn.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sellReturn = $this->sellReturnRepository->findWithoutFail($id);

        if (empty($sellReturn)) {
            Flash::error('Sell Return not found');

            return redirect(route('sellReturns.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // added by dandisy
        $sell = \App\Models\Sell::where('store', $userStore)->get();
        $customer = \App\Models\Customer::all();
        $store = \App\Models\Store::all();
        $paymentmethod = \App\Models\PaymentMethod::all();

        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->whereNull('barcodes.deleted_at');
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            ->where('barcodes.status', 'sold')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();

        $sellReturnItems = \App\Models\SellReturnItem::where('sell_return_id', $id)->get();

        // edited by dandisy
        // return view('sell_returns.edit')->with('sellReturn', $sellReturn);
        return view('sell_returns.edit')
            ->with('sellReturn', $sellReturn)
            ->with('sell', $sell)
            ->with('customer', $customer)
            ->with('store', $store)
            ->with('paymentmethod', $paymentmethod)
            ->with('items', $items)
            ->with('sellReturnItems', $sellReturnItems);        
    }

    /**
     * Update the specified SellReturn in storage.
     *
     * @param  int              $id
     * @param UpdateSellReturnRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellReturnRequest $request)
    {
        $sellReturn = $this->sellReturnRepository->findWithoutFail($id);
        $sellReturnValueBefore = $sellReturn->bill_amount;

        if (empty($sellReturn)) {
            Flash::error('Sell Return not found');

            return redirect(route('sellReturns.index'));
        }

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;
        $input['attendance_id'] = $attendanceId;
        $input['store'] = $userStore;

        // $sellReturn = $this->sellReturnRepository->update($input, $id);
        $sellReturn = $sellReturn->update($input);

        // deleting items
        $existingItems = \App\Models\SellReturnItem::where('sell_return_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\SellReturnItem::where('sell_return_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }
        // update barcode
        $existingBarcodes = \App\Models\Barcode::where('sell_return_id', $id)
            ->where('store', $userStore)
            ->pluck('barcode')
            ->toArray();
        if($existingBarcodes) {
            $updateBarcodes = array_diff(array_values($existingBarcodes), $request->unique_barcode);
            if($updateBarcodes) {
                \App\Models\Barcode::where('sell_return_id', $id)
                    ->where('store', $userStore)
                    ->whereIn('barcode', $updateBarcodes)
                    ->update([
                        'status' => 'sold',
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }
        foreach($request->sell_return_id as $k => $relId) {
            if($relId) {
                $existingSellReturnItem = \App\Models\SellReturnItem::find($relId);
                $existingItemAmount = $existingSellReturnItem->item_amount;

                $existingSellReturnItem->update([
                    'item' =>  $request->item[$k],
                    'store' => $userStore,
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'item_return_type' => $request->item_return_type[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);
            
                if($request->item_return_type[$k] == 'ready') {
                    $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                    $stockItem->update(['item_amount' => $stockItem->item_amount + ($request->item_amount[$k] - $existingItemAmount)]);
                }
                if($request->item_return_type[$k] == 'rejected') {
                    $itemReturn = \App\Models\RejectedItem::where('store', $userStore)->where('item', $item)->first();
                    if($itemReturn) {
                        $itemReturn->update(['item_amount' => $itemReturn->item_amount + ($request->item_amount[$k] - $existingItemAmount)]);
                    } else {
                        \App\Models\RejectedItem::create([
                            'store' => $userStore,
                            'source' => 'return',
                            'item' => $item,
                            'unique_barcode' => $request->unique_barcode[$k],
                            'item_amount' => $request->item_amount[$k],
                            'note' => $request->item_note[$k],
                            'model_type' => 'SellReturn',
                            'model_id' => $id,
                            'attendance_id' => $attendanceId,
                        ]);
                    }
                }
            } else {
                \App\Models\SellReturnItem::create([
                    'sell_return_id' => $id,
                    'store' => $userStore,
                    'item' =>  $request->item[$k],                    
                    'unique_barcode' => $request->unique_barcode[$k],
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'item_return_type' => $request->item_return_type[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);
            
                if($request->item_return_type[$k] == 'ready') {
                    $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                    $stockItem->update(['item_amount' => $stockItem->item_amount + $request->item_amount[$k]]);
                }
                if($request->item_return_type[$k] == 'rejected') {
                    $itemReturn = \App\Models\RejectedItem::where('store', $userStore)->where('item', $item)->first();
                    if($itemReturn) {
                        $itemReturn->update(['item_amount' => $itemReturn->item_amount + $request->item_amount[$k]]);
                    } else {
                        \App\Models\RejectedItem::create([
                            'store' => $userStore,
                            'source' => 'return',
                            'item' => $item,
                            'unique_barcode' => $request->unique_barcode[$k],
                            'item_amount' => $request->item_amount[$k],
                            'note' => $request->item_note[$k],
                            'model_type' => 'SellReturn',
                            'model_id' => $id,
                            'attendance_id' => $attendanceId,
                        ]);
                    }
                }
            }

            if($request->unique_barcode[$k]) {
                \App\Models\Barcode::where('item', $request->item[$k])
                    ->where('barcode', $request->unique_barcode[$k])
                    ->where('store', $userStore)
                    ->first()
                    ->update([
                        'status' => $request->item_return_type[$k],
                        'sell_return_id' => $id,
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'SellReturn',
            'value' => $request->bill_amount - $sellReturnValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'sellReturns.update : ' . $id
        ]);

        Flash::success('Sell Return updated successfully.');

        return redirect(route('sellReturns.index'));
    }

    /**
     * Remove the specified SellReturn from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sellReturn = $this->sellReturnRepository->findWithoutFail($id);

        if (empty($sellReturn)) {
            Flash::error('Sell Return not found');

            return redirect(route('sellReturns.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->sellReturnRepository->delete($id);
        $sellReturn->delete();

        $sellReturnItems = \App\Models\SellReturnItem::where('sell_return_id', $id);
        foreach($sellReturnItems->get() as $k => $item) {
            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount - $item->item_amount]);
        }
        $sellReturnItems->delete();
        \App\Models\Barcode::where('sell_return_id', $id)->update([
            'status' => 'sold',
            'updated_by' => Auth::user()->id
        ]);

        \App\Services\RecapService::handle([
            'model_type' => 'SellReturn',
            'value' => -1 * $sellReturn->bill_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'sellReturns.delete : ' . $id
        ]);

        Flash::success('Sell Return deleted successfully.');

        return redirect(route('sellReturns.index'));
    }

    /**
     * Store data SellReturn from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $sellReturn = $this->sellReturnRepository->create($item->toArray());
            });
        });

        Flash::success('Sell Return saved successfully.');

        return redirect(route('sellReturns.index'));
    }
}
