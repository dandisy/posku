<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductPromoRequest;
use App\Http\Requests\UpdateProductPromoRequest;
use App\Repositories\ProductPromoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class ProductPromoController extends AppBaseController
{
    /** @var  ProductPromoRepository */
    private $productPromoRepository;

    public function __construct(ProductPromoRepository $productPromoRepo)
    {
        $this->middleware('auth');
        $this->productPromoRepository = $productPromoRepo;
    }

    /**
     * Display a listing of the ProductPromo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->productPromoRepository->pushCriteria(new RequestCriteria($request));
        $productPromos = $this->productPromoRepository->all();

        return view('product_promos.index')
            ->with('productPromos', $productPromos);
    }

    /**
     * Show the form for creating a new ProductPromo.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::all();
        $promoConditions = \App\Models\PromoCondition::all();
        $productPromoConditions = collect();
        

        // edited by dandisy
        // return view('product_promos.create');
        return view('product_promos.create')
            ->with('store', $store)
            ->with('item', $item)
            ->with('promoConditions', $promoConditions)
            ->with('productPromoConditions', $productPromoConditions);
    }

    /**
     * Store a newly created ProductPromo in storage.
     *
     * @param CreateProductPromoRequest $request
     *
     * @return Response
     */
    public function store(CreateProductPromoRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        // $productPromo = $this->productPromoRepository->create($input);
        $productPromo = \App\Models\ProductPromo::create([
            'item' => $request->item,
            'value' => $request->value,
            'publish_on' => $request->publish_on,
            'publish_off' => $request->publish_off,
            'store' => $request->store,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->condition as $k => $condition) {
            \App\Models\ProductPromoCondition::create([
                'product_promo_id' => $productPromo->id,
                'condition' => $condition,
                'value' => $request->value_condition[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
        }

        \App\Services\RecapService::handle([
            'model_type' => 'ProductPromo',
            'value' => $request->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'productPromos.store : ' . $productPromo->id
        ]);

        Flash::success('Product Promo saved successfully.');

        return redirect(route('productPromos.index'));
    }

    /**
     * Display the specified ProductPromo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productPromo = $this->productPromoRepository->findWithoutFail($id);

        if (empty($productPromo)) {
            Flash::error('Product Promo not found');

            return redirect(route('productPromos.index'));
        }

        return view('product_promos.show')->with('productPromo', $productPromo);
    }

    /**
     * Show the form for editing the specified ProductPromo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productPromo = $this->productPromoRepository->findWithoutFail($id);

        if (empty($productPromo)) {
            Flash::error('Product Promo not found');

            return redirect(route('productPromos.index'));
        }

        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::all();
        $promoConditions = \App\Models\PromoCondition::all();
        $productPromoConditions = \App\Models\ProductPromoCondition::where('product_promo_id', $productPromo->id)->get();

        // edited by dandisy
        // return view('product_promos.edit')->with('productPromo', $productPromo);
        return view('product_promos.edit')
            ->with('productPromo', $productPromo)
            ->with('store', $store)
            ->with('item', $item)
            ->with('promoConditions', $promoConditions)
            ->with('productPromoConditions', $productPromoConditions);        
    }

    /**
     * Update the specified ProductPromo in storage.
     *
     * @param  int              $id
     * @param UpdateProductPromoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductPromoRequest $request)
    {
        $productPromo = $this->productPromoRepository->findWithoutFail($id);
        $productPromoValueBefore = $productPromo->value;

        if (empty($productPromo)) {
            Flash::error('Product Promo not found');

            return redirect(route('productPromos.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $productPromo = $this->productPromoRepository->update($input, $id);
        $productPromo = $productPromo->update($input);

        // deleting conditions
        $existingConditions = \App\Models\ProductPromoCondition::where('product_promo_id', $id)->pluck('condition')->toArray();
        if($existingConditions) {
            $deleteConditions = array_diff(array_values($existingConditions), $request->condition);
            if($deleteConditions) {
                \App\Models\ProductPromoCondition::where('product_promo_id', $id)->whereIn('condition', $deleteConditions)->delete();
            }
        }
        foreach($request->product_promo_id as $k => $relId) {
            if($relId) {
                (\App\Models\ProductPromoCondition::find($relId))->update([
                    'condition' => $request->condition[$k],
                    'value' => $request->value[$k],
                    'updated_by' => Auth::user()->id
                ]);
            } else {
                \App\Models\ProductPromoCondition::create([
                    'product_promo_id' => $id,
                    'condition' => $request->condition[$k],
                    'value' => $request->value[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'ProductPromo',
            'value' => $request->value - $productPromoValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'productPromos.update : ' . $id
        ]);

        Flash::success('Product Promo updated successfully.');

        return redirect(route('productPromos.index'));
    }

    /**
     * Remove the specified ProductPromo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productPromo = $this->productPromoRepository->findWithoutFail($id);

        if (empty($productPromo)) {
            Flash::error('Product Promo not found');

            return redirect(route('productPromos.index'));
        }

        // $this->productPromoRepository->delete($id);
        $productPromo->delete();

        \App\Models\ProductPromoCondition::where('product_promo_id', $id)->delete();

        \App\Services\RecapService::handle([
            'model_type' => 'ProductPromo',
            'value' => -1 * $productPromo->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'productPromos.delete : ' . $id
        ]);

        Flash::success('Product Promo deleted successfully.');

        return redirect(route('productPromos.index'));
    }

    /**
     * Store data ProductPromo from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $productPromo = $this->productPromoRepository->create($item->toArray());
            });
        });

        Flash::success('Product Promo saved successfully.');

        return redirect(route('productPromos.index'));
    }
}
