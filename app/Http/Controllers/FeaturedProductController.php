<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFeaturedProductRequest;
use App\Http\Requests\UpdateFeaturedProductRequest;
use App\Repositories\FeaturedProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class FeaturedProductController extends AppBaseController
{
    /** @var  FeaturedProductRepository */
    private $featuredProductRepository;

    public function __construct(FeaturedProductRepository $featuredProductRepo)
    {
        $this->middleware('auth');
        $this->featuredProductRepository = $featuredProductRepo;
    }

    /**
     * Display a listing of the FeaturedProduct.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->featuredProductRepository->pushCriteria(new RequestCriteria($request));
        $featuredProducts = $this->featuredProductRepository->with(['store', 'item'])->all();

        return view('featured_products.index')
            ->with('featuredProducts', $featuredProducts);
    }

    /**
     * Show the form for creating a new FeaturedProduct.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('featured_products.create');
        return view('featured_products.create')
            ->with('store', $store)
            ->with('item', $item);
    }

    /**
     * Store a newly created FeaturedProduct in storage.
     *
     * @param CreateFeaturedProductRequest $request
     *
     * @return Response
     */
    public function store(CreateFeaturedProductRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $featuredProduct = $this->featuredProductRepository->create($input);
        $featuredProduct = \App\Models\FeaturedProduct::create($input);

        \App\Services\LogService::handle([
            'description' => 'featuredProducts.store : ' . $featuredProduct->id
        ]);

        Flash::success('Featured Product saved successfully.');

        return redirect(route('featuredProducts.index'));
    }

    /**
     * Display the specified FeaturedProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $featuredProduct = $this->featuredProductRepository->findWithoutFail($id);

        if (empty($featuredProduct)) {
            Flash::error('Featured Product not found');

            return redirect(route('featuredProducts.index'));
        }

        return view('featured_products.show')->with('featuredProduct', $featuredProduct);
    }

    /**
     * Show the form for editing the specified FeaturedProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $featuredProduct = $this->featuredProductRepository->findWithoutFail($id);

        if (empty($featuredProduct)) {
            Flash::error('Featured Product not found');

            return redirect(route('featuredProducts.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('featured_products.edit')->with('featuredProduct', $featuredProduct);
        return view('featured_products.edit')
            ->with('featuredProduct', $featuredProduct)
            ->with('store', $store)
            ->with('item', $item);        
    }

    /**
     * Update the specified FeaturedProduct in storage.
     *
     * @param  int              $id
     * @param UpdateFeaturedProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeaturedProductRequest $request)
    {
        $featuredProduct = $this->featuredProductRepository->findWithoutFail($id);

        if (empty($featuredProduct)) {
            Flash::error('Featured Product not found');

            return redirect(route('featuredProducts.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $featuredProduct = $this->featuredProductRepository->update($input, $id);
        $featuredProduct = $featuredProduct->update($input);

        \App\Services\LogService::handle([
            'description' => 'featuredProducts.update : ' . $id
        ]);

        Flash::success('Featured Product updated successfully.');

        return redirect(route('featuredProducts.index'));
    }

    /**
     * Remove the specified FeaturedProduct from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $featuredProduct = $this->featuredProductRepository->findWithoutFail($id);

        if (empty($featuredProduct)) {
            Flash::error('Featured Product not found');

            return redirect(route('featuredProducts.index'));
        }

        // $this->featuredProductRepository->delete($id);
        $featuredProduct->delete();

        \App\Services\LogService::handle([
            'description' => 'featuredProducts.delete : ' . $id
        ]);

        Flash::success('Featured Product deleted successfully.');

        return redirect(route('featuredProducts.index'));
    }

    /**
     * Store data FeaturedProduct from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $featuredProduct = $this->featuredProductRepository->create($item->toArray());
            });
        });

        Flash::success('Featured Product saved successfully.');

        return redirect(route('featuredProducts.index'));
    }
}
