<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDebtRequest;
use App\Http\Requests\UpdateDebtRequest;
use App\Repositories\DebtRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class DebtController extends AppBaseController
{
    /** @var  DebtRepository */
    private $debtRepository;

    public function __construct(DebtRepository $debtRepo)
    {
        $this->middleware('auth');
        $this->debtRepository = $debtRepo;
    }

    /**
     * Display a listing of the Debt.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->debtRepository->pushCriteria(new RequestCriteria($request));
        $debts = $this->debtRepository->all();

        return view('debts.index')
            ->with('debts', $debts);
    }

    /**
     * Show the form for creating a new Debt.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('debts.create');
        return view('debts.create')
            ->with('store', $store);
    }

    /**
     * Store a newly created Debt in storage.
     *
     * @param CreateDebtRequest $request
     *
     * @return Response
     */
    public function store(CreateDebtRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $debt = $this->debtRepository->create($input);
        $debt = \App\Models\Debt::create($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Debt',
            'value' => $request->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'debts.store : ' . $debt->id
        ]);

        Flash::success('Debt saved successfully.');

        return redirect(route('debts.index'));
    }

    /**
     * Display the specified Debt.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $debt = $this->debtRepository->findWithoutFail($id);

        if (empty($debt)) {
            Flash::error('Debt not found');

            return redirect(route('debts.index'));
        }

        return view('debts.show')->with('debt', $debt);
    }

    /**
     * Show the form for editing the specified Debt.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $debt = $this->debtRepository->findWithoutFail($id);

        if (empty($debt)) {
            Flash::error('Debt not found');

            return redirect(route('debts.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('debts.edit')->with('debt', $debt);
        return view('debts.edit')
            ->with('debt', $debt)
            ->with('store', $store);        
    }

    /**
     * Update the specified Debt in storage.
     *
     * @param  int              $id
     * @param UpdateDebtRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDebtRequest $request)
    {
        $debt = $this->debtRepository->findWithoutFail($id);
        $debtValueBefore = $debt->value;

        if (empty($debt)) {
            Flash::error('Debt not found');

            return redirect(route('debts.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $debt = $this->debtRepository->update($input, $id);
        $debt = $debt->update($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Cost',
            'value' => $request->value - $debtValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'debts.update : ' . $id
        ]);

        Flash::success('Debt updated successfully.');

        return redirect(route('debts.index'));
    }

    /**
     * Remove the specified Debt from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $debt = $this->debtRepository->findWithoutFail($id);

        if (empty($debt)) {
            Flash::error('Debt not found');

            return redirect(route('debts.index'));
        }

        // $this->debtRepository->delete($id);
        $debt->delete();

        \App\Services\RecapService::handle([
            'model_type' => 'Cost',
            'value' => -1 * $debt->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'debts.update : ' . $id
        ]);

        Flash::success('Debt deleted successfully.');

        return redirect(route('debts.index'));
    }

    /**
     * Store data Debt from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $debt = $this->debtRepository->create($item->toArray());
            });
        });

        Flash::success('Debt saved successfully.');

        return redirect(route('debts.index'));
    }
}
