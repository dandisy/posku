<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStoreFrontStockRequest;
use App\Http\Requests\UpdateStoreFrontStockRequest;
use App\Repositories\StoreFrontStockRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class StoreFrontStockController extends AppBaseController
{
    /** @var  StoreFrontStockRepository */
    private $storeFrontStockRepository;

    public function __construct(StoreFrontStockRepository $storeFrontStockRepo)
    {
        $this->middleware('auth');
        $this->storeFrontStockRepository = $storeFrontStockRepo;
    }

    /**
     * Display a listing of the StoreFrontStock.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeFrontStockRepository->pushCriteria(new RequestCriteria($request));
        $storeFrontStocks = $this->storeFrontStockRepository->with(['store', 'item'])->all();

        return view('store_front_stocks.index')
            ->with('storeFrontStocks', $storeFrontStocks);
    }

    /**
     * Show the form for creating a new StoreFrontStock.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('store_front_stocks.create');
        return view('store_front_stocks.create')
            ->with('item', $item);
    }

    /**
     * Store a newly created StoreFrontStock in storage.
     *
     * @param CreateStoreFrontStockRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreFrontStockRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $storeFrontStock = $this->storeFrontStockRepository->create($input);
        $storeFrontStock = \App\Models\StoreFrontStock::create($input);

        \App\Services\LogService::handle([
            'description' => 'storeFrontStocks.store : ' . $storeFrontStock->id
        ]);

        Flash::success('Store Front Stock saved successfully.');

        return redirect(route('storeFrontStocks.index'));
    }

    /**
     * Display the specified StoreFrontStock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $storeFrontStock = $this->storeFrontStockRepository->findWithoutFail($id);

        if (empty($storeFrontStock)) {
            Flash::error('Store Front Stock not found');

            return redirect(route('storeFrontStocks.index'));
        }

        return view('store_front_stocks.show')->with('storeFrontStock', $storeFrontStock);
    }

    /**
     * Show the form for editing the specified StoreFrontStock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        $storeFrontStock = $this->storeFrontStockRepository->findWithoutFail($id);

        if (empty($storeFrontStock)) {
            Flash::error('Store Front Stock not found');

            return redirect(route('storeFrontStocks.index'));
        }

        // edited by dandisy
        // return view('store_front_stocks.edit')->with('storeFrontStock', $storeFrontStock);
        return view('store_front_stocks.edit')
            ->with('storeFrontStock', $storeFrontStock)
            ->with('item', $item);        
    }

    /**
     * Update the specified StoreFrontStock in storage.
     *
     * @param  int              $id
     * @param UpdateStoreFrontStockRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreFrontStockRequest $request)
    {
        $storeFrontStock = $this->storeFrontStockRepository->findWithoutFail($id);

        if (empty($storeFrontStock)) {
            Flash::error('Store Front Stock not found');

            return redirect(route('storeFrontStocks.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $storeFrontStock = $this->storeFrontStockRepository->update($input, $id);        
        $storeFrontStock = $storeFrontStock->update($input);

        \App\Services\LogService::handle([
            'description' => 'storeFrontStocks.update : ' . $id
        ]);

        Flash::success('Store Front Stock updated successfully.');

        return redirect(route('storeFrontStocks.index'));
    }

    /**
     * Remove the specified StoreFrontStock from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $storeFrontStock = $this->storeFrontStockRepository->findWithoutFail($id);

        if (empty($storeFrontStock)) {
            Flash::error('Store Front Stock not found');

            return redirect(route('storeFrontStocks.index'));
        }

        // $this->storeFrontStockRepository->delete($id);
        $storeFrontStock->delete();

        \App\Services\LogService::handle([
            'description' => 'storeFrontStocks.delete : ' . $id
        ]);

        Flash::success('Store Front Stock deleted successfully.');

        return redirect(route('storeFrontStocks.index'));
    }

    /**
     * Store data StoreFrontStock from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $storeFrontStock = $this->storeFrontStockRepository->create($item->toArray());
            });
        });

        Flash::success('Store Front Stock saved successfully.');

        return redirect(route('storeFrontStocks.index'));
    }
}
