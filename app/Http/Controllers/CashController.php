<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCashRequest;
use App\Http\Requests\UpdateCashRequest;
use App\Repositories\CashRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class CashController extends AppBaseController
{
    /** @var  CashRepository */
    private $cashRepository;

    public function __construct(CashRepository $cashRepo)
    {
        $this->middleware('auth');
        $this->cashRepository = $cashRepo;
    }

    /**
     * Display a listing of the Cash.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cashRepository->pushCriteria(new RequestCriteria($request));
        $cashes = $this->cashRepository->all();

        return view('cashes.index')
            ->with('cashes', $cashes);
    }

    /**
     * Show the form for creating a new Cash.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('cashes.create');
        return view('cashes.create')
            ->with('store', $store);
    }

    /**
     * Store a newly created Cash in storage.
     *
     * @param CreateCashRequest $request
     *
     * @return Response
     */
    public function store(CreateCashRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;


        // $cash = $this->cashRepository->create($input);
        $cash = \App\Models\Cash::create($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Cash',
            'value' => $request->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'cashes.store : ' . $cash->id
        ]);

        Flash::success('Cash saved successfully.');

        return redirect(route('cashes.index'));
    }

    /**
     * Display the specified Cash.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cash = $this->cashRepository->findWithoutFail($id);

        if (empty($cash)) {
            Flash::error('Cash not found');

            return redirect(route('cashes.index'));
        }

        return view('cashes.show')->with('cash', $cash);
    }

    /**
     * Show the form for editing the specified Cash.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cash = $this->cashRepository->findWithoutFail($id);

        if (empty($cash)) {
            Flash::error('Cash not found');

            return redirect(route('cashes.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('cashes.edit')->with('cash', $cash);
        return view('cashes.edit')
            ->with('cash', $cash)
            ->with('store', $store);        
    }

    /**
     * Update the specified Cash in storage.
     *
     * @param  int              $id
     * @param UpdateCashRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCashRequest $request)
    {
        $cash = $this->cashRepository->findWithoutFail($id);
        $cashValueBefore = $cash->value;

        if (empty($cash)) {
            Flash::error('Cash not found');

            return redirect(route('cashes.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $cash = $this->cashRepository->update($input, $id);
        $cash = $cash->update($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Cash',
            'value' => $request->value - $cashValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'cashes.update : ' . $id
        ]);

        Flash::success('Cash updated successfully.');

        return redirect(route('cashes.index'));
    }

    /**
     * Remove the specified Cash from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cash = $this->cashRepository->findWithoutFail($id);

        if (empty($cash)) {
            Flash::error('Cash not found');

            return redirect(route('cashes.index'));
        }

        // $this->cashRepository->delete($id);
        $cash->delete($id);

        \App\Services\RecapService::handle([
            'model_type' => 'Cash',
            'value' => -1 * $cash->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'cashes.delete : ' . $id
        ]);

        Flash::success('Cash deleted successfully.');

        return redirect(route('cashes.index'));
    }

    /**
     * Store data Cash from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $cash = $this->cashRepository->create($item->toArray());
            });
        });

        Flash::success('Cash saved successfully.');

        return redirect(route('cashes.index'));
    }
}
