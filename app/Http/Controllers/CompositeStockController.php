<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCompositeStockRequest;
use App\Http\Requests\UpdateCompositeStockRequest;
use App\Repositories\CompositeStockRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class CompositeStockController extends AppBaseController
{
    /** @var  CompositeStockRepository */
    private $compositeStockRepository;

    public function __construct(CompositeStockRepository $compositeStockRepo)
    {
        $this->middleware('auth');
        $this->compositeStockRepository = $compositeStockRepo;
    }

    /**
     * Display a listing of the CompositeStock.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->compositeStockRepository->pushCriteria(new RequestCriteria($request));
        $compositeStocks = $this->compositeStockRepository->with(['store', 'item'])->all();

        return view('composite_stocks.index')
            ->with('compositeStocks', $compositeStocks);
    }

    /**
     * Show the form for creating a new CompositeStock.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::has('composite')->get();
        

        // edited by dandisy
        // return view('composite_stocks.create');
        return view('composite_stocks.create')
            ->with('store', $store)
            ->with('item', $item);
    }

    /**
     * Store a newly created CompositeStock in storage.
     *
     * @param CreateCompositeStockRequest $request
     *
     * @return Response
     */
    public function store(CreateCompositeStockRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;
        $input['store'] = $userStore;

        $input['composite_id'] = \App\Models\Composite::where('item', $request->item)->first()->id;

        // $compositeStock = $this->compositeStockRepository->create($input);
        $compositeStock = \App\Models\CompositeStock::create($input);

        $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item)->first();
        if($stockItem) {
            $compositeStock = $stockItem->update([
                'item_amount' => $stockItem->item_amount + $request->item_amount,
                'updated_by' => Auth::user()->id
            ]);
        } else {
            \App\Models\Stock::create($input);
        }

        // $ingredients = \App\Models\CompositeItem::where('store', $userStore)->where('composite_id', $request->composite_id)->get();
        $ingredients = \App\Models\CompositeItem::where('composite_id', $request->composite_id)->get();
        foreach($ingredients as $item) {
            $stockIngredient = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            $stockIngredient->update([
                'item_amount' => $stockIngredient->item_amount - ($item->item_amount * $request->item_amount),
                'updated_by' => Auth::user()->id
            ]);
        }

        \App\Services\LogService::handle([
            'description' => 'compositeStocks.store : ' . $compositeStock->id
        ]);

        Flash::success('Composite Stock saved successfully.');

        return redirect(route('compositeStocks.index'));
    }

    /**
     * Display the specified CompositeStock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $compositeStock = $this->compositeStockRepository->findWithoutFail($id);

        if (empty($compositeStock)) {
            Flash::error('Composite Stock not found');

            return redirect(route('compositeStocks.index'));
        }

        return view('composite_stocks.show')->with('compositeStock', $compositeStock);
    }

    /**
     * Show the form for editing the specified CompositeStock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $compositeStock = $this->compositeStockRepository->findWithoutFail($id);

        if (empty($compositeStock)) {
            Flash::error('Composite Stock not found');

            return redirect(route('compositeStocks.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::has('composite')->get();

        // edited by dandisy
        // return view('composite_stocks.edit')->with('compositeStock', $compositeStock);
        return view('composite_stocks.edit')
            ->with('compositeStock', $compositeStock)
            ->with('store', $store)
            ->with('item', $item);        
    }

    /**
     * Update the specified CompositeStock in storage.
     *
     * @param  int              $id
     * @param UpdateCompositeStockRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompositeStockRequest $request)
    {
        $compositeStock = $this->compositeStockRepository->findWithoutFail($id);
        $existingCompositeStockAmount = $compositeStock->item_amount;

        if (empty($compositeStock)) {
            Flash::error('Composite Stock not found');

            return redirect(route('compositeStocks.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;
        $input['store'] = $userStore;

        $input['composite_id'] = \App\Models\Composite::where('item', $request->item)->first()->id;

        // $compositeStock = $this->compositeStockRepository->update($input, $id);
        $compositeStock = $compositeStock->update($input);

        $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $id)->first();
        $stockItem->update([
            'item_amount' => $stockItem->item_amount + ($request->item_amount - $existingCompositeStockAmount),
            'updated_by' => Auth::user()->id
        ]);

        // $ingredients = \App\Models\CompositeItem::where('store', $userStore)->where('composite_id', $request->composite_id)->get();
        $ingredients = \App\Models\CompositeItem::where('composite_id', $request->composite_id)->get();
        foreach($ingredients as $item) {
            $stockIngredient = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            $stockIngredient->update([
                'item_amount' => $stockIngredient->item_amount - ($item->item_amount * ($request->item_amount - $existingCompositeStockAmount)),
                'updated_by' => Auth::user()->id
            ]);
        }

        \App\Services\LogService::handle([
            'description' => 'compositeStocks.update : ' . $id
        ]);

        Flash::success('Composite Stock updated successfully.');

        return redirect(route('compositeStocks.index'));
    }

    /**
     * Remove the specified CompositeStock from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $compositeStock = $this->compositeStockRepository->findWithoutFail($id);
        $existingCompositeStockAmount = $compositeStock->item_amount;

        if (empty($compositeStock)) {
            Flash::error('Composite Stock not found');

            return redirect(route('compositeStocks.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->compositeStockRepository->delete($id);
        $compositeStock->delete();

        $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $id)->first();
        $stockItem->update([
            'item_amount' => $stockItem->item_amount - $compositeStock->item_amount,
            'updated_by' => Auth::user()->id
        ]);

        // $ingredients = \App\Models\CompositeItem::where('store', $userStore)->where('composite_id', $compositeStock->composite_id)->get();
        $ingredients = \App\Models\CompositeItem::where('composite_id', $compositeStock->composite_id)->get();
        foreach($ingredients as $item) {
            $stockIngredient = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            $stockIngredient->update([
                'item_amount' => $stockIngredient->item_amount + ($item->item_amount * $existingCompositeStockAmount),
                'updated_by' => Auth::user()->id
            ]);
        }

        \App\Services\LogService::handle([
            'description' => 'compositeStocks.delete : ' . $id
        ]);

        Flash::success('Composite Stock deleted successfully.');

        return redirect(route('compositeStocks.index'));
    }

    /**
     * Store data CompositeStock from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $compositeStock = $this->compositeStockRepository->create($item->toArray());
            });
        });

        Flash::success('Composite Stock saved successfully.');

        return redirect(route('compositeStocks.index'));
    }
}
