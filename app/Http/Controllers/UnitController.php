<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUnitRequest;
use App\Http\Requests\UpdateUnitRequest;
use App\Repositories\UnitRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class UnitController extends AppBaseController
{
    /** @var  UnitRepository */
    private $unitRepository;

    public function __construct(UnitRepository $unitRepo)
    {
        $this->middleware('auth');
        $this->unitRepository = $unitRepo;
    }

    /**
     * Display a listing of the Unit.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->unitRepository->pushCriteria(new RequestCriteria($request));
        $units = $this->unitRepository->all();

        return view('units.index')
            ->with('units', $units);
    }

    /**
     * Show the form for creating a new Unit.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('units.create');
        return view('units.create');
    }

    /**
     * Store a newly created Unit in storage.
     *
     * @param CreateUnitRequest $request
     *
     * @return Response
     */
    public function store(CreateUnitRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $unit = $this->unitRepository->create($input);
        $unit = \App\Models\Unit::create($input);

        \App\Services\LogService::handle([
            'description' => 'units.store : ' . $unit->id
        ]);

        Flash::success('Unit saved successfully.');

        return redirect(route('units.index'));
    }

    /**
     * Display the specified Unit.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $unit = $this->unitRepository->findWithoutFail($id);

        if (empty($unit)) {
            Flash::error('Unit not found');

            return redirect(route('units.index'));
        }

        return view('units.show')->with('unit', $unit);
    }

    /**
     * Show the form for editing the specified Unit.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $unit = $this->unitRepository->findWithoutFail($id);

        if (empty($unit)) {
            Flash::error('Unit not found');

            return redirect(route('units.index'));
        }

        // edited by dandisy
        // return view('units.edit')->with('unit', $unit);
        return view('units.edit')
            ->with('unit', $unit);        
    }

    /**
     * Update the specified Unit in storage.
     *
     * @param  int              $id
     * @param UpdateUnitRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnitRequest $request)
    {
        $unit = $this->unitRepository->findWithoutFail($id);

        if (empty($unit)) {
            Flash::error('Unit not found');

            return redirect(route('units.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $unit = $this->unitRepository->update($input, $id);
        $unit = $unit->update($input);

        \App\Services\LogService::handle([
            'description' => 'units.update : ' . $id
        ]);

        Flash::success('Unit updated successfully.');

        return redirect(route('units.index'));
    }

    /**
     * Remove the specified Unit from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $unit = $this->unitRepository->findWithoutFail($id);

        if (empty($unit)) {
            Flash::error('Unit not found');

            return redirect(route('units.index'));
        }

        // $this->unitRepository->delete($id);
        $unit->delete();

        \App\Services\LogService::handle([
            'description' => 'units.delete : ' . $id
        ]);

        Flash::success('Unit deleted successfully.');

        return redirect(route('units.index'));
    }

    /**
     * Store data Unit from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $unit = $this->unitRepository->create($item->toArray());
            });
        });

        Flash::success('Unit saved successfully.');

        return redirect(route('units.index'));
    }
}
