<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStockTransferItemRequest;
use App\Http\Requests\UpdateStockTransferItemRequest;
use App\Repositories\StockTransferItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class StockTransferItemController extends AppBaseController
{
    /** @var  StockTransferItemRepository */
    private $stockTransferItemRepository;

    public function __construct(StockTransferItemRepository $stockTransferItemRepo)
    {
        $this->middleware('auth');
        $this->stockTransferItemRepository = $stockTransferItemRepo;
    }

    /**
     * Display a listing of the StockTransferItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->stockTransferItemRepository->pushCriteria(new RequestCriteria($request));
        $stockTransferItems = $this->stockTransferItemRepository->all();

        return view('stock_transfer_items.index')
            ->with('stockTransferItems', $stockTransferItems);
    }

    /**
     * Show the form for creating a new StockTransferItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        $unit = \App\Models\Unit::all();
        $wrap = \App\Models\Wrap::all();
        

        // edited by dandisy
        // return view('stock_transfer_items.create');
        return view('stock_transfer_items.create')
            ->with('item', $item)
            ->with('unit', $unit)
            ->with('wrap', $wrap);
    }

    /**
     * Store a newly created StockTransferItem in storage.
     *
     * @param CreateStockTransferItemRequest $request
     *
     * @return Response
     */
    public function store(CreateStockTransferItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $stockTransferItem = $this->stockTransferItemRepository->create($input);
        $stockTransferItem = \App\Models\StockTransferItem::create($input);

        \App\Services\LogService::handle([
            'description' => 'stockTransferItems.store : ' . $stockTransferItem->id
        ]);

        Flash::success('Stock Transfer Item saved successfully.');

        return redirect(route('stockTransferItems.index'));
    }

    /**
     * Display the specified StockTransferItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stockTransferItem = $this->stockTransferItemRepository->findWithoutFail($id);

        if (empty($stockTransferItem)) {
            Flash::error('Stock Transfer Item not found');

            return redirect(route('stockTransferItems.index'));
        }

        return view('stock_transfer_items.show')->with('stockTransferItem', $stockTransferItem);
    }

    /**
     * Show the form for editing the specified StockTransferItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stockTransferItem = $this->stockTransferItemRepository->findWithoutFail($id);

        if (empty($stockTransferItem)) {
            Flash::error('Stock Transfer Item not found');

            return redirect(route('stockTransferItems.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();
        $unit = \App\Models\Unit::all();
        $wrap = \App\Models\Wrap::all();

        // edited by dandisy
        // return view('stock_transfer_items.edit')->with('stockTransferItem', $stockTransferItem);
        return view('stock_transfer_items.edit')
            ->with('stockTransferItem', $stockTransferItem)
            ->with('item', $item)
            ->with('unit', $unit)
            ->with('wrap', $wrap);        
    }

    /**
     * Update the specified StockTransferItem in storage.
     *
     * @param  int              $id
     * @param UpdateStockTransferItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockTransferItemRequest $request)
    {
        $stockTransferItem = $this->stockTransferItemRepository->findWithoutFail($id);

        if (empty($stockTransferItem)) {
            Flash::error('Stock Transfer Item not found');

            return redirect(route('stockTransferItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $stockTransferItem = $this->stockTransferItemRepository->update($input, $id);
        $stockTransferItem = $stockTransferItem->update($input);

        \App\Services\LogService::handle([
            'description' => 'stockTransferItems.update : ' . $id
        ]);

        Flash::success('Stock Transfer Item updated successfully.');

        return redirect(route('stockTransferItems.index'));
    }

    /**
     * Remove the specified StockTransferItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $stockTransferItem = $this->stockTransferItemRepository->findWithoutFail($id);

        if (empty($stockTransferItem)) {
            if($request->ajax()){
                return 'Stock Transfer Item not found';
            }

            Flash::error('Stock Transfer Item not found');

            return redirect(route('stockTransferItems.index'));
        }

        // $this->stockTransferItemRepository->delete($id);
        $stockTransferItem->delete();

        \App\Services\LogService::handle([
            'description' => 'stockTransferItems.delete : ' . $id
        ]);

        if($request->ajax()){
            return 'Stock Transfer Item deleted successfully.';
        }

        Flash::success('Stock Transfer Item deleted successfully.');

        return redirect(route('stockTransferItems.index'));
    }

    /**
     * Store data StockTransferItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $stockTransferItem = $this->stockTransferItemRepository->create($item->toArray());
            });
        });

        Flash::success('Stock Transfer Item saved successfully.');

        return redirect(route('stockTransferItems.index'));
    }
}
