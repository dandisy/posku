<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use App\Repositories\PaymentMethodRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added  by dandisy

class PaymentMethodController extends AppBaseController
{
    /** @var  PaymentMethodRepository */
    private $paymentMethodRepository;

    public function __construct(PaymentMethodRepository $paymentMethodRepo)
    {
        $this->middleware('auth');
        $this->paymentMethodRepository = $paymentMethodRepo;
    }

    /**
     * Display a listing of the PaymentMethod.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->paymentMethodRepository->pushCriteria(new RequestCriteria($request));
        $paymentMethods = $this->paymentMethodRepository->all();

        return view('payment_methods.index')
            ->with('paymentMethods', $paymentMethods);
    }

    /**
     * Show the form for creating a new PaymentMethod.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('payment_methods.create');
        return view('payment_methods.create');
    }

    /**
     * Store a newly created PaymentMethod in storage.
     *
     * @param CreatePaymentMethodRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentMethodRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $paymentMethod = $this->paymentMethodRepository->create($input);
        $paymentMethod = \App\Models\PaymentMethod::create($input);

        \App\Services\LogService::handle([
            'description' => 'paymentMethods.store : ' . $paymentMethod->id
        ]);

        Flash::success('Payment Method saved successfully.');

        return redirect(route('paymentMethods.index'));
    }

    /**
     * Display the specified PaymentMethod.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paymentMethod = $this->paymentMethodRepository->findWithoutFail($id);

        if (empty($paymentMethod)) {
            Flash::error('Payment Method not found');

            return redirect(route('paymentMethods.index'));
        }

        return view('payment_methods.show')->with('paymentMethod', $paymentMethod);
    }

    /**
     * Show the form for editing the specified PaymentMethod.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $paymentMethod = $this->paymentMethodRepository->findWithoutFail($id);

        if (empty($paymentMethod)) {
            Flash::error('Payment Method not found');

            return redirect(route('paymentMethods.index'));
        }

        // edited by dandisy
        // return view('payment_methods.edit')->with('paymentMethod', $paymentMethod);
        return view('payment_methods.edit')
            ->with('paymentMethod', $paymentMethod);        
    }

    /**
     * Update the specified PaymentMethod in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentMethodRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentMethodRequest $request)
    {
        $paymentMethod = $this->paymentMethodRepository->findWithoutFail($id);

        if (empty($paymentMethod)) {
            Flash::error('Payment Method not found');

            return redirect(route('paymentMethods.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $paymentMethod = $this->paymentMethodRepository->update($input, $id);
        $paymentMethod = $paymentMethod->update($input);

        \App\Services\LogService::handle([
            'description' => 'paymentMethods.update : ' . $id
        ]);

        Flash::success('Payment Method updated successfully.');

        return redirect(route('paymentMethods.index'));
    }

    /**
     * Remove the specified PaymentMethod from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paymentMethod = $this->paymentMethodRepository->findWithoutFail($id);

        if (empty($paymentMethod)) {
            Flash::error('Payment Method not found');

            return redirect(route('paymentMethods.index'));
        }

        // $this->paymentMethodRepository->delete($id);
        $paymentMethod->delete();

        \App\Services\LogService::handle([
            'description' => 'paymentMethods.delete : ' . $id
        ]);

        Flash::success('Payment Method deleted successfully.');

        return redirect(route('paymentMethods.index'));
    }

    /**
     * Store data PaymentMethod from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $paymentMethod = $this->paymentMethodRepository->create($item->toArray());
            });
        });

        Flash::success('Payment Method saved successfully.');

        return redirect(route('paymentMethods.index'));
    }
}
