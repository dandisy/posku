<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePurchaseReturnRequest;
use App\Http\Requests\UpdatePurchaseReturnRequest;
use App\Repositories\PurchaseReturnRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy
use Illuminate\Support\Facades\DB;

class PurchaseReturnController extends AppBaseController
{
    /** @var  PurchaseReturnRepository */
    private $purchaseReturnRepository;

    public function __construct(PurchaseReturnRepository $purchaseReturnRepo)
    {
        $this->middleware('auth');
        $this->purchaseReturnRepository = $purchaseReturnRepo;
    }

    /**
     * Display a listing of the PurchaseReturn.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->purchaseReturnRepository->pushCriteria(new RequestCriteria($request));
        $purchaseReturns = $this->purchaseReturnRepository->with(['supplier'])->all();

        return view('purchase_returns.index')
            ->with('purchaseReturns', $purchaseReturns);
    }

    /**
     * Show the form for creating a new PurchaseReturn.
     *
     * @return Response
     */
    public function create()
    {
        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // added by dandisy
        $purchase = \App\Models\Purchase::where('store', $userStore)->get();
        $store = \App\Models\Store::all();
        $supplier = \App\Models\Supplier::all();

        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->whereNull('barcodes.deleted_at')
                    ->whereIn('barcodes.status', ['ready', 'rejected']);
            })
            ->join('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();

        $purchaseReturnItems = collect();
        

        // edited by dandisy
        // return view('purchase_returns.create');
        return view('purchase_returns.create')
            ->with('purchase', $purchase)
            ->with('store', $store)
            ->with('supplier', $supplier)
            ->with('items', $items)
            ->with('purchaseReturnItems', $purchaseReturnItems);
    }

    /**
     * Store a newly created PurchaseReturn in storage.
     *
     * @param CreatePurchaseReturnRequest $request
     *
     * @return Response
     */
    public function store(CreatePurchaseReturnRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;

        // $purchaseReturn = $this->purchaseReturnRepository->create($input);
        $purchaseReturn = \App\Models\PurchaseReturn::create([
            'purchase_id' => $request->purchase_id,
            'store' => $userStore,
            'supplier' => $request->supplier,
            'receipt_code' => $request->receipt_code,
            'receipt_amount' => $request->receipt_amount,
            'note' => $request->note,
            'attendance_id' => $attendanceId,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\PurchaseReturnItem::create([
                'purchase_return_id' => $purchaseReturn->id,
                'store' => $userStore,
                'item' => $item,
                'unique_barcode' => $request->unique_barcode[$k],
                'item_discount' => $request->item_discount[$k],
                'item_price' => $request->item_price[$k],
                'item_amount' => $request->item_amount[$k],
                'discount_subtotal' => $request->discount_subtotal[$k],
                'price_subtotal' => $request->price_subtotal[$k],
                'tax' => $request->tax[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount[$k]]);

            if($request->unique_barcode[$k]) {
                \App\Models\Barcode::where('item', $item)
                    ->where('barcode', $request->unique_barcode[$k])
                    ->where('store', $userStore)
                    ->first()
                    ->update([
                        'status' => 'returned',
                        'purchase_return_id' => $purchaseReturn->id,
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'PurchaseReturn',
            'value' => $request->receipt_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'purchaseReturns.store : ' . $purchaseReturn->id
        ]);

        Flash::success('Purchase Return saved successfully.');

        return redirect(route('purchaseReturns.index'));
    }

    /**
     * Display the specified PurchaseReturn.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $purchaseReturn = $this->purchaseReturnRepository->findWithoutFail($id);

        if (empty($purchaseReturn)) {
            Flash::error('Purchase Return not found');

            return redirect(route('purchaseReturns.index'));
        }

        return view('purchase_returns.show')->with('purchaseReturn', $purchaseReturn);
    }

    /**
     * Show the form for editing the specified PurchaseReturn.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $purchaseReturn = $this->purchaseReturnRepository->findWithoutFail($id);

        if (empty($purchaseReturn)) {
            Flash::error('Purchase Return not found');

            return redirect(route('purchaseReturns.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // added by dandisy
        $purchase = \App\Models\Purchase::where('store', $userStore)->get();
        $store = \App\Models\Store::all();
        $supplier = \App\Models\Supplier::all();

        // $items = \App\Models\Item::all();
        $items = DB::table('items')
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->whereNull('barcodes.deleted_at')
                    ->whereIn('barcodes.status', ['ready', 'rejected']);
            })
            ->join('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.sell_price', 'items.is_many_unique_barcode', 
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock'
            )
            ->selectRaw('CONCAT(items.name, IF(LENGTH(items.barcode), " - ", ""), COALESCE(items.barcode, ""), IF(LENGTH(barcodes.barcode), " - ", ""), COALESCE(barcodes.barcode, "")) as name_barcode')
            ->get();

        $purchaseReturnItems = \App\Models\PurchaseReturnItem::where('purchase_return_id', $id)->get();

        // edited by dandisy
        // return view('purchase_returns.edit')->with('purchaseReturn', $purchaseReturn);
        return view('purchase_returns.edit')
            ->with('purchaseReturn', $purchaseReturn)
            ->with('purchase', $purchase)
            ->with('store', $store)
            ->with('supplier', $supplier)
            ->with('items', $items)
            ->with('purchaseReturnItems', $purchaseReturnItems);        
    }

    /**
     * Update the specified PurchaseReturn in storage.
     *
     * @param  int              $id
     * @param UpdatePurchaseReturnRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePurchaseReturnRequest $request)
    {
        $purchaseReturn = $this->purchaseReturnRepository->findWithoutFail($id);
        $purchaseReturnValueBefore = $purchaseReturn->receipt_amount;

        if (empty($purchaseReturn)) {
            Flash::error('Purchase Return not found');

            return redirect(route('purchaseReturns.index'));
        }

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;
        $input['attendance_id'] = $attendanceId;
        $input['store'] = $userStore;

        // $purchaseReturn = $this->purchaseReturnRepository->update($input, $id);
        $purchaseReturn = $purchaseReturn->update($input);

        // deleting items
        $existingItems = \App\Models\PurchaseReturnItem::where('purchase_return_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\PurchaseReturnItem::where('purchase_return_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }
        // update barcode
        $existingBarcodes = \App\Models\Barcode::where('purchase_return_id', $id)
            ->where('store', $userStore)
            ->pluck('barcode')
            ->toArray();
        if($existingBarcodes) {
            $updateBarcodes = array_diff(array_values($existingBarcodes), $request->unique_barcode);
            if($updateBarcodes) {
                \App\Models\Barcode::where('purchase_return_id', $id)
                    ->where('store', $userStore)
                    ->whereIn('barcode', $updateBarcodes)
                    ->update([
                        'status' => 'ready',
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }
        foreach($request->purchase_return_id as $k => $relId) {
            if($relId) {
                $existingPurchaseReturnItem = \App\Models\PurchaseReturnItem::find($relId);
                $existingItemAmount = $existingPurchaseReturnItem->item_amount;

                $existingPurchaseReturnItem->update([
                    'store' => $userStore,
                    'item' =>  $request->item[$k],
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);
                
                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockItem->update(['item_amount' => $stockItem->item_amount - ($request->item_amount[$k] - $existingItemAmount)]);
            } else {
                \App\Models\PurchaseReturnItem::create([
                    'purchase_return_id' => $id,
                    'store' => $userStore,
                    'item' =>  $request->item[$k],
                    'unique_barcode' => $request->unique_barcode[$k],
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount[$k]]);
            }

            if($request->unique_barcode[$k]) {
                \App\Models\Barcode::where('item', $request->item[$k])
                    ->where('barcode', $request->unique_barcode[$k])
                    ->where('store', $userStore)
                    ->first()
                    ->update([
                        'status' => 'returned',
                        'purchase_return_id' => $id,
                        'updated_by' => Auth::user()->id
                    ]);
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'PurchaseReturn',
            'value' => $request->receipt_amount - $purchaseReturnValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'purchaseReturns.update : ' . $id
        ]);

        Flash::success('Purchase Return updated successfully.');

        return redirect(route('purchaseReturns.index'));
    }

    /**
     * Remove the specified PurchaseReturn from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $purchaseReturn = $this->purchaseReturnRepository->findWithoutFail($id);

        if (empty($purchaseReturn)) {
            Flash::error('Purchase Return not found');

            return redirect(route('purchaseReturns.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->purchaseReturnRepository->delete($id);
        $purchaseReturn->delete();

        $purchaseReturnItems = \App\Models\PurchaseReturnItem::where('purchase_return_id', $id);
        foreach($purchaseReturnItems->get() as $k => $item) {
            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount + $item->item_amount]);
        }
        $purchaseReturnItems->delete();
        \App\Models\Barcode::where('purchase_return_id', $id)->update([
            'status' => 'ready',
            'updated_by' => Auth::user()->id
        ]);

        \App\Services\RecapService::handle([
            'model_type' => 'PurchaseReturn',
            'value' => -1 * $purchaseReturn->receipt_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'purchaseReturns.delete : ' . $id
        ]);

        Flash::success('Purchase Return deleted successfully.');

        return redirect(route('purchaseReturns.index'));
    }

    /**
     * Store data PurchaseReturn from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $purchaseReturn = $this->purchaseReturnRepository->create($item->toArray());
            });
        });

        Flash::success('Purchase Return saved successfully.');

        return redirect(route('purchaseReturns.index'));
    }
}
