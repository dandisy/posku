<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $userStore = $attendance->store;

        $cashes = \App\Models\Cash::where('store', $userStore)->where('attendance_id', $attendance->id)->get();
        $sells = \App\Models\Sell::where('store', $userStore)->get();
        $sellAttendance = $sells->where('attendance_id', $attendance->id)->sum('bill_amount');
        $stocks = \App\Models\Stock::where('store', $userStore)->get();
        $storeFrontStockTotal = \App\Models\StoreFrontStock::where('store', $userStore)->sum('item_amount');
        $storeFrontStockCount = \App\Models\StoreFrontStock::where('store', $userStore)->count();

        // $cashDebit = 0;
        // $cashCredit = 0;
        // foreach($cashes as $k => $cash) {
        //     $cashDebit += $cash->debit;
        //     $cashCredit += $cash->credit;
        // }
        // $cashValue = $cashDebit - $cashCredit;
        $cashValue = \App\Models\Cash::where('store', $userStore)->where('attendance_id', $attendance->id)->sum('value');
        
        $sellValue = 0;
        $sellTransactionCount = 0;
        $sellVoid = 0;
        $sellNewCustomer = 0;
        $sellCount = 0;
        foreach($sells as $k => $sell) {
            $sellCount += 1;

            if($sell->payment_status == 'failed') {
                $sellVoid += 1;
            } else {
                $sellValue += $sell->bill_amount;
                $sellTransactionCount += 1;
            }

            if($sell->customer == 1) {
                $sellNewCustomer += 1;
            }
        }

        $stockTotal = 0;
        $stockItemCount = 0;
        $stockRunningOut = 0;
        foreach($stocks as $k => $stock) {
            $stockTotal += $stock->item_amount;
            $stockItemCount += 1;

            if($stock->item_amount <= 5) {
                $stockRunningOut += 1;
            }
        }

        $sellValueMonth_1 = \App\Models\Sell::where('store', $userStore)->whereMonth(
            'created_at', '=', \Carbon\Carbon::now()->subMonth()->month
        )->sum('bill_amount');
        $sellValueMonth_2 = \App\Models\Sell::where('store', $userStore)->whereMonth(
            'created_at', '=', \Carbon\Carbon::now()->subMonth()->subMonth()->month
        )->sum('bill_amount');
        $sellValueMonth_3 = \App\Models\Sell::where('store', $userStore)->whereMonth(
            'created_at', '=', \Carbon\Carbon::now()->subMonth()->subMonth()->subMonth()->month
        )->sum('bill_amount');

        $sellValueDay_1 = \App\Models\Sell::where('store', $userStore)->whereDay(
            'created_at', '=', \Carbon\Carbon::now()->subDay()->day
        )->sum('bill_amount');
        $sellValueDay_2 = \App\Models\Sell::where('store', $userStore)->whereDay(
            'created_at', '=', \Carbon\Carbon::now()->subDay()->subDay()->day
        )->sum('bill_amount');
        $sellValueDay_3 = \App\Models\Sell::where('store', $userStore)->whereDay(
            'created_at', '=', \Carbon\Carbon::now()->subDay()->subDay()->subDay()->day
        )->sum('bill_amount');

        return view('home', compact(
            'cashValue', 
            'sellValue', 
            'sellTransactionCount', 
            'sellVoid', 
            'sellNewCustomer', 
            'sellCount', 
            'stockTotal', 
            'stockItemCount', 
            'stockRunningOut', 
            'storeFrontStockTotal', 
            'storeFrontStockCount', 
            'sellValueMonth_1', 
            'sellValueMonth_2', 
            'sellValueMonth_3', 
            'sellValueDay_1', 
            'sellValueDay_2', 
            'sellValueDay_3', 
            'sellAttendance', 
            'attendance'
        ));
    }
}
