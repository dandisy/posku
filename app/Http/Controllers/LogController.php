<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLogRequest;
use App\Http\Requests\UpdateLogRequest;
use App\Repositories\LogRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class LogController extends AppBaseController
{
    /** @var  LogRepository */
    private $logRepository;

    public function __construct(LogRepository $logRepo)
    {
        $this->middleware('auth');
        $this->logRepository = $logRepo;
    }

    /**
     * Display a listing of the Log.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->logRepository->pushCriteria(new RequestCriteria($request));
        $logs = $this->logRepository->all();

        return view('logs.index')
            ->with('logs', $logs);
    }

    /**
     * Show the form for creating a new Log.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('logs.create');
        return view('logs.create')
            ->with('store', $store);
    }

    /**
     * Store a newly created Log in storage.
     *
     * @param CreateLogRequest $request
     *
     * @return Response
     */
    public function store(CreateLogRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $log = $this->logRepository->create($input);
        $log = \App\Models\Log::create($input);

        Flash::success('Log saved successfully.');

        return redirect(route('logs.index'));
    }

    /**
     * Display the specified Log.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $log = $this->logRepository->findWithoutFail($id);

        if (empty($log)) {
            Flash::error('Log not found');

            return redirect(route('logs.index'));
        }

        return view('logs.show')->with('log', $log);
    }

    /**
     * Show the form for editing the specified Log.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $log = $this->logRepository->findWithoutFail($id);

        if (empty($log)) {
            Flash::error('Log not found');

            return redirect(route('logs.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('logs.edit')->with('log', $log);
        return view('logs.edit')
            ->with('log', $log)
            ->with('store', $store);        
    }

    /**
     * Update the specified Log in storage.
     *
     * @param  int              $id
     * @param UpdateLogRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLogRequest $request)
    {
        $log = $this->logRepository->findWithoutFail($id);

        if (empty($log)) {
            Flash::error('Log not found');

            return redirect(route('logs.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $log = $this->logRepository->update($input, $id);
        $log = $log->update($input);

        Flash::success('Log updated successfully.');

        return redirect(route('logs.index'));
    }

    /**
     * Remove the specified Log from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $log = $this->logRepository->findWithoutFail($id);

        if (empty($log)) {
            Flash::error('Log not found');

            return redirect(route('logs.index'));
        }

        // $this->logRepository->delete($id);
        $log->delete();

        Flash::success('Log deleted successfully.');

        return redirect(route('logs.index'));
    }

    /**
     * Store data Log from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $log = $this->logRepository->create($item->toArray());
            });
        });

        Flash::success('Log saved successfully.');

        return redirect(route('logs.index'));
    }
}
