<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSellRequest;
use App\Http\Requests\UpdateSellRequest;
use App\Repositories\SellRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy
use Illuminate\Support\Facades\DB;

class SellController extends AppBaseController
{
    /** @var  SellRepository */
    private $sellRepository;

    public function __construct(SellRepository $sellRepo)
    {
        $this->middleware('auth');
        $this->sellRepository = $sellRepo;
    }

    /**
     * Display a listing of the Sell.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sellRepository->pushCriteria(new RequestCriteria($request));
        $sells = $this->sellRepository->with(['customer'])->all();

        return view('sells.index')
            ->with('sells', $sells);
    }

    /**
     * Show the form for creating a new Sell.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $customer = \App\Models\Customer::all();
        $store = \App\Models\Store::all();
        $paymentmethod = \App\Models\PaymentMethod::all();

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $items = \App\Models\Item::with('featured')->get();
        $items = DB::table('items')
            ->leftJoin('featured_products', function($query) {
                $query->on('featured_products.item', '=', 'items.id')
                    ->on('featured_products.store', '=', 'items.store')
                    ->whereNull('featured_products.deleted_at');
            })
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->where('barcodes.status', 'ready')
                    ->whereNull('barcodes.deleted_at');
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->leftJoin('store_front_stocks', function($query) {
                $query->on('store_front_stocks.item', '=', 'items.id')
                    ->on('store_front_stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.barcode', 'items.sell_price', 'items.is_many_unique_barcode', 
                'featured_products.publish_on as featured_publish_on',
                'featured_products.publish_off as featured_publish_off',
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock',
                'store_front_stocks.item_amount as front_stock'
            )
            ->get();        

        // edited by dandisy
        // return view('sells.create');
        return view('sells.create')
            ->with('customer', $customer)
            ->with('store', $store)
            ->with('paymentmethod', $paymentmethod)
            ->with('items', $items);
    }

    /**
     * Store a newly created Sell in storage.
     *
     * @param CreateSellRequest $request
     *
     * @return Response
     */
    public function store(CreateSellRequest $request)
    {
        $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;

        // $sell = $this->sellRepository->create($input);
        $sell = \App\Models\Sell::create([
            'customer' => $request->customer,
            'store' => $userStore,
            'bill_amount' => $request->bill_amount,
            'table_number' => $request->table_number,
            'table_order_status' => $request->table_order_status,
            'payment_method' => $request->payment_method,
            'payment_status' => $request->payment_status,
            'selling_type' => 'purchase',
            'note' => $request->note,
            'is_draft' => $request->is_draft,
            'attendance_id' => $attendanceId,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\SellItem::create([
                'sell_id' => $sell->id,
                'store' => $userStore,
                'item' => $item,
                'item_name' => $request->item_name[$k],
                'unique_barcode' => $request->unique_barcode[$k],
                'item_discount' => $request->item_discount[$k],
                'item_price' => $request->item_price[$k],
                'item_amount' => $request->item_amount[$k],
                'discount_subtotal' => $request->discount_subtotal[$k],
                'price_subtotal' => $request->price_subtotal[$k],
                'tax' => $request->tax[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            $stockFrontItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $item)->first();
            if($stockFrontItem) {
                $stockFrontItem->update(['item_amount' => $stockFrontItem->item_amount - $request->item_amount[$k]]);
            }

            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount[$k]]);

            $uBarcode = \App\Models\Barcode::where('store', $userStore)
                ->where('item', $item)
                ->where('barcode', $request->unique_barcode[$k])
                ->first();
            if($uBarcode) {
                $uBarcode->update([
                    'status' => 'sold',
                    'sell_id' => $sell->id,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'Sell',
            'value' => $request->bill_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'sells.store : ' . $sell->id
        ]);

        if($request->ajax()){
            return $sell;
        }

        Flash::success('Sell saved successfully.');

        return redirect(route('sells.create'));
    }

    /**
     * Display the specified Sell.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sell = $this->sellRepository->findWithoutFail($id);

        if (empty($sell)) {
            Flash::error('Sell not found');

            return redirect(route('sells.index'));
        }

        return view('sells.show')->with('sell', $sell);
    }

    /**
     * Show the form for editing the specified Sell.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sell = $this->sellRepository->findWithoutFail($id);

        if (empty($sell)) {
            Flash::error('Sell not found');

            return redirect(route('sells.index'));
        }

        // added by dandisy
        $customer = \App\Models\Customer::all();
        $store = \App\Models\Store::all();
        $paymentmethod = \App\Models\PaymentMethod::all();

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $items = \App\Models\Item::with('featured')->get();
        $items = DB::table('items')
            ->leftJoin('featured_products', function($query) {
                $query->on('featured_products.item', '=', 'items.id')
                    ->on('featured_products.store', '=', 'items.store')
                    ->whereNull('featured_products.deleted_at');
            })
            ->leftJoin('barcodes', function($query) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->where('barcodes.status', 'ready')
                    ->whereNull('barcodes.deleted_at');
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->leftJoin('store_front_stocks', function($query) {
                $query->on('store_front_stocks.item', '=', 'items.id')
                    ->on('store_front_stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.barcode', 'items.sell_price', 'items.is_many_unique_barcode', 
                'featured_products.publish_on as featured_publish_on',
                'featured_products.publish_off as featured_publish_off',
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock',
                'store_front_stocks.item_amount as front_stock'
            )
            ->get();  

        $sellItems = \App\Models\SellItem::where('sell_id', $id)->get();

        $itemsExisting = DB::table('items')
            ->leftJoin('featured_products', function($query) {
                $query->on('featured_products.item', '=', 'items.id')
                    ->on('featured_products.store', '=', 'items.store')
                    ->whereNull('featured_products.deleted_at');
            })
            ->leftJoin('barcodes', function($query) use ($sellItems) {
                $query->on('barcodes.item', '=', 'items.id')
                    ->on('barcodes.store', '=', 'items.store')
                    ->where('barcodes.status', 'sold')
                    ->whereNull('barcodes.deleted_at');

                if($sellItems) {
                    $query->whereIn('barcodes.barcode', $sellItems->pluck('unique_barcode')->toArray());
                }
            })
            ->leftJoin('stocks', function($query) {
                $query->on('stocks.item', '=', 'items.id')
                    ->on('stocks.store', '=', 'items.store');
            })
            ->leftJoin('store_front_stocks', function($query) {
                $query->on('store_front_stocks.item', '=', 'items.id')
                    ->on('store_front_stocks.store', '=', 'items.store');
            })
            ->where('items.store', $userStore)
            ->whereNull('items.deleted_at')
            ->where('barcodes.status', 'sold')
            // ->where('store_front_stocks.item_amount', '>', 0)
            ->select(
                'items.id', 'items.image', 'items.name', 'items.category', 'items.code', 'items.barcode', 'items.sell_price', 'items.is_many_unique_barcode', 
                'featured_products.publish_on as featured_publish_on',
                'featured_products.publish_off as featured_publish_off',
                'barcodes.barcode as unique_barcode', 'barcodes.status as barcode_status',
                'stocks.item_amount as stock',
                'store_front_stocks.item_amount as front_stock'
            )
            ->get();

        // edited by dandisy
        // return view('sells.edit')->with('sell', $sell);
        return view('sells.edit')
            ->with('sell', $sell)
            ->with('customer', $customer)
            ->with('store', $store)
            ->with('paymentmethod', $paymentmethod)
            ->with('items', $items)
            ->with('sellItems', $sellItems)
            ->with('itemsExisting', $itemsExisting);        
    }

    /**
     * Update the specified Sell in storage.
     *
     * @param  int              $id
     * @param UpdateSellRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellRequest $request)
    {
        $sell = $this->sellRepository->findWithoutFail($id);
        $sellValueBefore = $sell->bill_amount;

        if (empty($sell)) {
            Flash::error('Sell not found');

            return redirect(route('sells.index'));
        }

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;
        $input['attendance_id'] = $attendanceId;
        $input['store'] = $userStore;

        // $sell = $this->sellRepository->update($input, $id);
        $sell = $sell->update($input);

        // deleting items
        $existingItems = \App\Models\SellItem::where('sell_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\SellItem::where('sell_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }
        foreach($request->sell_id as $k => $relId) {
            if($relId) {
                $existingSellItem = \App\Models\SellItem::find($relId);
                $existingItemAmount = $existingSellItem->item_amount;

                $existingSellItem->update([
                    'item' => $request->item[$k],
                    'store' => $userStore,
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);

                $stockFrontItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockFrontItem->update(['item_amount' => $stockFrontItem->item_amount + ($request->item_amount[$k] - $existingItemAmount)]);

                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockItem->update(['item_amount' => $stockItem->item_amount + ($request->item_amount[$k] - $existingItemAmount)]);
            } else {
                \App\Models\SellItem::create([
                    'sell_id' => $sell->id,
                    'store' => $userStore,
                    'item' => $request->item[$k],
                    'item_name' => $request->item_name[$k],
                    'unique_barcode' => $request->unique_barcode[$k],
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $stockFrontItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockFrontItem->update(['item_amount' => $stockFrontItem->item_amount - $request->item_amount[$k]]);

                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount[$k]]);
            }

            // update barcode
            $existingBarcodes = \App\Models\Barcode::where('sell_id', $id)
                ->where('item', $request->item[$k])
                ->where('store', $userStore)
                ->pluck('barcode')
                ->toArray();
            if($existingBarcodes) {
                $updateBarcodes = array_diff(array_values($existingBarcodes), $request->unique_barcode[$request->item_id[$k]]);
                if($updateBarcodes) {
                    \App\Models\Barcode::where('sell_id', $id)
                    ->where('item', $request->item[$k])
                    ->where('store', $userStore)
                    ->whereIn('barcode', $updateBarcodes)
                    ->update([
                        'status' => 'ready',
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }
            if($request->unique_barcode[$request->item_id[$k]]) {
                foreach($request->unique_barcode[$request->item_id[$k]] as $barcode) {
                    $currentBarcode = \App\Models\Barcode::where('sell_id', $id)
                        ->where('item', $request->item[$k])
                        ->where('store', $userStore)
                        ->where('barcode', $barcode)
                        ->first();                    
                    if($currentBarcode) {
                        $currentBarcode->update([
                            'status' => 'sold',
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'Sell',
            'value' => $request->bill_amount - $sellValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'sells.update : ' . $id
        ]);

        Flash::success('Sell updated successfully.');

        return redirect(route('sells.index'));
    }

    /**
     * Remove the specified Sell from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sell = $this->sellRepository->findWithoutFail($id);

        if (empty($sell)) {
            Flash::error('Sell not found');

            return redirect(route('sells.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->sellRepository->delete($id);
        $sell->delete();

        $sellItems = \App\Models\SellItem::where('sell_id', $id)->get();
        foreach($sellItems as $k => $item) {
            $stockFrontItem = \App\Models\StoreFrontStock::where('store', $userStore)->where('item', $item->item)->first();
            $stockFrontItem->update(['item_amount' => $stockFrontItem->item_amount + $item->item_amount]);
            
            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item->item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount + $item->item_amount]);

            \App\Models\Barcode::where('store', $userStore)
                ->where('item', $item->item)
                ->where('barcode', $item->unique_barcode[$k])
                ->update(['status' => 'ready']);
        }
        $sellItems->delete();
        \App\Models\Barcode::where('sell_id', $id)->update([
            'status' => 'ready',
            'updated_by' => Auth::user()->id
        ]);

        \App\Services\RecapService::handle([
            'model_type' => 'Sell',
            'value' => -1 * $sell->bill_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'sells.delete : ' . $id
        ]);

        Flash::success('Sell deleted successfully.');

        return redirect(route('sells.index'));
    }

    /**
     * Store data Sell from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $sell = $this->sellRepository->create($item->toArray());
            });
        });

        Flash::success('Sell saved successfully.');

        return redirect(route('sells.index'));
    }
}
