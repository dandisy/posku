<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePromoConditionRequest;
use App\Http\Requests\UpdatePromoConditionRequest;
use App\Repositories\PromoConditionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class PromoConditionController extends AppBaseController
{
    /** @var  PromoConditionRepository */
    private $promoConditionRepository;

    public function __construct(PromoConditionRepository $promoConditionRepo)
    {
        $this->middleware('auth');
        $this->promoConditionRepository = $promoConditionRepo;
    }

    /**
     * Display a listing of the PromoCondition.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->promoConditionRepository->pushCriteria(new RequestCriteria($request));
        $promoConditions = $this->promoConditionRepository->all();

        return view('promo_conditions.index')
            ->with('promoConditions', $promoConditions);
    }

    /**
     * Show the form for creating a new PromoCondition.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('promo_conditions.create');
        return view('promo_conditions.create');
    }

    /**
     * Store a newly created PromoCondition in storage.
     *
     * @param CreatePromoConditionRequest $request
     *
     * @return Response
     */
    public function store(CreatePromoConditionRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $promoCondition = $this->promoConditionRepository->create($input);
        $promoCondition = \App\Models\PromoCondition::create($input);

        \App\Services\LogService::handle([
            'description' => 'promoConditions.store : ' . $promoCondition->id
        ]);

        Flash::success('Promo Condition saved successfully.');

        return redirect(route('promoConditions.index'));
    }

    /**
     * Display the specified PromoCondition.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $promoCondition = $this->promoConditionRepository->findWithoutFail($id);

        if (empty($promoCondition)) {
            Flash::error('Promo Condition not found');

            return redirect(route('promoConditions.index'));
        }

        return view('promo_conditions.show')->with('promoCondition', $promoCondition);
    }

    /**
     * Show the form for editing the specified PromoCondition.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $promoCondition = $this->promoConditionRepository->findWithoutFail($id);

        if (empty($promoCondition)) {
            Flash::error('Promo Condition not found');

            return redirect(route('promoConditions.index'));
        }

        // edited by dandisy
        // return view('promo_conditions.edit')->with('promoCondition', $promoCondition);
        return view('promo_conditions.edit')
            ->with('promoCondition', $promoCondition);        
    }

    /**
     * Update the specified PromoCondition in storage.
     *
     * @param  int              $id
     * @param UpdatePromoConditionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromoConditionRequest $request)
    {
        $promoCondition = $this->promoConditionRepository->findWithoutFail($id);

        if (empty($promoCondition)) {
            Flash::error('Promo Condition not found');

            return redirect(route('promoConditions.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $promoCondition = $this->promoConditionRepository->update($input, $id);
        $promoCondition = $promoCondition->update($input);

        \App\Services\LogService::handle([
            'description' => 'promoConditions.update : ' . $id
        ]);

        Flash::success('Promo Condition updated successfully.');

        return redirect(route('promoConditions.index'));
    }

    /**
     * Remove the specified PromoCondition from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $promoCondition = $this->promoConditionRepository->findWithoutFail($id);

        if (empty($promoCondition)) {
            Flash::error('Promo Condition not found');

            return redirect(route('promoConditions.index'));
        }

        // $this->promoConditionRepository->delete($id);
        $promoCondition->delete();

        \App\Services\LogService::handle([
            'description' => 'promoConditions.delete : ' . $id
        ]);

        Flash::success('Promo Condition deleted successfully.');

        return redirect(route('promoConditions.index'));
    }

    /**
     * Store data PromoCondition from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $promoCondition = $this->promoConditionRepository->create($item->toArray());
            });
        });

        Flash::success('Promo Condition saved successfully.');

        return redirect(route('promoConditions.index'));
    }
}
