<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSellItemRequest;
use App\Http\Requests\UpdateSellItemRequest;
use App\Repositories\SellItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class SellItemController extends AppBaseController
{
    /** @var  SellItemRepository */
    private $sellItemRepository;

    public function __construct(SellItemRepository $sellItemRepo)
    {
        $this->middleware('auth');
        $this->sellItemRepository = $sellItemRepo;
    }

    /**
     * Display a listing of the SellItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sellItemRepository->pushCriteria(new RequestCriteria($request));
        $sellItems = $this->sellItemRepository->all();

        return view('sell_items.index')
            ->with('sellItems', $sellItems);
    }

    /**
     * Show the form for creating a new SellItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('sell_items.create');
        return view('sell_items.create')
            ->with('item', $item);
    }

    /**
     * Store a newly created SellItem in storage.
     *
     * @param CreateSellItemRequest $request
     *
     * @return Response
     */
    public function store(CreateSellItemRequest $request)
    {
        $input = $request->all();

        $input['item_name'] = \App\Models\Item::find($request->item)->name;

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $sellItem = $this->sellItemRepository->create($input);
        $sellItem = \App\Models\SellItem::create($input);

        \App\Services\LogService::handle([
            'description' => 'sellItems.store : ' . $sellItem->id
        ]);

        Flash::success('Sell Item saved successfully.');

        return redirect(route('sellItems.index'));
    }

    /**
     * Display the specified SellItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sellItem = $this->sellItemRepository->findWithoutFail($id);

        if (empty($sellItem)) {
            Flash::error('Sell Item not found');

            return redirect(route('sellItems.index'));
        }

        return view('sell_items.show')->with('sellItem', $sellItem);
    }

    /**
     * Show the form for editing the specified SellItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sellItem = $this->sellItemRepository->findWithoutFail($id);

        if (empty($sellItem)) {
            Flash::error('Sell Item not found');

            return redirect(route('sellItems.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('sell_items.edit')->with('sellItem', $sellItem);
        return view('sell_items.edit')
            ->with('sellItem', $sellItem)
            ->with('item', $item);        
    }

    /**
     * Update the specified SellItem in storage.
     *
     * @param  int              $id
     * @param UpdateSellItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellItemRequest $request)
    {
        $sellItem = $this->sellItemRepository->findWithoutFail($id);

        if (empty($sellItem)) {
            Flash::error('Sell Item not found');

            return redirect(route('sellItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $sellItem = $this->sellItemRepository->update($input, $id);
        $sellItem = $sellItem->update($input);

        \App\Services\LogService::handle([
            'description' => 'sellItems.update : ' . $id
        ]);

        Flash::success('Sell Item updated successfully.');

        return redirect(route('sellItems.index'));
    }

    /**
     * Remove the specified SellItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $sellItem = $this->sellItemRepository->findWithoutFail($id);

        if (empty($sellItem)) {
            if($request->ajax()){
                return 'Sell Item not found';
            }

            Flash::error('Sell Item not found');

            return redirect(route('sellItems.index'));
        }

        // $this->sellItemRepository->delete($id);
        $sellItem->delete();

        \App\Services\LogService::handle([
            'description' => 'sellItems.delete : ' . $id
        ]);
        
        if($request->ajax()){
            return 'Sell Item deleted successfully.';
        }

        Flash::success('Sell Item deleted successfully.');

        return redirect(route('sellItems.index'));
    }

    /**
     * Store data SellItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $sellItem = $this->sellItemRepository->create($item->toArray());
            });
        });

        Flash::success('Sell Item saved successfully.');

        return redirect(route('sellItems.index'));
    }
}
