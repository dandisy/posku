<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRejectedItemRequest;
use App\Http\Requests\UpdateRejectedItemRequest;
use App\Repositories\RejectedItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class RejectedItemController extends AppBaseController
{
    /** @var  RejectedItemRepository */
    private $rejectedItemRepository;

    public function __construct(RejectedItemRepository $rejectedItemRepo)
    {
        $this->middleware('auth');
        $this->rejectedItemRepository = $rejectedItemRepo;
    }

    /**
     * Display a listing of the RejectedItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rejectedItemRepository->pushCriteria(new RequestCriteria($request));
        $rejectedItems = $this->rejectedItemRepository->with(['store', 'item'])->all();

        return view('rejected_items.index')
            ->with('rejectedItems', $rejectedItems);
    }

    /**
     * Show the form for creating a new RejectedItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('rejected_items.create');
        return view('rejected_items.create')
            ->with('store', $store)
            ->with('item', $item);
    }

    /**
     * Store a newly created RejectedItem in storage.
     *
     * @param CreateRejectedItemRequest $request
     *
     * @return Response
     */
    public function store(CreateRejectedItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $rejectedItem = $this->rejectedItemRepository->create($input);
        $rejectedItem = \App\Models\RejectedItem::create($input);

        $stockItem = \App\Models\Stock::where('store', $attendance->store)->where('item', $request->item)->first();
        $stockItem->update(['item_amount' => $stockItem->item_amount - $request->item_amount]);

        Flash::success('Rejected Item saved successfully.');

        return redirect(route('rejectedItems.index'));
    }

    /**
     * Display the specified RejectedItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rejectedItem = $this->rejectedItemRepository->findWithoutFail($id);

        if (empty($rejectedItem)) {
            Flash::error('Rejected Item not found');

            return redirect(route('rejectedItems.index'));
        }

        return view('rejected_items.show')->with('rejectedItem', $rejectedItem);
    }

    /**
     * Show the form for editing the specified RejectedItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rejectedItem = $this->rejectedItemRepository->findWithoutFail($id);

        if (empty($rejectedItem)) {
            Flash::error('Rejected Item not found');

            return redirect(route('rejectedItems.index'));
        }

        // added by dandisy
        $store = \App\Models\Store::all();
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('rejected_items.edit')->with('rejectedItem', $rejectedItem);
        return view('rejected_items.edit')
            ->with('rejectedItem', $rejectedItem)
            ->with('store', $store)
            ->with('item', $item);        
    }

    /**
     * Update the specified RejectedItem in storage.
     *
     * @param  int              $id
     * @param UpdateRejectedItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRejectedItemRequest $request)
    {
        $rejectedItem = $this->rejectedItemRepository->findWithoutFail($id);

        if (empty($rejectedItem)) {
            Flash::error('Rejected Item not found');

            return redirect(route('rejectedItems.index'));
        }
        $existingItemAmount = $existingSellReturnItem->item_amount;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $rejectedItem = $this->rejectedItemRepository->update($input, $id);
        $rejectedItem = $rejectedItem->update($input);

        $stockItem = \App\Models\Stock::where('store', $attendance->store)->where('item', $request->item)->first();
        $stockItem->update(['item_amount' => $stockItem->item_amount - ($request->item_amount - $existingItemAmount)]);

        Flash::success('Rejected Item updated successfully.');

        return redirect(route('rejectedItems.index'));
    }

    /**
     * Remove the specified RejectedItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rejectedItem = $this->rejectedItemRepository->findWithoutFail($id);

        if (empty($rejectedItem)) {
            Flash::error('Rejected Item not found');

            return redirect(route('rejectedItems.index'));
        }

        // $this->rejectedItemRepository->delete($id);
        $rejectedItem->delete();

        Flash::success('Rejected Item deleted successfully.');

        return redirect(route('rejectedItems.index'));
    }

    /**
     * Store data RejectedItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $rejectedItem = $this->rejectedItemRepository->create($item->toArray());
            });
        });

        Flash::success('Rejected Item saved successfully.');

        return redirect(route('rejectedItems.index'));
    }
}
