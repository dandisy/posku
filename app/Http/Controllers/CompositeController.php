<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCompositeRequest;
use App\Http\Requests\UpdateCompositeRequest;
use App\Repositories\CompositeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class CompositeController extends AppBaseController
{
    /** @var  CompositeRepository */
    private $compositeRepository;

    public function __construct(CompositeRepository $compositeRepo)
    {
        $this->middleware('auth');
        $this->compositeRepository = $compositeRepo;
    }

    /**
     * Display a listing of the Composite.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->compositeRepository->pushCriteria(new RequestCriteria($request));
        $composites = $this->compositeRepository->with(['store', 'product'])->all();

        return view('composites.index')
            ->with('composites', $composites);
    }

    /**
     * Show the form for creating a new Composite.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $items = \App\Models\Item::all();
        $units = \App\Models\Unit::all();
        $compositeItems = collect();
        

        // edited by dandisy
        // return view('composites.create');
        return view('composites.create')
            ->with('store', $store)
            ->with('items', $items)
            ->with('units', $units)
            ->with('compositeItems', $compositeItems);
    }

    /**
     * Store a newly created Composite in storage.
     *
     * @param CreateCompositeRequest $request
     *
     * @return Response
     */
    public function store(CreateCompositeRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        // $composite = $this->compositeRepository->create($input);

        $composite = \App\Models\Composite::create([
            'store' => $request->store,
            'product' => $request->product,
            'note' => $request->note,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\Composite::create([
                'composite_id' => $composite->id,
                'item' => $item,
                'item_amount' => $request->item_amount[$k],
                'unit' => $request->unit[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
        }

        \App\Services\LogService::handle([
            'description' => 'composites.store : ' . $composite->id
        ]);

        Flash::success('Composite saved successfully.');

        return redirect(route('composites.index'));
    }

    /**
     * Display the specified Composite.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $composite = $this->compositeRepository->findWithoutFail($id);

        if (empty($composite)) {
            Flash::error('Composite not found');

            return redirect(route('composites.index'));
        }

        return view('composites.show')->with('composite', $composite);
    }

    /**
     * Show the form for editing the specified Composite.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $composite = $this->compositeRepository->findWithoutFail($id);

        if (empty($composite)) {
            Flash::error('Composite not found');

            return redirect(route('composites.index'));
        }

        // added by dandisy
        $store = \App\Models\Store::all();
        $items = \App\Models\Item::all();
        $units = \App\Models\Unit::all();

        // $compositeItems = \App\Models\CompositeItem::where('composite_id', $composite->id)->get();
        $compositeItems = \Illuminate\Support\Facades\DB::table('composite_items')
            ->join('items', 'items.id', '=', 'composite_items.item')
            ->where('composite_items.composite_id', $composite->id)
            ->select('composite_items.*', 'items.name as item_name')
            ->get();

        // edited by dandisy
        // return view('composites.edit')->with('composite', $composite);
        return view('composites.edit')
            ->with('composite', $composite)
            ->with('store', $store)
            ->with('items', $items)
            ->with('units', $units)
            ->with('compositeItems', $compositeItems);        
    }

    /**
     * Update the specified Composite in storage.
     *
     * @param  int              $id
     * @param UpdateCompositeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompositeRequest $request)
    {
        $composite = $this->compositeRepository->findWithoutFail($id);

        if (empty($composite)) {
            Flash::error('Composite not found');

            return redirect(route('composites.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $composite = $this->compositeRepository->update($input, $id);

        $composite = $composite->update($input);

        // deleting items
        $existingItems = \App\Models\CompositeItem::where('composite_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\CompositeItem::where('composite_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }
        foreach($request->composite_id as $k => $relId) {
            if($relId) {
                (\App\Models\CompositeItem::find($relId))->update([
                    'item' => $request->item[$k],
                    'item_amount' => $request->item_amount[$k],
                    'unit' => $request->unit[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);
            } else {
                \App\Models\CompositeItem::create([
                    'composite_id' => $id,
                    'item' => $request->item[$k],
                    'item_amount' => $request->item_amount[$k],
                    'unit' => $request->unit[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        \App\Services\LogService::handle([
            'description' => 'composites.update : ' . $id
        ]);

        Flash::success('Composite updated successfully.');

        return redirect(route('composites.index'));
    }

    /**
     * Remove the specified Composite from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $composite = $this->compositeRepository->findWithoutFail($id);

        if (empty($composite)) {
            Flash::error('Composite not found');

            return redirect(route('composites.index'));
        }
        // $this->compositeRepository->delete($id);
        $composite->delete();

        \App\Models\CompositeItem::where('composite_id', $id)->delete();

        \App\Services\LogService::handle([
            'description' => 'composites.delete : ' . $id
        ]);

        Flash::success('Composite deleted successfully.');

        return redirect(route('composites.index'));
    }

    /**
     * Store data Composite from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $composite = $this->compositeRepository->create($item->toArray());
            });
        });

        Flash::success('Composite saved successfully.');

        return redirect(route('composites.index'));
    }
}
