<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBarcodeRequest;
use App\Http\Requests\UpdateBarcodeRequest;
use App\Repositories\BarcodeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class BarcodeController extends AppBaseController
{
    /** @var  BarcodeRepository */
    private $barcodeRepository;

    public function __construct(BarcodeRepository $barcodeRepo)
    {
        $this->middleware('auth');
        $this->barcodeRepository = $barcodeRepo;
    }

    /**
     * Display a listing of the Barcode.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->barcodeRepository->pushCriteria(new RequestCriteria($request));
        $barcodes = $this->barcodeRepository->with(['store', 'item'])->all();

        return view('barcodes.index')
            ->with('barcodes', $barcodes);
    }

    /**
     * Show the form for creating a new Barcode.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('barcodes.create');
        return view('barcodes.create')
            ->with('item', $item)
            ->with('store', $store);
    }

    /**
     * Store a newly created Barcode in storage.
     *
     * @param CreateBarcodeRequest $request
     *
     * @return Response
     */
    public function store(CreateBarcodeRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $barcode = $this->barcodeRepository->create($input);
        $barcode = \App\Models\Barcode::create($input);

        Flash::success('Barcode saved successfully.');

        return redirect(route('barcodes.index'));
    }

    /**
     * Display the specified Barcode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $barcode = $this->barcodeRepository->findWithoutFail($id);

        if (empty($barcode)) {
            Flash::error('Barcode not found');

            return redirect(route('barcodes.index'));
        }

        return view('barcodes.show')->with('barcode', $barcode);
    }

    /**
     * Show the form for editing the specified Barcode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $barcode = $this->barcodeRepository->findWithoutFail($id);

        if (empty($barcode)) {
            Flash::error('Barcode not found');

            return redirect(route('barcodes.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('barcodes.edit')->with('barcode', $barcode);
        return view('barcodes.edit')
            ->with('barcode', $barcode)
            ->with('item', $item)
            ->with('store', $store);        
    }

    /**
     * Update the specified Barcode in storage.
     *
     * @param  int              $id
     * @param UpdateBarcodeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBarcodeRequest $request)
    {
        $barcode = $this->barcodeRepository->findWithoutFail($id);

        if (empty($barcode)) {
            Flash::error('Barcode not found');

            return redirect(route('barcodes.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $barcode = $this->barcodeRepository->update($input, $id);
        $barcode = $barcode->update($input);

        Flash::success('Barcode updated successfully.');

        return redirect(route('barcodes.index'));
    }

    /**
     * Remove the specified Barcode from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $barcode = $this->barcodeRepository->findWithoutFail($id);

        if (empty($barcode)) {
            Flash::error('Barcode not found');

            return redirect(route('barcodes.index'));
        }

        // $this->barcodeRepository->delete($id);
        $barcode->delete();

        Flash::success('Barcode deleted successfully.');

        return redirect(route('barcodes.index'));
    }

    /**
     * Store data Barcode from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $barcode = $this->barcodeRepository->create($item->toArray());
            });
        });

        Flash::success('Barcode saved successfully.');

        return redirect(route('barcodes.index'));
    }
}
