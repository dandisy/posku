<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePurchaseItemRequest;
use App\Http\Requests\UpdatePurchaseItemRequest;
use App\Repositories\PurchaseItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class PurchaseItemController extends AppBaseController
{
    /** @var  PurchaseItemRepository */
    private $purchaseItemRepository;

    public function __construct(PurchaseItemRepository $purchaseItemRepo)
    {
        $this->middleware('auth');
        $this->purchaseItemRepository = $purchaseItemRepo;
    }

    /**
     * Display a listing of the PurchaseItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->purchaseItemRepository->pushCriteria(new RequestCriteria($request));
        $purchaseItems = $this->purchaseItemRepository->all();

        return view('purchase_items.index')
            ->with('purchaseItems', $purchaseItems);
    }

    /**
     * Show the form for creating a new PurchaseItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('purchase_items.create');
        return view('purchase_items.create')
            ->with('item', $item);
    }

    /**
     * Store a newly created PurchaseItem in storage.
     *
     * @param CreatePurchaseItemRequest $request
     *
     * @return Response
     */
    public function store(CreatePurchaseItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $purchaseItem = $this->purchaseItemRepository->create($input);
        $purchaseItem = \App\Models\PurchaseItemR::create($input);

        \App\Services\LogService::handle([
            'description' => 'purchaseItems.store : ' . $purchaseItem->id
        ]);

        Flash::success('Purchase Item saved successfully.');

        return redirect(route('purchaseItems.index'));
    }

    /**
     * Display the specified PurchaseItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $purchaseItem = $this->purchaseItemRepository->findWithoutFail($id);

        if (empty($purchaseItem)) {
            Flash::error('Purchase Item not found');

            return redirect(route('purchaseItems.index'));
        }

        return view('purchase_items.show')->with('purchaseItem', $purchaseItem);
    }

    /**
     * Show the form for editing the specified PurchaseItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $purchaseItem = $this->purchaseItemRepository->findWithoutFail($id);

        if (empty($purchaseItem)) {
            Flash::error('Purchase Item not found');

            return redirect(route('purchaseItems.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('purchase_items.edit')->with('purchaseItem', $purchaseItem);
        return view('purchase_items.edit')
            ->with('purchaseItem', $purchaseItem)
            ->with('item', $item);        
    }

    /**
     * Update the specified PurchaseItem in storage.
     *
     * @param  int              $id
     * @param UpdatePurchaseItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePurchaseItemRequest $request)
    {
        $purchaseItem = $this->purchaseItemRepository->findWithoutFail($id);

        if (empty($purchaseItem)) {
            Flash::error('Purchase Item not found');

            return redirect(route('purchaseItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $purchaseItem = $this->purchaseItemRepository->update($input, $id);
        $purchaseItem = $purchaseItem->update($input);

        \App\Services\LogService::handle([
            'description' => 'purchaseItems.update : ' . $id
        ]);

        Flash::success('Purchase Item updated successfully.');

        return redirect(route('purchaseItems.index'));
    }

    /**
     * Remove the specified PurchaseItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $purchaseItem = $this->purchaseItemRepository->findWithoutFail($id);

        if (empty($purchaseItem)) {
            if($request->ajax()){
                return 'Purchase Item not found';
            }

            Flash::error('Purchase Item not found');

            return redirect(route('purchaseItems.index'));
        }

        // $this->purchaseItemRepository->delete($id);
        $purchaseItem->delete();

        \App\Services\LogService::handle([
            'description' => 'purchaseItems.delete : ' . $id
        ]);
        
        if($request->ajax()){
            return 'Purchase Item deleted successfully.';
        }

        Flash::success('Purchase Item deleted successfully.');

        return redirect(route('purchaseItems.index'));
    }

    /**
     * Store data PurchaseItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $purchaseItem = $this->purchaseItemRepository->create($item->toArray());
            });
        });

        Flash::success('Purchase Item saved successfully.');

        return redirect(route('purchaseItems.index'));
    }
}
