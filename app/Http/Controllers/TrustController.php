<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTrustRequest;
use App\Http\Requests\UpdateTrustRequest;
use App\Repositories\TrustRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class TrustController extends AppBaseController
{
    /** @var  TrustRepository */
    private $trustRepository;

    public function __construct(TrustRepository $trustRepo)
    {
        $this->middleware('auth');
        $this->trustRepository = $trustRepo;
    }

    /**
     * Display a listing of the Trust.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->trustRepository->pushCriteria(new RequestCriteria($request));
        $trusts = $this->trustRepository->all();

        return view('trusts.index')
            ->with('trusts', $trusts);
    }

    /**
     * Show the form for creating a new Trust.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('trusts.create');
        return view('trusts.create')
            ->with('store', $store);
    }

    /**
     * Store a newly created Trust in storage.
     *
     * @param CreateTrustRequest $request
     *
     * @return Response
     */
    public function store(CreateTrustRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $trust = $this->trustRepository->create($input);
        $trust = \App\Models\Trust::create($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Trust',
            'value' => $request->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'trusts.store : ' . $trust->id
        ]);

        Flash::success('Trust saved successfully.');

        return redirect(route('trusts.index'));
    }

    /**
     * Display the specified Trust.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $trust = $this->trustRepository->findWithoutFail($id);

        if (empty($trust)) {
            Flash::error('Trust not found');

            return redirect(route('trusts.index'));
        }

        return view('trusts.show')->with('trust', $trust);
    }

    /**
     * Show the form for editing the specified Trust.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $trust = $this->trustRepository->findWithoutFail($id);

        if (empty($trust)) {
            Flash::error('Trust not found');

            return redirect(route('trusts.index'));
        }
        
        // added by dandisy
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('trusts.edit')->with('trust', $trust);
        return view('trusts.edit')
            ->with('trust', $trust)
            ->with('store', $store);        
    }

    /**
     * Update the specified Trust in storage.
     *
     * @param  int              $id
     * @param UpdateTrustRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTrustRequest $request)
    {
        $trust = $this->trustRepository->findWithoutFail($id);
        $trustValueBefore = $trust->value;

        if (empty($trust)) {
            Flash::error('Trust not found');

            return redirect(route('trusts.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $input['attendance_id'] = $attendance->id;
        $input['store'] = $attendance->store;

        // $trust = $this->trustRepository->update($input, $id);
        $trust = $trust->update($input);

        \App\Services\RecapService::handle([
            'model_type' => 'Trust',
            'value' => $request->value - $trustValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'trusts.update : ' . $id
        ]);

        Flash::success('Trust updated successfully.');

        return redirect(route('trusts.index'));
    }

    /**
     * Remove the specified Trust from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $trust = $this->trustRepository->findWithoutFail($id);

        if (empty($trust)) {
            Flash::error('Trust not found');

            return redirect(route('trusts.index'));
        }

        \App\Services\LogService::handle([
            'description' => 'trusts.delete : ' . $id
        ]);

        // $this->trustRepository->delete($id);
        $trust->delete();

        \App\Services\RecapService::handle([
            'model_type' => 'Trust',
            'value' => -1 * $trust->value,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'trusts.delete : ' . $id
        ]);

        Flash::success('Trust deleted successfully.');

        return redirect(route('trusts.index'));
    }

    /**
     * Store data Trust from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $trust = $this->trustRepository->create($item->toArray());
            });
        });

        Flash::success('Trust saved successfully.');

        return redirect(route('trusts.index'));
    }
}
