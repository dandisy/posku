<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCompositeItemRequest;
use App\Http\Requests\UpdateCompositeItemRequest;
use App\Repositories\CompositeItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class CompositeItemController extends AppBaseController
{
    /** @var  CompositeItemRepository */
    private $compositeItemRepository;

    public function __construct(CompositeItemRepository $compositeItemRepo)
    {
        $this->middleware('auth');
        $this->compositeItemRepository = $compositeItemRepo;
    }

    /**
     * Display a listing of the CompositeItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->compositeItemRepository->pushCriteria(new RequestCriteria($request));
        $compositeItems = $this->compositeItemRepository->all();

        return view('composite_items.index')
            ->with('compositeItems', $compositeItems);
    }

    /**
     * Show the form for creating a new CompositeItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        $unit = \App\Models\Unit::all();
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('composite_items.create');
        return view('composite_items.create')
            ->with('item', $item)
            ->with('unit', $unit)
            ->with('store', $store);
    }

    /**
     * Store a newly created CompositeItem in storage.
     *
     * @param CreateCompositeItemRequest $request
     *
     * @return Response
     */
    public function store(CreateCompositeItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;
        $input['store'] = $userStore;

        // $compositeItem = $this->compositeItemRepository->create($input);
        $compositeItem = \App\Models\CompositeItem::create($input);

        \App\Services\LogService::handle([
            'description' => 'compositeItems.store : ' . $compositeItem->id
        ]);

        Flash::success('Composite Item saved successfully.');

        return redirect(route('compositeItems.index'));
    }

    /**
     * Display the specified CompositeItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $compositeItem = $this->compositeItemRepository->findWithoutFail($id);

        if (empty($compositeItem)) {
            Flash::error('Composite Item not found');

            return redirect(route('compositeItems.index'));
        }

        return view('composite_items.show')->with('compositeItem', $compositeItem);
    }

    /**
     * Show the form for editing the specified CompositeItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $compositeItem = $this->compositeItemRepository->findWithoutFail($id);

        if (empty($compositeItem)) {
            Flash::error('Composite Item not found');

            return redirect(route('compositeItems.index'));
        }

        // added by dandisy
        $item = \App\Models\Item::all();
        $unit = \App\Models\Unit::all();
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('composite_items.edit')->with('compositeItem', $compositeItem);
        return view('composite_items.edit')
            ->with('compositeItem', $compositeItem)
            ->with('item', $item)
            ->with('unit', $unit)
            ->with('store', $store);        
    }

    /**
     * Update the specified CompositeItem in storage.
     *
     * @param  int              $id
     * @param UpdateCompositeItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompositeItemRequest $request)
    {
        $compositeItem = $this->compositeItemRepository->findWithoutFail($id);

        if (empty($compositeItem)) {
            Flash::error('Composite Item not found');

            return redirect(route('compositeItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;
        $input['store'] = $userStore;

        // $compositeItem = $this->compositeItemRepository->update($input, $id);
        $compositeItem = $compositeItem->update($input, $id);

        \App\Services\LogService::handle([
            'description' => 'compositeItems.update : ' . $id
        ]);

        Flash::success('Composite Item updated successfully.');

        return redirect(route('compositeItems.index'));
    }

    /**
     * Remove the specified CompositeItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $compositeItem = $this->compositeItemRepository->findWithoutFail($id);

        if (empty($compositeItem)) {
            if($request->ajax()){
                return 'Composite Item not found';
            }

            Flash::error('Composite Item not found');

            return redirect(route('compositeItems.index'));
        }

        // $this->compositeItemRepository->delete($id);
        $compositeItem->delete();

        \App\Services\LogService::handle([
            'description' => 'compositeItems.delete : ' . $id
        ]);

        if($request->ajax()){
            return 'Composite Item deleted successfully.';
        }

        Flash::success('Composite Item deleted successfully.');

        return redirect(route('compositeItems.index'));
    }

    /**
     * Store data CompositeItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $compositeItem = $this->compositeItemRepository->create($item->toArray());
            });
        });

        Flash::success('Composite Item saved successfully.');

        return redirect(route('compositeItems.index'));
    }
}
