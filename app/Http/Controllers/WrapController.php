<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWrapRequest;
use App\Http\Requests\UpdateWrapRequest;
use App\Repositories\WrapRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class WrapController extends AppBaseController
{
    /** @var  WrapRepository */
    private $wrapRepository;

    public function __construct(WrapRepository $wrapRepo)
    {
        $this->middleware('auth');
        $this->wrapRepository = $wrapRepo;
    }

    /**
     * Display a listing of the Wrap.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->wrapRepository->pushCriteria(new RequestCriteria($request));
        $wraps = $this->wrapRepository->all();

        return view('wraps.index')
            ->with('wraps', $wraps);
    }

    /**
     * Show the form for creating a new Wrap.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        

        // edited by dandisy
        // return view('wraps.create');
        return view('wraps.create');
    }

    /**
     * Store a newly created Wrap in storage.
     *
     * @param CreateWrapRequest $request
     *
     * @return Response
     */
    public function store(CreateWrapRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $wrap = $this->wrapRepository->create($input);
        $wrap = \App\Models\Wrap::create($input);

        \App\Services\LogService::handle([
            'description' => 'wraps.store : ' . $wrap->id
        ]);

        Flash::success('Wrap saved successfully.');

        return redirect(route('wraps.index'));
    }

    /**
     * Display the specified Wrap.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wrap = $this->wrapRepository->findWithoutFail($id);

        if (empty($wrap)) {
            Flash::error('Wrap not found');

            return redirect(route('wraps.index'));
        }

        return view('wraps.show')->with('wrap', $wrap);
    }

    /**
     * Show the form for editing the specified Wrap.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // added by dandisy
        

        $wrap = $this->wrapRepository->findWithoutFail($id);

        if (empty($wrap)) {
            Flash::error('Wrap not found');

            return redirect(route('wraps.index'));
        }

        // edited by dandisy
        // return view('wraps.edit')->with('wrap', $wrap);
        return view('wraps.edit')
            ->with('wrap', $wrap);        
    }

    /**
     * Update the specified Wrap in storage.
     *
     * @param  int              $id
     * @param UpdateWrapRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWrapRequest $request)
    {
        $wrap = $this->wrapRepository->findWithoutFail($id);

        if (empty($wrap)) {
            Flash::error('Wrap not found');

            return redirect(route('wraps.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $wrap = $this->wrapRepository->update($input, $id);
        $wrap = $wrap->update($input);

        \App\Services\LogService::handle([
            'description' => 'wraps.update : ' . $id
        ]);

        Flash::success('Wrap updated successfully.');

        return redirect(route('wraps.index'));
    }

    /**
     * Remove the specified Wrap from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wrap = $this->wrapRepository->findWithoutFail($id);

        if (empty($wrap)) {
            Flash::error('Wrap not found');

            return redirect(route('wraps.index'));
        }

        // $this->wrapRepository->delete($id);
        $wrap->delete();

        \App\Services\LogService::handle([
            'description' => 'wraps.delete : ' . $id
        ]);

        Flash::success('Wrap deleted successfully.');

        return redirect(route('wraps.index'));
    }

    /**
     * Store data Wrap from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $wrap = $this->wrapRepository->create($item->toArray());
            });
        });

        Flash::success('Wrap saved successfully.');

        return redirect(route('wraps.index'));
    }
}
