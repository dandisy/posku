<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePurchaseRequest;
use App\Http\Requests\UpdatePurchaseRequest;
use App\Repositories\PurchaseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class PurchaseController extends AppBaseController
{
    /** @var  PurchaseRepository */
    private $purchaseRepository;

    public function __construct(PurchaseRepository $purchaseRepo)
    {
        $this->middleware('auth');
        $this->purchaseRepository = $purchaseRepo;
    }

    /**
     * Display a listing of the Purchase.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->purchaseRepository->pushCriteria(new RequestCriteria($request));
        $purchases = $this->purchaseRepository->with(['supplier'])->all();

        return view('purchases.index')
            ->with('purchases', $purchases);
    }

    /**
     * Show the form for creating a new Purchase.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $store = \App\Models\Store::all();
        $supplier = \App\Models\Supplier::all();
        $items = \App\Models\Item::all();
        $purchaseItems = collect();
        

        // edited by dandisy
        // return view('purchases.create');
        return view('purchases.create')
            ->with('store', $store)
            ->with('supplier', $supplier)
            ->with('items', $items)
            ->with('purchaseItems', $purchaseItems)
            ->with('itemBarcodes', collect());
    }

    /**
     * Store a newly created Purchase in storage.
     *
     * @param CreatePurchaseRequest $request
     *
     * @return Response
     */
    public function store(CreatePurchaseRequest $request)
    {
        // $input = $request->all();

        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;

        // $purchase = $this->purchaseRepository->create($input);
        $purchase = \App\Models\Purchase::create([
            'store' => $userStore,
            'supplier' => $request->supplier,
            'receipt_code' => $request->receipt_code,
            'receipt_amount' => $request->receipt_amount,
            'note' => $request->note,
            'attendance_id' => $attendanceId,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        foreach($request->item as $k => $item) {
            \App\Models\PurchaseItem::create([
                'purchase_id' => $purchase->id,
                'store' => $userStore,
                'item' => $item,
                'item_discount' => $request->item_discount[$k],
                'item_price' => $request->item_price[$k],
                'item_amount' => $request->item_amount[$k],
                'discount_subtotal' => $request->discount_subtotal[$k],
                'price_subtotal' => $request->price_subtotal[$k],
                'tax' => $request->tax[$k],
                'note' => $request->item_note[$k],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
            
            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item)->first();
            if($stockItem) {
                $stockItem->update(['item_amount' => $stockItem->item_amount + $request->item_amount[$k]]);
            } else {
                \App\Models\Stock::create([
                    'store' => $userStore,
                    'item' => $item, 
                    'item_amount' => $request->item_amount[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);
            }
            
            if($request->barcode[$request->item_id[$k]]) {
                foreach($request->barcode[$request->item_id[$k]] as $barcode) {
                    $test = \App\Models\Barcode::create([
                        'purchase_id' => $purchase->id,
                        'item' => $item,
                        'barcode' => $barcode,
                        'store' => $userStore,
                        'status' => 'ready',
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'Purchase',
            'value' => $request->receipt_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'purchases.store : ' . $purchase->id
        ]);

        Flash::success('Purchase saved successfully.');

        return redirect(route('purchases.index'));
    }

    /**
     * Display the specified Purchase.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (empty($purchase)) {
            Flash::error('Purchase not found');

            return redirect(route('purchases.index'));
        }

        return view('purchases.show')->with('purchase', $purchase);
    }

    /**
     * Show the form for editing the specified Purchase.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (empty($purchase)) {
            Flash::error('Purchase not found');

            return redirect(route('purchases.index'));
        }

        // added by dandisy
        $store = \App\Models\Store::all();
        $supplier = \App\Models\Supplier::all();

        $items = \App\Models\Item::all();
        // $purchaseItems = \App\Models\PurchaseItem::where('purchase_id', $id)->get();
        $purchaseItems = \Illuminate\Support\Facades\DB::table('purchase_items')
            ->join('items', 'items.id', '=', 'purchase_items.item')
            ->where('purchase_items.purchase_id', $id)
            ->select('purchase_items.*', 'items.name as item_name')
            ->get();

        // edited by dandisy
        // return view('purchases.edit')->with('purchase', $purchase);
        return view('purchases.edit')
            ->with('purchase', $purchase)
            ->with('store', $store)
            ->with('supplier', $supplier)
            ->with('items', $items)
            ->with('purchaseItems', $purchaseItems)
            ->with('itemBarcodes', \App\Models\Barcode::where('purchase_id', $id)->get());        
    }

    /**
     * Update the specified Purchase in storage.
     *
     * @param  int              $id
     * @param UpdatePurchaseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePurchaseRequest $request)
    {
        $purchase = $this->purchaseRepository->findWithoutFail($id);
        $purchaseValueBefore = $purchase->receipt_amount;

        if (empty($purchase)) {
            Flash::error('Purchase not found');

            return redirect(route('purchases.index'));
        }

        $attendance = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first();
        $attendanceId = $attendance->id;
        $userStore = $attendance->store;
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;
        $input['attendance_id'] = $attendanceId;
        $input['store'] = $userStore;

        // $purchase = $this->purchaseRepository->update($input, $id);
        $purchase = $purchase->update($input);

        // deleting items
        $existingItems = \App\Models\PurchaseItem::where('purchase_id', $id)->pluck('item')->toArray();
        if($existingItems) {
            $deleteItems = array_diff(array_values($existingItems), $request->item);
            if($deleteItems) {
                \App\Models\PurchaseItem::where('purchase_id', $id)->whereIn('item', $deleteItems)->delete();
            }
        }  
        foreach($request->purchase_id as $k => $relId) {
            if($relId) {
                $existingPurchaseItem = \App\Models\PurchaseItem::find($request->item_id[$k]);
                $existingItemAmount = $existingPurchaseItem->item_amount;

                $existingPurchaseItem->update([
                    'item' =>  $request->item[$k],
                    'store' => $userStore,
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'note' => $request->item_note[$k],
                    'updated_by' => Auth::user()->id
                ]);

                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                $stockItem->update(['item_amount' => $stockItem->item_amount + ($request->item_amount[$k] - $existingItemAmount)]);
            } else {
                \App\Models\PurchaseItem::create([
                    'purchase_id' => $id,
                    'store' => $userStore,
                    'item' =>  $request->item[$k],
                    'item_discount' => $request->item_discount[$k],
                    'item_price' => $request->item_price[$k],
                    'item_amount' => $request->item_amount[$k],
                    'discount_subtotal' => $request->discount_subtotal[$k],
                    'price_subtotal' => $request->price_subtotal[$k],
                    'tax' => $request->tax[$k],
                    'note' => $request->item_note[$k],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $request->item[$k])->first();
                if($stockItem) {
                    $stockItem->update(['item_amount' => $stockItem->item_amount + $request->item_amount[$k]]);
                } else {
                    \App\Models\Stock::create([
                        'store' => $userStore,
                        'item' => $request->item[$k], 
                        'item_amount' => $request->item_amount[$k],
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }

            // deleting barcode
            $existingBarcodes = \App\Models\Barcode::where('purchase_id', $id)
                ->where('item', $request->item[$k])
                ->pluck('barcode')
                ->toArray();
            if($existingBarcodes) {
                $deleteBarcodes = array_diff(array_values($existingBarcodes), $request->barcode[$request->item_id[$k]]);
                if($deleteBarcodes) {
                    \App\Models\Barcode::where('purchase_id', $id)
                        ->where('item', $request->item[$k])
                        ->whereIn('barcode', $deleteBarcodes)
                        ->delete();
                }
            }            
            if($request->barcode[$request->item_id[$k]]) {
                foreach($request->barcode[$request->item_id[$k]] as $barcode) {
                    if(!(\App\Models\Barcode::where('purchase_id', $id)->where('item', $request->item[$k])->where('barcode', $barcode)->first())) {
                        \App\Models\Barcode::create([
                            'purchase_id' => $id,
                            'item' => $request->item[$k],
                            'barcode' => $barcode,
                            'store' => $userStore,
                            'status' => 'ready',
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }

        \App\Services\RecapService::handle([
            'model_type' => 'Purchase',
            'value' => $request->receipt_amount - $purchaseValueBefore,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'purchases.update : ' . $id
        ]);

        Flash::success('Purchase updated successfully.');

        return redirect(route('purchases.index'));
    }

    /**
     * Remove the specified Purchase from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (empty($purchase)) {
            Flash::error('Purchase not found');

            return redirect(route('purchases.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->purchaseRepository->delete($id);
        $purchase->delete();

        $purchaseItems = \App\Models\PurchaseItem::where('purchase_id', $id)->get();
        foreach($purchaseItems as $k => $item) {
            $stockItem = \App\Models\Stock::where('store', $userStore)->where('item', $item)->first();
            $stockItem->update(['item_amount' => $stockItem->item_amount - $item->item_amount]);
        }
        $purchaseItems->delete();
        \App\Models\Barcode::where('purchase_id', $id)->delete();

        \App\Services\RecapService::handle([
            'model_type' => 'Purchase',
            'value' => -1 * $purchase->receipt_amount,
            'model_field' => 'value',
            'operation' => 'sum'
        ]);

        \App\Services\LogService::handle([
            'description' => 'purchases.delete : ' . $id
        ]);

        Flash::success('Purchase deleted successfully.');

        return redirect(route('purchases.index'));
    }

    /**
     * Store data Purchase from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $purchase = $this->purchaseRepository->create($item->toArray());
            });
        });

        Flash::success('Purchase saved successfully.');

        return redirect(route('purchases.index'));
    }
}
