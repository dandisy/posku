<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePurchaseReturnItemRequest;
use App\Http\Requests\UpdatePurchaseReturnItemRequest;
use App\Repositories\PurchaseReturnItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class PurchaseReturnItemController extends AppBaseController
{
    /** @var  PurchaseReturnItemRepository */
    private $purchaseReturnItemRepository;

    public function __construct(PurchaseReturnItemRepository $purchaseReturnItemRepo)
    {
        $this->middleware('auth');
        $this->purchaseReturnItemRepository = $purchaseReturnItemRepo;
    }

    /**
     * Display a listing of the PurchaseReturnItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->purchaseReturnItemRepository->pushCriteria(new RequestCriteria($request));
        $purchaseReturnItems = $this->purchaseReturnItemRepository->all();

        return view('purchase_return_items.index')
            ->with('purchaseReturnItems', $purchaseReturnItems);
    }

    /**
     * Show the form for creating a new PurchaseReturnItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('purchase_return_items.create');
        return view('purchase_return_items.create')
            ->with('item', $item);
    }

    /**
     * Store a newly created PurchaseReturnItem in storage.
     *
     * @param CreatePurchaseReturnItemRequest $request
     *
     * @return Response
     */
    public function store(CreatePurchaseReturnItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $purchaseReturnItem = $this->purchaseReturnItemRepository->create($input);
        $purchaseReturnItem = \App\Models\PurchaseReturnItem::create($input);

        \App\Services\LogService::handle([
            'description' => 'purchaseReturnItems.store : ' . $purchaseReturnItem->id
        ]);

        Flash::success('Purchase Return Item saved successfully.');

        return redirect(route('purchaseReturnItems.index'));
    }

    /**
     * Display the specified PurchaseReturnItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $purchaseReturnItem = $this->purchaseReturnItemRepository->findWithoutFail($id);

        if (empty($purchaseReturnItem)) {
            Flash::error('Purchase Return Item not found');

            return redirect(route('purchaseReturnItems.index'));
        }

        return view('purchase_return_items.show')->with('purchaseReturnItem', $purchaseReturnItem);
    }

    /**
     * Show the form for editing the specified PurchaseReturnItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $purchaseReturnItem = $this->purchaseReturnItemRepository->findWithoutFail($id);

        if (empty($purchaseReturnItem)) {
            Flash::error('Purchase Return Item not found');

            return redirect(route('purchaseReturnItems.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('purchase_return_items.edit')->with('purchaseReturnItem', $purchaseReturnItem);
        return view('purchase_return_items.edit')
            ->with('purchaseReturnItem', $purchaseReturnItem)
            ->with('item', $item);        
    }

    /**
     * Update the specified PurchaseReturnItem in storage.
     *
     * @param  int              $id
     * @param UpdatePurchaseReturnItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePurchaseReturnItemRequest $request)
    {
        $purchaseReturnItem = $this->purchaseReturnItemRepository->findWithoutFail($id);

        if (empty($purchaseReturnItem)) {
            Flash::error('Purchase Return Item not found');

            return redirect(route('purchaseReturnItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $purchaseReturnItem = $this->purchaseReturnItemRepository->update($input, $id);
        $purchaseReturnItem = $purchaseReturnItem->update($input);

        \App\Services\LogService::handle([
            'description' => 'purchaseReturnItems.update : ' . $id
        ]);

        Flash::success('Purchase Return Item updated successfully.');

        return redirect(route('purchaseReturnItems.index'));
    }

    /**
     * Remove the specified PurchaseReturnItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $purchaseReturnItem = $this->purchaseReturnItemRepository->findWithoutFail($id);

        if (empty($purchaseReturnItem)) {
            if($request->ajax()){
                return 'Purchase Return Item not found';
            }

            Flash::error('Purchase Return Item not found');

            return redirect(route('purchaseReturnItems.index'));
        }

        // $this->purchaseReturnItemRepository->delete($id);
        $purchaseReturnItem->delete();

        \App\Services\LogService::handle([
            'description' => 'purchaseReturnItems.delete : ' . $id
        ]);
        
        if($request->ajax()){
            return 'Purchase Return Item deleted successfully.';
        }

        Flash::success('Purchase Return Item deleted successfully.');

        return redirect(route('purchaseReturnItems.index'));
    }

    /**
     * Store data PurchaseReturnItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $purchaseReturnItem = $this->purchaseReturnItemRepository->create($item->toArray());
            });
        });

        Flash::success('Purchase Return Item saved successfully.');

        return redirect(route('purchaseReturnItems.index'));
    }
}
