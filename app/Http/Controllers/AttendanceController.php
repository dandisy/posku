<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAttendanceRequest;
use App\Http\Requests\UpdateAttendanceRequest;
use App\Repositories\AttendanceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class AttendanceController extends AppBaseController
{
    /** @var  AttendanceRepository */
    private $attendanceRepository;

    public function __construct(AttendanceRepository $attendanceRepo)
    {
        $this->middleware('auth');
        $this->attendanceRepository = $attendanceRepo;
    }

    /**
     * Display a listing of the Attendance.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->attendanceRepository->pushCriteria(new RequestCriteria($request));
        $attendances = $this->attendanceRepository->all();

        return view('attendances.index')
            ->with('attendances', $attendances);
    }

    /**
     * Show the form for creating a new Attendance.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $user = \App\Models\User::all();
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('attendances.create');
        return view('attendances.create')
            ->with('user', $user)
            ->with('store', $store);
    }

    /**
     * Store a newly created Attendance in storage.
     *
     * @param CreateAttendanceRequest $request
     *
     * @return Response
     */
    public function store(CreateAttendanceRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $attendance = $this->attendanceRepository->create($input);
        $attendance = \App\Models\Attendance::create($input);

        \App\Services\LogService::handle([
            'description' => 'attenances.store : ' . $attendance->id
        ]);

        Flash::success('Attendance saved successfully.');

        // return redirect(route('attendances.index'));
        return redirect('/home');
    }

    /**
     * Display the specified Attendance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $attendance = $this->attendanceRepository->findWithoutFail($id);

        if (empty($attendance)) {
            Flash::error('Attendance not found');

            return redirect(route('attendances.index'));
        }

        return view('attendances.show')->with('attendance', $attendance);
    }

    /**
     * Show the form for editing the specified Attendance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $attendance = $this->attendanceRepository->findWithoutFail($id);

        if (empty($attendance)) {
            Flash::error('Attendance not found');

            return redirect(route('attendances.index'));
        }
        
        // added by dandisy
        $user = \App\Models\User::all();
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('attendances.edit')->with('attendance', $attendance);
        return view('attendances.edit')
            ->with('attendance', $attendance)
            ->with('user', $user)
            ->with('store', $store);        
    }

    /**
     * Update the specified Attendance in storage.
     *
     * @param  int              $id
     * @param UpdateAttendanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAttendanceRequest $request)
    {
        $attendance = $this->attendanceRepository->findWithoutFail($id);

        if (empty($attendance)) {
            Flash::error('Attendance not found');

            return redirect(route('attendances.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $attendance = $this->attendanceRepository->update($input, $id);
        $attendance = $attendance->update($input);

        \App\Services\LogService::handle([
            'description' => 'attenances.update : ' . $id
        ]);

        Flash::success('Attendance updated successfully.');

        return redirect(route('attendances.index'));
    }

    /**
     * Remove the specified Attendance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $attendance = $this->attendanceRepository->findWithoutFail($id);

        if (empty($attendance)) {
            Flash::error('Attendance not found');

            return redirect(route('attendances.index'));
        }

        // $this->attendanceRepository->delete($id);
        $attendance->delete();

        \App\Services\LogService::handle([
            'description' => 'attenances.delete : ' . $id
        ]);

        Flash::success('Attendance deleted successfully.');

        return redirect(route('attendances.index'));
    }

    /**
     * Store data Attendance from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $attendance = $this->attendanceRepository->create($item->toArray());
            });
        });

        Flash::success('Attendance saved successfully.');

        return redirect(route('attendances.index'));
    }
}
