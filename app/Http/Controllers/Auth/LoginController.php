<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/attendances/create';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->user();

        // $user->token;

        $check = \App\User::where('email', 'google.'.$user->user['email'])->first();

        if(!$check) {
            if($user) {
                \App\User::create([
                    "name" => str_replace(' ', '', $user->user['name']),
                    "full_name" => $user->user['name'],
                    "email" => 'google.'.$user->user['email'],
                    "password" => \Illuminate\Support\Facades\Hash::make($user->user['email'])
                ]);
            }
        }

        return redirect('http://localhost:8080/welcome?email='.$user->user['email']);
    }

    protected function logout(\Illuminate\Http\Request $request)
    {
        \App\Services\LogService::handle([
            'description' => 'logged.out : ' . $request->user()->id
        ]);

        \Illuminate\Support\Facades\Auth::logout();
        return redirect('/');
    }
}
