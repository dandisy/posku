<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Repositories\ItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class ItemController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->middleware('auth');
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->itemRepository->pushCriteria(new RequestCriteria($request));
        $items = $this->itemRepository->with(['store'])->all();

        return view('items.index')
            ->with('items', $items);
    }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $category = \App\Models\Category::all();
        $unit = \App\Models\Unit::all();
        $wrap = \App\Models\Wrap::all();
        $store = \App\Models\Store::all();
        

        // edited by dandisy
        // return view('items.create');
        return view('items.create')
            ->with('category', $category)
            ->with('unit', $unit)
            ->with('wrap', $wrap)
            ->with('store', $store)
            ->with('itemBarcodes', collect());
    }

    /**
     * Store a newly created Item in storage.
     *
     * @param CreateItemRequest $request
     *
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        if($request->barcode) {
            $alreadyBarcode = \App\Models\Item::where('barcode', $request->barcode)->first();

            if($alreadyBarcode) {
                Flash::error('Cannot save already existing barcode in database.');

                return redirect(route('items.index'));
            }
        }

        // $item = $this->itemRepository->create($input);
        $item = \App\Models\Item::create($input);

        \App\Services\LogService::handle([
            'description' => 'images.store : ' . $item->id
        ]);

        Flash::success('Item saved successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Display the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        return view('items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;
        
        // added by dandisy
        $category = \App\Models\Category::all();
        $unit = \App\Models\Unit::all();
        $wrap = \App\Models\Wrap::all();
        $store = \App\Models\Store::all();

        // edited by dandisy
        // return view('items.edit')->with('item', $item);
        return view('items.edit')
            ->with('item', $item)
            ->with('category', $category)
            ->with('unit', $unit)
            ->with('wrap', $wrap)
            ->with('store', $store)
            ->with('itemBarcodes', \App\Models\Barcode::where('item', $item->id)->where('store', $userStore)->get());        
    }

    /**
     * Update the specified Item in storage.
     *
     * @param  int              $id
     * @param UpdateItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemRequest $request)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        if($request->barcode) {
            if($request->barcode != $item->barcode) {
                $alreadyBarcode = \App\Models\Item::where('barcode', $request->barcode)->first();

                if($alreadyBarcode) {
                    Flash::success('Cannot save already existing barcode in database.');

                    return redirect(route('items.index'));
                }
            }
        }
        
        $input = $request->all();

        if(isset($input['unique_barcode'])) {
            $barcodes = $input['unique_barcode'];
            unset($input['unique_barcode']);
        }

        $input['updated_by'] = Auth::user()->id;
        
        // $item = $this->itemRepository->update($input, $id);
        $item->update($input);

        \App\Services\LogService::handle([
            'description' => 'items.update : ' . $id
        ]);

        Flash::success('Item updated successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        $userStore = \App\Models\Attendance::where('created_by', Auth::user()->id)->latest()->first()->store;

        // $this->itemRepository->delete($id);
        $item->delete();
        \App\Models\Barcode::where('item', $id)->where('store', $userStore)->delete();

        \App\Services\LogService::handle([
            'description' => 'items.store : ' . $id
        ]);

        Flash::success('Item deleted successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Store data Item from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $item = $this->itemRepository->create($item->toArray());
            });
        });

        Flash::success('Item saved successfully.');

        return redirect(route('items.index'));
    }
}
