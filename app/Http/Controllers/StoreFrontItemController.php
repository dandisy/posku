<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStoreFrontItemRequest;
use App\Http\Requests\UpdateStoreFrontItemRequest;
use App\Repositories\StoreFrontItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class StoreFrontItemController extends AppBaseController
{
    /** @var  StoreFrontItemRepository */
    private $storeFrontItemRepository;

    public function __construct(StoreFrontItemRepository $storeFrontItemRepo)
    {
        $this->middleware('auth');
        $this->storeFrontItemRepository = $storeFrontItemRepo;
    }

    /**
     * Display a listing of the StoreFrontItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeFrontItemRepository->pushCriteria(new RequestCriteria($request));
        $storeFrontItems = $this->storeFrontItemRepository->all();

        return view('store_front_items.index')
            ->with('storeFrontItems', $storeFrontItems);
    }

    /**
     * Show the form for creating a new StoreFrontItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('store_front_items.create');
        return view('store_front_items.create')
            ->with('item', $item);
    }

    /**
     * Store a newly created StoreFrontItem in storage.
     *
     * @param CreateStoreFrontItemRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreFrontItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $storeFrontItem = $this->storeFrontItemRepository->create($input);
        $storeFrontItem = \App\Models\StoreFrontItem::create($input);

        \App\Services\LogService::handle([
            'description' => 'storeFrontItems.store : ' . $storeFrontItem->id
        ]);

        Flash::success('Store Front Item saved successfully.');

        return redirect(route('storeFrontItems.index'));
    }

    /**
     * Display the specified StoreFrontItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $storeFrontItem = $this->storeFrontItemRepository->findWithoutFail($id);

        if (empty($storeFrontItem)) {
            Flash::error('Store Front Item not found');

            return redirect(route('storeFrontItems.index'));
        }

        return view('store_front_items.show')->with('storeFrontItem', $storeFrontItem);
    }

    /**
     * Show the form for editing the specified StoreFrontItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $storeFrontItem = $this->storeFrontItemRepository->findWithoutFail($id);

        if (empty($storeFrontItem)) {
            Flash::error('Store Front Item not found');

            return redirect(route('storeFrontItems.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('store_front_items.edit')->with('storeFrontItem', $storeFrontItem);
        return view('store_front_items.edit')
            ->with('storeFrontItem', $storeFrontItem)
            ->with('item', $item);        
    }

    /**
     * Update the specified StoreFrontItem in storage.
     *
     * @param  int              $id
     * @param UpdateStoreFrontItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreFrontItemRequest $request)
    {
        $storeFrontItem = $this->storeFrontItemRepository->findWithoutFail($id);

        if (empty($storeFrontItem)) {
            Flash::error('Store Front Item not found');

            return redirect(route('storeFrontItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $storeFrontItem = $this->storeFrontItemRepository->update($input, $id);
        $storeFrontItem = $storeFrontItem->update($input);

        \App\Services\LogService::handle([
            'description' => 'storeFrontItems.update : ' . $id
        ]);

        Flash::success('Store Front Item updated successfully.');

        return redirect(route('storeFrontItems.index'));
    }

    /**
     * Remove the specified StoreFrontItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $storeFrontItem = $this->storeFrontItemRepository->findWithoutFail($id);

        if (empty($storeFrontItem)) {
            if($request->ajax()){
                return 'Store Front Item not found';
            }

            Flash::error('Store Front Item not found');

            return redirect(route('storeFrontItems.index'));
        }

        // $this->storeFrontItemRepository->delete($id);
        $storeFrontItem->delete();

        \App\Services\LogService::handle([
            'description' => 'storeFrontItems.delete : ' . $id
        ]);
        
        if($request->ajax()){
            return 'Store Front Item deleted successfully.';
        }

        Flash::success('Store Front Item deleted successfully.');

        return redirect(route('storeFrontItems.index'));
    }

    /**
     * Store data StoreFrontItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $storeFrontItem = $this->storeFrontItemRepository->create($item->toArray());
            });
        });

        Flash::success('Store Front Item saved successfully.');

        return redirect(route('storeFrontItems.index'));
    }
}
