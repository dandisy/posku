<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSellReturnItemRequest;
use App\Http\Requests\UpdateSellReturnItemRequest;
use App\Repositories\SellReturnItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth; // added by dandisy
use Illuminate\Support\Facades\Storage; // added by dandisy
use Maatwebsite\Excel\Facades\Excel; // added by dandisy

class SellReturnItemController extends AppBaseController
{
    /** @var  SellReturnItemRepository */
    private $sellReturnItemRepository;

    public function __construct(SellReturnItemRepository $sellReturnItemRepo)
    {
        $this->middleware('auth');
        $this->sellReturnItemRepository = $sellReturnItemRepo;
    }

    /**
     * Display a listing of the SellReturnItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sellReturnItemRepository->pushCriteria(new RequestCriteria($request));
        $sellReturnItems = $this->sellReturnItemRepository->all();

        return view('sell_return_items.index')
            ->with('sellReturnItems', $sellReturnItems);
    }

    /**
     * Show the form for creating a new SellReturnItem.
     *
     * @return Response
     */
    public function create()
    {
        // added by dandisy
        $item = \App\Models\Item::all();
        

        // edited by dandisy
        // return view('sell_return_items.create');
        return view('sell_return_items.create')
            ->with('item', $item);
    }

    /**
     * Store a newly created SellReturnItem in storage.
     *
     * @param CreateSellReturnItemRequest $request
     *
     * @return Response
     */
    public function store(CreateSellReturnItemRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        // $sellReturnItem = $this->sellReturnItemRepository->create($input);
        $sellReturnItem = \App\Models\SellReturnItem::create($input);

        \App\Services\LogService::handle([
            'description' => 'sellReturnItems.store : ' . $sellReturnItem->id
        ]);

        Flash::success('Sell Return Item saved successfully.');

        return redirect(route('sellReturnItems.index'));
    }

    /**
     * Display the specified SellReturnItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sellReturnItem = $this->sellReturnItemRepository->findWithoutFail($id);

        if (empty($sellReturnItem)) {
            Flash::error('Sell Return Item not found');

            return redirect(route('sellReturnItems.index'));
        }

        return view('sell_return_items.show')->with('sellReturnItem', $sellReturnItem);
    }

    /**
     * Show the form for editing the specified SellReturnItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sellReturnItem = $this->sellReturnItemRepository->findWithoutFail($id);

        if (empty($sellReturnItem)) {
            Flash::error('Sell Return Item not found');

            return redirect(route('sellReturnItems.index'));
        }
        
        // added by dandisy
        $item = \App\Models\Item::all();

        // edited by dandisy
        // return view('sell_return_items.edit')->with('sellReturnItem', $sellReturnItem);
        return view('sell_return_items.edit')
            ->with('sellReturnItem', $sellReturnItem)
            ->with('item', $item);        
    }

    /**
     * Update the specified SellReturnItem in storage.
     *
     * @param  int              $id
     * @param UpdateSellReturnItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellReturnItemRequest $request)
    {
        $sellReturnItem = $this->sellReturnItemRepository->findWithoutFail($id);

        if (empty($sellReturnItem)) {
            Flash::error('Sell Return Item not found');

            return redirect(route('sellReturnItems.index'));
        }
        
        $input = $request->all();
        $input['updated_by'] = Auth::user()->id;

        // $sellReturnItem = $this->sellReturnItemRepository->update($input, $id);
        $sellReturnItem = $sellReturnItem->update($input);

        \App\Services\LogService::handle([
            'description' => 'sellReturnItems.update : ' . $id
        ]);

        Flash::success('Sell Return Item updated successfully.');

        return redirect(route('sellReturnItems.index'));
    }

    /**
     * Remove the specified SellReturnItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $sellReturnItem = $this->sellReturnItemRepository->findWithoutFail($id);

        if (empty($sellReturnItem)) {
            if($request->ajax()){
                return 'Sell Return Item not found';
            }

            Flash::error('Sell Return Item not found');

            return redirect(route('sellReturnItems.index'));
        }

        // $this->sellReturnItemRepository->delete($id);
        $sellReturnItem->delete();

        \App\Services\LogService::handle([
            'description' => 'sellReturnItems.delete : ' . $id
        ]);
        
        if($request->ajax()){
            return 'Sell Return Item deleted successfully.';
        }

        Flash::success('Sell Return Item deleted successfully.');

        return redirect(route('sellReturnItems.index'));
    }

    /**
     * Store data SellReturnItem from an excel file in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function import(Request $request)
    {
        Excel::load($request->file('file'), function($reader) {
            $reader->each(function ($item) {
                $sellReturnItem = $this->sellReturnItemRepository->create($item->toArray());
            });
        });

        Flash::success('Sell Return Item saved successfully.');

        return redirect(route('sellReturnItems.index'));
    }
}
