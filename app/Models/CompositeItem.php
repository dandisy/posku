<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CompositeItem",
 *      required={"composite_id", "store", "item", "item_amount", "Unit"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="composite_id",
 *          description="composite_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="store",
 *          description="store",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="item",
 *          description="item",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="unique_barcode",
 *          description="unique_barcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="item_amount",
 *          description="item_amount",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="unit",
 *          description="unit",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CompositeItem extends Model
{
    use SoftDeletes;

    public $table = 'composite_items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'composite_id',
        'store',
        'item',
        'unique_barcode',
        'item_amount',
        'unit',
        'note',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'composite_id' => 'integer',
        'store' => 'integer',
        'item' => 'integer',
        'unique_barcode' => 'string',
        'item_amount' => 'double',
        'unit' => 'integer',
        'note' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'composite_id' => 'required',
        'store' => 'required',
        'item' => 'required',
        'item_amount' => 'required',
        'unit' => 'required'
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class, 'item', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unit()
    {
        return $this->belongsTo(\App\Models\Unit::class, 'unit', 'id');
    }
}
