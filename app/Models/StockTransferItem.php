<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="StockTransferItem",
 *      required={"stock_transfer_id", "item", "unit", "wrap", "item_amount"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="stock_transfer_id",
 *          description="stock_transfer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="item",
 *          description="item",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="unique_barcode",
 *          description="unique_barcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="unit",
 *          description="unit",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="wrap",
 *          description="wrap",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="item_amount",
 *          description="item_amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class StockTransferItem extends Model
{
    use SoftDeletes;

    public $table = 'stock_transfer_items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'stock_transfer_id',
        'item',
        'unique_barcode',
        'unit',
        'wrap',
        'item_amount',
        'note',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'stock_transfer_id' => 'integer',
        'item' => 'integer',
        'unique_barcode' => 'string',
        'unit' => 'string',
        'wrap' => 'string',
        'item_amount' => 'string',
        'note' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'stock_transfer_id' => 'required',
        'item' => 'required',
        'unit' => 'required',
        'wrap' => 'required',
        'item_amount' => 'required'
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class, 'item', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unit()
    {
        return $this->belongsTo(\App\Models\Unit::class, 'unit', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function wrap()
    {
        return $this->belongsTo(\App\Models\Wrap::class, 'wrap', 'id');
    }
}
