<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Purchase",
 *      required={"store", "supplier", "receipt_code", "receipt_amount"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="store",
 *          description="store",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="supplier",
 *          description="supplier",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="receipt_code",
 *          description="receipt_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="receipt_amount",
 *          description="receipt_amount",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="balance",
 *          description="balance",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="is_draft",
 *          description="is_draft",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="attendance_id",
 *          description="attendance_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Purchase extends Model
{
    use SoftDeletes;

    public $table = 'purchases';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'store',
        'supplier',
        'receipt_code',
        'receipt_amount',
        'note',
        'balance',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'store' => 'integer',
        'supplier' => 'integer',
        'receipt_code' => 'string',
        'receipt_amount' => 'string',
        'note' => 'string',
        'balance' => 'double',
        'is_draft' => 'integer',
        'attendance_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store' => 'required',
        'supplier' => 'required',
        'receipt_code' => 'required',
        'receipt_amount' => 'required'
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function supplier()
    {
        return $this->belongsTo(\App\Models\Supplier::class, 'supplier', 'id');
    }
}
