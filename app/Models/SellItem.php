<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="SellItem",
 *      required={"sell_id", "store", "item", "item_price", "item_amount", "price_subtotal"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sell_id",
 *          description="sell_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="store",
 *          description="store",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="item",
 *          description="item",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="item_name",
 *          description="item_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="unique_barcode",
 *          description="unique_barcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="item_discount",
 *          description="item_discount",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="item_price",
 *          description="item_price",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="item_amount",
 *          description="item_amount",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="discount_subtotal",
 *          description="discount_subtotal",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="price_subtotal",
 *          description="price_subtotal",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="tax",
 *          description="tax",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class SellItem extends Model
{
    use SoftDeletes;

    public $table = 'sell_items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'sell_id',
        'store',
        'item',
        'item_name',
        'unique_barcode',
        'item_discount',
        'item_price',
        'item_amount',
        'discount_subtotal',
        'price_subtotal',
        'tax',
        'note',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'sell_id' => 'integer',
        'store' => 'integer',
        'item' => 'integer',
        'item_name' => 'string',
        'unique_barcode' => 'string',
        'item_discount' => 'double',
        'item_price' => 'double',
        'item_amount' => 'integer',
        'discount_subtotal' => 'double',
        'price_subtotal' => 'double',
        'tax' => 'double',
        'note' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sell_id' => 'required',
        'store' => 'required',
        'item' => 'required',
        'item_price' => 'required',
        'item_amount' => 'required',
        'price_subtotal' => 'required'
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class, 'item', 'id');
    }
}
