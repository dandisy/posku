<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Item",
 *      required={"name", "category", "code", "unit", "wrap", "purchase_price"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="category",
 *          description="category",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="barcode",
 *          description="barcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="unit",
 *          description="unit",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="wrap",
 *          description="wrap",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="store",
 *          description="store",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="purchase_price",
 *          description="purchase_price",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="sell_price",
 *          description="sell_price",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="tax",
 *          description="tax",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="minimal_stock",
 *          description="minimal_stock",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_many_unique_barcode",
 *          description="is_many_unique_barcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Item extends Model
{
    use SoftDeletes;

    public $table = 'items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'category',
        'code',
        'barcode',
        'description',
        'unit',
        'wrap',
        'store',
        'purchase_price',
        'sell_price',
        'tax',
        'minimal_stock',
        'image',
        'is_many_unique_barcode',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'category' => 'string',
        'code' => 'string',
        'barcode' => 'string',
        'description' => 'string',
        'unit' => 'string',
        'wrap' => 'string',
        'store' => 'string',
        'purchase_price' => 'double',
        'sell_price' => 'double',
        'tax' => 'double',
        'minimal_stock' => 'double',
        'image' => 'string',
        'is_many_unique_barcode' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'category' => 'required',
        'code' => 'required',
        'unit' => 'required',
        'wrap' => 'required',
        'purchase_price' => 'required'
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unit()
    {
        return $this->belongsTo(\App\Models\Unit::class, 'unit', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function wrap()
    {
        return $this->belongsTo(\App\Models\Wrap::class, 'wrap', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function composite()
    {
        return $this->hasOne(\App\Models\CompositeItem::class, 'item', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function featured()
    {
        return $this->hasOne(\App\Models\FeaturedProduct::class, 'item', 'id');
    }
}
