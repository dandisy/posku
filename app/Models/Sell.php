<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Sell",
 *      required={"customer", "bill_amount", "payment_method", "payment_status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer",
 *          description="customer",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="store",
 *          description="store",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bill_amount",
 *          description="bill_amount",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="table_number",
 *          description="table_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="table_order_status",
 *          description="table_order_status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_method",
 *          description="payment_method",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_status",
 *          description="payment_status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="selling_type",
 *          description="selling_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="balance",
 *          description="balance",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="is_draft",
 *          description="is_draft",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="attendance_id",
 *          description="attendance_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Sell extends Model
{
    use SoftDeletes;

    public $table = 'sells';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer',
        'store',
        'bill_amount',
        'table_number',
        'table_order_status',
        'payment_method',
        'payment_status',
        'selling_type',
        'note',
        'balance',
        'is_draft',
        'attendance_id',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer' => 'string',
        'store' => 'integer',
        'bill_amount' => 'double',
        'table_number' => 'string',
        'table_order_status' => 'integer',
        'payment_method' => 'integer',
        'payment_status' => 'string',
        'selling_type' => 'string',
        'note' => 'string',
        'balance' => 'double',
        'is_draft' => 'integer',
        'attendance_id' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer' => 'required',
        'bill_amount' => 'required',
        'payment_method' => 'required',
        'payment_status' => 'required'
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paymentMethod()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class, 'payment_method', 'id');
    }
}
