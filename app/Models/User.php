<?php

namespace App\Models;

use Eloquent as Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'full_name', 'reference_code', 'password', 'notif_token', 'forgot_token', 'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_admin' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required'
    ];

    public function profile() {
        return $this->hasOne('App\Models\Profile', 'user_id', 'id');
    }

    public function preferences() {
        return $this->hasMany('App\Models\Preference', 'created_by', 'id');
    }

}
