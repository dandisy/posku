<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        \App\Models\User::class => \App\Policies\UserPolicy::class,
        \App\Models\Profile::class => \App\Policies\ProfilePolicy::class,
        \App\Models\Preference::class => \App\Policies\PreferencePolicy::class,
        \App\Models\Banner::class => \App\Policies\BannerPolicy::class,
        \App\Models\Board::class => \App\Policies\BoardPolicy::class,
        \App\Models\Quest::class => \App\Policies\QuestPolicy::class,
        \App\Models\Redeem::class => \App\Policies\RedeemPolicy::class,
        \App\Models\Ambassador::class => \App\Policies\AmbassadorPolicy::class,
        \App\Models\History::class => \App\Policies\HistoryPolicy::class,
        \App\Models\Quiz::class => \App\Policies\QuizPolicy::class,
        \App\Models\QuizOption::class => \App\Policies\QuizOptionPolicy::class,
        \App\Models\PublicPost::class => \App\Policies\PublicPostPolicy::class,
        \App\Models\Page::class => \App\Policies\PagePolicy::class,
        \App\Models\Product::class => \App\Policies\ProductPolicy::class,
        \App\Models\Shipment::class => \App\Policies\ShipmentPolicy::class,
        \App\Models\StoreLocation::class => \App\Policies\StoreLocationPolicy::class,
        \App\Models\Community::class => \App\Policies\CommunityPolicy::class,
        \App\Models\Moment::class => \App\Policies\MomentPolicy::class,
        \App\Models\MomentImage::class => \App\Policies\MomentImagePolicy::class,
        \App\Models\MomentParticipant::class => \App\Policies\MomentParticipantPolicy::class,
        \App\Models\MomentInvitation::class => \App\Policies\MomentInvitationPolicy::class,
        \App\Models\Discussion::class => \App\Policies\DiscussionPolicy::class,
        \App\Models\Comment::class => \App\Policies\CommentPolicy::class,
        \App\Models\Mark::class => \App\Policies\MarkPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        // Passport::tokensExpireIn(now()->addDays(15));

        // Passport::refreshTokensExpireIn(now()->addDays(30));

        Gate::before(function ($user, $ability) {
            if ($user->is_admin) {
                return true;
            }
        });

        Gate::define('admin', function ($user) {
            return $user->is_admin;
        });
    }
}
